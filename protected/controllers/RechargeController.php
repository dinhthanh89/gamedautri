<?php
Yii::app()->theme = Common::getTheme();

class RechargeController extends Controller {   

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    //public $layout = 'blank';
    
    public function actionIndex() {             
        if(isset($_POST['NLNapThe'])){
            $soseri = $_POST['txtSoSeri'];
            $sopin = $_POST['txtSoPin'];
            $type_card = $_POST['select_method'];
            $username = $_POST['ref_code'];

            if ($_POST['txtSoSeri'] == "" ) {                
                Common::alert('Vui lòng nhập số Seri', Yii::app()->request->urlReferrer);
                exit();
            }
            if ($_POST['txtSoPin'] == "" ) {
                Common::alert('Vui lòng nhập Mã thẻ', Yii::app()->request->urlReferrer);
                exit();
            }
            
            $user = User::model()->find('username = "' . $username . '"');
            if (!isset($user)) {
                Common::alert('Tài khoản của bạn không tồn tại trong hệ thống', Yii::app()->request->urlReferrer);
            }
                                          
            Common::includeTheCaoMobile();
            //Tiến hành kết nối thanh toán Thẻ cào.
            $call = new MobiCard();
            $rs = new Result();
            $ref_code = 'NapTien-' . $username . '-' . date('His-dmY');
            $rs = $call->CardPay($sopin, $soseri, $type_card, $ref_code, $username, "0934571589", "thanhnguyendinh89@gmail.com");           
            if ($rs->error_code == '00') {
                // Ghi log
                $rechargeCardLog = new RechargeCardLog();
                $rechargeCardLog->pin = $sopin;
                $rechargeCardLog->seri = $soseri;
                $rechargeCardLog->card_type = $type_card;
                $rechargeCardLog->username = $user->username;
                $rechargeCardLog->value = $rs->card_amount;
                $rechargeCardLog->save();            
                // Cập nhật data tại đây
                $amount = $rs->card_amount;            
                switch ($type_card) {
                    case 'VIETTEL': {
                            $type = $this->getRechargeTypeOfCard('viettel',$amount);
                            $view_type = 'Viettel';
                            break;
                        }
                    case 'VNP': {
                            $type = $this->getRechargeTypeOfCard('vinaphone',$amount);
                            $view_type = 'Vinaphone';
                            break;
                        }
                    case 'VMS': {
                            $type = $this->getRechargeTypeOfCard('mobifone',$amount);
                            $view_type = 'Mobifone';
                            break;
                        }
                    case 'VCOIN': {
                            $type = $this->getRechargeTypeOfCard('vcoin',$amount);
                            $view_type = 'Vcoin';
                            break;
                        }
                    case 'GATE': {
                            $type = $this->getRechargeTypeOfCard('gate',$amount);
                            $view_type = 'Gate';
                            break;
                        }
                }
                $userGameActionId = $user->userGameAction('recharge');
                $userGold = $user->rechargeMoney($amount, $type);
                if ($userGold) {
                    $userChip = $user->convertGoldByChip($userGold);
                    $userChipBonus = 0;
                    if($userGameActionId)
                        $userChipBonus = $user->bonusChips($userGameActionId,$userGold);

                    $totalChip = $userChip + $userChipBonus;
                    $electroServer = new ElectroServerBridge();
                    $result = $electroServer->sendUserInfo($user->id, 'chip', $totalChip);                     
                    MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($totalChip, 0, ',', '.')." chip từ hệ thống Esimo qua thẻ cào ".$view_type."!",1);                    
                    $message = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($totalChip, 0, ',', '.') . ' Chip  vào tài khoản của bạn';
                    Common::alert($message, Yii::app()->request->urlReferrer);
                } else {                   
                    $message = 'Nạp tiền thất bại';
                    Common::alert($message, Yii::app()->request->urlReferrer);
                }
            } else {
                Common::alert($rs->error_message, Yii::app()->request->urlReferrer);                
            }            
            
        }
        $this->render('index');
    }
    
    public function getRechargeTypeOfCard($code, $amount){           
        $criteria = new CDbCriteria;
        $criteria->alias = 'recharge_type';
        $criteria->join = "INNER JOIN telco on telco.id = recharge_type.telco_id";
        $criteria->join .= " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
        $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
        $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
        $criteria->addCondition('telco.code_identifier="'.$code.'" AND recharge_method.code_identifier = "mobile_card" AND topup_provider.code_identifier="vmg" AND recharge_method_provider.code='.$amount);          
        $rechargeType = RechargeType::model()->find($criteria);
        if(isset($rechargeType))
            $type =  $rechargeType->id;        
        else 
            $type = 0;
        return $type;
    }
}?>