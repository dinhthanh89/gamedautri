<?php
Yii::app()->theme = Common::getTheme();

class NewsController extends Controller {   
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public $meta_title;
    public $meta_keyword = "phom, tien len mien nam, chan, game, esimo";
    public $meta_description = "Esimo, game Esimo, Tải game Esimo miễn phí về di động.";       
    public $layout='main';
    
     public function actionIndex() {        
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'   
        $this->meta_title = Yii::app()->name;       
        $this->pageTitle = $this->meta_title;
        Yii::app()->clientScript->registerMetaTag($this->meta_description, 'Description');
        Yii::app()->clientScript->registerMetaTag($this->meta_keyword, 'Keywords');		
        Yii::app()->clientScript->registerMetaTag($this->meta_title,null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('images/Logo_v.jpg')),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag($this->meta_description,null,null,array('property'=>'og:description'));                
                 
        if(Yii::app()->theme->name == 'mobile'){
            $page = Yii::app()->request->getParam('p');
            if(!isset($page))
                $page = 1;
            $countPost =  Post::model()->count('status = 1 AND category_id = 8');            
            $dataPost = new CActiveDataProvider('Post', array(
                'criteria' => array(
                    'order' => 'create_time DESC',
                    'condition' => 'status = 1 AND category_id = 8',                        
                ),
                'pagination' => array(
                    'pageSize' => $page * 6,
                ),                    
            ));            
            $this->render('index', array('dataPost'=>$dataPost,'countPost'=>$countPost, 'page'=>$page));
        }else{
            $dataPost = new CActiveDataProvider('Post', array(
                'criteria' => array(
                    'order' => 'create_time DESC',
                    'condition' => 'status = 1 AND category_id = 8',                        
                ),
                'pagination' => array(
                    'pageSize' => 5,
                ),                    
            ));
            $otherPost = Post::model()->findAll('status = 1 AND category_id = 8 ORDER BY create_time DESC  LIMIT 0,10');
            $dataGuide = Post::model()->findAll('status = 1 AND category_id = 14 ORDER BY create_time DESC  LIMIT 0,10');        
            $this->render('index', array('dataPost'=>$dataPost, 'dataGuide'=>$dataGuide, 'otherPost'=>$otherPost));
        }
    }
    
    public function actionGuide(){        
        $this->meta_title = Yii::app()->name;       
        $this->pageTitle = $this->meta_title;
        Yii::app()->clientScript->registerMetaTag($this->meta_description, 'Description');
        Yii::app()->clientScript->registerMetaTag($this->meta_keyword, 'Keywords');		
        Yii::app()->clientScript->registerMetaTag($this->meta_title,null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('images/Logo_v.jpg')),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag($this->meta_description,null,null,array('property'=>'og:description'));                
        if($_POST){
            $content = Yii::app()->request->getParam('content');
            if(empty($content)){
                Common::alert('Bạn chưa nhập nội dung phản hồi',Yii::app()->request->urlReferrer);
            }
            $feedback = new Feedback();
            $feedback->content = $content;
            $user = Yii::app()->user->getUser();
            $feedback->user_id = $user->id;
            if($feedback->save()){
                Common::alert('Gửi phản hồi thành công. Cảm ơn bạn đã góp ý cho hệ thống chúng tôi!',Yii::app()->request->urlReferrer);
            }
        }                   
        $id = Yii::app()->request->getParam('id');
        if(isset($id) && is_numeric($id)){
            $dataCategory = PostCategory::model()->findByPk($id);
            if(!isset($dataCategory))
                $this->redirect(Yii::app()->createUrl('news/guide'));
            $dataPostCategory = PostCategory::model()->findAll('status = 1 AND parent_id = 14');           
            $dataPost = new CActiveDataProvider('Post', array(
                    'criteria' => array(                        
                        'condition' => 'status = 1 AND category_id = '.$id,                        
                    ),
                    'pagination' => array(
                        'pageSize' => 10,
                    ),                    
                ));               
            $this->render('guide_list', array('dataPostCategory'=>$dataPostCategory,'dataPost'=>$dataPost, 'dataCategory'=>$dataCategory));
        }else{
            $dataPostCategory = new CActiveDataProvider('PostCategory', array(
                    'criteria' => array(                        
                        'condition' => 'status = 1 AND parent_id = 14',                        
                    ),
                    'pagination' => array(
                        'pageSize' => 9,
                    ),                    
                )); 
            $this->render('guide', array('dataPostCategory'=>$dataPostCategory));
        }
    }
    
    
    public function actionDetail() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php' 
        
        if($_POST){
            $content = Yii::app()->request->getParam('content');
            if(empty($content)){
                Common::alert('Bạn chưa nhập nội dung phản hồi',Yii::app()->request->urlReferrer);
            }
            $feedback = new Feedback();
            $feedback->content = $content;
            $user = Yii::app()->user->getUser();
            $feedback->user_id = $user->id;
            if($feedback->save()){
                Common::alert('Gửi phản hồi thành công. Cảm ơn bạn đã góp ý cho hệ thống chúng tôi!',Yii::app()->request->urlReferrer);
            }
        }  
        $slug = Yii::app()->request->getParam('slug');
        if(isset($slug)){
            $dataPost = Post::model()->find('slug="'.$slug.'"');
            if(!isset($dataPost)){
                $this->redirect(Yii::app()->createUrl('news'));
            }
        }else{                        
            $id = Yii::app()->request->getParam('id');
            if(!isset($id) || !is_numeric($id))
                $this->redirect(Yii::app()->createUrl('news'));
            $dataPost = Post::model()->findByPk($id);
            if(!isset($dataPost))
                $this->redirect(Yii::app()->createUrl('news'));
        }
        // seo
        $this->meta_title = Yii::app()->name;
        $meta_title = (isset($dataPost->meta_title)) ? $dataPost->meta_title : $this->meta_title;
        $meta_description = (isset($dataPost->meta_description)) ? $dataPost->meta_description : $this->meta_description;
        $meta_keyword = (isset($dataPost->meta_keyword)) ? $dataPost->meta_keyword : $this->meta_keyword;
        $this->pageTitle = $meta_title;
        Yii::app()->clientScript->registerMetaTag($meta_description, 'Description');
        Yii::app()->clientScript->registerMetaTag($meta_keyword, 'Keywords');		
        Yii::app()->clientScript->registerMetaTag($meta_title,null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl($dataPost->main_image)),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('bai-viet/'.$slug)),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag($meta_description,null,null,array('property'=>'og:description'));               
        
        if(Yii::app()->theme->name == 'mobile'){            
            $this->render('detail', array('dataPost'=>$dataPost));
        }else{
            $otherPost = Post::model()->findAll('status = 1 AND category_id = 8 AND id != '.$dataPost->id.' ORDER BY create_time DESC  LIMIT 0,10');
            $dataGuide = Post::model()->findAll('status = 1 AND category_id = '.$dataPost->category_id.' ORDER BY create_time DESC  LIMIT 0,10');
            $this->render('detail', array('dataPost'=>$dataPost, 'otherPost'=>$otherPost, 'dataGuide'=>$dataGuide));
        }
    }
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }        
    
    public function actionHot(){
        $this->layout = 'blank';
        $criteria = new CDbCriteria();
        $criteria->addCondition('category_id=8 AND status = 1');
        $criteria->order = "create_time DESC";
        $criteria->limit = 5;
        $model = Post::model()->findAll($criteria);
        $this->render('hot', array('model'=>$model));        
    }
    
    public function actionGuideMobile(){
        $this->layout = 'blank';
        $slug = Yii::app()->request->getParam('slug');
        if(isset($slug)){
            $dataPost = Post::model()->find('slug="'.$slug.'"');
            if(!isset($dataPost)){
                $this->redirect(Yii::app()->createUrl('news'));
            }
        }else{                        
            $id = Yii::app()->request->getParam('id');
            if(!isset($id) || !is_numeric($id))
                $this->redirect(Yii::app()->createUrl('news'));
            $dataPost = Post::model()->findByPk($id);
            if(!isset($dataPost))
                $this->redirect(Yii::app()->createUrl('news'));
        }
        $this->render('guide_mobile', array('dataPost'=>$dataPost)); 
    }

}