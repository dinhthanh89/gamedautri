<?php

class ApiTournamentController extends Controller {

    public function actionIndex() {
        
    }   

    public function actionGetTournament() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = Tournament::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại giải đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['name'] = $model->name;
                $return['handler'] = $model->handler;
                $return['display_name'] = $model->display_name;
                $return['description'] = $model->description;
                $return['game_type'] = $model->game_type;
            }
        } else {
            $model = Tournament::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại giải đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['handler'] = $items->handler;
                    $return['items'][$index]['display_name'] = $items->display_name;                    
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['game_type'] = $items->game_type;                    
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $handler = Yii::app()->request->getParam('handler');
            $display_name = Yii::app()->request->getParam('display_name');            
            $description = Yii::app()->request->getParam('description');
            $game_type = Yii::app()->request->getParam('game_type');
            if (!isset($name) || !isset($game_type)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new Tournament();
                $model->name = $name;
                $model->handler = $handler;                
                $model->display_name = $display_name;
                $model->description = $description;
                $model->game_type = $game_type;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = Tournament::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                
                $handler = Yii::app()->request->getParam('handler');
                if (isset($handler))
                    $model->handler = $handler;
                $display_name = Yii::app()->request->getParam('display_name');
                if (isset($display_name)) {                    
                    $model->display_name = $display_name;
                }
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $game_type = Yii::app()->request->getParam('game_type');
                if (isset($game_type))
                    $model->game_type = $game_type;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Tournament::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentActive() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = TournamentActive::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại giải đấu active";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['tournament_id'] = $model->tournament_id;
                $return['start_time_registration'] = $model->start_time_registration;
                $return['end_time_registration'] = $model->end_time_registration;
                $return['start_time'] = $model->start_time;
                $return['end_time'] = $model->end_time;
                $return['config'] = $model->config;
                $return['result'] = $model->result;
                $return['max_players'] = $model->max_players;
                $return['registration_winner'] = $model->registration_winner;
            }
        } else {
            $tournament_id = Yii::app()->request->getParam('tournament_id');
            if(!isset($tournament_id))
                $model = TournamentActive::model()->findAll();
            else
                $model = TournamentActive::model()->findAll('tournament_id='.$tournament_id);
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại giải đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['start_time_registration'] = $items->start_time_registration;
                    $return['items'][$index]['end_time_registration'] = $items->end_time_registration;                    
                    $return['items'][$index]['start_time'] = $items->start_time;
                    $return['items'][$index]['end_time'] = $items->end_time;                    
                    $return['items'][$index]['config'] = $items->config;                    
                    $return['items'][$index]['result'] = $items->result;                    
                    $return['items'][$index]['max_players'] = $items->max_players; 
                    $return['items'][$index]['registration_winner'] = $items->registration_winner; 
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateTournamentActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_id = Yii::app()->request->getParam('tournament_id');
            $tournamentModel = Tournament::model()->findByPk($tournament_id);
            if(!isset($tournamentModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tai tournament';
                echo json_encode($return);die;
            }
            $start_time_registration = Yii::app()->request->getParam('start_time_registration');
            $end_time_registration = Yii::app()->request->getParam('end_time_registration');            
            $start_time = Yii::app()->request->getParam('start_time');
            $end_time = Yii::app()->request->getParam('end_time');
            $config = Yii::app()->request->getParam('config');
            $result = Yii::app()->request->getParam('result');
            $max_players = Yii::app()->request->getParam('max_players');
            $registration_winner = Yii::app()->request->getParam('registration_winner');
            if (!isset($start_time_registration) || !isset($end_time_registration)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentActive();
                $model->tournament_id = $tournament_id;
                $model->start_time_registration = $start_time_registration;                
                $model->end_time_registration = $end_time_registration;
                $model->start_time = $start_time;
                $model->end_time = $end_time;
                $model->config = $config;
                $model->result = $result;
                $model->max_players = $max_players;
                $model->registration_winner = $registration_winner;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateTournamentActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentActive::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $tournament_id = Yii::app()->request->getParam('tournament_id');
                if(isset($tournament_id)){
                    $tournamentModel = Tournament::model()->findByPk($tournament_id);
                    if(!isset($tournamentModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament';
                        echo json_encode($return);die;
                    }
                    $model->tournament_id = $tournament_id;
                }
                
                $start_time_registration = Yii::app()->request->getParam('start_time_registration');
                if (isset($start_time_registration))
                    $model->start_time_registration = $start_time_registration;
                
                $end_time_registration = Yii::app()->request->getParam('end_time_registration');
                if (isset($end_time_registration))
                    $model->end_time_registration = $end_time_registration;
                $start_time = Yii::app()->request->getParam('start_time');
                if (isset($start_time)) {                    
                    $model->start_time = $start_time;
                }
                $end_time = Yii::app()->request->getParam('end_time');
                if (isset($end_time))
                    $model->end_time = $end_time;
                $config = Yii::app()->request->getParam('config');
                if (isset($config))
                    $model->config = $config;
                $result = Yii::app()->request->getParam('result');
                if (isset($result))
                    $model->result = $result;
                $max_players = Yii::app()->request->getParam('max_players');
                if (isset($max_players))
                    $model->max_players = $max_players;
                $registration_winner = Yii::app()->request->getParam('registration_winner');
                if (isset($registration_winner))
                    $model->registration_winner = $registration_winner;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteTournamentActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_id = Yii::app()->request->getParam('tournement_id');
            if(isset($tournament_id)){
                $model = TournamentActive::model()->findAll('tournament_id = '.$tournament_id);
                if($model){
                    foreach($model as $item){
                        $item->delete();
                    }
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    
                }else{
                    $return['code'] = 1;
                    $return['message'] = "Xóa dữ liệu thất bại";                    
                }
                echo json_encode($return);
            }
            $id = Yii::app()->request->getParam('id');            
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentActive::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }    
    
    //code by Linh
    
    public function actionGetTournamentRegistration(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentRegistration::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người đăng ký";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['user_id'] = $model->user_id;                
                $return['user_activity_id'] = $model->user_activity_id;
                $return['tournament_active_id'] = $model->tournament_active_id;
                $return['timestamp'] = $model->timestamp;
                $return['username'] = $model->username;
            }
        } else {
            $tournament_active_id = Yii::app()->request->getParam('tournament_active_id');
            if($tournament_active_id)
                $model = TournamentRegistration::model()->findAll('tournament_active_id='.$tournament_active_id);
            else    
                $model = TournamentRegistration::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người đăng ký";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_id'] = $items->user_id;
                    $return['items'][$index]['user_activity_id'] = $items->user_activity_id;
                    $return['items'][$index]['tournament_active_id'] = $items->tournament_active_id;                    
                    $return['items'][$index]['timestamp'] = $items->timestamp;
                    $return['items'][$index]['username'] = $items->username;                    
                }
            }
        }
        echo json_encode($return);
    }   
    
    public function actionCreateTournamentRegistration() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_active_id = Yii::app()->request->getParam('tournament_active_id');
            $tournamentregistrationModel = TournamentActive::model()->findByPk($tournament_active_id);
            if(!isset($tournamentregistrationModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại thời gian giải đấu';
                echo json_encode($return);die;
            }
            $user_id = Yii::app()->request->getParam('user_id');
            $user_activity_id = Yii::app()->request->getParam('user_activity_id');
            $timestamp = Yii::app()->request->getParam('timestamp');
            $username = Yii::app()->request->getParam('username');
            if (!isset($timestamp) || !isset($username) || !isset($user_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentRegistration();
                $model->user_id = $user_id;
                $model->user_activity_id = $user_activity_id;                
                $model->tournament_active_id = $tournament_active_id;
                $model->timestamp = $timestamp;
                $model->username = $username;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentRegistration() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentRegistration::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $tournament_active_id = Yii::app()->request->getParam('tournament_active_id');
                if(isset($tournament_active_id)){
                    $tournamentactiveModel = TournamentActive::model()->findByPk($tournament_active_id);
                    if(!isset($tournamentactiveModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament active';
                        echo json_encode($return);die;
                    }
                    $model->tournament_active_id = $tournament_active_id;
                }
                
                $user_id = Yii::app()->request->getParam('user_id');
                if (isset($user_id))
                    $model->user_id = $user_id;
                
                $user_activity_id = Yii::app()->request->getParam('user_activity_id');
                if (isset($user_activity_id))
                    $model->user_activity_id = $user_activity_id;
                
                $timestamp = Yii::app()->request->getParam('timestamp');
                if (isset($timestamp)) {                    
                    $model->timestamp = $timestamp;
                }
                
                $username = Yii::app()->request->getParam('username');
                if (isset($username))
                    $model->username = $username;
                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentGroup(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentGroup::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có nhóm đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['display_name'] = $model->display_name;                
                $return['tournament_active_id'] = $model->tournament_active_id;
                $return['tournament_round_id'] = $model->tournament_round_id;
                $return['max_player'] = $model->max_player;
                $return['start_time'] = $model->start_time;
                $return['end_time'] = $model->end_time;
                $return['num_players_continuous'] = $model->num_players_continuous;
                $return['max_matchs'] = $model->max_matchs;
            }
        } else {  
            $model = TournamentGroup::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có nhóm đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['display_name'] = $items->display_name;
                    $return['items'][$index]['tournament_active_id'] = $items->tournament_active_id;
                    $return['items'][$index]['tournament_round_id'] = $items->tournament_round_id;                    
                    $return['items'][$index]['max_player'] = $items->max_player;
                    $return['items'][$index]['start_time'] = $items->start_time;                    
                    $return['items'][$index]['end_time'] = $items->end_time;                    
                    $return['items'][$index]['num_players_continuous'] = $items->num_players_continuous;                    
                    $return['items'][$index]['max_matchs'] = $items->max_matchs;                    
                }
            }
        }
        echo json_encode($return);
    }    
    
    public function actionCreateTournamentGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_active_id = Yii::app()->request->getParam('tournament_active_id');
            $tournamentgroupModel = TournamentActive::model()->findByPk($tournament_active_id);
            if(!isset($tournamentgroupModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại thời gian giải đấu';
                echo json_encode($return);die;
            }
            
            $display_name = Yii::app()->request->getParam('display_name');
            $max_player = Yii::app()->request->getParam('max_player');
            $start_time = Yii::app()->request->getParam('start_time');
            $tournament_round_id = Yii::app()->request->getParam('tournament_round_id');
            
            $tournamentroundModel = TournamentRound::model()->findByPk($tournament_round_id);
            if(!isset($tournamentroundModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại vòng đấu';
                echo json_encode($return);die;
            }
            $end_time = Yii::app()->request->getParam('end_time');
            $num_players_continuous = Yii::app()->request->getParam('num_players_continuous');
            $max_matchs = Yii::app()->request->getParam('max_matchs');
            if (!isset($display_name) || !isset($max_matchs) || !isset($num_players_continuous) || !is_numeric($max_matchs)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentGroup();
                $model->display_name = $display_name;
                $model->tournament_active_id = $tournament_active_id;                
                $model->tournament_round_id = $tournament_round_id;
                $model->max_player = $max_player;
                $model->start_time = $start_time;
                $model->end_time = $end_time;
                $model->num_players_continuous = $num_players_continuous;
                $model->max_matchs = $max_matchs;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentGroup::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $tournament_active_id = Yii::app()->request->getParam('tournament_active_id');
                if(isset($tournament_active_id)){
                    $tournamentactiveModel = TournamentActive::model()->findByPk($tournament_active_id);
                    if(!isset($tournamentactiveModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament active';
                        echo json_encode($return);die;
                    }
                    $model->tournament_active_id = $tournament_active_id;
                }
                
                $display_name = Yii::app()->request->getParam('display_name');
                if (isset($display_name))
                    $model->display_name = $display_name;
                
                $tournament_round_id = Yii::app()->request->getParam('tournament_round_id');
                if (isset($tournament_round_id))
                    $model->tournament_round_id = $tournament_round_id;
                
                $max_player = Yii::app()->request->getParam('max_player');
                if (isset($max_player)) {                    
                    $model->max_player = $max_player;
                }
                
                $start_time = Yii::app()->request->getParam('start_time');
                if (isset($start_time))
                    $model->start_time = $start_time;
                
                $end_time = Yii::app()->request->getParam('end_time');
                if (isset($end_time))
                    $model->end_time = $end_time;
                
                $num_players_continuous = Yii::app()->request->getParam('num_players_continuous');
                if (isset($num_players_continuous))
                    $model->num_players_continuous = $num_players_continuous;
                
                $max_matchs = Yii::app()->request->getParam('max_matchs');
                if (isset($max_matchs))
                    $model->max_matchs = $max_matchs;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionDeleteTournamentGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_active_id = Yii::app()->request->getParam('tournement_active_id');
            if(isset($tournament_id)){
                $model = TournamentGroup::model()->findAll('tournament_active_id = '.$tournament_active_id);
                if($model){
                    foreach($model as $item){
                        $item->delete();
                    }
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    
                }else{
                    $return['code'] = 1;
                    $return['message'] = "Xóa dữ liệu thất bại";                    
                }
                echo json_encode($return);
            }
            $tournament_round_id = Yii::app()->request->getParam('tournement_round_id');
            if(isset($tournament_round_id)){
                $model = TournamentGroup::model()->findAll('tournament_round_id = '.$tournament_round_id);
                if($model){
                    foreach($model as $item){
                        $item->delete();
                    }
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    
                }else{
                    $return['code'] = 1;
                    $return['message'] = "Xóa dữ liệu thất bại";                    
                }
                echo json_encode($return);
            }
            $id = Yii::app()->request->getParam('id');            
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentGroup::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentMatch(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentMatch::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có trận đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['group_id'] = $model->group_id;                
                $return['game_log_id'] = $model->game_log_id;
                $return['result_game'] = $model->result_game;
            }
        } else {  
            $model = TournamentMatch::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có trận đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['group_id'] = $items->group_id;
                    $return['items'][$index]['game_log_id'] = $items->game_log_id;
                    $return['items'][$index]['result_game'] = $items->result_game;
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateTournamentMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $group_id = Yii::app()->request->getParam('group_id');
            $tournamentgroupModel = TournamentGroup::model()->findByPk($group_id);
            if(!isset($tournamentgroupModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại nhóm đấu';
                echo json_encode($return);die;
            }
            $game_log_id = Yii::app()->request->getParam('game_log_id');
            $result_game = Yii::app()->request->getParam('result_game');
            if (!isset($group_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentMatch();
                $model->group_id = $group_id;
                $model->game_log_id = $game_log_id;                
                $model->result_game = $result_game;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentMatch::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $group_id = Yii::app()->request->getParam('group_id');
                if(isset($group_id)){
                    $tournamentgroupModel = TournamentGroup::model()->findByPk($group_id);
                    if(!isset($tournamentgroupModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament group';
                        echo json_encode($return);die;
                    }
                    $model->group_id = $group_id;
                }
                
                $game_log_id = Yii::app()->request->getParam('game_log_id');
                if (isset($game_log_id))
                    $model->game_log_id = $game_log_id;
                
                $result_game = Yii::app()->request->getParam('result_game');
                if (isset($result_game))
                    $model->result_game = $result_game;
                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentPlayerInGroup(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentPlayerInGroup::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người nào trong nhóm";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['group_id'] = $model->group_id;                
                $return['registration_id'] = $model->registration_id;
                $return['user_id'] = $model->user_id;
                $return['user_name'] = $model->user_name;
                $return['point'] = $model->point;
                $return['is_continuous'] = $model->is_continuous;
            }
        } else {  
            $model = TournamentPlayerInGroup::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người nào trong nhóm";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['group_id'] = $items->group_id;
                    $return['items'][$index]['registration_id'] = $items->registration_id;
                    $return['items'][$index]['user_id'] = $items->user_id;                    
                    $return['items'][$index]['user_name'] = $items->user_name;
                    $return['items'][$index]['point'] = $items->point;                    
                    $return['items'][$index]['is_continuous'] = $items->is_continuous;                   
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateTournamentPlayerInGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $group_id = Yii::app()->request->getParam('group_id');
            $playergroupModel = TournamentGroup::model()->findByPk($group_id);
            if(!isset($playergroupModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại nhóm đấu';
                echo json_encode($return);die;
            }
            $registration_id = Yii::app()->request->getParam('registration_id');
            $user_id = Yii::app()->request->getParam('user_id');
            $user_name = Yii::app()->request->getParam('user_name');
            $point = Yii::app()->request->getParam('point');
            $is_continuous = Yii::app()->request->getParam('is_continuous');
            if (!isset($user_name) || !isset($is_continuous) || !isset($point) || !isset($registration_id) || !isset($user_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentPlayerInGroup();
                $model->group_id = $group_id;
                $model->registration_id = $registration_id;                
                $model->user_id = $user_id;
                $model->user_name = $user_name;
                $model->point = $point;
                $model->is_continuous = $is_continuous;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentPlayerInGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentPlayerInGroup::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $group_id = Yii::app()->request->getParam('group_id');
                if(isset($group_id)){
                    $tournamentgroupModel = TournamentGroup::model()->findByPk($group_id);
                    if(!isset($tournamentgroupModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament group';
                        echo json_encode($return);die;
                    }
                    $model->group_id = $group_id;
                }
                
                $registration_id = Yii::app()->request->getParam('registration_id');
                if (isset($registration_id))
                    $model->registration_id = $registration_id;
                
                $user_id = Yii::app()->request->getParam('user_id');
                if (isset($user_id))
                    $model->user_id = $user_id;
                
                $user_name = Yii::app()->request->getParam('user_name');
                if (isset($user_name)) {                    
                    $model->user_name = $user_name;
                }
                
                $point = Yii::app()->request->getParam('point');
                if (isset($point))
                    $model->point = $point;
                
                $is_continuous = Yii::app()->request->getParam('is_continuous');
                if (isset($is_continuous))
                    $model->is_continuous = $is_continuous;
                                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentPlayerInMatch(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentPlayerInMatch::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người nào trong trận";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['match_id'] = $model->match_id;                
                $return['user_id'] = $model->user_id;
                $return['user_name'] = $model->user_name;
                $return['win'] = $model->win;
                $return['point'] = $model->point;
            }
        } else {  
            $model = TournamentPlayerInMatch::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có người nào trong trận";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['match_id'] = $items->match_id;
                    $return['items'][$index]['user_id'] = $items->user_id;
                    $return['items'][$index]['user_name'] = $items->user_name;                    
                    $return['items'][$index]['win'] = $items->win;
                    $return['items'][$index]['point'] = $items->point;            
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateTournamentPlayerInMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $match_id = Yii::app()->request->getParam('match_id');
            $tournamentmatchModel = TournamentMatch::model()->findByPk($match_id);
            if(!isset($tournamentmatchModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại trận đấu';
                echo json_encode($return);die;
            }
            $user_id = Yii::app()->request->getParam('user_id');
            $user_name = Yii::app()->request->getParam('user_name');
            $win = Yii::app()->request->getParam('win');
            $point = Yii::app()->request->getParam('point');
            if (!isset($user_name) || !isset($user_id) || !isset($win) || !isset($point)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentPlayerInMatch();
                $model->match_id = $match_id;
                $model->user_id = $user_id;                
                $model->user_name = $user_name;
                $model->win = $win;
                $model->start_time = $point;
                $model->point = $end_time;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentPlayerInMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentPlayerInMatch::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $match_id = Yii::app()->request->getParam('match_id');
                if(isset($match_id)){
                    $tournamentmatchModel = TournamentMatch::model()->findByPk($match_id);
                    if(!isset($tournamentmatchModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament match';
                        echo json_encode($return);die;
                    }
                    $model->match_id = $match_id;
                }
                
                $user_id = Yii::app()->request->getParam('user_id');
                if (isset($user_id))
                    $model->user_id = $user_id;
                
                $user_name = Yii::app()->request->getParam('user_name');
                if (isset($user_name))
                    $model->user_name = $user_name;
                
                $win = Yii::app()->request->getParam('win');
                if (isset($win)) {                    
                    $model->win = $win;
                }
                
                $point = Yii::app()->request->getParam('point');
                if (isset($point))
                    $model->point = $point;
                                                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTournamentRound(){
        $return = array();
        $id = Yii::app()->request->getParam('id'); 
        if (isset($id)) {
            $model = TournamentRound::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có vòng đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;                
                $return['name'] = $model->name;                
                $return['tournament_id'] = $model->tournament_id;
                $return['next_round_id'] = $model->next_round_id;
                $return['num_players_continuous'] = $model->num_players_continuous;
            }
        } else {  
            $model = TournamentRound::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có vòng đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['next_round_id'] = $items->next_round_id;                    
                    $return['items'][$index]['num_players_continuous'] = $items->num_players_continuous;
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateTournamentRound() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournament_id = Yii::app()->request->getParam('tournament_id');
            $tournamentModel = Tournament::model()->findByPk($tournament_id);
            if(!isset($tournamentModel)){
                $return['code'] = 3;
                $return['message'] = 'Khong ton tại giải đấu';
                echo json_encode($return);die;
            }
            $name = Yii::app()->request->getParam('name');
            $next_round_id = Yii::app()->request->getParam('next_round_id');
            $num_players_continuous = Yii::app()->request->getParam('num_players_continuous');
            $min_players = Yii::app()->request->getParam('min_players');
            if (!isset($name) || !isset($min_players) || !isset($next_round_id) || !isset($num_players_continuous)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new TournamentRound();
                $model->name = $name;
                $model->tournament_id = $tournament_id;                
                $model->next_round_id = $next_round_id;                
                $model->num_players_continuous = $num_players_continuous;
                $model->min_players = $min_players;
                if ($model->save()) {
                    $return['id'] = $model->id;                    
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['error'] = $model->errors;
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateTournamentRound() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentRound::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $tournament_id = Yii::app()->request->getParam('tournament_id');
                if(isset($tournament_id)){
                    $tournamentModel = Tournament::model()->findByPk($tournament_id);
                    if(!isset($tournamentModel)){
                        $return['code'] = 3;
                        $return['message'] = 'Khong ton tai tournament';
                        echo json_encode($return);die;
                    }
                    $model->tournament_id = $tournament_id;
                }
                
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                
                $next_round_id = Yii::app()->request->getParam('next_round_id');
                if (isset($next_round_id))
                    $model->next_round_id = $next_round_id;
                
                $num_players_continuous = Yii::app()->request->getParam('num_players_continuous');
                if (isset($num_players_continuous)) {                    
                    $model->num_players_continuous = $num_players_continuous;
                }
                
                $min_players = Yii::app()->request->getParam('min_players');
                if (isset($min_players))
                    $model->min_players = $min_players;
                                                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    //end code by Linh
    public function actionCreateUsers() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournameId = Yii::app()->request->getParam('tournameId');
            if (!isset($tournameId)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Tournament::model()->findByPk($tournameId);
                if (isset($model)) {
                    //die($tournameId);
                    $firstMatchGroup = TournamentMatchGroup::model()->find('tournament_id = "' . $tournameId . '" AND is_start_group = "1"');
                    if (!empty($firstMatchGroup)) {
                        $criteria2 = new CDbCriteria();
                        $criteria2->addCondition('played_yet = 1');
                        $criteria2->addCondition('tournament_match_group_id = ' . $firstMatchGroup->id);
                        $played_matchs = TournamentMatch::model()->find($criteria2);

                        if (!empty($played_matchs)) {
                            $return['code'] = 5;
                            $return['message'] = "Giải đấu đã có trận đấu thi đấu!";
                            echo json_encode($return);
                            die;
                        } else {
                            $numerUserRegister = rand(17, 200);
                            $usernames = array();
                            for ($i = 0; $i < $numerUserRegister; $i++) {
                                $firstName = array('tung', 'thanh', 'tuyen', 'bach', 'nam', 'khang', 'ngocanh', 'son', 'phuong', 'huy', 'duc', 'trang', 'huyen', 'nhung', 'giang', 'thao');
                                $lastName = array('deptrai', 'xinhgai', 'cute', 'mongto', 'bungphe', 'ngudon', 'dangyeu', 'dola', 'vnd', 'matthon', 'sieunhan', 'meocon', 'thichanc', 'hailua', 'rerach', 'sida', 'danchoi', 'sanhdieu');
                                $first = rand(0, count($firstName) - 1);
                                $last = rand(0, count($lastName) - 1);
                                $username = $firstName[$first] . $lastName[$last];
                                
                                $userExsit = User::model()->find('username = "'.$username.'"');
                                if(empty($userExsit)){
                                    $userModel = new User();
                                    $userModel->username = $username;
                                    $userModel->repeat_password = 'tungdeptrai';
                                    $userModel->init_password = 'tungdeptrai';
                                    array_push($usernames, $username);
                                    if ($userModel->register()) {
                                        $userRegisterExsit = UserRegisterTournament::model()->find('user_id = "'.$userModel->id.'" AND tournament_id = "'.$tournameId.'"' );
                                        if (empty($userRegisterExsit)) {

                                            $userRegister = new UserRegisterTournament();
                                            $userRegister->user_id = $userModel->id;
                                            $userRegister->tournament_id = $tournameId;
                                            $userRegister->start_tournament_match_group_id = $firstMatchGroup->id;

                                            if (!$userRegister->save()) {
                                                $return['code'] = 2;
                                                $return['message'] = "Tạo người chơi không thành công!";
                                                echo json_encode($return);
                                                die;
                                            }

                                        };
                                    };
                                }else{
                                    $userRegisterExsit = UserRegisterTournament::model()->find('user_id = "'.$userExsit->id.'" AND tournament_id = "'.$tournameId.'"' );
                                    if (empty($userRegisterExsit)) {
                                        $userRegister = new UserRegisterTournament();
                                        $userRegister->user_id = $userExsit->id;
                                        $userRegister->tournament_id = $tournameId;
                                        $userRegister->start_tournament_match_group_id = $firstMatchGroup->id;
                                        
                                        if (!$userRegister->save()) {
                                            $return['code'] = 2;
                                            $return['message'] = "Tạo người chơi không thành công!";
                                            echo json_encode($return);
                                            die;
                                        }

                                    };
                                }
                                
                                
                            };
                            
                            $return['code'] = 0;
                            $return['message'] = "Tạo người chơi thành công!";
                        }
                    } else {
                        $return['code'] = 2;
                        $return['message'] = "Giải đấu chưa có vòng đầu!";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại giải đấu với ID truyền vào!";
                }
            }
        };
        echo json_encode($return);
    }

    public function actionUpdateTournamentBoard() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournameBoard = Yii::app()->request->getParam('tournameBoard');
            $tournameId = Yii::app()->request->getParam('tournameId');
            if (!isset($tournameId) || !isset($tournameBoard)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Tournament::model()->findByPk($tournameId);
                if (isset($model)) {
                    //die($tournameId);
                    $tournamentMathGroups = TournamentMatchGroup::model()->findAll("tournament_id = " . $tournameId);
//                    $return['code'] = 10;
//                    $return['tournameBoard'] = $tournameBoard;
                    $tournamentMathGroupsId = array();
                    foreach ($tournamentMathGroups as $index => $items) {
                        $tournamentMathGroupsId[$index] = $items->id;
                    }
                    //Echo count($tournameBoard);
                    for ($i = 0; $i < count($tournameBoard); $i++) {
                        if (isset($tournameBoard[$i])) {
                            //Common::dump($tournameBoard[$i]);
                            for ($j = 0; $j < count($tournameBoard[$i]); $j++) {
                                if (!in_array($tournameBoard[$i][$j], $tournamentMathGroupsId)) {
                                    $return['code'] = 4;
                                    $return['message'] = " Vòng đấu không tồn tại trong giải đấu!";
                                    echo json_encode($return);
                                    die;
                                } else {
                                    $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($tournameBoard[$i][$j]);
                                    //Common::dump($tournamentMatchGroup);die;
                                    if ($j == 0) {
                                        $tournamentMatchGroup->is_start_group = 1;
                                    } else {
                                        $tournamentMatchGroup->is_start_group = 0;
                                    }
                                    if (isset($tournameBoard[$i][$j + 1])) {
                                        $tournamentMatchGroup->next_id = $tournameBoard[$i][$j + 1];
                                    } else {
                                        $tournamentMatchGroup->next_id = NULL;
                                    }

                                    if (!$tournamentMatchGroup->save()) {
                                        $return['code'] = 5;
                                        $return['message'] = "Không thể lưu vòng đấu!";
                                        echo json_encode($return);
                                        die;
                                    } else {
                                        $return['items'][$j]['id'] = $tournamentMatchGroup->id;
                                        $return['items'][$j]['tournament_id'] = $tournamentMatchGroup->tournament_id;
                                        $return['items'][$j]['next'] = $tournamentMatchGroup->next;
                                        $return['items'][$j]['is_start_group'] = $tournamentMatchGroup->is_start_group;
                                        //$return['items'][$index]['max_player'] = $items->max_player;
                                    }
                                }
                            }
                        }
                    }
                    //die;
                    $return['code'] = 0;
                    $return['message'] = "Lưu vòng đấu thành công!";
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại giải đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionRunTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournameMatchGroupId = Yii::app()->request->getParam('tournameMatchGroupId');
            if (!isset($tournameMatchGroupId)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentMatchGroup::model()->findByPk($tournameMatchGroupId);
                if (isset($model)) {
                    $matchs = TournamentMatch::model()->findAll("tournament_match_group_id = " . $tournameMatchGroupId);
                    if (!empty($matchs)) {
                        
                        $criteria = new CDbCriteria();
                        $criteria->addCondition('played_yet = 0');
                        $criteria->addCondition('tournament_match_group_id = ' . $tournameMatchGroupId);
                        $matchNotPlayed = TournamentMatch::model()->find($criteria);
                        
                        if (!isset($matchNotPlayed)) {
                            $return['code'] = 0;
                            $return['message'] = "Vòng đấu đã thi đấu xong!";
                            echo json_encode($return);
                            die;
                        };
                        
                        foreach ($matchs as $match) {
                            if ($match->played_yet != 1) {
                                $users = UserPlayInTournamentMatch::model()->findAll('tournament_match_id = ' . $match->id);
                                if (!empty($users)) {
                                    $lengthUsers = count($users);
                                    if ($lengthUsers < 2) {
                                        $return['code'] = 3;
                                        $return['message'] = "Tồn tại trận đấu có 1 người chơi!";
                                        echo json_encode($return);
                                        die;
                                    }
                                }else{
                                   $match->delete();
                                }
                            }
                        }
                        
                        foreach ($matchs as $match) {
                            if ($match->played_yet != 1) {
                                $users = UserPlayInTournamentMatch::model()->findAll('tournament_match_id = ' . $match->id);
                                if (!empty($users)) {
                                    $lengthUsers = count($users);
                                    if ($lengthUsers >= 2) {
                                        $randPosition = rand(0, $lengthUsers - 1);
                                        $point = rand(2, 17);
                                        $win = UserPlayResult::model()->find('code_identifier = "win"');
                                        $lose = UserPlayResult::model()->find('code_identifier = "lose"');
                                        foreach ($users as $index => $user) {
                                            $result = new UserPlayInTournamentMatchResult();
                                            $result->user_play_in_tournament_match_id = $user->id;
                                            if ($randPosition == $index) {
                                                $result->user_play_result_id = $win->id;
                                                $result->point = $point * ($lengthUsers - 1);
                                            } else {
                                                $result->user_play_result_id = $lose->id;
                                                $result->point = 0 - $point;
                                            }
                                            if (!($result->save())) {
                                                $return['code'] = 3;
                                                $return['message'] = "Tạo kết quả không thành công!";
                                                echo json_encode($return);
                                                die;
                                            }
                                        }
                                        $match->played_yet = 1;
                                        if (!($match->save())) {
                                            $return['code'] = 4;
                                            $return['message'] = "Lưu trận đấu không thành công!";
                                            echo json_encode($return);
                                            die;
                                        };
                                    }
                                }else{
                                   $match->delete();
                                }
                            }
                        }
                        $return['code'] = 0;
                        $return['message'] = "Khởi chạy vòng đấu thành công!";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Không tồn tại trận đấu trong vòng đấu!";
                    }
                    //die;
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại vòng đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionSummaryOutTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournameMatchGroupId = Yii::app()->request->getParam('tournameMatchGroupId');
            if (!isset($tournameMatchGroupId)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentMatchGroup::model()->findByPk($tournameMatchGroupId);
                if (isset($model)) {

                    $criteria = new CDbCriteria();
                    $criteria->addCondition('played_yet = 0');
                    $criteria->addCondition('tournament_match_group_id = ' . $tournameMatchGroupId);
                    $matchNotPlayed = TournamentMatch::model()->find($criteria);

                    if (isset($matchNotPlayed)) {
                        $return['code'] = 1;
                        $return['message'] = "Tồn tại trận đấu chưa thi đấu!";
                        echo json_encode($return);
                        die;
                    };

                    $matchs = TournamentMatch::model()->findAll("tournament_match_group_id = " . $tournameMatchGroupId);
                    if (!empty($matchs)) {

                        $criteria = new CDbCriteria;
                        $criteria->alias = "user_play_in_tournament_match_result";
                        $criteria->join = "INNER JOIN user_play_in_tournament_match on user_play_in_tournament_match_result.user_play_in_tournament_match_id = user_play_in_tournament_match.id";
                        $criteria->join .= " INNER JOIN tournament_match on tournament_match.id = user_play_in_tournament_match.tournament_match_id";
                        $criteria->join .= " INNER JOIN user_register_tournament on user_register_tournament.id = user_play_in_tournament_match.user_register_tournament_id";
                        $criteria->addCondition('tournament_match.tournament_match_group_id = ' . $tournameMatchGroupId);
                        $criteria->group = "user_register_tournament.id";
                        $criteria->select = '* , user_register_tournament.id as user_id, sum(point) as sumPoint';
                        $criteria->order = "sumPoint desc";
                        $usersPoint = UserPlayInTournamentMatchResult::model()->findAll($criteria);

                        $return['result'] = array();
                        foreach ($usersPoint as $index => $userPoint) {
                            $return['result'][$index]['id'] = $userPoint['user_id'];
                            $return['result'][$index]['point'] = $userPoint['sumPoint'];
                            $return['result'][$index]['username'] = $userPoint->userPlayInTournamentMatch->userRegisterTournament->user->username;

                            $criteria2 = new CDbCriteria;
                            $criteria2->alias = "user_play_in_tournament_match_result";
                            $criteria2->join = "INNER JOIN user_play_in_tournament_match on user_play_in_tournament_match_result.user_play_in_tournament_match_id = user_play_in_tournament_match.id";
                            $criteria2->join .= " INNER JOIN tournament_match on tournament_match.id = user_play_in_tournament_match.tournament_match_id";
                            $criteria2->join .= " INNER JOIN user_register_tournament on user_register_tournament.id = user_play_in_tournament_match.user_register_tournament_id";
                            $criteria2->join .= " INNER JOIN user_play_result on user_play_result.id = user_play_in_tournament_match_result.user_play_result_id";
                            //$criteria2->join .= " INNER JOIN tournament_rule on user_play_result.id = tournament_rule.user_player_result_id";
                            $criteria2->addCondition('tournament_match.tournament_match_group_id = ' . $tournameMatchGroupId);
                            $criteria2->addCondition('user_play_result.code_identifier in ("foward_by_in_top","eliminated")');
                            $criteria2->addCondition('user_register_tournament.id = ' . $userPoint->userPlayInTournamentMatch->userRegisterTournament->id);
                            $criteria2->group = "user_register_tournament.id";
                            //$criteria->addCondition('tournament_rule.ok_to_forward = 1');            
                            $totalResults = UserPlayInTournamentMatchResult::model()->findAll($criteria2);
                            if (empty($totalResults)) {
                                //$return['result'][$index]['firstResult'] = $totalResults[0]->user_play_result;
                                $result = new UserPlayInTournamentMatchResult();
                                $result->user_play_in_tournament_match_id = $userPoint->userPlayInTournamentMatch->id;
                                $forward = UserPlayResult::model()->find('code_identifier = "foward_by_in_top"');
                                $eliminated = UserPlayResult::model()->find('code_identifier = "eliminated"');
                                if ($index < 32) {
                                    $result->user_play_result_id = $forward->id;
                                    $result->point = 0;
                                } else {
                                    $result->user_play_result_id = $eliminated->id;
                                    $result->point = 0;
                                };

                                if (!($result->save())) {
                                    $return['code'] = 3;
                                    $return['message'] = "Tạo kết quả không thành công!";
                                    echo json_encode($return);
                                    die;
                                }
                            };
                        };
                        //Common::dump($usersPoint);die;


                        $return['code'] = 0;
                        $return['message'] = "Tổng kết vòng đấu thành công!";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Không tồn tại trận đấu trong vòng đấu!";
                    }
                    //die;
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại vòng đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionSummaryInTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $tournameMatchGroupId = Yii::app()->request->getParam('tournameMatchGroupId');
            if (!isset($tournameMatchGroupId)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentMatchGroup::model()->findByPk($tournameMatchGroupId);
                if (isset($model)) {

                    $criteria = new CDbCriteria();
                    $criteria->addCondition('played_yet = 0');
                    $criteria->addCondition('tournament_match_group_id = ' . $tournameMatchGroupId);
                    $matchNotPlayed = TournamentMatch::model()->find($criteria);

                    if (isset($matchNotPlayed)) {
                        $return['code'] = 1;
                        $return['message'] = "Tồn tại trận đấu chưa thi đấu!";
                        echo json_encode($return);
                        die;
                    };

                    $matchs = TournamentMatch::model()->findAll("tournament_match_group_id = " . $tournameMatchGroupId);

                    if (!empty($matchs)) {
                        $criteria = new CDbCriteria();
                        $criteria->addCondition('tournament_match_group_id = ' . $tournameMatchGroupId);
                        $criteria->select = 'max(table_index) as maxIndex';
                        $max = TournamentMatch::model()->find($criteria);

                        $i = 0;

                        for ($i = 1; $i <= $max['maxIndex']; $i++) {
                            $criteria = new CDbCriteria;
                            $criteria->alias = "user_play_in_tournament_match_result";
                            $criteria->join = "INNER JOIN user_play_in_tournament_match on user_play_in_tournament_match_result.user_play_in_tournament_match_id = user_play_in_tournament_match.id";
                            $criteria->join .= " INNER JOIN tournament_match on tournament_match.id = user_play_in_tournament_match.tournament_match_id";
                            $criteria->join .= " INNER JOIN user_register_tournament on user_register_tournament.id = user_play_in_tournament_match.user_register_tournament_id";
                            $criteria->addCondition('tournament_match.tournament_match_group_id = ' . $tournameMatchGroupId);
                            $criteria->addCondition('tournament_match.table_index = ' . $i);
                            $criteria->group = "user_register_tournament.id";
                            $criteria->select = '* , user_register_tournament.id as user_id, sum(point) as sumPoint';
                            $criteria->order = "sumPoint desc";
                            $usersPoint = UserPlayInTournamentMatchResult::model()->findAll($criteria);
                            //Common::dump($usersPoint);die;
                            $return['result'] = array();
                            foreach ($usersPoint as $index => $userPoint) {

                                $criteria2 = new CDbCriteria;
                                $criteria2->alias = "user_play_in_tournament_match_result";
                                $criteria2->join = "INNER JOIN user_play_in_tournament_match on user_play_in_tournament_match_result.user_play_in_tournament_match_id = user_play_in_tournament_match.id";
                                $criteria2->join .= " INNER JOIN tournament_match on tournament_match.id = user_play_in_tournament_match.tournament_match_id";
                                $criteria2->join .= " INNER JOIN user_register_tournament on user_register_tournament.id = user_play_in_tournament_match.user_register_tournament_id";
                                $criteria2->join .= " INNER JOIN user_play_result on user_play_result.id = user_play_in_tournament_match_result.user_play_result_id";
                                //$criteria2->join .= " INNER JOIN tournament_rule on user_play_result.id = tournament_rule.user_player_result_id";
                                $criteria2->addCondition('tournament_match.tournament_match_group_id = ' . $tournameMatchGroupId);
                                $criteria2->addCondition('tournament_match.table_index = ' . $i);
                                $criteria2->addCondition('user_play_result.code_identifier in ("foward_by_1st","foward_by_2nd","eliminated")');
                                $criteria2->addCondition('user_register_tournament.id = ' . $userPoint->userPlayInTournamentMatch->userRegisterTournament->id);
                                $criteria2->group = "user_register_tournament.id";
                                //$criteria->addCondition('tournament_rule.ok_to_forward = 1');            
                                $totalResults = UserPlayInTournamentMatchResult::model()->find($criteria2);
                                //Common::dump($totalResults);die;
                                if (empty($totalResults)) {
                                    //die('hehehe');
                                    //$return['result'][$index]['firstResult'] = $totalResults[0]->user_play_result;
                                    $result = new UserPlayInTournamentMatchResult();
                                    $result->user_play_in_tournament_match_id = $userPoint->userPlayInTournamentMatch->id;
                                    $forward_1st = UserPlayResult::model()->find('code_identifier = "foward_by_1st"');
                                    $forward_2nd = UserPlayResult::model()->find('code_identifier = "foward_by_2nd"');
                                    $eliminated = UserPlayResult::model()->find('code_identifier = "eliminated"');
                                    if ($index < 1) {
                                        $result->user_play_result_id = $forward_1st->id;
                                        $result->point = 0;
                                    } else if ($index < 2) {
                                        $result->user_play_result_id = $forward_2nd->id;
                                        $result->point = 0;
                                    } else {
                                        $result->user_play_result_id = $eliminated->id;
                                        $result->point = 0;
                                    };

                                    if (!($result->save())) {
                                        $return['code'] = 3;
                                        $return['message'] = "Tạo kết quả không thành công!";
                                        echo json_encode($return);
                                        die;
                                    } else {
                                        
                                    }
                                };
                            }
                        }
                        //Common::dump($usersPoint);die;


                        $return['code'] = 0;
                        $return['message'] = "Tổng kết vòng đấu thành công!";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Không tồn tại trận đấu trong vòng đấu!";
                    }
                    //die;
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại vòng đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateMatchsInGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $matchs = Yii::app()->request->getParam('tournameMatchs');
            $matchGroupId = Yii::app()->request->getParam('matchGroupId');
            $numberMatch = Yii::app()->request->getParam('numberMatch');
            if (!isset($matchGroupId) || !isset($matchs)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentMatchGroup::model()->findByPk($matchGroupId);
                if (isset($model)) {
                    //Common::dump($matchs);
                    $criteria = new CDbCriteria;

                    $criteria2 = new CDbCriteria();
                    $criteria2->addCondition('played_yet = 1');
                    $criteria2->addCondition('tournament_match_group_id = ' . $matchGroupId);
                    $played_matchs = TournamentMatch::model()->find($criteria2);
                    if (!empty($played_matchs)) {
                        $return['code'] = 5;
                        $return['message'] = "Không thể sửa vòng đấu đã có trận đấu thi đấu!";
                        echo json_encode($return);
                        die;
                    }

                    if ($numberMatch >= 1) {
                        $tournamentMatchs = TournamentMatch::model()->findAll('tournament_match_group_id = ' . $matchGroupId);
                        foreach ($tournamentMatchs as $tournamentMatch) {
                            $matchUsers = UserPlayInTournamentMatch::model()->findAll('tournament_match_id = ' . $tournamentMatch->id);
                            foreach ($matchUsers as $matchUser) {
                                if (!($matchUser->delete())) {
                                    $return['code'] = 5;
                                    $return['message'] = "Xóa người chơi trong trận đấu thất bại!";
                                    echo json_encode($return);
                                    die;
                                };
                            }
                            if (!($tournamentMatch->delete())) {
                                $return['code'] = 6;
                                $return['message'] = "Xóa trận đấu thất bại!";
                                echo json_encode($return);
                                die;
                            };
                        };
                        foreach ($matchs as $match) {
                            $currentNumberMatch = $numberMatch;
                            if (isset($match['numberMatch'])) {
                                if ($match['numberMatch'] < $numberMatch) {
                                    $currentNumberMatch = $match['numberMatch'];
                                }
                            };
                            for ($i = 1; $i <= $currentNumberMatch; $i++) {
                                $curMatch = new TournamentMatch();
                                $curMatch->played_yet = 0;
                                $curMatch->tournament_match_group_id = $matchGroupId;
                                $curMatch->delay_time_to_start = 30;
                                $curMatch->match_index = $i;
                                $curMatch->table_index = $match['table'];
                                if (!($curMatch->save())) {
                                    $return['code'] = 4;
                                    $return['message'] = "Lưu trận đấu thất bại!";
                                    echo json_encode($return);
                                    die;
                                };
                                if (isset($match['users'])) {
                                    foreach ($match['users'] as $index => $user) {
                                        //Common::dump($user->data);die;
                                        if (isset($user['data'])) {
                                            $currentUser = new UserPlayInTournamentMatch();
                                            $currentUser->user_register_tournament_id = $user['data']['id'];
                                            $currentUser->tournament_match_id = $curMatch->id;
                                            $currentUser->user_index = $index;
                                            if (!($currentUser->save())) {
                                                $return['code'] = 5;
                                                $return['message'] = "Lưu người chơi thất bại!";
                                                Common::dump($currentUser);
                                                //echo json_encode($return);
                                                die;
                                            };
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $return['code'] = 0;
                    $return['message'] = "Lưu vòng đấu thành công!";
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại vòng đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetMatchGroupInfo() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $matchGroupId = Yii::app()->request->getParam('matchGroupId');
            if (!isset($matchGroupId)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $matchGroup = TournamentMatchGroup::model()->findByPk($matchGroupId);
                if (isset($matchGroup)) {
                    $matchs = TournamentMatch::model()->findAll('tournament_match_group_id = ' . $matchGroupId);
                    if (empty($matchs)) {
                        $return['code'] = 0;
                        $return['message'] = "Không tồn tại trận đấu";
                        $return['played'] = "0";
                        $return['data'] = array();
                    } else {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                        $data = array();

                        $criteria2 = new CDbCriteria();
                        $criteria2->addCondition('played_yet = 1');
                        $criteria2->addCondition('tournament_match_group_id = ' . $matchGroupId);
                        $played_matchs = TournamentMatch::model()->find($criteria2);
                        if (!empty($played_matchs)) {
                            $return['played'] = "1";
                            foreach ($matchs as $index => $match) {
                                $data[$index]['match']['id'] = $match->id;
                                $data[$index]['match']['name'] = $match->name;
                                $data[$index]['match']['start_time_plan'] = $match->start_time_plan;
                                $data[$index]['match']['played_yet'] = $match->played_yet;
                                $data[$index]['match']['game_log_id'] = $match->game_log_id;
                                $data[$index]['match']['tournament_match_group_id'] = $match->tournament_match_group_id;
                                $data[$index]['match']['end_time'] = $match->end_time;
                                $data[$index]['match']['start_time'] = $match->start_time;
                                $data[$index]['match']['delay_time_to_start'] = $match->delay_time_to_start;
                                $data[$index]['match']['match_index'] = $match->match_index;
                                $data[$index]['match']['table_index'] = $match->table_index;

                                $matchId = $match->id;
                                $users = UserPlayInTournamentMatch::model()->findAll('tournament_match_id = ' . $matchId);
                                $userData = array();
                                foreach ($users as $i => $user) {
                                    //$userData[$i]['id'] = $user->id;
                                    $userData[$i]['id'] = $user->user_register_tournament_id;
                                    $userData[$i]['tournament_match_id'] = $user->tournament_match_id;
                                    $userData[$i]['user_play_result_id'] = $user->user_play_result_id;
                                    $userData[$i]['user_index'] = $user->user_index;
                                    $userData[$i]['username'] = $user->userRegisterTournament->user->username;
                                    
                                    $criteria3 = new CDbCriteria();
                                    $criteria3->alias = "user_play_in_tournament_match_result";
                                    $criteria3->join = 'INNER JOIN user_play_result ON user_play_in_tournament_match_result.user_play_result_id = user_play_result.id';
                                    $criteria3->addCondition('user_play_result.code_identifier in ("win","lose")');
                                    $criteria3->addCondition('user_play_in_tournament_match_result.user_play_in_tournament_match_id = ' . $user->id);
                                    $userResult = UserPlayInTournamentMatchResult::model()->find($criteria3);
                                    if(!empty($userResult)){
                                        $userData[$i]['result'] = $userResult->userPlayResult->name;
                                        $userData[$i]['point'] = $userResult->point;
                                    }else{
//                                        $return['code'] = 3;
//                                        $return['message'] = "Không có kết quả của trận đấu đã thi đấu!";
//                                        echo json_encode($return);die;
                                    }
                                };
                                $data[$index]['users'] = $userData;
                            }
                        } else {

                            $criteria = new CDbCriteria();
                            $criteria->addCondition('match_index = 1');
                            $criteria->addCondition('tournament_match_group_id = ' . $matchGroupId);
                            $matchs = TournamentMatch::model()->findAll($criteria);
                            $data = array();

                            foreach ($matchs as $index => $match) {

                                $user = UserPlayInTournamentMatch::model()->find('tournament_match_id = ' . $match->id);
                                if (!empty($user)) {
                                    $user_register = UserRegisterTournament::model()->findByPk($user->user_register_tournament_id);
                                    $criteria3 = new CDbCriteria();
                                    $criteria3->alias = "user_play_in_tournament_match";
                                    $criteria3->join = 'INNER JOIN tournament_match ON user_play_in_tournament_match.tournament_match_id = tournament_match.id';
                                    $criteria3->join .= ' INNER JOIN user_register_tournament ON user_play_in_tournament_match.user_register_tournament_id = user_register_tournament.id';
                                    $criteria3->addCondition('tournament_match.tournament_match_group_id ="' . $matchGroupId . '"');
                                    $criteria3->addCondition('user_register_tournament.id ="' . $user_register->id . '"');
                                    $countMatch = UserPlayInTournamentMatch::model()->count($criteria3);

                                    $data[$index]['numberMatch'] = $countMatch;
                                    $data[$index]['match']['table'] = $index + 1;
                                    $data[$index]['match']['id'] = $match->id;
                                    $data[$index]['match']['name'] = $match->name;
                                    $data[$index]['match']['start_time_plan'] = $match->start_time_plan;
                                    $data[$index]['match']['played_yet'] = $match->played_yet;
                                    $data[$index]['match']['game_log_id'] = $match->game_log_id;
                                    $data[$index]['match']['tournament_match_group_id'] = $match->tournament_match_group_id;
                                    $data[$index]['match']['end_time'] = $match->end_time;
                                    $data[$index]['match']['start_time'] = $match->start_time;
                                    $data[$index]['match']['delay_time_to_start'] = $match->delay_time_to_start;
                                    $data[$index]['match']['match_index'] = $match->match_index;
                                    $data[$index]['match']['table_index'] = $match->table_index;

                                    $matchId = $match->id;
                                    $users = UserPlayInTournamentMatch::model()->findAll('tournament_match_id = ' . $matchId);
                                    $userData = array();
                                    foreach ($users as $i => $user) {
                                        //$userData[$i]['id'] = $user->id;
                                        $userData[$i]['id'] = $user->user_register_tournament_id;
                                        $userData[$i]['tournament_match_id'] = $user->tournament_match_id;
                                        $userData[$i]['user_play_result_id'] = $user->user_play_result_id;
                                        $userData[$i]['user_index'] = $user->user_index;
                                        $userData[$i]['username'] = $user->userRegisterTournament->user->username;
                                    };
                                    $data[$index]['users'] = $userData;
                                }
                            };


                            $return['played'] = "0";
                        };
                        $return['data'] = $data;
                    }

                    $returnUserList = array();
                    $criteria = new CDbCriteria;
                    $criteria->alias = "user_register_tournament";
                    $criteria->join = 'INNER JOIN tournament_match_group ON user_register_tournament.start_tournament_match_group_id = tournament_match_group.id';
                    $criteria->addCondition('tournament_match_group.id ="' . $matchGroupId . '"');
                    $currentUserList = UserRegisterTournament::model()->findAll($criteria);
                    foreach ($currentUserList as $i => $user) {
                        $currentUser = array();
                        $currentUser['id'] = $user->id;
                        $currentUser['username'] = $user->user->username;
                        array_push($returnUserList, $currentUser);
                    };

                    $criteria3 = new CDbCriteria;
                    $criteria3->alias = "tournament_match_group";
                    $criteria3->addCondition('tournament_match_group.next_id ="' . $matchGroupId . '"');
                    $result = TournamentMatchGroup::model()->find($criteria3);
                    if (!empty($result)) {
                        $oldMatchGroupId = $result->id;
                        $criteria2 = new CDbCriteria;
                        $criteria2->alias = "user_register_tournament";
                        $criteria2->join = 'INNER JOIN user_play_in_tournament_match ON user_play_in_tournament_match.user_register_tournament_id = user_register_tournament.id';
                        $criteria2->join .= ' INNER JOIN tournament_match ON user_play_in_tournament_match.tournament_match_id = tournament_match.id';
                        $criteria2->join .= ' INNER JOIN user_play_in_tournament_match_result ON user_play_in_tournament_match_result.user_play_in_tournament_match_id = user_play_in_tournament_match.id';
                        $criteria2->join .= ' INNER JOIN user_play_result ON user_play_in_tournament_match_result.user_play_result_id = user_play_result.id';
                        $criteria2->join .= ' INNER JOIN tournament_rule ON tournament_rule.user_player_result_id = user_play_result.id';

                        $criteria2->addCondition('tournament_match.tournament_match_group_id ="' . $oldMatchGroupId . '"');
                        $criteria2->addCondition('tournament_rule.ok_to_forward = 1');
                        $criteria2->group = 'user_register_tournament.id';
                        $currentUserList2 = UserRegisterTournament::model()->findAll($criteria2);

                        //Common::dump($currentUserList2);die;

                        foreach ($currentUserList2 as $i => $user) {
                            $currentUser = array();
                            $currentUser['id'] = $user->id;
                            $currentUser['username'] = $user->user->username;
                            array_push($returnUserList, $currentUser);
                        };
                    }

                    $return['currentUserList'] = $returnUserList;

                    $criteria = new CDbCriteria();
                    $criteria->addCondition('tournament_match_group_id = ' . $matchGroupId);
                    $criteria->select = 'max(match_index) as maxIndex';
                    $max = TournamentMatch::model()->find($criteria);

                    $return['matchGroupInfo'] = array();
                    $return['matchGroupInfo']['id'] = $matchGroup->id;
                    $return['matchGroupInfo']['tournament_id'] = $matchGroup->tournament_id;
                    $return['matchGroupInfo']['name'] = $matchGroup->name;
                    $return['matchGroupInfo']['type_id'] = $matchGroup->type_id;
                    $return['matchGroupInfo']['next_id'] = $matchGroup->next_id;
                    $return['matchGroupInfo']['is_start_group'] = $matchGroup->is_start_group;
                    $return['matchGroupInfo']['number_match'] = $max['maxIndex'];
                    $return['matchGroupInfo']['code_identifier'] = $matchGroup->type->code_identifier;
                    if (!empty($played_matchs)) {
                        $return['matchGroupInfo']['played_yet'] = 1;    
                    }else{
                        $return['matchGroupInfo']['played_yet'] = 0;   
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại vòng đấu với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetTournamentMatchGroupType() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = TournamentMatchGroupType::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại tournament MatchGroup";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['name'] = $model->name;
                $return['description'] = $model->description;
                $return['items'] = array();
                $tournamentMathGroups = TournamentMatchGroup::model()->findAll('match_group =' . $id);
                foreach ($tournamentMathGroups as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['next'] = $items->next;
                    $return['items'][$index]['is_start_group'] = $items->is_start_group;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                    //$return['items'][$index]['max_player'] = $items->max_player;
                }
            }
        } else {
            $model = TournamentMatchGroupType::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại tournament MatchGroup";
                $return['items'] = array();
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateTournamentMatchGroupType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new TournamentMatchGroupType();
                $model->name = $name;
                $model->description = $description;
                $model->code_identifier = $code_identifier;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateTournamentMatchGroupType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentMatchGroupType::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $description = Yii::app()->request->getParam('description');
                $code_identifier = Yii::app()->request->getParam('code_identifier');
                if (isset($description))
                    $model->description = $description;
                if (isset($description))
                    $model->description = $description;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteTournamentMatchGroupType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $tournamentMatchGroup = TournamentMatchGroup::model()->find('type_id =' . $id);
                if (isset($tournamentMatchGroup)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn phải xóa các vòng đấu  con trước";
                } else {
                    $model = TournamentMatchGroupType::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetTournamentMatchGroup() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $tournament_id = Yii::app()->request->getParam('tournament_id');
        if (isset($id)) {
            $model = TournamentMatchGroup::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại giải đấu";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['type_id'] = $model->type_id;
                $return['tournament_id'] = $model->tournament_id;
                $return['next_id'] = $model->next_id;
                $return['is_start_group'] = $model->is_start_group;
            }
        } else {
            if (isset($tournament_id)) {
                $model = TournamentMatchGroup::model()->findAll("tournament_id = " . $tournament_id);
            } else {
                $model = TournamentMatchGroup::model()->findAll();
            }
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại vòng đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['type_id'] = $items->type_id;
                    $return['items'][$index]['next_id'] = $items->next_id;
                    $return['items'][$index]['is_start_group'] = $items->is_start_group;
                    $return['items'][$index]['code_identifier'] = $items->type->code_identifier;
                    
                    $criteria = new CDbCriteria();
                    $criteria->addCondition('tournament_match_group_id = ' . $items->id);
                    $criteria->select = 'max(match_index) as maxIndex';
                    $max = TournamentMatch::model()->find($criteria);
                    $return['items'][$index]['number_match'] = $max['maxIndex'];
                    
                    $criteria2 = new CDbCriteria();
                    $criteria2->addCondition('played_yet = 1');
                    $criteria2->addCondition('tournament_match_group_id = ' . $items->id);
                    $played_matchs = TournamentMatch::model()->find($criteria2);
                    if (empty($played_matchs)) {
                        $return['items'][$index]['played_yet'] = 0;
                    }else{
                        $return['items'][$index]['played_yet'] = 1;
                    };

//                    $return['items'][$index]['max_player'] = $items->max_player;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $type_id = Yii::app()->request->getParam('type_id');
            $tournament_id = Yii::app()->request->getParam('tournament_id');
            $name = Yii::app()->request->getParam('name');
            $next_id = Yii::app()->request->getParam('next_id');
            $is_start_group = Yii::app()->request->getParam('is_start_group');
            //$max_player = Yii::app()->request->getParam('max_player');
            if (!isset($type_id) || !isset($tournament_id) || !isset($is_start_group)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                if (!empty($next_id) && $next_id != 0) {
                    $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($next_id);
                    if (!isset($tournamentMatchGroup)) {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại vòng đấu tiếp theo";
                        echo json_encode($return);
                        die;
                    }
                }

                $tournament = Tournament::model()->findByPk($tournament_id);
                if (!isset($tournament)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại giải đấu";
                    echo json_encode($return);
                    die;
                }


                $model = new TournamentMatchGroup();
                $model->type_id = $type_id;
                $model->name = $name;
                $model->tournament_id = $tournament_id;
                $model->is_start_group = $is_start_group;
                $model->next_id = $next_id;
                //$model->max_player = $max_player;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = TournamentMatchGroup::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $next_id = Yii::app()->request->getParam('next_id');
                if (!empty($next_id)) {
                    $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($next_id);
                    if (!isset($tournamentMatchGroup)) {
                        $return['code'] = 5;
                        $return['message'] = "Vòng đấu tiếp theo không tồn tại";
                        $return['next_id'] = $next_id;
                        echo json_encode($return);
                        die;
                    }
                    $model->next_id = $next_id;
                }
                $is_start_group = Yii::app()->request->getParam('is_start_group');
                if (isset($is_start_group))
                    $model->is_start_group = $is_start_group;
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
//                $max_player = Yii::app()->request->getParam('max_player');
//                if (isset($max_player))
//                    $model->max_player = $max_player;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteTournamentMatchGroup() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $userRegisterTournament = UserRegisterTournament::model()->find('start_tournament_match_group_id =' . $id);
                if (isset($userRegisterTournament)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn phải xóa user đăng kí vòng đấu trước";
                } else {
                    $model = TournamentMatchGroup::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserRegisterTournament() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $user_id = Yii::app()->request->getParam('user_id');
        $tournament_id = Yii::app()->request->getParam('tournament_id');
        if (!empty($user_id)) {
            $model = UserRegisterTournament::model()->find('user_id =' . $user_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại user đăng kí giải đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['user_id'] = $model->user_id;
                $return['tournament_id'] = $model->tournament_id;
                $return['start_tournament_match_group_id'] = $model->start_tournament_match_group_id;
                $return['register_time'] = $model->register_time;
                $return['slogan'] = $model->slogan;
            }
            $return["user_id"] = $user_id;
            echo json_encode($return);
            die;
        }
        if (!empty($tournament_id)) {
            $model = UserRegisterTournament::model()->findAll('tournament_id =' . $tournament_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại user đăng kí giải đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['user_id'] = $items->user_id;
                    $return['items'][$index]['username'] = $items->user->username;
                    $return['items'][$index]['start_tournament_match_group_id'] = $items->start_tournament_match_group_id;
                    $return['items'][$index]['register_time'] = $items->register_time;
                    $return['items'][$index]['slogan'] = $items->slogan;
                }
            }
            echo json_encode($return);
            die;
        }
        if (!empty($id)) {
            $model = UserRegisterTournament::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại user đăng kí";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['user_id'] = $model->user_id;
                $return['tournament_id'] = $model->tournament_id;
                $return['start_tournament_match_group_id'] = $model->start_tournament_match_group_id;
                $return['register_time'] = $model->register_time;
                $return['slogan'] = $model->slogan;
            }
        } else {
            $model = UserRegisterTournament::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại user đăng kí giải đấu nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
//                K::dump($model);die;
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_id'] = $items->user_id;
                    $return['items'][$index]['tournament_id'] = $items->tournament_id;
                    $return['items'][$index]['start_tournament_match_group_id'] = $items->start_tournament_match_group_id;
                    $return['items'][$index]['register_time'] = $items->register_time;
                    $return['items'][$index]['slogan'] = $items->slogan;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserRegisterTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_id = Yii::app()->request->getParam('user_id');
            $tournament_id = Yii::app()->request->getParam('tournament_id');
            $start_tournament_match_group_id = Yii::app()->request->getParam('start_tournament_match_group_id');
            $register_time = Yii::app()->request->getParam('register_time');
            $slogan = Yii::app()->request->getParam('slogan');
            if (!isset($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $user = User::model()->findByPk($user_id);
                if (!isset($user)) {
                    $return['code'] = 3;
                    $return['message'] = "User không tồn tại";
                    echo json_encode($return);
                    die;
                }

                $tournament = Tournament::model()->findByPk($tournament_id);
                if (!isset($tournament)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại giải đấu";
                    echo json_encode($return);
                    die;
                }
                $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($start_tournament_match_group_id);
                if (!isset($tournamentMatchGroup)) {
                    $return['code'] = 5;
                    $return['message'] = "Không tồn tại vòng đấu";
                    echo json_encode($return);
                    die;
                }

                $model = new UserRegisterTournament();
                $model->user_id = $user_id;
                $model->tournament_id = $tournament_id;
                $model->start_tournament_match_group_id = $start_tournament_match_group_id;
                $model->register_time = $register_time;
                $model->slogan = $slogan;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateUserRegisterTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = UserRegisterTournament::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $user_id = Yii::app()->request->getParam('user_id');
                if (isset($user_id)) {
                    $user = Tournament::model()->findByPk($user_id);
                    if (!isset($user)) {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại user";
                        echo json_encode($return);
                        die;
                    }
                    $model->user_id = $user_id;
                }

                $tournament_id = Yii::app()->request->getParam('tournament_id');
                if (isset($tournament_id)) {
                    $tournament = Tournament::model()->findByPk($tournament_id);
                    if (!isset($tournament)) {
                        $return['code'] = 4;
                        $return['message'] = "Giải đấu không tồn tại";
                        echo json_encode($return);
                        die;
                    }
                    $model->tournament_id = $tournament_id;
                }

                $start_tournament_match_group_id = Yii::app()->request->getParam('start_tournament_match_group_id');
                if (isset($start_tournament_match_group_id)) {
                    $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($start_tournament_match_group_id);
                    if (!isset($tournamentMatchGroup)) {
                        $return['code'] = 5;
                        $return['message'] = "Tên vòng đấu không tồn tại";
                        echo json_encode($return);
                        die;
                    }
                    $model->start_tournament_match_group_id = $start_tournament_match_group_id;
                }
                $register_time = Yii::app()->request->getParam('register_time');
                if (isset($register_time))
                    $model->register_time = $register_time;
                $slogan = Yii::app()->request->getParam('slogan');
                if (isset($slogan))
                    $model->slogan = $slogan;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteUserRegisterTournament() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $userPlayInTournamentMatch = UserPlayInTournamentMatch::model()->find('user_register_tournament_id =' . $id);
                if (isset($userPlayInTournamentMatch)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn pải xóa trong bảng user_play_in_tournament_match trước";
                } else {
                    $model = UserRegisterTournament::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserPlayResultCategory() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = UserPlayResultCategory::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại UserPlayResultCategory";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['name'] = $model->name;
                $return['description'] = $model->description;
                $return['items'] = array();
                $userPlayResult = UserPlayResult::model()->findAll('category_id =' . $id);
                foreach ($userPlayResult as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['category_id'] = $items->category_id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['description'] = $items->description;
                }
            }
        } else {
            $model = UserPlayResultCategory::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại UserPlayResultCategory";
                $return['items'] = array();
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['description'] = $items->description;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserPlayResultCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new UserPlayResultCategory();
                $model->name = $name;
                $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateUserPlayResultCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = UserPlayResultCategory::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteUserPlayResultCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $userPlayResult = UserPlayResult::model()->find('category_id =' . $id);
                if (isset($userPlayResult)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn phải xóa trong bảng user_play_result trước";
                } else {
                    $model = UserPlayResultCategory::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserPlayResult() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = UserPlayResult::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['name'] = $model->name;
                $return['category_id'] = $model->category_id;
                $return['description'] = $model->description;
            }
        } else {
            $model = UserPlayResult::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['category_id'] = $items->category_id;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserPlayResult() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $category_id = Yii::app()->request->getParam('category_id');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            if (!isset($name) || !isset($code_identifier) || !isset($category_id) || !is_numeric($category_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $userPlayResultCategory = UserPlayResultCategory::model()->findByPk($category_id);
                if (!isset($userPlayResultCategory)) {
                    $return['code'] = 3;
                    $return['message'] = "UserPlayResultCategory không tồn tại";
                } else {
                    $model = new UserPlayResult();
                    $model->name = $name;
                    $model->description = $description;
                    $model->category_id = $category_id;
                    $model->code_identifier = $code_identifier;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateUserPlayResult() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = UserPlayResult::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $category_id = Yii::app()->request->getParam('category_id');
                if (isset($category_id)) {
                    $userPlayResultCategory = UserPlayResultCategory::model()->findByPk($category_id);
                    if (!isset($userPlayResultCategory)) {
                        $return['code'] = 4;
                        $return['message'] = "Category không tồn tại";
                        echo json_encode($return);
                        die;
                    }
                    $model->category_id = $category_id;
                }
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $code_identifier = Yii::app()->request->getParam('code_identifier');
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteUserPlayResult() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = UserPlayResult::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetTournamentRule() {
//        echo'aaa';die;
        $return = array();
        $user_player_result_id = Yii::app()->request->getParam('user_player_result_id');
        $tournament_id = Yii::app()->request->getParam('tournament_id');
        $tournament_match_group_id = Yii::app()->request->getParam('tournament_match_group_id');
        $criteria = new CDbCriteria();
        if (isset($tournament_id)) {
            $criteria->alias = "tournament_rule";
            $criteria->join = ' INNER JOIN tournament_match_group ON tournament_rule.tournament_match_group_id = tournament_match_group.id';
            $criteria->addCondition(' tournament_match_group.tournament_id = ' . $tournament_id);
        } else {
            if (isset($user_player_result_id)) {
                $criteria->addCondition('user_player_result_id=' . $user_player_result_id);
            }
            if (isset($tournament_match_group_id)) {
                $criteria->addCondition('tournament_match_group_id=' . $tournament_match_group_id);
            }
        }
        $model = TournamentRule::model()->findAll($criteria);
        if (empty($model)) {
            $return['code'] = 1;
            $return['message'] = "Không có tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
//                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['user_player_result_id'] = $items->user_player_result_id;
                $return['items'][$index]['tournament_match_group_id'] = $items->tournament_match_group_id;
                $return['items'][$index]['description'] = $items->description;
                $return['items'][$index]['added_money'] = $items->added_money;
                $return['items'][$index]['ok_to_forward'] = $items->ok_to_forward;
            }
        }
        echo json_encode($return);
    }

    public function actionCreateTournamentRule() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_player_result_id = Yii::app()->request->getParam('user_player_result_id');
            $tournament_match_group_id = Yii::app()->request->getParam('tournament_match_group_id');
            $description = Yii::app()->request->getParam('description');
            $added_money = Yii::app()->request->getParam('added_money');
            $ok_to_forward = Yii::app()->request->getParam('ok_to_forward');
            if (!isset($user_player_result_id) || !isset($added_money) || !isset($ok_to_forward) || !is_numeric($user_player_result_id) || !isset($tournament_match_group_id) || !is_numeric($tournament_match_group_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $userPlayResult = UserPlayResult::model()->findByPk($user_player_result_id);
                if (!isset($userPlayResult)) {
                    $return['code'] = 3;
                    $return['message'] = "UserPlayResult không tồn tại";
                    echo json_encode($return);
                    die;
                }
                $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($tournament_match_group_id);
                if (!isset($tournamentMatchGroup)) {
                    $return['code'] = 4;
                    $return['message'] = "TournamentMatchGroup không tồn tại";
                    echo json_encode($return);
                    die;
                }
                $criteria = new CDbCriteria();
                $criteria->addCondition('user_player_result_id=' . $user_player_result_id);
                $criteria->addCondition('tournament_match_group_id=' . $tournament_match_group_id);
                $model = TournamentRule::model()->findAll($criteria);
                if (empty($model)) {
                    $model = new TournamentRule();
                    $model->user_player_result_id = $user_player_result_id;
                    $model->tournament_match_group_id = $tournament_match_group_id;
                    $model->description = $description;
                    $model->added_money = $added_money;
                    $model->ok_to_forward = $ok_to_forward;
                    if ($model->save()) {
//                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Tournament-Rule đã tồn tại';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateTournamentRule() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_player_result_id = Yii::app()->request->getParam("user_player_result_id");
            $tournament_match_group_id = Yii::app()->request->getParam("tournament_match_group_id");
            $model = TournamentRule::model()->find('user_player_result_id="' . $user_player_result_id . '" AND tournament_match_group_id="' . $tournament_match_group_id . '"');
            if (!isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Không tồn tại TournamentRule phù hợp';
            } else {
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $added_money = Yii::app()->request->getParam('added_money');
                if (isset($added_money))
                    $model->added_money = $added_money;
                $ok_to_forward = Yii::app()->request->getParam('ok_to_forward');
                if (isset($ok_to_forward))
                    $model->ok_to_forward = $ok_to_forward;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteTournamentRule() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_player_result_id = Yii::app()->request->getParam("user_player_result_id");
            $tournament_match_group_id = Yii::app()->request->getParam("tournament_match_group_id");
            if (!isset($user_player_result_id) || !is_numeric($user_player_result_id) || !isset($tournament_match_group_id) || !is_numeric($tournament_match_group_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = TournamentRule::model()->find('user_player_result_id="' . $user_player_result_id . '" AND tournament_match_group_id="' . $tournament_match_group_id . '"');
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserPlayInTournamentMatch() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $user_register_tournament_id = Yii::app()->request->getParam('user_register_tournament_id');

        if (isset($user_register_tournament_id)) {
            $model = UserPlayInTournamentMatch::model()->findAll('user_register_tournament_id =' . $user_register_tournament_id);
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại UserPlayInTournamentMatch";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_register_tournament_id'] = $items->user_register_tournament_id;
                    $return['items'][$index]['tournament_match_id'] = $items->tournament_match_id;
                    $return['items'][$index]['user_play_result_id'] = $items->user_play_result_id;
                }
            }
            echo json_encode($return);
            die;
        }
        $tournament_match_id = Yii::app()->request->getParam('tournament_match_id');
        if (isset($tournament_match_id)) {
            $model = UserPlayInTournamentMatch::model()->findAll('tournament_match_id=' . $tournament_match_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại TournamentMatch";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_register_tournament_id'] = $items->user_register_tournament_id;
                    $return['items'][$index]['tournament_match_id'] = $items->tournament_match_id;
                    $return['items'][$index]['user_play_result_id'] = $items->user_play_result_id;
                }
            }
            echo json_encode($return);
            die;
        }

        $user_play_result_id = Yii::app()->request->getParam('user_play_result_id');
        if (isset($user_play_result_id)) {
            $model = UserPlayInTournamentMatch::model()->findAll('user_play_result_id=' . $user_play_result_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại UserPlayResult";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $model->id;
                    $return['items'][$index]['user_register_tournament_id'] = $model->user_tournament_match_id;
                    $return['items'][$index]['tournament_match_id'] = $model->tournament_match_id;
                    $return['items'][$index]['user_play_result_id'] = $model->user_play_result_id;
                }
            }
            echo json_encode($return);
            die;
        }

        if (isset($id)) {
            $model = UserPlayInTournamentMatch::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại UserPlayInTournamentMatch";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['user_register_tournament_id'] = $model->user_tournament_match_id;
                $return['tournament_match_id'] = $model->tournament_match_id;
                $return['user_play_result_id'] = $model->user_play_result_id;
            }
        } else {
            $model = UserPlayInTournamentMatch::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại UserPlayInTournamentMatch";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_register_tournament_id'] = $items->user_register_tournament_id;
                    $return['items'][$index]['tournament_match_id'] = $items->tournament_match_id;
                    $return['items'][$index]['user_play_result_id'] = $items->user_play_result_id;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserPlayInTournamentMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_register_tournament_id = Yii::app()->request->getParam('user_register_tournament_id');
            $tournament_match_id = Yii::app()->request->getParam('tournament_match_id');
            $user_play_result_id = Yii::app()->request->getParam('user_play_result_id');
            if (!isset($user_register_tournament_id) || !isset($tournament_match_id) || !isset($user_play_result_id) || !is_numeric($user_register_tournament_id) || !is_numeric($tournament_match_id) || !is_numeric($user_play_result_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $userRegisterTournament = UserRegisterTournament::model()->findByPk($user_register_tournament_id);
                if (!isset($userRegisterTournament)) {
                    $return['code'] = 3;
                    $return['message'] = "UserRegister không tồn tại";
                    echo json_encode($return);
                    die;
                }

                $tournamentMatch = TournamentMatch::model()->findByPk($tournament_match_id);
                if (!isset($tournamentMatch)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại trận đấu";
                    echo json_encode($return);
                    die;
                }
                $userPlayResult = UserPlayResult::model()->findByPk($user_play_result_id);
                if (!isset($userPlayResult)) {
                    $return['code'] = 5;
                    $return['message'] = "Không tồn tại UserPlayResult";
                    echo json_encode($return);
                    die;
                }
                $model = UserPlayInTournamentMatch::model()->find('user_register_tournament_id = ' . $user_register_tournament_id . ' AND tournament_match_id =' . $tournament_match_id);
                if (empty($model)) {
                    $model = new UserPlayInTournamentMatch();
                    $model->user_register_tournament_id = $user_register_tournament_id;
                    $model->tournament_match_id = $tournament_match_id;
                    $model->user_play_result_id = $user_play_result_id;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 2;
                    $return['message'] = 'Một bàn chơi không thể tồn tại 2 người chơi cùng ID';
                }
            }
        }
        echo json_encode($return);
    }

//    public function actionGetTournamentMatch() {
//        $return = array();
//        $id = Yii::app()->request->getParam('id');
//        $tournament_match_group_id = Yii::app()->request->getParam('tournament_match_group_id');
//        if (isset($tournament_match_group_id)) {
//            $model = TournamentMatch::model()->findAll('tournament_match_group_id =' . $tournament_match_group_id);
//            if (!isset($model)) {
//                $return['code'] = 1;
//                $return['message'] = "Không tồn tại TournamentMatch";
//            } else {
//                $return['code'] = 0;
//                $return['message'] = "Thành công";
//                $return['items'] = array();
//                foreach ($model as $index => $items) {
//                    $return['items'][$index]['id'] = $items->id;
//                    $return['items'][$index]['name'] = $items->name;
//                    $return['items'][$index]['start_time_plan'] = $items->start_time_plan;
//                    $return['items'][$index]['played_yet'] = $items->played_yet;
//                    $return['items'][$index]['game_log_id'] = $items->game_log_id;
//                    $return['items'][$index]['tournament_match_group_id'] = $items->tournament_match_group_id;
//                    $return['items'][$index]['end_time'] = $items->end_time;
//                    $return['items'][$index]['start_time'] = $items->start_time;
//                    $return['items'][$index]['delay_time_to_start'] = $items->delay_time_to_start;
//                }
//            }
//            echo json_encode($return);
//            die;
//        }
//        $game_log_id = Yii::app()->request->getParam('game_log_id');
//        if (isset($game_log_id)) {
//            $model = TournamentMatch::model()->find('game_log_id=' . $game_log_id);
//            if (!isset($model)) {
//                $return['code'] = 1;
//                $return['message'] = "Không tồn tại TournamentMatch";
//            } else {
//                $return['code'] = 0;
//                $return['message'] = "Thành công";
//                $return['items'] = array();
//                foreach ($model as $index => $items) {
//                    $return['items'][$index]['id'] = $items->id;
//                    $return['items'][$index]['name'] = $items->name;
//                    $return['items'][$index]['start_time_plan'] = $items->start_time_plan;
//                    $return['items'][$index]['played_yet'] = $items->played_yet;
//                    $return['items'][$index]['game_log_id'] = $items->game_log_id;
//                    $return['items'][$index]['tournament_match_group_id'] = $items->tournament_match_group_id;
//                    $return['items'][$index]['end_time'] = $items->end_time;
//                    $return['items'][$index]['start_time'] = $items->start_time;
//                    $return['items'][$index]['delay_time_to_start'] = $items->delay_time_to_start;
//                }
//            }
//            echo json_encode($return);
//            die;
//        }
//
//        if (isset($id)) {
//            $model = TournamentMatch::model()->findByPk($id);
//            if (!isset($model)) {
//                $return['code'] = 1;
//                $return['message'] = "Không tồn tại TournamentMatch";
//            } else {
//                $return['code'] = 0;
//                $return['message'] = "Thành công";
//                $return['items'] = $model->id;
//                $return['name'] = $model->name;
//                $return['start_time_plan'] = $model->start_time_plan;
//                $return['played_yet'] = $model->played_yet;
//                $return['game_log_id'] = $model->game_log_id;
//                $return['tournament_match_group_id'] = $model->tournament_match_group_id;
//                $return['end_time'] = $model->end_time;
//                $return['start_time'] = $model->start_time;
//                $return['delay_time_to_start'] = $model->delay_time_to_start;
//            }
//        } else {
//            $model = TournamentMatch::model()->findAll();
//            if (!isset($model)) {
//                $return['code'] = 1;
//                $return['message'] = "Không tồn tại TournamentMatch";
//            } else {
//                $return['code'] = 0;
//                $return['message'] = "Thành công";
//                $return['items'] = array();
//                foreach ($model as $index => $items) {
//
//                    $return['items'][$index]['id'] = $items->id;
//                    $return['items'][$index]['name'] = $items->name;
//                    $return['items'][$index]['start_time_plan'] = $items->start_time_plan;
//                    $return['items'][$index]['played_yet'] = $items->played_yet;
//                    $return['items'][$index]['game_log_id'] = $items->game_log_id;
//                    $return['items'][$index]['tournament_match_group_id'] = $items->tournament_match_group_id;
//                    $return['items'][$index]['end_time'] = $items->end_time;
//                    $return['items'][$index]['start_time'] = $items->start_time;
//                    $return['items'][$index]['delay_time_to_start'] = $items->delay_time_to_start;
//                }
//            }
//        }
//        echo json_encode($return);
//    }

//    public function actionCreateTournamentMatch() {
//        $return = array();
//        $token = Yii::app()->request->getParam('accessToken');
//        $session = new CHttpSession();
//        $session->open();
//        $access_token = $session['accessToken'];
//        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
//            $return['code'] = -1;
//            $return['message'] = "Bạn không có quyền truy cập";
//        } else {
//            $name = Yii::app()->request->getParam('name');
//            $start_time_plan = Yii::app()->request->getParam('start_time_plan');
//            $played_yet = Yii::app()->request->getParam('played_yet');
//            $game_log_id = Yii::app()->request->getParam('game_log_id');
//            $tournament_match_group_id = Yii::app()->request->getParam('tournament_match_group_id');
//            $end_time = Yii::app()->request->getParam('end_time');
//            $start_time = Yii::app()->request->getParam('start_time');
//            $delay_time_to_start = Yii::app()->request->getParam('delay_time_to_start');
//            if (!isset($name) || !isset($played_yet) || !isset($tournament_match_group_id) || !isset($delay_time_to_start) || !is_numeric($tournament_match_group_id)) {
//                $return['code'] = 2;
//                $return['message'] = "Dữ liệu nhập vào không đúng";
//            } else {
//                $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($tournament_match_group_id);
//                if (!isset($tournamentMatchGroup)) {
//                    $return['code'] = 4;
//                    $return['message'] = "Không tồn tại vòng đấu";
//                    echo json_encode($return);
//                    die;
//                }
//
//                $model = new TournamentMatch();
//                $model->name = $name;
//                $model->start_time_plan = $start_time_plan;
//                $model->played_yet = $played_yet;
//                $model->game_log_id = $game_log_id;
//                $model->tournament_match_group_id = $tournament_match_group_id;
//                $model->end_time = $end_time;
//                $model->start_time = $start_time;
//                $model->delay_time_to_start = $delay_time_to_start;
//                if ($model->save()) {
//                    $return['id'] = $model->id;
//                    $return['code'] = 0;
//                    $return['message'] = 'Thành công';
//                } else {
//                    $return['code'] = 1;
//                    $return['message'] = 'Không thành công';
//                }
//            }
//        }
//        echo json_encode($return);
//    }
//
//    public function actionUpdateTournamentMatch() {
//        $return = array();
//        $token = Yii::app()->request->getParam('accessToken');
//        $session = new CHttpSession();
//        $session->open();
//        $access_token = $session['accessToken'];
//        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
//            $return['code'] = -1;
//            $return['message'] = "Bạn không có quyền truy cập";
//        } else {
//            $id = Yii::app()->request->getParam("id");
//            $model = TournamentMatch::model()->findByPk($id);
//            if (!isset($id) || !is_numeric($id) || !isset($model)) {
//                $return['code'] = 2;
//                $return['message'] = 'Dữ liệu nhập vào không đủ';
//            } else {
//                $tournament_match_group_id = Yii::app()->request->getParam('tournament_match_group_id');
//                if (isset($tournament_match_group_id)) {
//                    $tournamentMatchGroup = TournamentMatchGroup::model()->findByPk($tournament_match_group_id);
//                    if (!isset($tournamentMatchGroup)) {
//                        $return['code'] = 3;
//                        $return['message'] = "Vòng đấu không tồn tại";
//                        echo json_encode($return);
//                        die;
//                    }
//                    $model->tournament_match_group_id = $tournament_match_group_id;
//                }
//                $game_log_id = Yii::app()->request->getParam('game_log_id');
//
//                $name = Yii::app()->request->getParam('name');
//                if (isset($name))
//                    $model->name = $name;
//                $start_time_plan = Yii::app()->request->getParam('start_time_plan');
//                if (!empty($start_time_plan))
//                    $model->start_time_plan = $start_time_plan;
//                $played_yet = Yii::app()->request->getParam('played_yet');
//                if (isset($played_yet))
//                    $model->played_yet = $played_yet;
//                $end_time = Yii::app()->request->getParam('end_time');
//                if (!empty($end_time))
//                    $model->end_time = $end_time;
//                $start_time = Yii::app()->request->getParam('start_time');
//                if (!empty($start_time))
//                    $model->start_time = $start_time;
//                $delay_time_to_start = Yii::app()->request->getParam('delay_time_to_start');
//                if (isset($delay_time_to_start))
//                    $model->delay_time_to_start = $delay_time_to_start;
//                if ($model->save()) {
//                    $return['code'] = 0;
//                    $return['message'] = "Thành công";
//                } else {
//                    $return['code'] = 1;
//                    $return['message'] = "Lưu dữ liệu thất bại";
//                }
//            }
//        }
//        echo json_encode($return);
//    }

    public function actionDeleteTournamentMatch() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $userPlayInTournamentMatch = UserPlayInTournamentMatch::model()->find('tournament_match_id =' . $id);
                if (isset($userPlayInTournamentMatch)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn phải xóa trong bảng UserPlayInTournamentMatch trước";
                } else {
                    $model = TournamentMatch::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserGameAction() {
        $return = array();
        $user_play_in_tournament_match_id = Yii::app()->request->getParam('user_play_in_tournament_match_id');
        if (isset($user_play_in_tournament_match_id)) {
            $model = UserGameAction::model()->findAll('user_play_in_tournament_match_id=' . $user_play_in_tournament_match_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
//                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_play_in_tournament_match_id'] = $items->user_play_in_tournament_match_id;
                    $return['items'][$index]['game_action_id'] = $items->game_action_id;
                    $return['items'][$index]['timestamp'] = $items->timestamp;
                }
            }
            echo json_encode($return);
            die;
        }

        $game_action_id = Yii::app()->request->getParam('game_action_id');
        if (isset($game_action_id)) {
            $model = UserGameAction::model()->findAll('game_action_id=' . $game_action_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
//                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['user_play_in_tournament_match_id'] = $items->user_play_in_tournament_match_id;
                    $return['items'][$index]['game_action_id'] = $items->game_action_id;
                    $return['items'][$index]['timestamp'] = $items->timestamp;
                }
            }
            echo json_encode($return);
            die;
        }

        $model = UserGameAction::model()->findAll();
        if (empty($model)) {
            $return['code'] = 1;
            $return['message'] = "Không có tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
//                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['user_play_in_tournament_match_id'] = $items->user_play_in_tournament_match_id;
                $return['items'][$index]['game_action_id'] = $items->game_action_id;
                $return['items'][$index]['timestamp'] = $items->timestamp;
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserGameAction() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_play_in_tournament_match_id = Yii::app()->request->getParam('user_play_in_tournament_match_id');
            $game_action_id = Yii::app()->request->getParam('game_action_id');
            $timestamp = Yii::app()->request->getParam('timestamp');
            if (!isset($user_play_in_tournament_match_id) || !is_numeric($game_action_id) || !is_numeric($user_play_in_tournament_match_id) || !is_numeric($game_action_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $userPlayInTournamentMatch = UserPlayInTournamentMatch::model()->findByPk($user_play_in_tournament_match_id);
                if (!isset($userPlayInTournamentMatch)) {
                    $return['code'] = 3;
                    $return['message'] = "UserPlayInTournamentMatch không tồn tại";
                    echo json_encode($return);
                    die;
                }
                $gameAction = GameAction::model()->findByPk($game_action_id);
                if (!isset($gameAction)) {
                    $return['code'] = 4;
                    $return['message'] = "GameAction không tồn tại";
                    echo json_encode($return);
                    die;
                }
                $model = UserGameAction::model()->find('user_play_in_tournament_match_id = ' . $user_play_in_tournament_match_id . ' AND game_action_id =' . $game_action_id);
                if (empty($model)) {
                    $model = new UserGameAction();
                    $model->user_play_in_tournament_match_id = $user_play_in_tournament_match_id;
                    $model->game_action_id = $game_action_id;
//                    $newTime = date('Y-m-d H:i:s', $timestamp);
                    $model->timestamp = $timestamp;
                    if ($model->save()) {
//                    $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = 'Trùng cặp khóa ngoại';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateUserGameAction() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_play_in_tournament_match_id = Yii::app()->request->getParam("user_play_in_tournament_match_id");
            $game_action_id = Yii::app()->request->getParam("game_action_id");
            $model = UserGameAction::model()->find('user_play_in_tournament_match_id=' . $user_play_in_tournament_match_id . ' AND game_action_id=' . $game_action_id);
            if (!isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đúng';
            } else {
                $timestamp = Yii::app()->request->getParam('timestamp');
                if (isset($timestamp))
//                    $newTime = date('Y-m-d H:i:s', $timestamp);
                    $model->timestamp = $timestamp;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

}
