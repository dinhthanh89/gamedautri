<?php
Yii::app()->theme = Common::getTheme();

class SiteController extends Controller {

   /**
     * Declares class-based actions.
     */
    public $meta_title;
    public $meta_keyword = "phom, tien len mien nam, chan, game, gamedautri";
    public $meta_description = "game dautri, game Đấu trí, Tải game Đấu trí cờ bạc miễn phí về di động.";  
    protected  $app_id = '553026998169096';
    protected  $secret_key = 'b43e4d98cbc2633a2ce0acd13d71fdcf';
    
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }        

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {             
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'                  
        $this->seoPage();                                  
        if(Yii::app()->theme->name == 'mobile'){
            $model = Game::model()->findAll('feature =:feature ORDER BY `order`', array(':feature'=>0)); 
            $device = '';
            if(Common::isRunningiOS()){
                $device = "ios";      
                if(Common::isOperaMini())
                    Common::alert("Bạn nên sử dụng trình duyệt mặc định của điện thoại để được hỗ trợ tốt hơn.");
            }
            elseif(Common::isRunningAndroidOS()){                
                $device = "android";                
                if(Common::isOperaMini())
                    Common::alert("Bạn nên sử dụng trình duyệt mặc định của điện thoại để được hỗ trợ tốt hơn.");
            }else{                
                $device = "diff";
            }                       
            $this->render('index', array('model'=>$model, 'device'=>$device));
        }else{
            if (isset($_POST['user'])) {
                $user = new User('login');
                $user->attributes = $_POST['user'];
                if ($user->model()->login() === TRUE) {                    
                    echo 'Dang nhap thanh cong!';
                } else {
                    echo 'Dang nhap that bai!';
                }
            }       
            $games = Game::model()->findAll('feature != 0 LIMIT 0,5');            
            $configModel = Configuration::model()->find('name="recharge_sms"');
            if(isset($configModel)){
                $provider_sms = $configModel->value;
            }else{
                $provider_sms = "fibo";
            }
            $criteria = new CDbCriteria;
            $criteria->alias = "recharge_type";
            $criteria->join = "INNER JOIN recharge_method_provider on recharge_method_provider.id = recharge_type.recharge_method_provider_id";
            $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
            $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
            $criteria->addCondition('recharge_type.status = 1 AND recharge_method.code_identifier = "sms" AND topup_provider.code_identifier="'.$provider_sms.'"');            
            $criteria->group = "recharge_method_provider.id";
            $infoRecharge = RechargeType::model()->findAll($criteria);           
            //Enable slide
            Yii::app()->params['enableSlide'] = 1;
            // Render view
            $this->render('index', array('games'=>$games, 'infoRecharge'=>$infoRecharge));
        }                
    }     
    
    public function actionDetail(){
        if(Yii::app()->theme->name != 'mobile')
            $this->redirect (Yii::app()->homeUrl);
        $id = Yii::app()->request->getParam('id');
        if(!isset($id) || !is_numeric($id)){
            $this->redirect(Yii::app()->homeUrl);  
        }
        $model = Game::model()->findByPk($id); 
        if(!isset($model))
            $this->redirect(Yii::app()->homeUrl);  
        $device = '';
        if(Common::isRunningiOS()){
            $device = "ios";      
            if(Common::isOperaMini())
                Common::alert("Bạn nên sử dụng trình duyệt mặc định của điện thoại để được hỗ trợ tốt hơn.");
        }
        elseif(Common::isRunningAndroidOS()){                
            $device = "android";                
            if(Common::isOperaMini())
                Common::alert("Bạn nên sử dụng trình duyệt mặc định của điện thoại để được hỗ trợ tốt hơn.");
        }else{                
            $device = "diff";
        }                      
        $this->render('detail', array('model'=>$model, 'device'=>$device));
    }
    
    public function actionIntro(){             
        $this->seoPage();
        $post = Post::model()->findAll('category_id=1');        
        if(!isset($post))
            $this->redirect(Yii::app()->homeUrl);
        $this->render('intro', array('post'=>$post));
    }
    
    public function actionPolicy(){
        $post = Post::model()->find('category_id=2 AND status = 1');        
        if(!isset($post))
            $this->redirect(Yii::app()->homeUrl);        
        $this->render('policy',  array('post'=>$post));
    }
    
    public function seoPage(){
        Yii::app()->clientScript->registerMetaTag('gamedautri.com - Cổng game online trên Web và Mobile hàng đầu Việt Nam - Chắn - Tiến Lên Miền Nam - Phỏm', 'Description');
        Yii::app()->clientScript->registerMetaTag('phom, tien len mien nam, chan, game, gamedautri, dautri, cobac', 'Keywords');		
        Yii::app()->clientScript->registerMetaTag('Chơi chắn Online | Chơi phỏm Online | Chơi Tiến lên miền nam Online | Gamedautri.com',null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('images/Logo_v_2.png')),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag('Gamedautri.com - Cổng game online trên Web và Mobile hàng đầu Việt Nam - Chắn - Tiến Lên Miền Nam - Phỏm',null,null,array('property'=>'og:description'));               
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }        

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }
    
    public function actionLogin() {
        if (!Yii::app()->user->is_login()) {                                                            
            $model = new User('login');         
            $type = Yii::app()->request->getParam('type');
            $username = Yii::app()->request->getParam('username');
            $password = Yii::app()->request->getParam('password');
            if ($type == 'ajax') {
                $returnValue = array();
                if ($username != NULL && $password != NULL) {
                    if ($model->login($username, $password) == TRUE) {       
                        //$model->loginForum($username,$password);
                        $returnValue['code'] = 1;
                        $returnValue['message'] = 'Đăng nhập thành công, bạn có thể sử dụng chức năng người dùng';
                        $returnValue['redirect'] = Yii::app()->request->urlReferrer;                        
                    } else {
                        $returnValue['code'] = 0;
                        $returnValue['message'] = 'Tên đăng nhập hoặc mật khẩu không chính xác';
                    }
                } else {
                    $returnValue['code'] = -1;
                    $returnValue['message'] = 'Thông tin đăng nhập không đầy đủ';
                }
                echo json_encode($returnValue);
            } else {
                Yii::app()->request->redirect(Yii::app()->HomeUrl);
            }
        } else {
            Yii::app()->request->redirect(Yii::app()->HomeUrl);
        }
    }
        
    public function actionLoginMobile() {        
        if (!Yii::app()->user->is_login() && !empty($_POST)) {                                                            
            $model = new User('login');                     
            $username = Yii::app()->request->getParam('username');
            $password = Yii::app()->request->getParam('password');                            
            if ($username != NULL && $password != NULL) {
                if ($model->login($username, $password) == TRUE) {       
                    //$model->loginForum($username,$password);
                    Yii::app()->request->redirect(Yii::app()->HomeUrl);                                         
                } else {
                    Common::alert('Tên đăng nhập hoặc mật khẩu không chính xác',Yii::app()->request->urlReferrer);                          
                }
            } else {
                Common::alert('Thông tin đăng nhập không đầy đủ',Yii::app()->request->urlReferrer);                
            }            
        } else {
            $this->render('login');
        }
    }

    public function actionRegister() {                        
        if (Yii::app()->user->is_login()) {
            $this->redirect(Yii::app()->HomeUrl);
        }
        $partner = Yii::app()->request->getParam('partner');        
        $model = new User('register');
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-register-form') {
            $result = CActiveForm::validate($model);
            echo $result;
            Yii::app()->end();
        }
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];              
            if ($model->validate()) {                    
                $userModel = new User();
                $userModel->username = $model->username;
                $userModel->repeat_password = $model->repeat_password;
                $userModel->init_password = $model->init_password;                    
                if(!empty($model->email)){  
                    $userEmail = User::model()->find('email="'.$model->email.'"');
                    if(isset($userEmail))
                        Common::alert('Email của bạn đã được đăng kí', Yii::app()->homeUrl);
                    $userModel->email = $model->email;
                }
                if(!empty($model->mobile))
                    $userModel->mobile = $model->mobile;
                if ($userModel->register()) {        
                    $user = User::model()->findByPk($userModel->id);  
                    if(empty($partner) && isset(Yii::app()->request->cookies['partner']))
                        $partner = Yii::app()->request->cookies['partner']->value;
                    $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner));    
                    if(empty($partnerModel))
                        $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));    
                    $partner_id = $partnerModel->id;                    
                    $userPartnerModel = new UserPartner();
                    $userPartnerModel->user_id = $user->id;
                    $userPartnerModel->partner = $partner_id;
                    $userPartnerModel->save();
                    // login                        
                    $user->login();
                    //$user->loginForum($userModel->username,$userModel->init_password);
                    $this->redirect(Yii::app()->homeUrl);
                }
                else
                    Common::alert('Đăng kí thất bại', Yii::app()->homeUrl);                    
            }
            else {
                Common::alert('Đăng kí thất bại', Yii::app()->homeUrl);
            }
        }           
        $this->render('register', array('partner'=>$partner));
//        
    }

    public function actionForgotPassword() {
        if (!Yii::app()->user->is_login()) {
            $return = array();            
            $email = Yii::app()->request->getParam('email');
            if (isset($email)) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $return['code'] = -1;
                    $return['message'] = 'Không đúng định dạng email!';                      
                    echo json_encode($return);die;
                }
                $user = User::model()->find('email=:email', array(':email' => $email));
                if ($user == null) {
                    $return['code'] = -1;
                    $return['message'] = 'Email không tồn tại trong hệ thống, xin kiểm tra lại!';                    
                } else {
                    $userId = $user->id;
                    $ip = $_SERVER['REMOTE_ADDR'];
                    //$user->password = MyFunction::rand_string(8);
                    $accessToken = UserAccessToken::model()->createAccessToken($userId,$user->email, $ip);
                    $user->access_token = $accessToken;
                    $message = new YiiMailMessage;
                    //this points to the file test.php inside the view path
                    $message->view = "index";
                    $params = array('userinfo' => $user);
                    $message->subject = 'Lấy lại mật khẩu trên Gamedautri';
                    $message->setBody($params, 'text/html');
                    $message->addTo($email);
                    //$message->from = 'game@eposi.vn';
                    $message->setFrom(array('admin@esimo.vn' => 'Gamedautri.com'));
                    $result = Yii::app()->mail->send($message);
                    if ($result) {
                        $return['code'] = 1;
                        $return['redirect'] = Yii::app()->homeUrl;
                    } else {
                        $return['code'] = 0;
                        $return['message'] = 'Gửi mail không thành công!';
                    }
                }
            }else{
                $return['code'] = -2;
                $return['message'] = 'Bạn chưa nhập địa chỉ email!';
            }   
            echo json_encode($return);
        } else {
            $this->redirect(Yii::app()->homeUrl);
        }
    }
    
    public function actionResetPassword() {                
        $access_token = Yii::app()->request->getParam('access_token');
        $password = Yii::app()->request->getParam('password');
        $repassword = Yii::app()->request->getParam('repassword');        
        if ($access_token == null)
            Common::alert('Đường dẫn không hợp lệ', Yii::app()->homeUrl);
        else {            
            $user = UserAccessToken::model()->find('access_token=:access_token', array(':access_token' => $access_token));
            $duration = UserAccessToken::model()->checkForgotPasswordAccessToken($access_token);            
            if (!$user) {
                Common::alert('Đầu vào không chính xác', Yii::app()->homeUrl);
            } else {
                if ($duration) {
                    if($password != NULL){
                        if($password == $repassword){
                            $userModel = User::model()->findByPk($user->user_id);
                            if ($userModel->changePassword($password)) {
                                UserAccessToken::model()->deleteAll('access_token=:token', array(':token' => $access_token));
                                Common::alert('Mật khẩu mới được cập nhật', Yii::app()->homeUrl);
                            } else {
                                Common::alert('Mật khẩu mới chưa được cập nhật', Yii::app()->homeUrl);
                            }
                        }else{
                            Common::alert('Mật khẩu nhập lại không khớp', Yii::app()->request->urlReferrer);
                        }
                    }else{
                        $this->render('resetPassword', array('access_token' => $access_token));
                    }
                } else {
                    Common::alert('Link đã hết hạn', Yii::app()->homeUrl);
                }
            }
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogout() {          
        Yii::app()->user->logout();           
        unset(Yii::app()->request->cookies['accessToken']);   
       
//        $prefix_length = strlen('bb_');         
//	foreach ($_COOKIE AS $key => $val)
//	{
//            //setcookie($c_id, NULL, 1, "/", ".esimo.vn");
//            $index = strpos($key, 'bb_');                    
//            if ($index == 0 AND $index !== false)
//            {
//                $key = substr($key, $prefix_length);                
//                if (trim($key) == '')
//                {
//                        continue;
//                }
//                // vbsetcookie will add the cookie prefix
//                setcookie("bb_".$key, NULL, 1, "/", ".esimo.vn");
//            }                
//	}
//	
        //logout forum       
        //Common::includeBridgeVBB();
        //$forum = new vBulletin_Bridge();  
        //$forum->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionMessage() {
        $this->render('message');
    }

    public function actionFacebookLogin() {                    
        $config = array(
            'appId' => $this->app_id,
            'secret' => $this->secret_key,
        );
        $facebook = new Facebook($config);
        $facebook->destroySession();
        $user_id = $facebook->getUser();
//        var_dump($user_id);
        if ($user_id != NULL) {
            //die($facebook->getLogoutUrl(array('next' => Yii::app()->createAbsoluteUrl(Yii::app()->homeUrl))));
            // We have a user ID, so probably a logged in user.
            // If not, we'll get an exception, which we handle below.
            $user_profile = $facebook->api('/me', 'GET');
            // Check user exits
            if (User::model()->checkUserWithOpenId('facebook', $user_id)) {
                $user = User::model()->loginWithOpenId('facebook', $user_id);       
                
                $this->redirect(Yii::app()->homeUrl);
            } else {
//                if(isset($user_profile['email']&&$this->checkEmailExist($userProfile['email'])))
//                K::alert('Email(' . $user_profile['email'] . ') đã có trên hệ thống, xin đăng nhập bằng email của bạn');
                $session = new CHttpSession;
                $session->open();
                $session['openIdType'] = 'facebook';
                $session['profile'] = $user_profile;
//                var_dump($user_profile);
//                $this->render('register_openID');
                $this->render('register_openID');
                return;
            }
        } else {
// No user, print a link for the user to login
            $login_url = $facebook->getLoginUrl(array(
                'scope' => 'email,public_profile',
                'display' => 'page',
            ));
            $this->redirect($login_url);
        }
    }

    public function actionGoogleLogin() {
//$access_token = Yii::app()->request->getParam('access_token');
//        CVarDumper::dump($_GET);
//        echo'hehe';
//        echo "<script type='text/javascript'>alert(window.location)</script>";
        $endPoint = 'https://accounts.google.com/o/oauth2/auth';
        $client_id = '607813608493.apps.googleusercontent.com';
        $client_secret = 'gt6vITzxfEOrcCPCR0wKEpdg';
        $redirect_uri = Yii::app()->createAbsoluteUrl('site/GoogleLogin');
        $grant_type = 'authorization_code';
        $response_type = 'code';
        $approval_prompt = 'auto';
        $access_type = 'online';
        $scope = 'https://www.googleapis.com/auth/userinfo.profile + https://www.googleapis.com/auth/userinfo.email';
        if (isset($_GET['code'])) {
            $code = $_GET['code'];
            $data = array(
                'code' => $code,
                'grant_type' => $grant_type,
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri' => $redirect_uri,
//                'access_type' => $access_type,    
            );
            $url = 'https://accounts.google.com/o/oauth2/token';
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $jsonResult = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($jsonResult);

            $accessToken = $result->access_token;
            // GET user infomation:
            $apiUrl = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $accessToken;
            $apiCurl = curl_init($apiUrl);
            curl_setopt($apiCurl, CURLOPT_RETURNTRANSFER, TRUE);
            $jsonUserProfile = curl_exec($apiCurl);
            $userProfile = json_decode($jsonUserProfile, JSON_FORCE_OBJECT);
            if (User::model()->checkUserWithOpenId('google', $userProfile['id'])) {
                User::model()->loginWithOpenId('google', $userProfile['id']);
                $this->redirect(Yii::app()->homeUrl);
            } else {
                $session = new CHttpSession;
                $session->open();
                $session['openIdType'] = 'google';
                $session['profile'] = $userProfile;
                $this->render('register_openID');
                return;
            }
            return;
        }
        $loginUrl = $endPoint . '?scope=' . $scope . '&client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&response_type=' . $response_type;
        echo "<script type='text/javascript'>top.location.href = '$loginUrl';</script>";
    }

    public function actionRegisterOpenId() {
        $partner = Yii::app()->request->getParam('partner');
        if(empty($partner) && isset(Yii::app()->request->cookies['partner']))
            $partner = Yii::app()->request->cookies['partner']->value;
                    
        $model = new User('registerOpenId');
        if (isset($_POST['ajax'])) {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
//        $username = Yii::app()->request->getParam('username');
//        $password = Yii::app()->request->getParam('password');
            
            if($model->init_password != $model->repeat_password){
                Common::alert('Password gõ lại không khớp', Yii::app()->request->urlReferrer);
            }
            
            $session = new CHttpSession;
            $session->open();
            $user_profile = $session['profile'];
            $user = User::model()->registerWithOpenId($session['openIdType'], $user_profile, $model->username, $model->init_password);
            if (!$user) {                                                
                Common::alert('Tạo tài khoản thất bại, xin thử lại', Yii::app()->request->urlReferrer);
            }
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner));    
            if(empty($partnerModel))
                $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));                
            $partner_id = $partnerModel->id;                    
            $userPartnerModel = new UserPartner();
            $userPartnerModel->user_id = $user->id;
            $userPartnerModel->partner = $partner_id;
            $userPartnerModel->save();
            User::model()->loginWithOpenId($session['openIdType'], $user_profile['id']);
            
            $this->redirect(Yii::app()->homeUrl);
        }
    }

    public function actionMergeOpenId() {
        $session = new CHttpSession;
        $session->open();
        $type = $session['openIdType'];
        $id = $session['profile']['id'];
//        $username = Yii::app()->request->getParam('username');
//        $password = Yii::app()->request->getParam('password');
        $username = $_POST['username'];
        $password = $_POST['password'];
        if (User::model()->mergeOpenId($username, $password, $type, $id))
            Common::alert('Đăng nhập thành công!', Yii::app()->homeUrl);
        else
            Common::alert('Thông tin đăng nhập không đúng!', Yii::app()->homeUrl);
    }

    public function actionGetAccessToken() {
        $type = Yii::app()->request->getParam('type', NULL);
        $ip = $_SERVER['REMOTE_ADDR'];
        $userId = null;
        if ($type == 0) {
            $appId = 0;
            $username = Yii::app()->request->getParam('userName', NULL);
            $password = Yii::app()->request->getParam('password', NULL);
            $user = User::model()->getUser($username, $password);
            if ($user !== NULL)
                $userId = $user->id;
            $accessToken = UserAccessToken::model()->createAccessToken($userId, $appId, $ip);
        }
        if ($type == 1) {
            $fbAccessToken = Yii::app()->request->getParam('fbAccessToken');
            if ($fbAccessToken != NULL) {
                $facebook = new Facebook(array(
                    'appId' => '172402297158',
                    'secret' => '6b25a2fac602cf63a27268c82a9447a6',
                ));
                $facebook->setAccessToken($fbAccessToken);
                $me = $facebook->api('/me');
                if ($me != NULL) {
                    $fbId = $me['id'];
                    $userId = 1;
                }
            }
        }
        echo $accessToken;
        Yii::app()->end();
    }

    public function actionCheckAccessToken() {
        $accessToken = Yii::app()->request->getParam('accessToken', NULL);
        //echo $accessToken;
        if ($accessToken == NULL)
            return;
        if (UserAccessToken::model()->checkAccessToken($accessToken) == TRUE)
            echo 1;
        else
            echo '0';
    }
    
    public function actionSetDevice(){        
        $device = Yii::app()->request->getParam('device');
        if($device == "mobile" || $device == "pc")
            Common::setTheme($device);
        $this->redirect(Yii::app()->homeUrl);
    }       
    
    public function actionPartner(){
        $partner = Yii::app()->request->getParam('p');
        $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner));    
        if(!empty($partnerModel)){
            Yii::app()->request->cookies['partner'] = new CHttpCookie('partner', $partner);  
            $this->redirect(Yii::app()->homeUrl);
        }                    
    }
    
    public function actionPartnerUnset(){
        //$partner = Yii::app()->request->cookies['partner']->value;
        unset(Yii::app()->request->cookies['partner']);
    }

    public function actionDownload() {
        $model = Game::model()->findByPk(1);
        $this->render('download', array('model' => $model));
    }

    public function actionVote() {
        $point = Yii::app()->request->getParam('point', NULL);
        $gameId = Yii::app()->request->getParam('id', NULL);
        if ($point == NULL || $gameId == NULL)
            echo '0';
        else {
            $game = Game::model()->findByPk($gameId);
            $vote = $game->vote($point);
            if ($vote)
                echo $vote;
            else
                echo '0';
        }
    }

    //Download file from database
    public function actionDownloadFile() {
        $id = Yii::app()->request->getParam('file', NULL);
        $return = array();
        //Nếu ko có biến truyền vào
        if ($id == NULL || $id <= 0) {
            $return['code'] = -1;
            $return['message'] = 'Error input variable';
        } else {
            $file = file::model()->findByPk($id);
            if ($file == NULL)
                $return = array(
                    'code' => 0,
                    'message' => 'File does not exist'
                );
            else if (!$file->checkDownload())
                $return = array(
                    'code' => -2,
                    'message' => 'Can not download file, please check permission'
                );
            else {
                $file->download();
                echo '1';
                return;
            }
        }
        echo json_encode($return);
    }

    //Get image to display 
    public function actionGetImageFile() {
        $id = Yii::app()->request->getParam('file', NULL);
        if ($id != NULL)
            $file = File::model()->findByPk($id);
        else
            return;
        if (!$file->getFile('image'))
            return;
    }

    public function actionPaymentSuccess() {
        require_once('protected/components/nganluong/nusoap.php');
        require_once('protected/components/nganluong/NL_MicroCheckout.php');
        require_once('protected/components/nganluong/config.php');
        //khai bao
        $obj = new NL_MicroCheckout(NL_MERCHANT_ID, NL_MERCHANT_PASS, URL_WS);

        if ($obj->checkReturnUrlAuto()) {
            $inputs = array(
                'token' => $obj->getTokenCode(), //$token_code,
            );
            $result = $obj->getExpressCheckout($inputs);
            if ($result != false) {
                if ($result['result_code'] != '00') {
                    die('Mã lỗi ' . $result['result_code'] . ' (' . $result['result_description'] . ') ');
                }
            } else {
                die('Lỗi kết nối tới cổng thanh toán Ngân Lượng');
            }
        } else {
            die('Tham số truyền không đúng');
        }

        if (isset($result) && !empty($result)) {
            if ($result['result_code'] == '00') {
                $user_id = Yii::app()->user->getUser()->id;
                $config = Configuration::model()->findByPk(14);
                $model = new Transaction();
                $model->user_id = $user_id;
                $model->type = 2;
                if ($config != NULL || $config->value != 0)
                    $model->value = floor($result['amount'] / $config->value);
                else
                    $model->value = $result['amount'] / 1000;
                $model->game_log_id = -1;                

                if ($model->save()) {
                    $user = User::model()->findByPk($user_id);
                    $chip = $user->chip;
                    $user->chip = $chip + $model->value;
                    if ($user->save()) {
                        $electroServer = new ElectroServerBridge();
                        $return = $electroServer->sendUserInfo($user_id, 'chip', $user->chip);
                        if ($return) {
                            //redirect('');
                        }
                    } else {
                        
                    }
                } else {
                    
                }
            } else {
                echo $result['result_description'];
            }
        }
    }         
     
}


