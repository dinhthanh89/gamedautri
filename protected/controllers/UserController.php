<?php
Yii::app()->theme = Common::getTheme();

class UserController extends Controller {
    
    protected $app_id='1401311996778175';
    protected $app_secret='a80d998dfc89ac8289b5f0a06ff6f0bd';
    protected $my_url = "http://test.esimo.vn/user/loginFacebook";
    protected $uid = '';
    
    public function actionExchange(){
         $currentUser = Yii::app()->user->getUser();
         if(empty($currentUser)){
             $this->redirect(Yii::app()->createUser(''));
         }
         $criteria = new CDbCriteria();
         $criteria->alias = 'product';
         $criteria->order = 'product.order ASC';
         $criteria->addCondition('product.category_product_id = 1 AND product.status = 1');         
         $product = Product::model()->findAll($criteria);
         $gold = Yii::app()->request->getParam('gold');         
         if(isset($_POST['exchangeSubmit'])){             
             if($currentUser->gold < $gold){
                 Common::alert('Số gold bạn đổi lớn hơn số gold bạn hiện có', Yii::app()->request->urlReferrer);
             }             
             if($currentUser->save()){
                $electroServer = new ElectroServerBridge();
                $returnElectroGold = $electroServer->sendUserInfo($currentUser->id, 'gold', -($gold));
                $chipReceiver = $currentUser->convertGoldByChip($gold); 
                $returnElectro = $electroServer->sendUserInfo($currentUser->id, 'chip', $chipReceiver);                
                Common::alert('Bạn đã đổi gold thành công', Yii::app()->request->urlReferrer);
             }else{
                 Common::alert('Đổi gold thất bại, vui lòng thử lại', Yii::app()->request->urlReferrer);
             }
         }
         $this->render('exchange',array('user'=>$currentUser, 'product'=>$product));
    }   

    public function actionIndex() {
        $type = Yii::app()->request->getParam('type', NULL);
        $menuCotent = Yii::app()->request->getParam('content', NULL);
        $currentUser = Yii::app()->user->getUser();
        if ($type == 'ajax') {
            if ($menuCotent == NULL) {
                echo '0';
                return;
            }
            $this->layout = 'blank';
            switch ($menuCotent) {
                case 2: {
                        $dataUserChat = new CActiveDataProvider('Message', array(
                            'criteria' => array(
                                'condition' => 'receiver= 32', //.Yii::app()->user->getUser()->id,                        				
                                'order' => 'time_sent DESC',
                                'group' => 'sender',
                            ),
                        ));
                        $dataUserFriend = new CActiveDataProvider('Buddies', array(
                            'criteria' => array(
                                'condition' => 'user2= 32 and user2_accept = 0', //.Yii::app()->user->getUser()->id,                        				                                                                
                            ),
                        ));
                        $this->render('message', array(
                            'dataUserChat' => $dataUserChat,
                            'dataUserFriend' => $dataUserFriend,
                        ));
                        return;
                    }
                case 6: {
                        $model = new Transaction('search');
                        if (isset($_GET['Transaction'])) {
                            $model->attributes = $_GET['Transation'];
                        }
                        //$model->attributes['user_id'] = $currentUser->id;
                        $this->render('history', array('model' => $model));
                        return;
                    }
                case 7: {
                        $this->render('setting');
                        return;
                    }
                default : {
                        return;
                    }
            }
        } else if ($type == 'friend') {
            
        }
        $this->render('index');
    }   

    public function actionChangeAvatar() {
        $fileExtAllow = array('jpg', 'jpeg', 'png');
        $return = array();
        $avatar = $_FILES['avatar'];
        $fileName = $avatar['name'];
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
        // If file extension is not allowed
        if (!in_array($fileExt, $fileExtAllow)) {
            $return['code'] = -1;
            $return['message'] = 'Định dạng file không phù hợp, xin hãy chọn lại';
            echo json_encode($return);
            return;
        }
        $folder = Configuration::model()->getConfig('user_folder');
        $result = File::model()->newUploadedFile($avatar, $folder);
        if ($result['code'] == 1) {
            //Save this avatar for user
            Yii::app()->user->getUser()->avatar = $result['file']->id;
            Yii::app()->user->getUser()->save();
            //Resize image
            if (Configuration::model()->getConfig('resize_uploaded_avatar') == 1)
                $result['file']->resizeImage(NULL, NULL, 205, 205);
            $return['code'] = 1;
            $return['message'] = 'Đổi avatar thành công';
            $return['imageLink'] = $result['file']->getDirectImageLink();
            echo json_encode($return);
            return;
        }
        if ($result['code'] == -1) {
            $return['code'] = -1;
            $return['message'] = 'Định dạng file không phù hợp, xin hãy chọn lại';
            echo json_encode($return);
            return;
        } else {
            $return['code'] = -2;
            $return['message'] = 'Không thể cập nhật avatar, xin thử lại sau';
            return json_encode($result);
        }
    }

    public function actionChangePassword() {
        Common::includeBridgeVBB();                        
        $currentUser = Yii::app()->user->getUser();
        $oldPassword = Yii::app()->request->getParam('oldPassword', NULL);
        $newPassword = Yii::app()->request->getParam('newPassword', NULL);
        $repeatPassword = Yii::app()->request->getParam('repeatPassword', NULL);
        $return = array();
        if (!$oldPassword || !$newPassword || !$repeatPassword) {
            $return['code'] = -1;
            $return['message'] = 'Đầu vào không chính xác';
        } else if ($newPassword != $repeatPassword) {
            $return['code'] = -2;
            $return['message'] = 'Đầu vào không chính xác';
        } else if (!$currentUser->checkPassword($oldPassword)) {
            $return['code'] = -3;
            $return['message'] = 'Mật khẩu nhập vào không chính xác';
        } else if ($currentUser->changePassword($newPassword)) {            
            $return['code'] = 1;
            $return['message'] = 'Đổi mật khẩu thành công';
        } else {
            $return['code'] = 0;
            $return['message'] = 'Đổi mật khẩu thất bại';
        }
        echo json_encode($return);
    }

    public function actionChangeInformation() {
        $currentUser = Yii::app()->user->getUser();
        // public information
        $lastname = Yii::app()->request->getParam('lastname', NULL);
        $middlename = Yii::app()->request->getParam('middlename', NULL);
        $firstname = Yii::app()->request->getParam('firstname', NULL);
        $birthday = Yii::app()->request->getParam('birthday', NULL);
        $gender = Yii::app()->request->getParam('gender', NULL);

        //private information
        $email = Yii::app()->request->getParam('email', NULL);
        $cmt = Yii::app()->request->getParam('cmt', NULL);
        $phone = Yii::app()->request->getParam('phone', NULL);
        $address = Yii::app()->request->getParam('address', NULL);

        if ($gender && $gender != 'male' && $gender != 'female')
            $gender = 'male';
        $info = array('lastname' => $lastname,
            'middlename' => $middlename,
            'firstname' => $firstname,
            'birthday' => $birthday,
            'gender' => $gender,
            'email' => $email,
            'cmt' => $cmt,
            'phone' => $phone,
            'address' => $address
        );
        $return = array();
        if ($currentUser->changeInformation($info)) {
            $return['code'] = 1;
            $return['message'] = 'Cập nhật thông tin các nhân thành công';
        } else {
            $return['code'] = 0;
            $return['message'] = 'thất bại';
        }
        echo json_encode($return);
    }

    public function actionConfirmFriend() {
        $id = Yii::app()->request->getParam('id', NULL);
        $status = Yii::app()->request->getParam('status', NULL);
        if (isset($id)) {
            $condition = 'user2_accept=' . Buddies::STATUS_WAIT;
            $model = Buddies::model()->findByPk($id);
            $model->user2_accept = $status;
            $return = array();
            if ($model->save()) {
                $return['code'] = 1;
                $return['message'] = 'Cập nhật thông tin các nhân thành công';
            } else {
                $return['code'] = 0;
                $return['message'] = 'Cập nhật thông tin các nhân thất bại';
            }
        }
        echo json_encode($return);
    }

    public function actionLoadComment() {
        $sender = 30; //Yii::app()->request->getParam('sender', NULL);
        $receiver = 32; //Yii::app()->request->getParam('receiver', NULL);        
        $dataComment = new CActiveDataProvider('Message', array(
            'criteria' => array(
                'condition' => 'sender= ' . $sender . ' and receiver = ' . $receiver, //.Yii::app()->user->getUser()->id,                        				                                                                
                'order' => 'time_sent DESC',
            ),
        ));
        $this->render('_message_comment', array(
            'dataComment' => $dataComment,
        ));
    }

    public function actionNewPassword() {
        $email = Yii::app()->request->getParam('email', NULL);
        $email = urldecode($email);
        $newpass = Yii::app()->request->getParam('newpass', NULL);
        $newpass = urldecode($newpass);
        $access_token = Yii::app()->request->getParam('access_token', NULL);
        $user = UserAccessToken::model()->find('access_token=:access_token', array(':access_token' => $access_token));
        $duration = UserAccessToken::model()->checkForgotPasswordAccessToken($access_token);
        $return = array();
        if (!$user || !$email || !$newpass || $user->user->email != $email) {
            Common::alert('Đầu vào không chính xác', Yii::app()->homeUrl);
        } else {
            if ($duration) {
                if ($user->user->changePassword($newpass)) {
                    Common::alert('Mật khẩu mới được cập nhật', Yii::app()->homeUrl);
                } else {
                    Common::alert('Mật khẩu mới chưa được cập nhật', Yii::app()->homeUrl);
                }
            } else {
                Common::alert('Link đã hết hạn', Yii::app()->homeUrl);
            }
        }

//        echo json_encode($return);
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {
            if (isset(Yii::app()->user->getUser()->id)) {
                $this->_model = User::model()->findByPk(Yii::app()->user->getUser()->id);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $this->_model;
    }   
    
    public function actionLoginFacebook(){   
        $session = new CHttpSession;
        $session->open();
        if(isset($session['accessTokenFb']))
            $this->redirect(Yii::app()->createUrl('chatFacebook'));
        else{            
            $access_token = $this->get_access_token($this->app_id,$this->app_secret,$this->my_url);  
            if(isset($access_token)){
                $session['accessTokenFb'] = $access_token;
            }else{
                $this->redirect(Yii::app()->homeUrl());
            }
        }
        
        $this->redirect(Yii::app()->createUrl('user/chatFacebook'));
    }
    
    public function actionChatFacebook(){
        $session = new CHttpSession;
        $session->open();
        if(!isset($session['accessTokenFb']))
            $this->redirect(Yii::app()->createUrl('user/loginFacebook'));
        $options = array(
           'uid' => $this->uid,
           'app_id' => $this->app_id,
           'server' => 'chat.facebook.com',
          );               
    }
    
    //Gets access_token with xmpp_login permission
    function get_access_token($app_id, $app_secret, $my_url){ 

      $code = Yii::app()->request->getParam('code');

      if(empty($code)) {
        $dialog_url = "https://www.facebook.com/dialog/oauth?scope=xmpp_login".
         "&client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url) ;
        echo("<script>top.location.href='" . $dialog_url . "'</script>");
      }
       $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
        . $app_id . "&redirect_uri=" . urlencode($my_url) 
        . "&client_secret=" . $app_secret 
        . "&code=" . $code;       
       $access_token = @file_get_contents($token_url);
        parse_str($access_token, $output);

        return($output['access_token']);
    }
}