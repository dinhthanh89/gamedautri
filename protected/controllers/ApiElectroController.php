<?php

class ApiElectroController extends Controller{
    public $layout = 'blank';
    
    public function actionIndex(){
        
    }
    
    public function actionPromotionLogin(){  
        return;
        $user_id = Yii::app()->request->getParam('user_id');                
        $user = User::model()->findByPk($user_id);                        
        if(isset($user)){
            $promotionActive = $user->getPromotionActive(5);                 
            if($promotionActive){
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $criteria->addCondition('user_id = '.$user_id.' AND timestamp <= "'.$promotionActive->end_time.'" AND timestamp >= "'.$promotionActive->start_time.'" AND game_action_id = 38');
                $userGameAction = UserGameAction::model()->findAll($criteria);                   
                if(isset($userGameAction) && count($userGameAction) == 1){                    
                    $user_game_action_id = $userGameAction[0]->id;                       
                    $bonus = $user->bonusChipsByLogin($user_game_action_id);                    
                    if($bonus != 0){
                        $electroServer = new ElectroServerBridge();
                        $result = $electroServer->sendUserInfo($user->id, 'chip', $bonus);
                        MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($bonus, 0, ',', '.')." chip từ hệ thống Esimo !",1);            
                    }
                }else{
                    
                }
            }
        }else{
            return false;
        }
    }        
}
?>
