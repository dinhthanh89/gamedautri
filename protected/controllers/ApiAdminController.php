 <?php

class ApiAdminController extends Controller {

    public function actionIndex() {
        
    }

    public function actionGetGameAction() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = GameAction::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại GameAction nào";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['name'] = $model->name;
                $return['parent_id'] = $model->parent_id;
                $return['point'] = $model->point;
                $return['description'] = $model->description;
                $return['game_id'] = $model->game_id;
                $return['code_identifier'] = $model->code_identifier;
            }
        } else {
            $model = GameAction::model()->findAll();
            if (empty($model)) {    
                $return['code'] = 0;
                $return['message'] = "Không có GameAction nào!";
                $return['items'] = array();
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['parent_id'] = $items->parent_id;
                    $return['items'][$index]['point'] = $items->point;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['game_id'] = $items->game_id;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateGameAction() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $parent_id = Yii::app()->request->getParam('parent_id');
            $description = Yii::app()->request->getParam('description');
            $point = Yii::app()->request->getParam('point');
            $game_id = Yii::app()->request->getParam('game_id');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                if (isset($game_id) && strlen($game_id) > 0) {
                    $game = Game::model()->findByPk($game_id);
                    if (!isset($game)) {
                        $return['code'] = 3;
                        $return['message'] = "Game không tồn tại";
                        echo json_encode($return);
                        die;
                    }
                    $model->game_id = $game_id;
                }
                $model = new GameAction();
                $model->name = $name;
                $model->parent_id = $parent_id;
                if (!isset($point) || !is_numeric($point))
                    $point = 0;
                $model->point = $point;
                $model->description = $description;

                $model->code_identifier = $code_identifier;                
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateGameAction() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = GameAction::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $parent_id = Yii::app()->request->getParam('parent_id');
                if (isset($parent_id) && strlen($parent_id) > 0) {
                    $gameActionCategory = GameActionCategory::model()->findByPk($parent_id);
                    if (!isset($gameActionCategory)) {
                        $return['code'] = 3;
                        $return['message'] = "GameCategory không tồn tại";
                        echo json_encode($return);
                        die;
                    };
                    $model->parent_id = $parent_id;
                }
                if (isset($game_id) && strlen($game_id) > 0) {
                    $game = Game::model()->findByPk($game_id);
                    if (!isset($game)) {
                        $return['code'] = 3;
                        $return['message'] = "Game không tồn tại";
                        echo json_encode($return);
                        die;
                    }
                    $model->game_id = $game_id;
                };
                $description = Yii::app()->request->getParam('description');
                $point = Yii::app()->request->getParam('point');
                $code_identifier = Yii::app()->request->getParam('code_identifier');
                if (isset($point))
                    $model->point = $point;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteGameAction() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = GameAction::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetGameActionCategory() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $model = GameActionCategory::model()->findByPk($id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại GameActionCategory";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['name'] = $model->name;
                $return['parent_id'] = $model->parent_id;
                $return['description'] = $model->description;
            }
        } else {
            $model = GameActionCategory::model()->findAll();
            if (empty($model)) {
                $return['code'] = 1;
                $return['message'] = "Không có GameActionCategory nào";
                $return['items'] = array();
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['parent_id'] = $items->parent_id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['description'] = $items->description;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateGameActionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
//        echo($access_token);

        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $parent_id = Yii::app()->request->getParam('parent_id');
            $description = Yii::app()->request->getParam('description');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new GameActionCategory();
                if (isset($parent_id)) {
                    if ($parent_id > 0) {
                        $gameCategory = GameActionCategory::model()->findByPk($parent_id);
                        if (!isset($gameCategory)) {
                            $return['code'] = 3;
                            $return['message'] = "parent_id không tồn tại";
                            echo json_encode($return);
                            die;
                        }
                        $model->parent_id = $parent_id;
                    } else {
                        $model->parent_id = null;
                    }
                }

                $model->name = $name;
                $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateGameActionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = GameActionCategory::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $parent_id = Yii::app()->request->getParam('parent_id');
                if (isset($parent_id)) {
                    if ($parent_id > 0) {
                        $gameCategory = GameActionCategory::model()->findByPk($parent_id);
                        if (!isset($gameCategory)) {
                            $return['code'] = 3;
                            $return['message'] = "parent_id không tồn tại";
                            echo json_encode($return);
                            die;
                        }
                        $model->parent_id = $parent_id;
                    } else {
                        $model->parent_id = null;
                    }
                }
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    K::dump($model->errors);
                    die;
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteGameActionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = GameActionCategory::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetGame() {
        $return = array();
        $game_id = Yii::app()->request->getParam('id');
        if (isset($game_id)) {
            $model = Game::model()->findByPk($game_id);
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['name'] = $model->name;
                $return['category_id'] = $model->category_id;
                $return['description'] = $model->description;
                $return['rule'] = $model->rule;
                $return['vote'] = $model->vote;
                $return['status'] = $model->status;
                $return['image'] = $model->image;
                $return['code_identifier'] = $model->code_identifier;
            }
            echo json_encode($return);
            die;
        } else {
            $model = Game::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['category_id'] = $items->category_id;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['rule'] = $items->rule;
                    $return['items'][$index]['vote'] = $items->vote;
                    $return['items'][$index]['status'] = $items->status;
                    $return['items'][$index]['image'] = $items->image;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetRechargeType() {
        $return = array();
        $model = RechargeType::model()->findAll();
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['name'] = $items->name;
                $return['items'][$index]['description'] = $items->description;
                $return['items'][$index]['ratio'] = $items->ratio;
                $return['items'][$index]['value'] = $items->value;
                $return['items'][$index]['provider'] = $items->provider;
                $return['items'][$index]['type'] = $items->type;
                $return['items'][$index]['code'] = $items->code;
            }
            }
        echo json_encode($return);
    }

    public function actionCreateGame() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $category_id = Yii::app()->request->getParam('category_id');
            $description = Yii::app()->request->getParam('description', NULL);
            $rule = Yii::app()->request->getParam('rule', NULL);
            $vote = Yii::app()->request->getParam('vote', NULL);
            $status = Yii::app()->request->getParam('status');
            $code_identifier = Yii::app()->request->getParam('code_identifier', NULL);
            if (!isset($name) || !isset($category_id) || !isset($status) || !is_numeric($category_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $category = Category::model()->findByPk($category_id);
                if (!isset($category)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại Category";
                    echo json_encode($return);
                    die;
                }
                $model = new Game();
                if (isset($name))
                    $model->name = $name;
                $model->category_id = 2;
                if (isset($description))
                    $model->description = $description;
                if (isset($rule))
                    $model->rule = $rule;
                if (isset($vote))
                    $model->vote = $vote;
                $model->status = $status;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;

                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/games/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                } else {
                    $return['code'] = 5;
                    $return['message'] = "Không tồn tại file";
                    echo json_encode($return);
                    die;
                }

                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateGame() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $category_id = Yii::app()->request->getParam('category_id');
            $description = Yii::app()->request->getParam('description');
            $vote = Yii::app()->request->getParam('vote');
            $status = Yii::app()->request->getParam('status');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                if (isset($category_id)) {
                    $category = Category::model()->findByPk($category_id);
                    if (!isset($category)) {
                        $return['code'] = 4;
                        $return['message'] = "Không tồn tại Category";
                        echo json_encode($return);
                        die;
                    }
                }
                $model = Game::model()->findByPk($id);
                if (isset($name))
                    $model->name = $name;
                if (isset($category_id))
                    $model->category_id = $category_id;
                if (isset($description))
                    $model->description = $description;
                if (isset($rule))
                    $model->rule = $rule;
                if (isset($vote))
                    $model->vote = $vote;
                $model->status = $status;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/games/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteGame() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Game::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Game";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetPartner() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $limit = Yii::app()->request->getParam('limit');
        $page = Yii::app()->request->getParam('page');
        if (isset($id)) {
            $model = Partner::model()->findByPk($id);
            if (isset($model)) {
                $return['code'] = 0;
                $return['message'] = 'Thành công';
                $return['id'] = $model->id;
                $return['code_identifier'] = $model->code_identifier;
                $return['name'] = $model->name;
                $return['address'] = $model->address;
                $return['tel'] = $model->tel;
                $return['mobile'] = $model->mobile;
                $return['email'] = $model->email;
                $return['website'] = $model->website;
                $return['ratio'] = $model->ratio;
            } else {
                $return['code'] = 1;
                $return['message'] = 'Không tồn tại kết quả';
            }
        } else {
            if (isset($limit) && isset($page)) {
                $criteria = new CDbCriteria;
                $start_id = $page * $limit;
                $criteria->offset = $start_id;
                $criteria->limit = $limit;
                $model = Partner::model()->findAll($criteria);
            } else {
                $model = Partner::model()->findAll();
            }
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $item) {
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['code_identifier'] = $item->code_identifier;
                    $return['items'][$index]['name'] = $item->name;
                    $return['items'][$index]['address'] = $item->address;
                    $return['items'][$index]['tel'] = $item->tel;
                    $return['items'][$index]['mobile'] = $item->mobile;
                    $return['items'][$index]['email'] = $item->email;
                    $return['items'][$index]['website'] = $item->website;
                    $return['items'][$index]['ratio'] = $item->ratio;
                }
            }
        }

        echo json_encode($return);
    }

    public function actionCreatePartner() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $address = Yii::app()->request->getParam('address');
            $tel = Yii::app()->request->getParam('tel');
            $mobile = Yii::app()->request->getParam('mobile');
            $email = Yii::app()->request->getParam('email');
            $website = Yii::app()->request->getParam('website');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $ratio = Yii::app()->request->getParam('ratio');
            if (!isset($name) || !isset($code_identifier) || !isset($ratio)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new Partner();
                $model->name = $name;
                $model->code_identifier = $code_identifier;
                $model->ratio = $ratio;
                if (isset($tel))
                    $model->tel = $tel;
                if (isset($mobile))
                    $model->mobile = $mobile;
                if (isset($address))
                    $model->address = $address;
                if (isset($email))
                    $model->email = $email;
                if (isset($website))
                    $model->website = $website;

                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePartner() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $address = Yii::app()->request->getParam('address');
            $tel = Yii::app()->request->getParam('tel');
            $mobile = Yii::app()->request->getParam('mobile');
            $email = Yii::app()->request->getParam('email');
            $website = Yii::app()->request->getParam('website');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $ratio = Yii::app()->request->getParam('ratio');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Partner::model()->findByPk($id);
                if (isset($name))
                    $model->name = $name;
                if (isset($address))
                    $model->address = $address;
                if (isset($tel))
                    $model->tel = $tel;
                if (isset($mobile))
                    $model->mobile = $mobile;
                if (isset($email))
                    $model->email = $email;
                if (isset($website))
                    $model->website = $website;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if (isset($ratio))
                    $model->ratio = $ratio;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePartner() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Partner::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Partner";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserPartner() {
        $return = array();
        $partner_id = Yii::app()->request->getParam('partner_id');
        $user_id = Yii::app()->request->getParam('user_id');
        if (isset($partner_id) && isset($user_id) && is_numeric($partner_id) && is_numeric($user_id)) {
            $model = UserPartner::model()->findAll('partner_id = ' . $partner_id . ' AND user_id=' . $user_id);
        } elseif (isset($partner_id) && is_numeric($partner_id)) {
            $model = UserPartner::model()->findAll('partner_id = ' . $partner_id);
        } elseif (isset($user_id) && is_numeric($user_id)) {
            $model = UserPartner::model()->findAll('user_id = ' . $user_id);
        } else {
            $model = UserPartner::model()->findAll();
        }
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['user_id'] = $item->user_id;
                $return['items'][$index]['partner_id'] = $item->partner_id;
            }
        }

        echo json_encode($return);
    }

    public function actionCreateUserPartner() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_id = Yii::app()->request->getParam('user_id');
            $partner_id = Yii::app()->request->getParam('partner_id');
            if (!isset($user_id) || !isset($partner_id) || !is_numeric($user_id) || !is_numeric($partner_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $user = User::model()->findByPk($user_id);
                if (!isset($user)) {
                    $return['code'] = 3;
                    $return['message'] = "Khong ton tai user";
                    echo json_encode($return);
                    die;
                }
                $partner = Partner::model()->findByPk($partner_id);
                if (!isset($partner)) {
                    $return['code'] = 4;
                    $return['message'] = "Khong ton tai partner";
                    echo json_encode($return);
                    die;
                }
                $model = new UserPartner();
                $model->user_id = $user_id;
                $model->partner_id = $partner_id;
                if ($model->save()) {
                    $return['user_id'] = $model->user_id;
                    $return['partner_id'] = $model->partner_id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteUserPartner() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user_id = Yii::app()->request->getParam('user_id');
            $partner_id = Yii::app()->request->getParam('partner_id');
            if (!isset($user_id) || !is_numeric($user_id) || !isset($partner_id) || !is_numeric($partner_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = UserPartner::model()->find('user_id=' . $user_id . ' AND partner_id=' . $partner_id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại User Partner";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetPlatform() {
        $return = array();
        $name = Yii::app()->request->getParam('name');
        if (isset($name)) {
            $model = Platform::model()->find('name = "' . $name . '"');
            if (isset($model)) {
                $return['code'] = 0;
                $return['message'] = 'Thành công';
                $return['id'] = $model->id;
                $return['name'] = $model->name;
                $return['code_identifier'] = $model->code_identifier;
                $return['description'] = $model->description;
                $return['screen_width'] = $model->screen_width;
                $return['screen_height'] = $model->screen_height;
                $return['version'] = $model->version;
            } else {
                $return['code'] = 1;
                $return['message'] = 'Không tồn tại kết quả';
            }
        } else {
            $model = Platform::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $item) {
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['name'] = $item->name;
                    $return['items'][$index]['code_identifier'] = $item->code_identifier;
                    $return['items'][$index]['description'] = $item->description;
                    $return['items'][$index]['screen_width'] = $item->screen_width;
                    $return['items'][$index]['screen_height'] = $item->screen_height;
                    $return['items'][$index]['version'] = $item->version;
                }
            }
        }

        echo json_encode($return);
    }

    public function actionCreatePlatform() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $description = Yii::app()->request->getParam('description');
            $screen_width = Yii::app()->request->getParam('screen_width');
            $screen_height = Yii::app()->request->getParam('screen_height');
            $version = Yii::app()->request->getParam('version');
            if (!isset($name) || !isset($code_identifier)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new Platform();
                $model->name = $name;
                $model->code_identifier = $code_identifier;
                if (isset($description))
                    $model->description = $description;
                if (isset($screen_width))
                    $model->screen_width = $screen_width;
                if (isset($screen_height))
                    $model->screen_height = $screen_height;
                if (isset($version))
                    $model->version = $version;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePlatform() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $description = Yii::app()->request->getParam('description');
            $screen_width = Yii::app()->request->getParam('screen_width');
            $screen_height = Yii::app()->request->getParam('screen_height');
            $version = Yii::app()->request->getParam('version');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Platform::model()->findByPk($id);
                if (isset($name))
                    $model->name = $name;
                if (isset($code_identifier))
                    $model->code_identifier = $code_identifier;
                if (isset($description))
                    $model->description = $description;
                if (isset($screen_width))
                    $model->screen_width = $screen_width;
                if (isset($screen_height))
                    $model->screen_height = $screen_height;
                if (isset($version))
                    $model->version = $version;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePlatform() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $gamePlatformModel = GamePlatform::model()->find('platform_id=' . $id);
                if (isset($gamePlatformModel)) {
                    $return['code'] = 4;
                    $return['message'] = "Phải xóa game platform trước";
                    echo json_encode($return);
                    die;
                }
                $model = Platform::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Platform";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetGamePlatform() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $game_id = Yii::app()->request->getParam('game_id');
        $platform_id = Yii::app()->request->getParam('platform_id');
        if (isset($id)) {
            $model = GamePlatform::model()->findByPk($id);
            if (isset($model)) {
                $return['code'] = 0;
                $return['message'] = 'Thành công';
                $return['id'] = $model->id;
                $return['game_id'] = $model->game_id;
                $return['platform_id'] = $model->platform_id;
                $return['market_url'] = $model->market_url;
                $return['bundle_id'] = $model->bundle_id;
            } else {
                $return['code'] = 1;
                $return['message'] = 'Không tồn tại kết quả';
            }
        } else {
            if (isset($game_id) && isset($platform_id) && is_numeric($game_id) && is_numeric($platform_id))
                $model = GamePlatform::model()->findAll('game_id=' . $game_id . ' AND platform_id=' . $platform_id);
            elseif (isset($platform_id) && is_numeric($platform_id))
                $model = GamePlatform::model()->findAll('platform_id=' . $platform_id);
            elseif (isset($game_id) && is_numeric($game_id))
                $model = GamePlatform::model()->findAll('game_id=' . $game_id);
            else
                $model = GamePlatform::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $item) {
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['game_id'] = $item->game_id;
                    $return['items'][$index]['platform_id'] = $item->platform_id;
                    $return['items'][$index]['market_url'] = $item->market_url;
                    $return['items'][$index]['bundle_id'] = $item->bundle_id;
                }
            }
        }

        echo json_encode($return);
    }

    public function actionCreateGamePlatform() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $game_id = Yii::app()->request->getParam('game_id');
            $platform_id = Yii::app()->request->getParam('platform_id');
            $market_url = Yii::app()->request->getParam('market_url');
            $bundle_id = Yii::app()->request->getParam('bundle_id');
            if (!isset($game_id) || !isset($platform_id) || !is_numeric($game_id) || !is_numeric($platform_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $gameModel = Game::model()->findByPk($game_id);
                if (!isset($gameModel)) {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại game_id";
                    echo json_encode($return);
                    die;
                }
                $platformModel = Platform::model()->findByPk($platform_id);
                if (!isset($platformModel)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại platform_id";
                    echo json_encode($return);
                    die;
                }
                $model = new GamePlatform();
                $model->game_id = $game_id;
                $model->platform_id = $platform_id;
                if (isset($market_url))
                    $model->market_url = $market_url;
                if (isset($bundle_id))
                    $model->bundle_id = $bundle_id;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateGamePlatform() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $game_id = Yii::app()->request->getParam('game_id');
            $platform_id = Yii::app()->request->getParam('platform_id');
            $market_url = Yii::app()->request->getParam('market_url');
            $bundle_id = Yii::app()->request->getParam('bundle_id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = GamePlatform::model()->findByPk($id);
                if (isset($game_id)) {
                    $gameModel = Game::model()->findByPk($game_id);
                    if (!isset($gameModel)) {
                        $return['code'] = 3;
                        $return['message'] = 'Không tồn tại game_id';
                        echo json_encode($return);
                        die;
                    }
                    $model->game_id = $game_id;
                }
                if (isset($platform_id)) {
                    $platformModel = Platform::model()->findByPk($platform_id);
                    if (!isset($platformModel)) {
                        $return['code'] = 4;
                        $return['message'] = 'Không tồn tại platform_id';
                        echo json_encode($return);
                        die;
                    }
                    $model->platform_id = $platform_id;
                }

                if (isset($market_url))
                    $model->market_url = $market_url;
                if (isset($bundle_id))
                    $model->bundle_id = $bundle_id;

                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetGameVersion() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $game_platform_id = Yii::app()->request->getParam('game_platform_id');
        $partner_id = Yii::app()->request->getParam('partner_id');
        if (isset($id) && is_numeric($id)) {
            $model = GameVersion::model()->findByPk($id);
            if (isset($model)) {
                $return['code'] = 0;
                $return['message'] = 'Thành công';
                $return['id'] = $model->id;
                $return['game_id'] = $model->gamePlatform->game->id;
                $return['game_name'] = $model->gamePlatform->game->name;
                $return['platform_name'] = $model->gamePlatform->platform->name;
                $return['game_platform_id'] = $model->game_platform_id;
                $return['core_version'] = $model->core_version;
                $return['build_version'] = $model->build_version;
                $return['code_revision'] = $model->code_revision;
                $return['file_path'] = $model->file_path;
                $return['size'] = $model->size;
                $return['force_update'] = $model->force_update;
                $return['enabled'] = $model->enabled;
                $return['visible'] = $model->visible;
                $return['create_date'] = $model->create_date;
                $return['last_active'] = $model->last_active;
                $return['change_log'] = $model->change_log;
                $return['description'] = $model->description;
                $return['download_times'] = $model->download_times;
                $return['partner_id'] = $model->partner_id;
                $return['partner_name'] = $model->partner->name;
                $return['link_update'] = $model->link_update;
            } else {
                $return['code'] = 1;
                $return['message'] = 'Không tồn tại kết quả';
            }
        } else {
            if(isset($partner_id) && isset($game_platform_id)){
                $model = GameVersion::model()->findAll('game_platform_id=' . $game_platform_id. ' AND partner_id='.$partner_id);
            }elseif (isset($game_platform_id) && is_numeric($game_platform_id))
                $model = GameVersion::model()->findAll('game_platform_id=' . $game_platform_id);
            elseif(isset($partner_id))
                $model = GameVersion::model()->findAll('partner_id='.$partner_id);
            else
                $model = GameVersion::model()->findAll();
            if (!isset($model)) {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại kết quả";
            } else {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($model as $index => $item) {
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['game_id'] = $item->gamePlatform->game->id;
                    $return['items'][$index]['game_name'] = $item->gamePlatform->game->name;
                    $return['items'][$index]['platform_name'] = $item->gamePlatform->platform->name;
                    $return['items'][$index]['game_platform_id'] = $item->game_platform_id;
                    $return['items'][$index]['core_version'] = $item->core_version;
                    $return['items'][$index]['build_version'] = $item->build_version;
                    $return['items'][$index]['code_revision'] = $item->code_revision;
                    $return['items'][$index]['file_path'] = $item->file_path;
                    $return['items'][$index]['file_path_name'] = $item->file_path;
                    $return['items'][$index]['size'] = $item->size;
                    $return['items'][$index]['force_update'] = $item->force_update;
                    $return['items'][$index]['enabled'] = $item->enabled;
                    $return['items'][$index]['visible'] = $item->visible;
                    $return['items'][$index]['create_date'] = $item->create_date;
                    $return['items'][$index]['last_active'] = $item->last_active;
                    $return['items'][$index]['change_log'] = $item->change_log;
                    $return['items'][$index]['description'] = $item->description;
                    $return['items'][$index]['download_times'] = $item->download_times;
                    $return['items'][$index]['partner_id'] = $item->partner_id;
                    $return['items'][$index]['partner_name'] = $item->partner->name;
                    $return['items'][$index]['link_update'] = $item->link_update;
                }
            }
        }

        echo json_encode($return);
    }
    
    public function actionGetGameVersionByGame(){
        $return = array();                    
        $game_id = Yii::app()->request->getParam('game_id', NULL);
        $partner_id = Yii::app()->request->getParam('partner_id');
        if(!isset($partner_id))
            $partner_id = 1;
        
        if(!isset($game_id) || !is_numeric($game_id)){
            $return['code'] = -2;
            $return['message'] = 'Thông tin đầu vào không đúng';
            echo json_encode($return);die;
        }
        
        $criteria = new CDbCriteria;
        $criteria->alias = "game_version";        
        $criteria->join = ' INNER JOIN game_platform ON game_version.game_platform_id = game_platform.id';           
        $criteria->addCondition(' game_platform.game_id = '.$game_id);
        $criteria->addCondition(' game_version.partner_id = '.$partner_id);
        $gameVersion = GameVersion::model()->findAll($criteria);
        if(!isset($gameVersion)){
            $return['code'] = -1;
            $return['message'] = "Không tìm thấy gameversion tương ứng";
        }else{            
            if (empty($gameVersion)) {
                $return['code'] = 1;
                $return['message'] = "Không tìm thấy gameverion";
            } else {		                
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['item'] = array();
                foreach ($gameVersion as $index => $item) {
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['game_platform_id'] = $item->game_platform_id;
                    $return['items'][$index]['core_version'] = $item->core_version;
                    $return['items'][$index]['build_version'] = $item->build_version;
                    $return['items'][$index]['code_revision'] = $item->code_revision;
                    $return['items'][$index]['file_path'] = $item->file_path;
                    $return['items'][$index]['file_path_name'] = $item->file_path;
                    $return['items'][$index]['size'] = $item->size;
                    $return['items'][$index]['force_update'] = $item->force_update;
                    $return['items'][$index]['enabled'] = $item->enabled;
                    $return['items'][$index]['visible'] = $item->visible;
                    $return['items'][$index]['create_date'] = $item->create_date;
                    $return['items'][$index]['last_active'] = $item->last_active;
                    $return['items'][$index]['change_log'] = $item->change_log;
                    $return['items'][$index]['description'] = $item->description;
                    $return['items'][$index]['download_times'] = $item->download_times;
                    $return['items'][$index]['partner_id'] = $item->partner_id;
                    $return['items'][$index]['partner_name'] = $item->partner->name;
                    $return['items'][$index]['link_update'] = $item->link_update;
                }                
            }
        }
        
        echo json_encode($return);
    }

    public function actionCreateGameVersion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $game_platform_id = Yii::app()->request->getParam('game_platform_id');
            $core_version = Yii::app()->request->getParam('core_version', NULL);
            $build_version = Yii::app()->request->getParam('build_version', NULL);
            $code_revision = Yii::app()->request->getParam('code_revision', NULL);
            $force_update = Yii::app()->request->getParam('force_update');
            $change_log = Yii::app()->request->getParam('change_log');
            $description = Yii::app()->request->getParam('description');
            $copy_configuration = Yii::app()->request->getParam('copy_configuration');  
            $partner_id = Yii::app()->request->getParam('partner_id');  
            $link_update = Yii::app()->request->getParam('link_update');
            
            $return['game_platform_id'] = $game_platform_id;
            $return['core_version'] = $core_version;
            $return['build_version'] = $build_version;
            $return['code_revision'] = $code_revision;
            $return['force_update'] = $force_update;
            $return['change_log'] = $change_log;
            if (!isset($game_platform_id) || !isset($core_version) || !isset($build_version) || !isset($code_revision) || !is_numeric($game_platform_id) || !isset($force_update) || !isset($change_log) || !isset($partner_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else { 
                $gamePlatformModel = GamePlatform::model()->findByPk($game_platform_id);
                if (!isset($gamePlatformModel)) {
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại Game platform";
                    echo json_encode($return);
                    die;
                }                                
                
                $gameVersion = GameVersion::model()->find('game_platform_id='.$game_platform_id.' AND core_version="'.$core_version.'" AND build_version="'.$build_version.'" AND code_revision = "'.$code_revision.'" AND partner_id = '.$partner_id);
                if(isset($gameVersion)){
                    $return['code'] = 6;
                    $return['message'] = "Đã tồn tại version, cần thay đổi version khác";
                    echo json_encode($return);
                    die;
                }
                                
                $model = new GameVersion();
                $model->game_platform_id = $game_platform_id;
                $model->core_version = $core_version;
                $model->build_version = $build_version;
                $model->code_revision = $code_revision;
                $model->force_update = $force_update;
                $model->change_log = $change_log;
                $model->description = $description;
                $model->enabled = 0; // mặc định là không phải là current version
                $model->visible = 1; // mặc định hiện thị    
                $model->partner_id = $partner_id;
                $model->download_times = 0;
                $model->link_update = $link_update;
                
                if (!empty($_FILES['file_path'])) {
                    $filename = $_FILES['file_path']['name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $name = $gamePlatformModel->game->code_identifier . '-' . $gamePlatformModel->platform->code_identifier . '-' . $model->core_version . '.' . $model->build_version . '.' . $model->code_revision . '-' . time() . '.' . $ext;
                    $fullpath = 'files/games/' . $name;
                    move_uploaded_file($_FILES['file_path']['tmp_name'], $fullpath);
                    $model->file_path = $fullpath;
                    $model->size = floor($_FILES['file_path']['size'] / 1024);
                } else {
                    $return['code'] = 5;
                    $return['message'] = 'Không file upload';
                    echo json_encode($return);
                    die;
                }
                if ($model->save()) {                                  
                    // copy configuration client version
                    if(isset($copy_configuration)){
                        $copy_configuration = json_decode($copy_configuration);
                        if(is_array($copy_configuration)){
                            foreach($copy_configuration as $item){
                                if(!empty($item->value)){
                                    $modelConfig = new ConfigurationClientVersion();
                                    $modelConfig->game_version_id = $model->id;
                                    $modelConfig->configuration_client_id = $item->id;
                                    $modelConfig->value = $item->value;
                                    $modelConfig->save();
                                }
                            }
                        }
                    }                                          
                    
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateGameVersion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $game_platform_id = Yii::app()->request->getParam('game_platform_id');
            $core_version = Yii::app()->request->getParam('core_version', NULL);
            $build_version = Yii::app()->request->getParam('build_version', NULL);
            $code_revision = Yii::app()->request->getParam('code_revision', NULL);
            $force_update = Yii::app()->request->getParam('force_update');
            $change_log = Yii::app()->request->getParam('change_log');
            $description = Yii::app()->request->getParam('description');
            $visible = Yii::app()->request->getParam('visible', NULL);
            $enabled = Yii::app()->request->getParam('enabled');
            $partner_id = Yii::app()->request->getParam('partner_id');
            $link_update = Yii::app()->request->getParam('link_update');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                if (isset($game_platform_id)) {
                    $gamePlatformModel = GamePlatform::model()->findByPk($game_platform_id);
                    if (!isset($gamePlatformModel)) {
                        $return['code'] = 4;
                        $return['message'] = "Không tồn tại Game platform";
                        echo json_encode($return);
                        die;
                    }
                    $model->game_platform_id = $game_platform_id;
                }                
                
                $model = GameVersion::model()->findByPk($id);
                if (isset($core_version))
                    $model->core_version = $core_version;
                if (isset($build_version))
                    $model->build_version = $build_version;
                if (isset($code_revision))
                    $model->code_revision = $code_revision;
                if (isset($force_update))
                    $model->force_update = $force_update;
                if (isset($visible))
                    $model->visible = $visible;
                if (isset($change_log))
                    $model->change_log = $change_log;
                if (isset($description))
                    $model->description = $description;
                if (isset($partner_id))
                    $model->partner_id = $partner_id;
                if(isset($link_update))
                    $model->link_update = $link_update;
                if (isset($enabled) && $enabled == 0 && $model->enabled == 1) { 
                    $modelVersion = GameVersion::model()->find('id != '.$model->id.' AND game_platform_id=' . $model->game_platform_id. ' AND partner_id='.$model->partner_id.' ORDER BY last_active DESC');
                    $modelVersion->enabled = 1;
                    $modelVersion->last_active = date('Y-m-d h:i:s');
                    $modelVersion->save();
                    $model->enabled = 0;
                }
                
                if (isset($enabled) && $enabled == 1) {                    
                    $modelVersion = GameVersion::model()->findAll('game_platform_id=' . $model->game_platform_id. ' AND partner_id='.$model->partner_id);
                    foreach ($modelVersion as $index) {
                        $index->enabled = 0;
                        if ($index->save()) {
                            
                        }
                    };                    
                    $model->enabled = 1;
                    $model->last_active = date('Y-m-d h:i:s');
                }
                //echo($_FILES['file_path']['size'] / 1024);
                if (!empty($_FILES['file_path'])) {
                    $filename = $_FILES['file_path']['name'];
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    $name = $gamePlatformModel->game->code_identifier . '-' . $gamePlatformModel->platform->code_identifier . '-' . $model->core_version . '.' . $model->build_version . '.' . $model->code_revision . '-' . time() . '.' . $ext;
                    $fullpath = 'files/games/' . $name;
                    move_uploaded_file($_FILES['file_path']['tmp_name'], $fullpath);
                    $model->file_path = $fullpath;
                    $model->size = floor($_FILES['file_path']['size'] / 1024);                    
                }
                if ($model->save()) {
                    if(isset($enabled) && $enabled == 1){
                        // xoa cache 
                        $memcache = Common::initMemcache();
                        $arr_key = array('current_version',$model->gamePlatform->game_id,$model->gamePlatform->platform->code_identifier,$model->partner->code_identifier);
                        $key_memcache = Common::build_key_memcache($arr_key);
                        $memcache->delete($key_memcache);
                        // xoa tiep cho chac :))    
                        $arr_key2 = array('current_version',$model->gamePlatform->game_id,$model->gamePlatform->platform->code_identifier);
                        $key_memcache2 = Common::build_key_memcache($arr_key2);
                        $memcache->delete($key_memcache2);
                    }                    
                    if (isset($enabled) && $enabled == 1 && $model->partner->code_identifier == 'esimo') { 
                        // if ios thi tao 1 file download.xml de download qua testflightapp.com
                        if($model->gamePlatform->platform->code_identifier == 'ios'){
                            
                            $name = 'download.xml';
                            if($model->gamePlatform->game->code_identifier == 'chan'){
                                $name = 'download_chan.xml';
                                $image_large = 'images/chan_icon_large.png';
                                $image_small = 'images/chan_icon_small.png';
                            }
                            if($model->gamePlatform->game->code_identifier == 'phom'){
                                $name = 'download_phom.xml';
                                $image_large = 'images/phom_icon_large.png';
                                $image_small = 'images/phom_icon_small.png';
                            }
                            if($model->gamePlatform->game->code_identifier == 'tlmn'){
                                $image_large = 'images/tlmn_icon_large.png';
                                $image_small = 'images/tlmn_icon_small.png';
                                $name = 'download_tlmn.xml';
                            }
                            
                            if($model->gamePlatform->game->code_identifier == 'esimo'){
                                $image_large = 'images/dautri_icon_large.png';
                                $image_small = 'images/dautri_icon_small.png';
                                $name = 'download_dautri.xml';
                            }   

                            $xml = new DOMDocument();        
                            $str = '<?xml version="1.0" encoding="UTF-8"?>
                            <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
                            <plist version="1.0">
                            <dict>
                                    <key>items</key>
                                    <array>
                                            <dict>
                                                    <key>assets</key>
                                                    <array>
                                                            <dict>
                                                                    <key>kind</key>
                                                                    <string>software-package</string>
                                                                    <key>url</key>
                                                                    <string>'.Yii::app()->createAbsoluteUrl($model->file_path).'</string>
                                                            </dict>
                                                            <dict>
                                                                    <key>kind</key>
                                                                    <string>full-size-image</string>
                                                                    <key>needs-shine</key>
                                                                    <false/>
                                                                    <key>url</key>
                                                                    <string>'.Yii::app()->createAbsoluteUrl($image_large).'</string>
                                                            </dict>
                                                            <dict>
                                                                    <key>kind</key>
                                                                    <string>display-image</string>
                                                                    <key>needs-shine</key>
                                                                    <false/>
                                                                    <key>url</key>
                                                                    <string>'.Yii::app()->createAbsoluteUrl($image_small).'</string>
                                                            </dict>
                                                    </array>
                                                    <key>metadata</key>
                                                    <dict>
                                                            <key>bundle-identifier</key>
                                                            <string>'.$model->gamePlatform->bundle_id.'</string>
                                                            <key>bundle-version</key>
                                                            <string>'.$model->core_version.'.'.$model->build_version.'.'.$model->code_revision.'</string>
                                                            <key>kind</key>
                                                            <string>software</string>
                                                            <key>subtitle</key>
                                                            <string>'.$model->gamePlatform->game->name.'</string>
                                                            <key>title</key>
                                                            <string>Esimo '.$model->gamePlatform->game->name.'</string>
                                                    </dict>
                                            </dict>
                                    </array>
                            </dict>
                            </plist>
                            ';        
                            $xml->loadXML($str);
                            $xml->save("files/games/".$name);
                        }
                    }                                                                       
                    
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCallElectroUpdateGameVersion(){
        $game_version_id = Yii::app()->request->getParam('game_version_id');        
        if(!isset($game_version_id)){
            die;
        }
        $model = GameVersion::model()->findByPk($game_version_id);
        if(!isset($model)) die;
        $modelConfig = ConfigurationClientVersion::model()->findAll('game_version_id='.$model->id);                                                              
        if(isset($modelConfig)){
            //xoa cache
            $arr_key1 = array('info_recharge',$model->gamePlatform->game->code_identifier,$model->gamePlatform->platform->code_identifier,$model->core_version,$model->build_version,$model->code_revision,$model->partner->code_identifier);
            $arr_key2 = array('info_recharge',$model->gamePlatform->game->code_identifier,$model->gamePlatform->platform->code_identifier,$model->core_version,$model->build_version,$model->code_revision);
            $memcache_key1 = Common::build_key_memcache($arr_key1);  
            $memcache_key2 = Common::build_key_memcache($arr_key2);  
            $memcache = Common::initMemcache();            
            $memcache->delete($memcache_key1);
            $memcache->delete($memcache_key2);            
            $json = array();
            $json['typeRealTime'] = "configuration_client";
            $json['items'] = array();
            foreach($modelConfig as $index => $item){
                $config_name = $item->configurationClient->name;
                $json['items'][$index]['name'] = $config_name;
                $json['items'][$index]['value'] = $item->value;
            }                        
            $config_json = json_encode($json);
            $electroServer = new ElectroServerBridge();
            $data = array();
            $data['platform'] = $model->gamePlatform->platform->code_identifier;
            $data['appId'] = $model->gamePlatform->game_id;
            $data['version']= $model->code_revision;
            $data['config'] = $config_json;
            $electroServer->send($electroServer->buildURLForAction('updateConfigClient', $data));  
        }
        die;
    }

    public function actionDeleteGameVersion() {        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');            
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
//                $gameVersionPartnerModel = GameVersionPartner::model()->findAll('game_version_id=' . $id);                
//                if (isset($gameVersionPartnerModel)) {
//                    foreach($gameVersionPartnerModel as $item){
//                        $item->delete();
//                    }                    
//                    $return['code'] = 4;
//                    $return['message'] = "Bạn phải xóa Gameversionpartner trước";
//                    echo json_encode($return);
//                    die;
//                }                
                $configurationClientVersion = ConfigurationClientVersion::model()->findAll('game_version_id=' . $id);
                if (isset($configurationClientVersion)) {
                    foreach($configurationClientVersion as $item){
                        $item->delete();
                    }                                        
//                    $return['code'] = 5;
//                    $return['message'] = "Bạn phải xóa ConfigurationClientVersion trước";
//                    echo json_encode($return);
//                    die;
                }
                $model = GameVersion::model()->findByPk($id);                
                if (!empty($model)) {
                    if($model->enabled == 1){                        
                         $modelVersion = GameVersion::model()->find('id != '.$id.' AND last_active != NULL AND game_platform_id=' . $model->game_platform_id. ' ORDER BY last_active DESC');                        
                         if(!isset($modelVersion)){
                            $return['code'] = 6;
                            $return['message'] = "Bạn không được phép xóa vì không có version thay thế nào được active";
                            echo json_encode($return);die;
                         }
                         $modelVersion->enabled = 1;
                         $modelVersion->last_active = date('Y-m-d h:i:s');
                         $modelVersion->save();
                    }    
                    $file_game = $model->file_path;
                    if ($model->delete()) {
                        @unlink($file_game);
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Game";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetUsers() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $criteria->select = "id,username,email,first_name,last_name,middle_name,birthday,gender,identity_card_number,mobile,address,facebook_id,google_id,yahoo_id,twitter_id,create_time,gold,chip,level,experience,is_virtual_player";
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $model = User::model()->findAll($criteria);
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['username'] = $items->username;
                $return['items'][$index]['email'] = $items->email;
                $return['items'][$index]['first_name'] = $items->first_name;
                $return['items'][$index]['last_name'] = $items->last_name;
                $return['items'][$index]['middle_name'] = $items->middle_name;
                $return['items'][$index]['birthday'] = $items->birthday;
                $return['items'][$index]['gender'] = $items->gender;
                $return['items'][$index]['identity_card_number'] = $items->identity_card_number;
                $return['items'][$index]['mobile'] = $items->mobile;
                $return['items'][$index]['address'] = $items->address;
                $return['items'][$index]['facebook_id'] = $items->facebook_id;
                $return['items'][$index]['google_id'] = $items->google_id;
                $return['items'][$index]['yahoo_id'] = $items->yahoo_id;
                $return['items'][$index]['twitter_id'] = $items->twitter_id;
                $return['items'][$index]['create_time'] = $items->create_time;
                $return['items'][$index]['gold'] = $items->gold;
                $return['items'][$index]['chip'] = $items->chip;
                $return['items'][$index]['level'] = $items->level;
                $return['items'][$index]['experience'] = $items->experience;
                $return['items'][$index]['is_virtual_player'] = $items->is_virtual_player;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $newModel = User::model()->count();
            if (count($newModel) % $limit > 0) {
                $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
            } else
                $return['count'] = $newModel / $limit - 1;
        }

        echo json_encode($return);
    }

    public function actionGetRecharge() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $start = $page * $limit;
        $criteria->offset = $start;
        $criteria->limit = $limit;
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $user_id = Yii::app()->request->getParam('user_id');
        $recharge_type_id = Yii::app()->request->getParam('recharge_type_id');
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("create_time > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("create_time < '" . $endDate . "'");
        }
        if(isset($user_id)){
            $criteria->addCondition("user_id=".$user_id);
        }
        
        if(isset($recharge_type_id)){
            $criteria->addCondition("recharge_type_id=".$recharge_type_id);
        }
        $model = Recharge::model()->findAll($criteria);
        
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['user_id'] = $items->user->username;
                $return['items'][$index]['value'] = $items->value;
                $return['items'][$index]['gold'] = $items->gold;
                $return['items'][$index]['recharge_type_name'] = $items->rechargeType->name;
                $return['items'][$index]['ratio'] = $items->ratio;
                $return['items'][$index]['create_time'] = $items->create_time;
                $return['items'][$index]['status'] = $items->status;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $total = Recharge::model()->count($criteria);
            if (count($total) % $limit > 0) {
                $return['count'] = ($total - ($total % $limit)) / $limit;
            } else
                $return['count'] = $total / $limit - 1;
        }

        echo json_encode($return);
    }

    public function actionSearchUsers() {
        $return = array();
        $criteria = new CDbCriteria;
        $criteria->alias = 'user';
        $page = Yii::app()->request->getParam('page');
        if (!isset($page)) {
            $page = 0;
        };
        $limit = Yii::app()->request->getParam('limit');
        if (!isset($limit)) {
            $limit = 30;
        };
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $criteria->order = 'user.id, user.username';
        $id = Yii::app()->request->getParam('id');
        if (isset($id)) {
            $criteria->addCondition('user.id LIKE "%' . $id . '%"');
        };
        $username = Yii::app()->request->getParam('username');
        if (isset($username)) {
            $criteria->addCondition('user.username LIKE "%' . $username . '%" OR user.id = "'.$username.'" OR user.email LIKE "%'.$username.'%"');
        };        
        
        $partner_id = Yii::app()->request->getParam('partner');
        if( isset($partner_id)){
            $criteria->join = 'INNER JOIN user_partner ON user_partner.user_id = user.id';
            $criteria->join .= ' INNER JOIN partner ON partner.id = user_partner.partner';
            $criteria->addCondition('user_partner.partner = '.$partner_id);
        }
        $criteria->select = "user.id,user.username,user.email,user.first_name,user.last_name,user.middle_name,user.birthday,user.gender,user.identity_card_number,user.mobile,user.address,user.facebook_id,user.google_id,user.yahoo_id,user.twitter_id,user.create_time,user.gold,user.chip,user.level,user.experience,user.is_virtual_player";
        $model = User::model()->findAll($criteria);
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['username'] = $items->username;
                $return['items'][$index]['email'] = $items->email;
                $return['items'][$index]['first_name'] = $items->first_name;
                $return['items'][$index]['last_name'] = $items->last_name;
                $return['items'][$index]['middle_name'] = $items->middle_name;
                $return['items'][$index]['birthday'] = $items->birthday;
                $return['items'][$index]['gender'] = $items->gender;
                $return['items'][$index]['identity_card_number'] = $items->identity_card_number;
                $return['items'][$index]['mobile'] = $items->mobile;
                $return['items'][$index]['address'] = $items->address;
                $return['items'][$index]['facebook_id'] = $items->facebook_id;
                $return['items'][$index]['google_id'] = $items->google_id;
                $return['items'][$index]['yahoo_id'] = $items->yahoo_id;
                $return['items'][$index]['twitter_id'] = $items->twitter_id;
                $return['items'][$index]['create_time'] = $items->create_time;
                $return['items'][$index]['gold'] = $items->gold;
                $return['items'][$index]['chip'] = $items->chip;
                $return['items'][$index]['level'] = $items->level;
                $return['items'][$index]['experience'] = $items->experience;
                $return['items'][$index]['is_virtual_player'] = $items->is_virtual_player;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $newModel = User::model()->count($criteria);
            if (count($newModel) % $limit > 0) {
                $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
            } else
                $return['count'] = $newModel / $limit - 1;
        }

        echo json_encode($return);
    }

    public function actionUpdateUser() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
//            $username = Yii::app()->request->getParam('username');
            $email = Yii::app()->request->getParam('email');
            $first_name = Yii::app()->request->getParam('first_name');
            $last_name = Yii::app()->request->getParam('last_name');
            $middle_name = Yii::app()->request->getParam('middle_name');
            $birthday = Yii::app()->request->getParam('birthday');
            $gender = Yii::app()->request->getParam('gender');
            $identity_card_number = Yii::app()->request->getParam('identity_card_number');
            $mobile = Yii::app()->request->getParam('mobile');
            $address = Yii::app()->request->getParam('address');
            $facebook_id = Yii::app()->request->getParam('facebook_id');
            $google_id = Yii::app()->request->getParam('google_id');
            $yahoo_id = Yii::app()->request->getParam('yahoo_id');
            $twitter_id = Yii::app()->request->getParam('twitter_id');
//            $create_time = Yii::app()->request->getParam('create_time');
            $role = Yii::app()->request->getParam('role');
//            $gold = Yii::app()->request->getParam('gold');
//            $chip = Yii::app()->request->getParam('chip');
//            $level = Yii::app()->request->getParam('level');
//            $experience = Yii::app()->request->getParam('experience');
//            $is_virtual_player = Yii::app()->request->getParam('is_virtual_player');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = User::model()->findByPk($id);
                if (!isset($model)) {
                    $return['code'] = 2;
                    $return['message'] = "Dữ liệu nhập vào không đúng ID";
                } else {
                    if (isset($email) && strlen($email) > 0) {
                        $tmp = User::model()->find('email = "' . $email . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "Email đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->email = $email;
                    }

                    if (isset($first_name))
                        $model->first_name = $first_name;
                    if (isset($last_name))
                        $model->last_name = $last_name;
                    if (isset($middle_name))
                        $model->middle_name = $middle_name;
                    if (isset($birthday) && strlen($birthday) > 0) {
                        $model->birthday = $birthday;
                        $return['birthday'] = $birthday;
                    }
                    if (isset($gender))
                        $model->gender = $gender;
                    if (isset($address))
                        $model->address = $address;
                    if (isset($identity_card_number) && strlen($identity_card_number) > 0) {
                        $tmp = User::model()->find('identity_card_number = "' . $identity_card_number . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "Số CMT đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->identity_card_number = $identity_card_number;
                    }
                    if (isset($mobile) && strlen($mobile) > 0) {
                        $tmp = User::model()->find('mobile = "' . $mobile . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['mobile'] = $mobile;
                            $return['id'] = $tmp->id;
                            $return['tmp'] = $tmp->mobile;
                            $return['code'] = 2;
                            $return['message'] = "SĐT đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->mobile = $mobile;
                    }

                    if (isset($facebook_id) && strlen($facebook_id) > 0) {
                        $tmp = User::model()->find('facebook_id = "' . $facebook_id . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "facebook_id đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->facebook_id = $facebook_id;
                    }

                    if (isset($google_id) && strlen($google_id) > 0) {
                        $tmp = User::model()->find('google_id = "' . $google_id . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "Email đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->google_id = $google_id;
                    }

                    if (isset($yahoo_id) && strlen($yahoo_id) > 0) {
                        $tmp = User::model()->find('yahoo_id = "' . $yahoo_id . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "Email đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->yahoo_id = $yahoo_id;
                    }
                    if (isset($twitter_id) && strlen($twitter_id) > 0) {
                        $tmp = User::model()->find('twitter_id = "' . $twitter_id . '"');
                        if (isset($tmp) && $tmp->id != $id) {
                            $return['code'] = 2;
                            $return['message'] = "twitter_id đã bị trùng!";
                            echo json_encode($return);
                            die;
                        } else
                            $model->twitter_id = $twitter_id;
                    }
                    if (isset($role))
                        $model->role = $role;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionNewSearchUsers() {

        $criteria = new CDbCriteria;
        $User = new User;
        $criteria->compare('id', $User->id);
        $criteria->compare('username', $User->username, true);
        $criteria->compare('email', $User->email, true);
        $criteria->compare('password', $User->password, true);
        $criteria->compare('salt', $User->salt, true);
        $criteria->compare('role', $User->role);
        $criteria->compare('gold', $User->gold, true);
        $criteria->compare('chip', $User->chip, true);
        K::dump(new CActiveDataProvider($User, array(
            'criteria' => $criteria,
        )));
        return new CActiveDataProvider($User, array(
            'criteria' => $criteria,
        ));
    }

    public function actionGetPost() {
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $post = Post::model()->findAll();
            if (isset($post)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($post as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['title'] = $items->title;
//                    $return['items'][$index]['main_image'] = $items->main_image;
                    $return['items'][$index]['content'] = $items->content;
                    $return['items'][$index]['sapo'] = $items->sapo;
                    $return['items'][$index]['slug'] = $items->slug;
                    $return['items'][$index]['meta_title'] = $items->meta_title;
                    $return['items'][$index]['meta_keyword'] = $items->meta_keyword;
                    $return['items'][$index]['meta_description'] = $items->meta_description;
                    $return['items'][$index]['status'] = $items->status;
                    $return['items'][$index]['create_time'] = $items->create_time;
                    $return['items'][$index]['priority'] = $items->priority;
                    $return['items'][$index]['category_id'] = $items->category_id;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại bài viết nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $post = Post::model()->findByPk($id);
                if (isset($post)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $post->id;
                    $return['title'] = $post->title;
                    $return['content'] = $post->content;
                    $return['sapo'] = $post->sapo;
                    $return['slug'] = $post->slug;
                    $return['meta_title'] = $post->meta_title;
                    $return['meta_keyword'] = $post->meta_keyword;
                    $return['meta_description'] = $post->meta_description;
                    $return['status'] = $post->status;
                    $return['create_time'] = $post->create_time;
                    $return['priority'] = $post->priority;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại bài viết nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }

    public function actionGetPostCategory() {
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $post = PostCategory::model()->findAll();
            if (isset($post)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($post as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['parent_id'] = $items->parent_id;
                    $return['items'][$index]['status'] = $items->status;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại bài viết nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $post = PostCategory::model()->findByPk($id);
                if (isset($post)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $post->id;
                    $return['name'] = $post->name;
                    $return['description'] = $post->description;
                    $return['parent_id'] = $post->parent_id;
                    $return['status'] = $post->status;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại bài viết nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }

    public function actionGetGameLog() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $startTime = Yii::app()->request->getParam('startTime');
        $endTime = Yii::app()->request->getParam('endTime');        
        
        $start_id = $page * $limit;       
        $user_id = Yii::app()->request->getParam('user_id');
        $username = Yii::app()->request->getParam('username');
        if(!isset($page))
            $page = 0;        
        if(!isset($limit))
            $limit = 30;
        if(!isset($startTime) && !isset($endTime)){                       
            $startTime = new MongoDate(time()-24*3600);
            $endTime = new MongoDate();
        }elseif(!isset($endTime) && isset($startTime)){            
            $startTime = new MongoDate(strtotime($startTime));
            $endTime = new MongoDate();
            if($startTime > $endTime){
                $return['code'] = -2;
                $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                $return['items'] = array();                                               
                $return['items']['startTime'] = $startTime;
                $return['items']['endTime'] = $endTime;
                echo json_encode($return);die;
            }
        }elseif(isset($endTime) && !isset($startTime)){                       
            $startTime = new MongoDate(strtotime($endTime)- 24*3600);
            $endTime = new MongoDate(strtotime($endTime));
        }else{            
            $startTime = new MongoDate(strtotime($startTime));
            $endTime = new MongoDate(strtotime($endTime));
        }                
        
        $criteria = new EMongoCriteria();    
        if(isset($user_id))
            $criteria->users->user_id('==', intval($user_id));
        if(isset($username))
            $criteria->users->username = new MongoRegex('/^'.$username.'/');     
        $criteria->timestamp(">=",$startTime);
        $criteria->timestamp("<=",$endTime);
        $count = MGameLog::model()->count($criteria);          
        $criteria->sort('timestamp',EMongoCriteria::SORT_DESC)->limit($limit)->offset($start_id);
        $gameLog = MGameLog::model()->findAll($criteria);              
        if (!empty($gameLog)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['total'] = $count;
            $return['items'] = array();
            foreach ($gameLog as $index => $items) {
                $return['items'][$index]['id'] = (string)$items->_id;
                $return['items'][$index]['result'] = $items->result;
                $return['items'][$index]['timestamp'] = date('Y-m-d H:i:s', $items->timestamp->sec);
                $return['items'][$index]['game_id'] = $items->game_id;                
                foreach($items->users as $i => $k){                    
                    $return['items'][$index]['users'][$i]['user_id']= $k->user_id;
                    $return['items'][$index]['users'][$i]['username']= $k->username;
                }
            }

        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại GameLog nào";
        }
        
        //}
        echo json_encode($return);
    }
    
    public function actionGetGameError() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $startTime = Yii::app()->request->getParam('startTime');
        $endTime = Yii::app()->request->getParam('endTime');        
        
        $start_id = $page * $limit;       
        $user_id = Yii::app()->request->getParam('user_id');    
        $username = Yii::app()->request->getParam('username');
        $game_version = Yii::app()->request->getParam('game_version');
        $environment = Yii::app()->request->getParam('environment');
        $app_id = Yii::app()->request->getParam('app_id');
        if(!isset($page))
            $page = 0;        
        if(!isset($limit))
            $limit = 30;
        if(!isset($startTime) && !isset($endTime)){                       
            $startTime = new MongoDate(time()-24*3600);
            $endTime = new MongoDate();
        }elseif(!isset($endTime) && isset($startTime)){            
            $startTime = new MongoDate(strtotime($startTime));
            $endTime = new MongoDate();
            if($startTime > $endTime){
                $return['code'] = -2;
                $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                $return['items'] = array();                                               
                $return['items']['startTime'] = $startTime;
                $return['items']['endTime'] = $endTime;
                echo json_encode($return);die;
            }
        }elseif(isset($endTime) && !isset($startTime)){                       
            $startTime = new MongoDate(strtotime($endTime)- 24*3600);
            $endTime = new MongoDate(strtotime($endTime));
        }else{            
            $startTime = new MongoDate(strtotime($startTime));
            $endTime = new MongoDate(strtotime($endTime));
        }                                       
        
        $criteria = new EMongoCriteria();    
        if(isset($user_id))
            $criteria->users->user_id('==', intval($user_id));
        if(isset($username))
            $criteria->users->username = new MongoRegex('/^'.$username.'/'); 
        if(isset($game_version))
            $criteria->game_version = new MongoRegex('/^'.$game_version.'/'); 
        if(isset($environment))
            $criteria->environment = new MongoRegex('/^'.$environment.'/'); 
        if(isset($app_id))
            $criteria->app_id('==', intval($app_id));
        $criteria->time(">=",$startTime);
        $criteria->time("<=",$endTime);
        $count = MGameError::model()->count($criteria);            
        $criteria->sort('time',EMongoCriteria::SORT_DESC)->limit($limit)->offset($start_id);
        $gameError = MGameError::model()->findAll($criteria);               
        if (!empty($gameError)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['total'] = $count;
            $return['items'] = array();
            foreach ($gameError as $index => $items) {
                $return['items'][$index]['id'] = (string)$items->_id;                
                $return['items'][$index]['user_id'] = $items->users[0]->user_id;
                $return['items'][$index]['username'] = $items->users[0]->username;
                $return['items'][$index]['time'] = date('Y-m-d H:i:s', $items->time->sec);
                $return['items'][$index]['game_version'] = $items->game_version;   
                $return['items'][$index]['app_id'] = $items->app_id;  
                $return['items'][$index]['scene'] = $items->scene; 
                $return['items'][$index]['environment'] = $items->environment; 
                $return['items'][$index]['error'] = $items->error; 
                $return['items'][$index]['detail'] = $items->detail; 
                $return['items'][$index]['debug_log'] = Yii::app()->createAbsoluteUrl('files/gameerror/'.$items->debug_log); 
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại GameError nào";
        }
        
        //}
        echo json_encode($return);
    }    

    public function actionGetConfiguration() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $gameLog = Configuration::model()->findAll($criteria);
            if (!empty($gameLog)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($gameLog as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['value'] = $items->value;
                    $return['items'][$index]['description'] = $items->description;
                }
                $criteria->offset = 0;
                $criteria->limit = 10000000;
                $newModel = Configuration::model()->count();
                if ($newModel % $limit > 0) {
                    $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
                } else
                    $return['count'] = $newModel / $limit - 1;
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn Configuration nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $gameLog = Configuration::model()->findByPk($id);
                if (isset($gameLog)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $gameLog->id;
                    $return['name'] = $gameLog->name;
                    $return['value'] = $gameLog->value;
                    $return['description'] = $gameLog->description;
                    $criteria->offset = 0;
                    $criteria->limit = 10000000;
                    $newModel = Configuration::model()->find('count (*) ');
                    if (count($newModel) % $limit > 0) {
                        $return['count'] = (count($newModel) - (count($newModel) % $limit)) / $limit;
                    } else
                        $return['count'] = count($newModel) / $limit - 1;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại Configuration nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }

    public function actionGetRoles() {              
        $role = Role::model()->findAll();        
        if (!empty($role)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($role as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['name'] = $items->name;
                $return['items'][$index]['partner_id'] = $items->partner_id;
                $return['items'][$index]['partner_name'] = $items->partner->name;
                $return['items'][$index]['partner_code_identifier'] = $items->partner->code_identifier;
                $return['items'][$index]['description'] = $items->description;
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai Role nào";
        }
        //}
        echo json_encode($return);
    }

    public function actionGetUserRoles() {
        $return = array();
        $id = Yii::app()->request->getParam('user_id');
        if (isset($id)) {
            $userRoles = UserRole::model()->findAll('user_id = "' . $id . '"');
            if (!empty($userRoles)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                foreach ($userRoles as $index) {
                    $return['roles'][] = $index->role_id;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tai Role nào";
            }
        } else {
            $role = Role::model()->findAll();
            if (!empty($role)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                foreach ($role as $index) {
                    $return['roles'][] = $index->id;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tai Role nào";
            }
        }
        //}
        echo json_encode($return);
    }
    
    public function actionGetUserByRole(){
        $return = array();
        $id = Yii::app()->request->getParam('role_id');
        if(!isset($id) || !is_numeric($id)){
            $return['code'] = -1;
            $return['message'] = "Tham số truyền vào không đúng";
            echo json_encode($return);die;
        }
        
        $userRoles = UserRole::model()->findAll('role_id = "' . $id . '"');
        if (!empty($userRoles)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['users'] = array();
            foreach ($userRoles as $index =>$item) {
                $return['users'][$index]['user_id'] = $item->user_id;
                $return['users'][$index]['username'] = $item->user->username;
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai User nào";
        }
        
        echo json_encode($return);
    }

    public function actionGetPermission() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $permission = Permission::model()->findAll();
        if (!empty($permission)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($permission as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['name'] = $items->name;
                $return['items'][$index]['description'] = $items->description;
                $return['items'][$index]['code_identifier'] = $items->code_identifier;
                $return['items'][$index]['category_id'] = $items->category_id;
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai Permission nào";
        }
        //}
        echo json_encode($return);
    }

    public function actionGetRolePermission() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        if (isset($page) && isset($limit)) {
            $start_id = $page * $limit;
            $criteria->offset = $start_id;
            $criteria->limit = $limit;
        }
        $rolepermission = RolePermission::model()->findAll($criteria);
        if (!empty($rolepermission)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($rolepermission as $index => $items) {
                $return['items'][$index]['role_id'] = $items->role_id;
                $return['items'][$index]['permission_id'] = $items->permission_id;
                $return['items'][$index]['description'] = $items->description;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $newModel = RolePermission::model()->count();

            if (isset($page) && isset($limit)) {
                if ($newModel % $limit > 0) {
                    $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
                } else
                    $return['count'] = $newModel / $limit - 1;
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai Role Permission nào";
        }
        //}
        echo json_encode($return);
    }

    public function actionGetUserRole() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $userRole = UserRole::model()->findAll($criteria);
        if (!empty($userRole)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($userRole as $index => $items) {
                $return['items'][$index]['role_id'] = $items->role_id;
                $return['items'][$index]['user_id'] = $items->user_id;
                $return['items'][$index]['timestamp'] = $items->timestamp;
                $return['items'][$index]['enabled'] = $items->enabled;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $newModel = UserRole::model()->count();
            if ($newModel % $limit > 0) {
                $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
            } else
                $return['count'] = $newModel / $limit - 1;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai Role Permission nào";
        }
        //}
        echo json_encode($return);
    }

    public function actionGetFeedBack() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        $start_id = $page * $limit;
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("timestamp > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("timestamp < '" . $endDate . "'");
        }
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $criteria->order = "timestamp DESC";
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $feedback = Feedback::model()->findAll($criteria);
//            $return['feedback'] = $feedback;
//            echo json_encode($return);die;
            if (!empty($feedback)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($feedback as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['game_id'] = $items->game_id;                    
                    $return['items'][$index]['game_name'] = (isset($items->game)) ? $items->game->name : "Tất cả";
                    $return['items'][$index]['user_id'] = $items->user_id;
                    $return['items'][$index]['email'] = $items->user->email;
                    $return['items'][$index]['username'] = $items->user->username;
                    $return['items'][$index]['title'] = $items->title;
                    $return['items'][$index]['content'] = $items->content;
                    $return['items'][$index]['timestamp'] = $items->timestamp;
                }
                $criteria->offset = 0;
                $criteria->limit = 10000000;
                $newModel = Feedback::model()->count();
                if ($newModel % $limit > 0) {
                    $return['count'] = ($newModel - ($newModel % $limit)) / $limit;
                } else
                    $return['count'] = $newModel / $limit - 1;
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn Feedback nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $feedback = Feedback::model()->findByPk($id);
                if (isset($feedback)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $feedback->id;
                    $return['game_id'] = $feedback->game_id;
                    $return['game_name'] = (isset($feedback->game)) ? $feedback->game->name : "Tất cả";
                    $return['user_id'] = $feedback->user_id;
                    $return['email'] = $feedback->email;
                    $return['username'] = $feedback->user->username;
                    $return['title'] = $feedback->title;
                    $return['content'] = $feedback->content;
                    $return['timestamp'] = $feedback->timestamp;
                    $criteria->offset = 0;
                    $criteria->limit = 10000000;
                    $newModel = Feedback::model()->find('count (*) ');
                    if (count($newModel) % $limit > 0) {
                        $return['count'] = (count($newModel) - (count($newModel) % $limit)) / $limit;
                    } else
                        $return['count'] = count($newModel) / $limit - 1;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại Feedback nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }

    public function actionGetUserds() {
//        echo'aa';die;
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $criteria = new CDbCriteria;
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $criteria->select = "id,username,email,first_name,last_name,middle_name,birthday,gender,identity_card_number,mobile,address,facebook_id,google_id,yahoo_id,twitter_id,create_time,gold,chip,level,experience,is_virtual_player";
        $model = User::model()->findAll($criteria);
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['username'] = $items->username;
                $return['items'][$index]['email'] = $items->email;
                $return['items'][$index]['first_name'] = $items->first_name;
                $return['items'][$index]['last_name'] = $items->last_name;
                $return['items'][$index]['middle_name'] = $items->middle_name;
                $return['items'][$index]['birthday'] = $items->birthday;
                $return['items'][$index]['gender'] = $items->gender;
                $return['items'][$index]['identity_card_number'] = $items->identity_card_number;
                $return['items'][$index]['mobile'] = $items->mobile;
                $return['items'][$index]['address'] = $items->address;
                $return['items'][$index]['facebook_id'] = $items->facebook_id;
                $return['items'][$index]['google_id'] = $items->google_id;
                $return['items'][$index]['yahoo_id'] = $items->yahoo_id;
                $return['items'][$index]['twitter_id'] = $items->twitter_id;
                $return['items'][$index]['create_time'] = $items->create_time;
                $return['items'][$index]['role'] = $items->role;
                $return['items'][$index]['gold'] = $items->gold;
                $return['items'][$index]['chip'] = $items->chip;
                $return['items'][$index]['level'] = $items->level;
                $return['items'][$index]['experience'] = $items->experience;
                $return['items'][$index]['is_virtual_player'] = $items->is_virtual_player;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $newModel = User::model()->findAll($criteria);
            if (count($newModel) % $limit > 0) {
                $return['count'] = (count($newModel) - (count($newModel) % $limit)) / $limit;
            } else
                $return['count'] = count($newModel) / $limit - 1;
        }

        echo json_encode($return);
    }

    public function actionUpdateGameLog() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $parent_id = Yii::app()->request->getParam('parent_id');
            $point = Yii::app()->request->getParam('point');
            $description = Yii::app()->request->getParam('description');
            $game_id = Yii::app()->request->getParam('game_id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {


                $model = GameLog::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;
                    if (isset($parent_id)) {
                        $gameActionCategory = GameActionCategory::model()->findByPk($parent_id);
                        if (!isset($category)) {
                            echo 'aaa';
                            die;
                            $return['code'] = 4;
                            $return['message'] = "Không tồn tại Game Action Category";
                            echo json_encode($return);
                            die;
                        } else
                            $model->parent_id = $parent_id;
                    }
                    if (isset($description))
                        $model->description = $description;
                    if (isset($point))
                        $model->point = $point;
                    if (isset($game_id)) {
                        $game = Game::model()->findByPk($game_id);
                        if (!isset($category)) {
                            echo 'aaa';
                            die;
                            $return['code'] = 4;
                            $return['message'] = "Không tồn tại Game ID";
                            echo json_encode($return);
                            die;
                        } else
                            $model->game_id = $game_id;
                    }
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 2;
                    $return['message'] = 'Không ton tai Game Log ID';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateConfiguration() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $value = Yii::app()->request->getParam('value');
            $description = Yii::app()->request->getParam('description');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Configuration::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;
                    if (isset($value)) {
                        $model->value = $value;
                    }
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {
                        $electroServer = new ElectroServerBridge();
                        $electroServer->send($electroServer->buildURLForAction('reset_db_configuration'));
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 2;
                    $return['message'] = 'Không ton tai Configuration ID';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateConfiguration() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $value = Yii::app()->request->getParam('value');
            $description = Yii::app()->request->getParam('description');
            if (!isset($name) || !isset($value)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new Configuration;
                $model->name = $name;
                $model->value = $value;
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $electroServer = new ElectroServerBridge();
                    $electroServer->send($electroServer->buildURLForAction('reset_db_configuration'));
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreatePost() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $title = Yii::app()->request->getParam('title');
            $category_id = Yii::app()->request->getParam('category_id');
            $sapo = Yii::app()->request->getParam('sapo');
            $content = Yii::app()->request->getParam('content');
            $slug = Yii::app()->request->getParam('slug');
            $meta_title = Yii::app()->request->getParam('meta_title');
            $meta_keyword = Yii::app()->request->getParam('meta_keyword');
            $meta_description = Yii::app()->request->getParam('meta_description');
            $status = Yii::app()->request->getParam('status');
            $priority = Yii::app()->request->getParam('priority');
            $is_post_facebook = Yii::app()->request->getParam('is_post_facebook');
            if (!isset($title) || !isset($sapo) || !isset($content)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $category = PostCategory::model()->findByPk($category_id);
                if (!isset($category)) {
                    $return['code'] = 3;
                    $return['message'] = "Danh mục không tồn tại";
                } else {
                    $model = new Post();
                    $model->title = $title;
                    if (!empty($_FILES['main_image'])) {
                        $filename = $_FILES['main_image']['name'];
                        $pos = strrpos($filename, '.');
                        $name = substr($filename, 0, $pos);
                        $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                        $name = Myvietstring::removeInvalidCharacter($name);
                        $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        // $real_name = $name . '.' . $ext;
                        $name.= '-' . time();
                        $name.= '.' . $ext;
                        $fullpath = 'files/post/' . $name;
                        move_uploaded_file($_FILES['main_image']['tmp_name'], $fullpath);
                        $model->main_image = $fullpath;
                    } else {
                        $return['code'] = 5;
                        $return['message'] = "Khong ton tai file";
                        echo json_encode($return);
                        die;
                    }
                    $model->sapo = $sapo;
                    $model->content = $content;
                    $model->category_id = $category_id;
                    if (empty($slug)) {
                        $slug = Common::simpleSlug($model->title);
                        $slug = $slug . ".htm";
                    }
                    $tmp = Post::model()->find('slug ="' . $slug . '"');
                    if (!empty($tmp)) {
                        $tmp1 = explode('.', $slug);
                        if (count($tmp1) > 1) {
                            $slug = $tmp1[0] . "-" . ($tmp->id + 1);
                            $slug = $slug . "." . $tmp1[1];
                        } else {
                            $slug = $slug . ($tmp->id + 1);
                        }
                    }
                    $model->slug = $slug;
                    $model->meta_title = $meta_title;
                    $model->meta_keyword = $meta_keyword;
                    $model->meta_description = $meta_description;
                    $model->status = $status;
                    $model->category_id = $category_id;
                    if (isset($priority))
                        $model->status = $status;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                        if(isset($is_post_facebook)){
                            $source = html_entity_decode($model->sapo, ENT_COMPAT, 'UTF-8'); 
                            $source = strip_tags($source);
                            $return['link'] = Yii::app()->createAbsoluteUrl('apiAdmin/postFacebook?title='.$model->title.'&sapo='.$source.'&link='.Yii::app()->createAbsoluteUrl('bai-viet/'.$model->slug).'&image='.Yii::app()->createAbsoluteUrl($model->main_image));
                        }
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionPostFacebook(){        
        $title = Yii::app()->request->getParam('title');
        $sapo = Yii::app()->request->getParam('sapo');
        $image = Yii::app()->request->getParam('image');
        $link = Yii::app()->request->getParam('link');        
        $appid = '165636903608352';
        $appsecret = '079882764ee9dd83a7f433b88a6a20ac';
        $pageId = '527582053993279';                

        $facebook = new Facebook(array(
          'appId' => $appid,
          'secret' => $appsecret,
          'cookie' => false,
        ));               

        $user = $facebook->getUser();           
        $accessToken = $facebook->getAccessToken();                   
        if ($user) {              
            try {
              $page_id = $pageId;
              $page_info = $facebook->api("/$page_id?fields=access_token");                         
              if( !empty($page_info['access_token']) ) {                  
                  $args = array
                    (
                    'access_token'  => $page_info['access_token'],
                    'message' => $title,
                    'name' => $title,
                    //'caption' => $sapo,
                    'link' => $link,
                    //'actions' => array('name'=>'Sweet FA','link'=>'http://www.facebookanswers.co.uk'),
                    'description' => $sapo,
                    'picture' => $image
                    );                         
                  $post_id = $facebook->api("/$page_id/feed","post",$args);                        
              } else {                  
                  $permissions = $facebook->api("/me/permissions");                            
                  if( !array_key_exists('publish_stream', $permissions['data'][0]) || 
                      !array_key_exists('manage_pages', $permissions['data'][0])) {
                      // We don't have one of the permissions
                      // Alert the admin or ask for the permission!                      
                      header( "Location: " . $facebook->getLoginUrl(array("scope" => "publish_stream, manage_pages")) );
                  }
              }
            } catch (FacebookApiException $e) {
              error_log($e);
              $user = null;
            }
        }
        if ($user) {            
            $logoutUrl = $facebook->getLogoutUrl();            
        } else {            
            $loginUrl = $facebook->getLoginUrl(array('scope'=>'manage_pages,publish_stream'));  
            $this->redirect($loginUrl);            
        }
        
        //$this->render('test', array());
    }        

    public function actionCreatePostCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $title = Yii::app()->request->getParam('title');
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $status = Yii::app()->request->getParam('status');
            $parent_id = Yii::app()->request->getParam('parent_id');
            if (!isset($name) || !isset($status)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new PostCategory();
                $model->name = $name;
                $model->status = $status;
                if (isset($description))
                    $model->description = $description;
                if (isset($parent_id))
                    $model->parent_id = $parent_id;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/post/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePost() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = Post::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $title = Yii::app()->request->getParam('title');
                $is_post_facebook = Yii::app()->request->getParam('is_post_facebook');
                if (isset($title))
                    $model->title = $title;
                if (!empty($_FILES['main_image'])) {
                    $filename = $_FILES['main_image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/post/' . $name;
                    move_uploaded_file($_FILES['main_image']['tmp_name'], $fullpath);
                    $model->main_image = $fullpath;
                }
                $sapo = Yii::app()->request->getParam('sapo');
                if (isset($sapo))
                    $model->sapo = $sapo;

                $content = Yii::app()->request->getParam('content');
                if (isset($content))
                    $model->content = $content;

                $slug = Yii::app()->request->getParam('slug');
                if (empty($slug)) {
                    $slug = Common::simpleSlug($model->title);
                    $slug = $slug . ".htm";
                }
                $tmp = Post::model()->find('slug ="' . $slug . '"');
                if(isset($tmp))
                    if ($tmp->id != $id) {
                        if (!empty($tmp)) {
                            $tmp1 = explode('.', $slug);
                            if (count($tmp1) > 1) {
                                $slug = $tmp1[0] . "-" . $id;
                                $slug = $slug . "." . $tmp1[1];
                            } else {
                                $slug = $slug . "-2";
                            }
                        }
                    }
                $model->slug = $slug;

                $meta_title = Yii::app()->request->getParam('meta_title');
                if (isset($meta_title))
                    $model->meta_title = $meta_title;

                $meta_keyword = Yii::app()->request->getParam('meta_keyword');
                if (isset($meta_keyword))
                    $model->meta_keyword = $meta_keyword;

                $meta_description = Yii::app()->request->getParam('meta_description');
                if (isset($meta_description))
                    $model->meta_description = $meta_description;
                $status = Yii::app()->request->getParam('status');
                if (isset($status))
                    $model->status = $status;
                $priority = Yii::app()->request->getParam('priority');
                if (isset($priority))
                    $model->priority = $priority;
                $category_id = Yii::app()->request->getParam('category_id');
                if (isset($category_id))
                    $model->category_id = $category_id;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    if(isset($is_post_facebook)){                        
                        $source = html_entity_decode($model->sapo, ENT_COMPAT, 'UTF-8');
                        $source = strip_tags($source);
                        $return['link'] = Yii::app()->createAbsoluteUrl('apiAdmin/postFacebook?title='.$model->title.'&sapo='.$source.'&link='.Yii::app()->createAbsoluteUrl('bai-viet/'.$model->slug).'&image='.Yii::app()->createAbsoluteUrl($model->main_image));
                    }
                    //$this->postFacebook($model->title, strip_tags($model->sapo),Yii::app()->createAbsoluteUrl('bai-viet/'.$model->slug),Yii::app()->createAbsoluteUrl($model->main_image));
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePostCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = PostCategory::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $parent_id = Yii::app()->request->getParam('parent_id');
                if (isset($parent_id))
                    $model->parent_id = $parent_id;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/post/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }    
                $status = Yii::app()->request->getParam('status');
                if (isset($status))
                    $model->status = $status;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateAds(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {            
            $game_id = Yii::app()->request->getParam('game_id'); 
            $partner_id = Yii::app()->request->getParam('partner_id');
            $type = Yii::app()->request->getParam('type');
            $index = Yii::app()->request->getParam('index');
            $description = Yii::app()->request->getParam('description');
            $scenes = Yii::app()->request->getParam('scenes');
            $url = Yii::app()->request->getParam('url');
            if (!isset($type) || !isset($index) || !isset($scenes) || !isset($description) || !isset($url)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new Ads();                
                $model->type = $type;
                $model->index = $index;
                $model->description = $description;
                $model->scenes =$scenes;
                $model->url = $url;
                if(!isset($partner_id)){
                    $partner_id = 1;
                }
                $model->partner_id = $partner_id;    
                if(isset($game_id))
                    $model->game_id = $game_id;                
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 20) ? substr($name, 0, 20) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $path = 'files/ads/' . $name;
                    $fullpath = Yii::app()->createAbsoluteUrl('files/ads/' . $name);
                    move_uploaded_file($_FILES['image']['tmp_name'], $path);
                    $model->image = $fullpath;
                } else {
                    $return['code'] = 5;
                    $return['message'] = "Khong ton tai file";
                    echo json_encode($return);
                    die;
                }
                if ($model->save()) {                    
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    Common::dump($model->errors);
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }                
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateAds() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = Ads::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $game_id = Yii::app()->request->getParam('game_id');
                if (isset($game_id))
                    $model->game_id = $game_id;
                $partner_id = Yii::app()->request->getParam('partner_id');
                if (isset($partner_id))
                    $model->partner_id = $partner_id;
                $index = Yii::app()->request->getParam('index');
                if (isset($index))
                    $model->index = $index;
                $type = Yii::app()->request->getParam('type');
                if (isset($type))
                    $model->type = $type;
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $scenes = Yii::app()->request->getParam('scenes');
                if (isset($scenes))
                    $model->scenes = $scenes;
                $url = Yii::app()->request->getParam('url');
                if (isset($url))
                    $model->url = $url;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $path = 'files/ads/' . $name;
                    $fullpath = Yii::app()->createAbsoluteUrl('files/ads/' . $name);
                    move_uploaded_file($_FILES['image']['tmp_name'], $path);
                    $model->image = $fullpath;
                }
                if ($model->save()) {
                    $json = array();
                    $json['typeRealTime'] = "ads";
                    $json['url'] = $model->url;
                    $json['index'] = $model->index;
                    $json['type'] = $model->type;
                    $json['image'] = $model->image;
                    $json['scenes'] = $model->scenes;
                    $json['description'] = $model->description;
                    $config_json = json_encode($json);
                    $electroServer = new ElectroServerBridge();
                    $data = array();                                        
                    $data['config'] = $config_json;
                    $electroServer->send($electroServer->buildURLForAction('updateConfigClient', $data));
                                        
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionDeleteAds() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');            
            if (!isset($id) || !is_numeric($id) ) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Ads::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Ads";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionSendMessage() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $userID = Yii::app()->request->getParam('userID');
        $content = Yii::app()->request->getParam('content');
        $showPopup = Yii::app()->request->getParam('showPopup');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $sendMessage = MUserMessage::model()->sendUserMessage($userID,$content,$showPopup);
            if ($sendMessage) {
                $return['code'] = 0;
                $return['message'] = "Gửi tin nhắn thành công";
            } else {
                $return['code'] = 1;
                $return['message'] = "Gửi tin nhắn thất bại";
            }
        }
        echo json_encode($return);
    }

    public function actionSendMessageForAll() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $content = Yii::app()->request->getParam('content');
            $showPopup = Yii::app()->request->getParam('showPopup');
            $actionSendMessageForAll = MUserMessage::model()->sendMessageForAll($content,$showPopup);
            if ($actionSendMessageForAll) {
                $return['code'] = 0;
                $return['message'] = "Gửi tin nhắn cho tất cả User thành công!";
            } else {
                $return['code'] = 1;
                $return['message'] = "Gửi tin nhắn cho tất cả User thất bại!";
            }
        }
        echo json_encode($return);
    }

    public function actionAddChip() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $userID = Yii::app()->request->getParam('userID');
        $value = Yii::app()->request->getParam('value');
        $description = Yii::app()->request->getParam('description');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user = User::model()->findByPk($userID);
            if (isset($user)) {
                if (!is_numeric($value)) {
                    $return['code'] = 2;
                    $return['message'] = "Chip phải là số nguyên";
                } else {
                    $user->chip += $value;
                }
                $transaction = new Transaction;
                $transaction->user_id = $userID;
                $transaction->value = $value;
                $transaction->description = $description;
                $transaction->type = 11;                                
                
                if (!$transaction->save()) {
                    $return['code'] = 2;
                    $return['message'] = "Lưu Transaction thất bại!";
                    echo json_encode($return);
                    die;
                }
                $userAdmin = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;
                $adminActionType = AdminActionType::model()->find("code_identifier='push_chips'");
                if(isset($adminActionType)){
                    $tmp = array();                    
                    $tmp['user_receiver'] = $user->username;
                    $tmp['transaction_id'] = $transaction->id;
                    $tmp['value'] = $value;
                    $tmp['description'] = $description;
                    $adminActionLog = new AdminActionLog();
                    $adminActionLog->type_id = $adminActionType->id;
                    $adminActionLog->user_id = $userAdmin->id;
                    $adminActionLog->content = json_encode($tmp);
                    $adminActionLog->save();
                }
                
                if ($user->save()) {
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'chip', $value);//chip + thêm
                    
//                    $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                    $electroServer2->sendUserInfo($user->id, 'chip', $value); 
                    
                    $sendMessage = MUserMessage::model()->sendUserMessage($userID,"Bạn vừa nhận được ".number_format($value, 0, ',', '.')." chip từ hệ thống!",1);
                    $user->changeChipSystem($value, 'minus');
                    $return['code'] = 0;
                    $return['message'] = "Add chip thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Add chip thất bại";
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại User ID";
            }
        }
        echo json_encode($return);
    }
    
    public function actionAddGold() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $userID = Yii::app()->request->getParam('userID');
        $value = Yii::app()->request->getParam('value');
        $description = Yii::app()->request->getParam('description');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user = User::model()->findByPk($userID);
            if (isset($user)) {
                if (!is_numeric($value)) {
                    $return['code'] = 2;
                    $return['message'] = "Gold phải là số nguyên";
                } else {
                    $user->gold += $value;
                }
                $transaction = new Transaction;
                $transaction->user_id = $userID;
                $transaction->value = $value;
                $transaction->description = $description;
                $transaction->type = 16;                                
                
                if (!$transaction->save()) {
                    $return['code'] = 2;
                    $return['message'] = "Lưu Transaction thất bại!";
                    echo json_encode($return);
                    die;
                }
                $userAdmin = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;
                $adminActionType = AdminActionType::model()->find("code_identifier='push_golds'");
                if(isset($adminActionType)){
                    $tmp = array();                    
                    $tmp['user_receiver'] = $user->username;
                    $tmp['transaction_id'] = $transaction->id;
                    $tmp['value'] = $value;
                    $tmp['description'] = $description;
                    $adminActionLog = new AdminActionLog();
                    $adminActionLog->type_id = $adminActionType->id;
                    $adminActionLog->user_id = $userAdmin->id;
                    $adminActionLog->content = json_encode($tmp);
                    $adminActionLog->save();
                }
                
                if ($user->save()) {
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'gold', $value);//chip + thêm
                    
//                    $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                    $electroServer2->sendUserInfo($user->id, 'gold', $value); 
                    
                    $sendMessage = MUserMessage::model()->sendUserMessage($userID,"Bạn vừa nhận được ".number_format($value, 0, ',', '.')." gold từ hệ thống!",1);
                    $user->changeGoldSystem($value, 'minus');
                    $return['code'] = 0;
                    $return['message'] = "Add gold thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Add gold thất bại";
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại User ID";
            }
        }
        echo json_encode($return);
    }

    public function actionResetPassword() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $userID = Yii::app()->request->getParam('userID');
        $email = Yii::app()->request->getParam('email');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user = User::model()->findByPk($userID);
            if (isset($user)) {
                $oldEmail = $user->email;
                if ($oldEmail != $email) {
                    $tmp = User::model()->find('email = "' . $email . '"');
                    if (isset($tmp)) {
                        $return['code'] = 3;
                        $return['message'] = "Emai nhập vào đã tồn tại";
                        echo json_encode($return);
                        die;
                    };
                    $user->email = $email;
                };
                $ip = $_SERVER['REMOTE_ADDR'];
                $user->password = MyFunction::rand_string(8);
                $accessToken = UserAccessToken::model()->createAccessToken($userID, $user->password, $ip);
                $user->access_token = $accessToken;
                $message = new YiiMailMessage;
                //this points to the file test.php inside the view path
                $message->view = "SetPassword";
                $params = array('userinfo' => $user);
                $message->subject = 'Reset mật khẩu trên Esimo';
                $message->setBody($params, 'text/html');
                $message->addTo($email);
                $message->from = 'game@eposi.vn';
                $result = Yii::app()->mail->send($message);
                if ($result) {
                    $return['code'] = 0;
                    $return['message'] = "Gửi Email thành công!";
                } else {
                    $return['code'] = 4;
                    $return['message'] = "Gửi Email thất bại";
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại User ID";
            }
        }
        echo json_encode($return);
    }

    public function actionGetNewUser() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {                             
            $page = Yii::app()->request->getParam('page');
            $limit = Yii::app()->request->getParam('limit');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            if(!isset($page))
                $page = 0;
            if(!isset($limit))
                $limit = 30;
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                           
                    $return['items']['typeStatistic'] = 'New User';
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                         
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            $criteria = new CDbCriteria;
            $criteria2 = new CDbCriteria;
            $criteria->alias = 'user';
            $criteria2->alias = 'user';
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';            
                $criteria->addCondition("user.create_time >= '" . $startTime . "'");
                $criteria2->addCondition("user.create_time >= '" . $startTime . "'");
            }
            if (isset($endDate)) {                
                $endTime = $endDate. ' 23:59:59';
                $endTimeActive = date("Y-m-d H:i:s", (strtotime($endTime) + 24 * 3600));
                $criteria->addCondition("user.create_time <='" . $endTime . "'");
                $criteria2->addCondition("user.create_time <='" . $endTime . "'");
            }       
           
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;            
            $partner = $user->getAdminPartner();            
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){
                        $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user.id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);   
                        
                        $criteria2->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user.id';
                        $criteria2->addCondition('user_partner.partner = '.$partner_id);
                        
                        $query='SELECT user.create_time, count(user.id) as number_user
                            FROM user
                            INNER JOIN user_partner on user_partner.user_id = user.id
                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'" 
                            AND user_partner.partner = "'.$partner_id.'" 
                            GROUP BY CAST(user.create_time AS DATE);';
                        
//                        $query2 = 'SELECT user.create_time, count(DISTINCT(user.id)) as number_user
//                            FROM user
//                            INNER JOIN user_partner on user_partner.user_id = user.id
//                            INNER JOIN transaction on transaction.user_id = user.id
//                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'" 
//                            AND user_partner.partner = "'.$partner_id.'" AND transaction.type = 3 AND transaction.timestamp <  "'.$endTimeActive.'"
//                            GROUP BY CAST(user.create_time AS DATE);';
                    }else{
                        $query='SELECT user.create_time, count(user.id) as number_user
                            FROM user
                            INNER JOIN user_partner on user_partner.user_id = user.id
                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'" 
                            GROUP BY CAST(user.create_time AS DATE);';
                        
//                        $query2 = 'SELECT user.create_time, count(DISTINCT(user.id)) as number_user
//                            FROM user
//                            INNER JOIN user_partner on user_partner.user_id = user.id
//                            INNER JOIN transaction on transaction.user_id = user.id
//                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'" 
//                            AND transaction.type = 3 AND transaction.timestamp <  "'.$endTimeActive.'"
//                            GROUP BY CAST(user.create_time AS DATE);';
                    }
                    
                }else{
                    $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user.id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id);  
                    
                    $criteria2->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user.id';
                    $criteria2->addCondition('user_partner.partner = '.$partner->id); 
                    
                    $query='SELECT user.create_time, count(user.id) as number_user
                            FROM user
                            INNER JOIN user_partner on user_partner.user_id = user.id
                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'"  
                            AND user_partner.partner = "'.$partner->id.'" 
                            GROUP BY CAST(user.create_time AS DATE);';   
//                    $query2 = 'SELECT user.create_time, count(DISTINCT(user.id)) as number_user
//                            FROM user
//                            INNER JOIN user_partner on user_partner.user_id = user.id
//                            INNER JOIN transaction on transaction.user_id = user.id
//                            WHERE user.create_time > "'.$startTime.'" AND user.create_time <= "'.$endTime.'" 
//                            AND user_partner.partner = "'.$partner->id.'" AND transaction.type = 3 AND transaction.timestamp <  "'.$endTimeActive.'"
//                            GROUP BY CAST(user.create_time AS DATE);';
                }
            }

            $countUser = User::model()->count($criteria);                        
            $users=Yii::app()->db->createCommand($query)->queryAll();            
            
            //$usersActive = Yii::app()->db->createCommand($query2)->queryAll();              
            if ($countUser > 0) {
                $countUserActive = 0;                
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                $return['items']['count'] = $countUser;
                $return['items']['page'] = $page;           
                $return['items']['typeStatistic'] = 'New User';
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['totalPage'] = $countUser;
                if (($countUser) % $limit > 0) {
                    $return['items']['totalPage'] = ($countUser - ($countUser % $limit)) / $limit;
                } else
                    $return['items']['totalPage'] = $countUser / $limit - 1;
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();
                $return['items']['detailList'] = array();           
                while ($startDate <= $endDate) {  
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    $return['items']['detail1'][$day] = 0; 
                    $return['items']['detail2'][$day] = 0; 
                    foreach($users as $user ){
                        $date = date('Y-m-d',strtotime($user['create_time']));                    
                        if($day == $date){
                            $return['items']['detail'][$day] = $user['number_user'];
                        }    
                    }    
//                    foreach($usersActive as $userActive ){                        
//                        $date = date('Y-m-d',strtotime($userActive['create_time']));                    
//                        if($day == $date){                
//                            $countUserActive += $userActive['number_user'];
//                            $return['items']['detail1'][$day] = $userActive['number_user'];
//                            $return['items']['detail2'][$day] = $return['items']['detail'][$day] - $userActive['number_user'];
//                        }    
//                    }  
                    $return['items']['detailList'][$day] = array();
                    $startDate += strtotime('+1 day', 0);
                } 
//                $return['items']['countUserActive'] = $countUserActive;
//                $return['items']['countUserInactive'] = $countUser - $countUserActive;
                
                $start_id = $page * $limit;
                $criteria2->offset = $start_id;
                $criteria2->limit = $limit;
                $criteria2->order= 'user.create_time DESC';
                $criteria2->select = "id,username,mobile,chip,user.create_time";
                $userList = User::model()->findAll($criteria2);             
                if (!empty($userList)) {
                    $index = 0;
                    $day_before = date('Y-m-d',strtotime($userList[0]->create_time));
                    foreach ($userList as $items) {                        
                        $day_curr = date('Y-m-d', strtotime($items->create_time));                    
                        if($day_curr < $day_before){
                            $day_before = $day_curr;
                            $index = 0;
                        }

                        $return['items']['detailList'][$day_curr][$index]['id'] = $items->id;
                        $return['items']['detailList'][$day_curr][$index]['username'] = $items->username;
    //                    $return['items']['detailList'][$day_curr][$index]['email'] = $items->email;
    //                        $return['items']['detailList'][$day][$index]['first_name'] = $items->first_name;
    //                        $return['items']['detailList'][$day][$index]['last_name'] = $items->last_name;
    //                        $return['items']['detailList'][$day][$index]['middle_name'] = $items->middle_name;
    //                        $return['items']['detailList'][$day][$index]['birthday'] = $items->birthday;
    //                        $return['items']['detailList'][$day][$index]['gender'] = $items->gender;
    //                    $return['items']['detailList'][$day_curr][$index]['identity_card_number'] = $items->identity_card_number;
                        $return['items']['detailList'][$day_curr][$index]['mobile'] = $items->mobile;
    //                    $return['items']['detailList'][$day_curr][$index]['address'] = $items->address;
    //                        $return['items']['detailList'][$day][$index]['facebook_id'] = $items->facebook_id;
    //                        $return['items']['detailList'][$day][$index]['google_id'] = $items->google_id;
    //                        $return['items']['detailList'][$day][$index]['yahoo_id'] = $items->yahoo_id;
    //                        $return['items']['detailList'][$day][$index]['twitter_id'] = $items->twitter_id;
                        $return['items']['detailList'][$day_curr][$index]['create_time'] = $items->create_time;
    //                    $return['items']['detailList'][$day_curr][$index]['gold'] = $items->gold;
                        $return['items']['detailList'][$day_curr][$index]['chip'] = $items->chip;
    //                        $return['items']['detailList'][$day][$index]['level'] = $items->level;
    //                        $return['items']['detailList'][$day][$index]['experience'] = $items->experience;
    //                    $return['items']['detailList'][$day_curr][$index]['is_virtual_player'] = $items->is_virtual_player;
                        $index ++;
                    }
                }            
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn User nào";
            }
        }
        echo json_encode($return);
    }

    public function actionGetUserActive() {        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {                   
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');        
            $game_id = Yii::app()->request->getParam('game_id');
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                           
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }

            $criteria = new CDbCriteria;            
            $criteria->alias = 'transaction';                    
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';
                $criteria->addCondition("transaction.timestamp > '" . $startTime . "'");
            };
            if (isset($endDate)) {
                $endTime = $endDate. ' 23:59:59';
                $criteria->addCondition("transaction.timestamp < '" . $endTime . "'");
            };
            $criteria->select = 'transaction.user_id';
            $criteria->join = 'INNER JOIN game_log as game_log on transaction.game_log_id = game_log.id';
            if(isset($game_id) && $game_id != 0)
                $criteria->addCondition('game_log.game_id = '.$game_id);
            $criteria->group = 'transaction.user_id';  

            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;            
            $partner = $user->getAdminPartner();            
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){
                        $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = transaction.user_id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);                          
                        
                        $query = "select 
                                    date(`transaction`.`timestamp`) as `date`,
                                    count(DISTINCT `transaction`.user_id) as number_transaction
                                from
                                    `transaction`
                                inner join `game_log` on `game_log`.id = `transaction`.game_log_id    
                                inner join `user_partner` on `user_partner`.user_id = `transaction`.user_id
                                where                                    
                                        `transaction`.`timestamp` > '".$startTime."'
                                        AND `transaction`.`timestamp` < '".$endTime."'";
                                        if(isset($game_id) && $game_id !=0) { $query .= " AND `game_log`.game_id = ".$game_id; }                                                                 
                                        
                                        $query .= " AND `user_partner`.partner = ".$partner_id."
                                group by `date`
                                        ";                                                
                    }else{
                        $query = "select 
                                    date(`transaction`.`timestamp`) as `date`,
                                    count(DISTINCT `transaction`.user_id) as number_transaction
                                from
                                    `transaction`
                                inner join `game_log` on `game_log`.id = `transaction`.game_log_id                                    
                                where                                    
                                        `transaction`.`timestamp` > '".$startTime."'
                                        AND `transaction`.`timestamp` < '".$endTime."'";
                                        if(isset($game_id) && $game_id != 0) { $query .= " AND `game_log`.game_id = ".$game_id; }                                                                                                         
                                        $query .= " group by `date`";                                         
                    }

                }else{
                    $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = transaction.user_id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id);                     

                    $query = "select 
                                    date(`transaction`.`timestamp`) as `date`,
                                    count(DISTINCT `transaction`.user_id) as number_transaction
                                from
                                    `transaction`
                                inner join `game_log` on `game_log`.id = `transaction`.game_log_id    
                                inner join `user_partner` on `user_partner`.user_id = `transaction`.user_id
                                where                                    
                                        `transaction`.`timestamp` > '".$startTime."'
                                        AND `transaction`.`timestamp` < '".$endTime."'";
                                        if(isset($game_id) && $game_id !=0) { $query .= " AND `game_log`.game_id = ".$game_id; }                                                                 
                                        
                                        $query .= " AND `user_partner`.partner = ".$partner->id."
                                group by `date`
                                        ";                       
                }
            }


            $transaction = Transaction::model()->count($criteria);
            $users = Yii::app()->db->createCommand($query)->queryAll();     
            if (!empty($transaction)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                $return['items']['typeStatistic'] = 'User Active';
                $return['items']['game_id'] = $game_id;
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['count'] = $transaction;
                
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();                 
                while ($startDate <= $endDate) {  
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    foreach($users as $user ){
                        $date = date('Y-m-d',strtotime($user['date']));                    
                        if($day == $date){
                            $return['items']['detail'][$day] = $user['number_transaction'];
                        }    
                    }                                    
                    $startDate += strtotime('+1 day', 0);
                } 
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn User nào";
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetNewUserInactive(){
        $return = array();        
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        if(!isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime("7 days ago"));               
        }elseif(!isset($endDate) && isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime($startDate));
            if($startDate > $endDate){
                $return['code'] = -2;
                $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                $return['items'] = array();                                           
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                echo json_encode($return);die;
            }
        }elseif(isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
        }else{
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", strtotime($startDate));  
        }
        
        $criteria = new CDbCriteria;
        $criteria1 = new CDbCriteria;
        if (isset($startDate)) {
            $startTime =$startDate. ' 00:00:00';
            $criteria->addCondition("user.create_time >= '" . $startTime . "'");
            $criteria1->addCondition("create_time >= '" . $startTime . "'");
        };
        if (isset($endDate)) {
            $endTime = $endDate.' 23:59:59';
            $criteria->addCondition("user.create_time <= '" . $endTime . "'");
            $criteria1->addCondition("create_time <= '" . $endTime . "'");
        };
        $criteria->addCondition("transaction.type = 3");
        $criteria->join = 'INNER JOIN user on transaction.user_id = user.id';
        $criteria->group = 'transaction.user_id';
        $criteria->alias = 'transaction';
//        $criteria
        $User = Transaction::model()->count($criteria);         
        $userRegister = User::model()->count($criteria1);     
        
        if (isset($userRegister)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'New User Inactive';            
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = $userRegister - $User;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn User nào";
        }
        echo json_encode($return);
    }
    
    public function actionGetUserLeaveGame(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {                             
            $page = Yii::app()->request->getParam('page');
            $limit = Yii::app()->request->getParam('limit');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            if(!isset($page))
                $page = 0;
            if(!isset($limit))
                $limit = 30;
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                           
                    $return['items']['typeStatistic'] = 'New User';
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            $criteria = new CDbCriteria;
            $criteria2 = new CDbCriteria;
            $criteria->alias = 'user';
            $criteria2->alias = 'user';
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';            
                $criteria->addCondition("user.create_time >= '" . $startTime . "'");
                $criteria2->addCondition("user.create_time >= '" . $startTime . "'");
            }
            if (isset($endDate)) {
                $endTime = $endDate. ' 23:59:59';
                $criteria->addCondition("user.create_time <='" . $endTime . "'");
                $criteria2->addCondition("user.create_time <='" . $endTime . "'");
            }       
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;   
            $partner = $user->getAdminPartner();     
            $datediff = floor((strtotime($endDate) - strtotime($startTime))/(60*60*24));
            $return['datediff'] = $datediff;                       
            
            for($i=0;$i<=$datediff; $i++){
//                $i=0;
                $currentDate = date("Y-m-d", (strtotime($startDate) + $i* 24 * 3600));
                $return['items']['details'][$currentDate]['currentTime'] = $currentDate;
//                $currentStartDate = date("Y-m-d", (strtotime($currentDate) - 7* 24 * 3600));
//                $current8thDateAgo = date("Y-m-d", (strtotime($currentDate) - 8* 24 * 3600));
//                $currentTime = $currentDate. ' 00:00:00';
//                $currentStartTime = $currentStartDate. ' 00:00:00';  
//                $current8thDateAgoTime = $current8thDateAgo. ' 00:00:00';  
//                $return['items']['details'][$currentDate]['currentTime'] = $currentTime;
//                $return['items']['details'][$currentDate]['currentStartTime'] = $currentStartTime;
//                $return['items']['details'][$currentDate]['current8thDateAgoTime'] = $current8thDateAgoTime;
//                $query ="SELECT 
//                            count(DISTINCT `user_game_action`.`user_id`) as `total`
//                        FROM
//                            `user_game_action`
//                        INNER JOIN `game_action` on `user_game_action`.`game_action_id` = `game_action`.`id`
//                        WHERE
//                            `game_action`.`code_identifier` = 'login'
//                            AND `timestamp` > '".$currentStartTime."'
//                            AND `timestamp` <= '".$currentTime."'
//                            AND `user_game_action`.`user_id` IN (
//                                SELECT 
//                                    DISTINCT `user_game_action`.`user_id`
//                                FROM
//                                    `user_game_action`
//                                INNER JOIN `game_action` on `user_game_action`.`game_action_id` = `game_action`.`id`
//                                WHERE
//                                    `game_action`.`code_identifier` = 'login'
//                                    AND `timestamp` > '".$current8thDateAgoTime."'
//                                    AND `timestamp` <= '".$currentStartTime."'
//                            )";  
//                $query2 = "SELECT 
//                                count(DISTINCT `user_game_action`.`user_id`) as `total`
//                            FROM
//                                `user_game_action`
//                            INNER JOIN `game_action` on `user_game_action`.`game_action_id` = `game_action`.`id`
//                            WHERE
//                                `game_action`.`code_identifier` = 'login'
//                                AND `timestamp` > '".$current8thDateAgoTime."'
//                                AND `timestamp` <= '".$currentStartTime."'";  
//                $currentLogin = Yii::app()->db->createCommand($query)->queryAll();   
//                $currentLogin8thAgo = Yii::app()->db->createCommand($query2)->queryAll();             
                //$return['items']['detail'][$currentDate] = $currentLogin8thAgo[0]['total'] - $currentLogin[0]['total'];  
                $leaveUsers = Yii::app()->db->createCommand("call count_users_leave_us_at(adddate('".$currentDate."', 0))")->queryAll();
                $return['items']['detail'][$currentDate] = $leaveUsers[0]['count'];
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                //Common::dump($leaveUsers);die;
            }
            $countUser = User::model()->count($criteria);     
        }
        echo json_encode($return);
    }
    
    public function actionGetUserLeaveGameOld(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {               
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            $before = Yii::app()->request->getParam('before');
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                           
                    $return['items']['typeStatistic'] = 'New User';
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            $criteria = new CDbCriteria;
            $criteria->alias = 'user_game_action';
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';        
                $criteria->addCondition("timestamp > '" . $startTime . "'");
            }
            if (isset($endDate)) {
                $endTime = $endDate. ' 23:59:59';
                $criteria->addCondition("timestamp < '" . $endTime . "'");
            }
            
            $criteria->join = 'INNER JOIN game_action as game_action on game_action.id = user_game_action.game_action_id';
            $criteria->addCondition('game_action.code_identifier = "login"');              
            $criteria->group = 'user_game_action.user_id';
            $totalUser = UserGameAction::model()->findAll($criteria);
            $countUser = count($totalUser);
            
            $query = "select 
                        date(`timestamp`) as `date`,
                        count(DISTINCT user_id) as number_action
                    from
                        `user_game_action`
                    where
                        `user_game_action`.`game_action_id` = 38
                            AND `timestamp` > '".$startTime."'
                            AND `timestamp` < '".$endTime."'
                    group by `date`
                            ";
                                        
            $user_game_actions = Yii::app()->db->createCommand($query)->queryAll();                                                
            if ($countUser > 0) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['count'] = $countUser;
                $return['items'] = array();
                $return['items']['typeStatistic'] = 'Number User login';
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['count'] = $countUser;
                
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();                 
                while ($startDate <= $endDate) {  
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    foreach($user_game_actions as $user_game_action ){
                        $date = date('Y-m-d',strtotime($user_game_action['date']));                    
                        if($day == $date){
                            $return['items']['detail'][$day] = $user_game_action['number_action'];
                        }    
                    }                                    
                    $startDate += strtotime('+1 day', 0);
                }  
            } else {
                $return['code'] = 1;
                $return['message'] = "Không có action nào";
            }
        }
        echo json_encode($return);
    }

    public function actionGetNewUserPlay() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if ($gameStatistic != 0) {
            $criteria->addCondition("game_log.game_id = '" . $gameStatistic . "'");
        };
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("user.create_time > '" . $startDate . "'");
        };
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("user.create_time < '" . $endDate . "'");
        };
        $criteria->join = 'INNER JOIN game_log on transaction.game_log_id = game_log.id INNER JOIN user on transaction.user_id = user.id';
        $criteria->group = 'transaction.user_id';
        $criteria->alias = 'transaction';
//        $criteria
        $User = Transaction::model()->findAll($criteria);
        if (!empty($User) > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'New User';
            $return['items']['gameStatistic'] = $gameStatistic;
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = count($User);
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn User nàooo";
        }
        echo json_encode($return);
    }        

    public function actionGetTax() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if ($gameStatistic != 0) {
            $criteria->addCondition("game_log.game_id = '" . $gameStatistic . "'");
        };
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("transaction.timestamp > '" . $startDate . "'");
        };
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("transaction.timestamp < '" . $endDate . "'");
        };
        $config = Configuration::model()->find('name=:name',array(':name'=>'percent_tax'));
        if(isset($config)){
            $criteria->addCondition("transaction.type = 3 ");        
            $criteria->join = 'INNER JOIN game_log on transaction.game_log_id = game_log.id';
            $criteria->alias = 'transaction';
            $criteria->select = 'sum(transaction.value) AS total';
            $criteria->addCondition("transaction.value > 0");
            $criteria->addCondition("transaction.user_id != 0");
            $total = Transaction::model()->findAll($criteria);
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Total Tax';
            $return['items']['gameStatistic'] = $gameStatistic;
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;        
            $return['items']['count'] = $total[0]->total * ($config->value / 100);
        }
        echo json_encode($return);
    }

    public function actionGetCurrentSystemChip() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $sum = Yii::app()->db->createCommand("SELECT SUM(`chip`) AS `total_money` FROM `user` where username = 'system'")->queryAll();
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Current System Chip';
            $return['items']['count'] = $sum[0]['total_money'];
        }
        echo json_encode($return);
    }

    public function actionGetTotalUserChip() {
        $return = array();        
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $sum = Yii::app()->db->createCommand("SELECT SUM(`chip`) AS `total_money` FROM `user` where username != 'system' and is_virtual_player = 0")->queryAll();
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Total User Chip';
            $return['items']['count'] = $sum[0]['total_money'];
        }
        echo json_encode($return);
    }

    public function actionGetTotalVitualPlayerChip() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $sum = Yii::app()->db->createCommand("SELECT SUM(`chip`) AS `total_money` FROM `user` where is_virtual_player = 1")->queryAll();
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Total Vitual Player Chip';
            $return['items']['count'] = $sum[0]['total_money'];
        }
        echo json_encode($return);
    }

    public function actionGetTotalRechargeChip() {
        $return = array();
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $query = "SELECT SUM(`value`) AS `total` FROM `transaction` where type IN (2,10)";
        
        if(!isset($page))
            $page = 0;
        if(!isset($limit))
            $limit = 30;
        if(!isset($endDate)&&!isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime("7 days ago"));
        };
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $query .= 'AND timestamp > "' . $startDate . '"';
        };
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $query .= 'AND timestamp < "' . $endDate . '"';
        };
        $sum = Yii::app()->db->createCommand($query)->queryAll();
        $return['code'] = 0;
        $return['message'] = "Thành công";
        $return['items'] = array();
        $return['items']['typeStatistic'] = 'Total Recharge Chip';
        $return['items']['count'] = $sum[0]['total'];
        echo json_encode($return);
    }

    public function actionGetTotalDallyChip() {
        $return = array();
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $query = "SELECT SUM(`value`) AS `total` FROM `transaction` where type IN (1)";
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $query .= 'AND timestamp > "' . $startDate . '"';
        };
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $query .= 'AND timestamp < "' . $endDate . '"';
        };
        $sum = Yii::app()->db->createCommand($query)->queryAll();
        $return['code'] = 0;
        $return['message'] = "Thành công";
        $return['items'] = array();
        $return['items']['typeStatistic'] = 'Total Dally Chip';
        $return['items']['count'] = $sum[0]['total'];
        echo json_encode($return);
    }

    public function actionGetTotalAdminPushChip() {
        $return = array();        
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        if(!isset($page))
            $page = 0;
        if(!isset($limit))
            $limit = 30;
        
        if(!isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime("7 days ago"));               
        }elseif(!isset($endDate) && isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime($startDate));
            if($startDate > $endDate){
                $return['code'] = -2;
                $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                $return['items'] = array();                                           
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                echo json_encode($return);die;
            }
        }elseif(isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
        }else{
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", strtotime($startDate));  
        }
        $criteria = new CDbCriteria;    
        $criteria2 = new CDbCriteria;
        $criteria->alias = "admin_action_log";
        $criteria->join = "INNER JOIN admin_action_type on admin_action_log.type_id = admin_action_type.id";
        if (isset($startDate)) {
            $startTime = $startDate. ' 00:00:00';
            $criteria->addCondition('admin_action_log.timestamp >= "' . $startTime . '"');            
            $criteria2->addCondition('timestamp >= "' . $startTime . '"'); 
        };
        if (isset($endDate)) {
            $endTime = $endDate.' 23:59:59';
            $criteria->addCondition('admin_action_log.timestamp <= "' . $endTime . '"');      
            $criteria2->addCondition('timestamp <= "' . $endTime . '"');       
        };        
        $criteria->addCondition('admin_action_type.code_identifier = "push_chips"'); 
        $criteria2->addCondition('type = 11');
        $countTransaction = Transaction::model()->count($criteria2);
        $criteria2->select = "sum(value) as total";
        $total = Transaction::model()->findAll($criteria2);            
        $start_id = $page * $limit;
        $criteria->offset = $start_id;
        $criteria->limit = $limit;
        $criteria->order = "admin_action_log.timestamp DESC";
        $transactionList = AdminActionLog::model()->findAll($criteria);   
        if ($countTransaction > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();            
            $return['items']['total'] = $total[0]['total'];
            $return['items']['page'] = $page;           
            $return['items']['typeStatistic'] = 'Total Admin Push Chip';
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;  
            if(isset($transactionList)){
                $return['items']['detailList'] = array();
                foreach($transactionList as $index => $item){
                    $content = json_decode($item->content);                    
                    $return['items']['detailList'][$index]['id'] = $item->id;
                    $return['items']['detailList'][$index]['user_push'] = $item->user->username;
                    $return['items']['detailList'][$index]['user_receiver'] = $content->user_receiver;
                    $return['items']['detailList'][$index]['value'] = $content->value;
                    $return['items']['detailList'][$index]['description'] = $content->description;
                    $return['items']['detailList'][$index]['transaction_id'] = $content->transaction_id;
                    $return['items']['detailList'][$index]['timestamp'] = $item->timestamp;
                }
            }
                        
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn Transaction nào";
        }
         echo json_encode($return);
    }

    public function actionGetTotalGold() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $partnerGoldStatistic = Yii::app()->request->getParam('partnerGoldStatistic');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            $type = Yii::app()->request->getParam('rechargeType');
            $provider = Yii::app()->request->getParam('provider');
            $page = Yii::app()->request->getParam('page');
            if(!isset($page))
                $page = 0;
            $limit = Yii::app()->request->getParam('limit');
            $criteria = new CDbCriteria;
            $criteria2 = new CDbCriteria;            
            $criteria->alias = 'recharge';
            $criteria2->alias = 'recharge';
            
            if ($partnerGoldStatistic != 0 && isset($partnerGoldStatistic)) {
                $criteria->addCondition("recharge_type.id = '" . $partnerGoldStatistic . "'");
            };
            
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                           
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }                 
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';
                $criteria->addCondition('recharge.create_time >= "' . $startTime . '"'); 
                $str_condition = 'recharge.create_time >= "' . $startTime . '"';                
            };
            if (isset($endDate)) {
                $endTime = $endDate.' 23:59:59';
                $criteria->addCondition('recharge.create_time <= "' . $endTime . '"'); 
                $str_condition .= ' AND recharge.create_time <= "' . $endTime . '"';                 
            };
            $criteria->join = 'INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id';            
            $criteria->join .= ' INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id';
            $criteria->join .= ' INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id ';
            $criteria->join .= ' INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id ';
            
            $criteria2->join = 'INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id';
            $criteria2->join .= ' INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id';
            $criteria2->join .= ' INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id ';
            $criteria2->join .= ' INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id ';
            $str2 = "";
            if($provider){
                $criteria->addCondition('topup_provider.code_identifier="'.$provider.'"'); 
                $str_condition .= ' AND topup_provider.code_identifier="'.$provider.'"';                
                $str2 = ' AND topup_provider.code_identifier="'.$provider.'"';
            }
            $str = "";
            if(!empty($type)){                               
               $criteria->addCondition('recharge_method.code_identifier="'.$type.'"');
               $str_condition .= ' AND recharge_method.code_identifier="'.$type.'"';               
               $str = ' AND recharge_method.code_identifier="'.$type.'"';
            }                                                       
                        
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;            
            $partner = $user->getAdminPartner();               
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){
                        $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);
                        
                        $criteria2->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                        $str_condition .= ' AND user_partner.partner = '.$partner_id;                        
                        
                        $query='SELECT recharge.create_time, sum(recharge.value) as total_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id 
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id
                            INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id 
                            INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'" '.$str.$str2.' 
                            AND user_partner.partner = "'.$partner_id.'" 
                            GROUP BY CAST(recharge.create_time AS DATE);';
                    }else{
                        $query='SELECT recharge.create_time, sum(recharge.value) as total_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id 
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id
                            INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id 
                            INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'" '.$str.$str2.'                             
                            GROUP BY CAST(recharge.create_time AS DATE);';
                    }                    
                }else{
                    $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id); 
                    
                    $criteria2->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                    $str_condition .= ' AND user_partner.partner = '.$partner->id;                    
                    
                    $query='SELECT recharge.create_time, sum(recharge.value) as total_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id 
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id
                            INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id 
                            INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'" '.$str.$str2.' 
                            AND user_partner.partner = "'.$partner->id.'" 
                            GROUP BY CAST(recharge.create_time AS DATE);';
                }
            }
            
            if($provider == 'vmg'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria2,'vmg','vmg', $str_condition);  
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig']; 
                $optionConfigSmsVmg = $calculatorRealRecharge['optConfigSms'];
            }elseif($provider == 'fibo'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria2,'fibo','', $str_condition); 
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig'];    
                $optionConfigSmsFibo = $calculatorRealRecharge['optConfigSms'];
            }elseif($provider == 'nganluong'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria2,'','nganluong', $str_condition);                 
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig'];   
                $optionConfigSmsNL = $calculatorRealRecharge['optConfigSms'];
            }elseif($provider == 'appota'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria2,'appota','appota', $str_condition);  
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig']; 
                $optionConfigSmsAppota = $calculatorRealRecharge['optConfigSms'];
            }else{                                              
                $calculatorRealRechargeNL = $this->calculatorRealRecharge($criteria2,'','nganluong', $str_condition);                   
                $calculatorRealRechargeFibo = $this->calculatorRealRecharge($criteria2,'fibo','', $str_condition);                                  
                $calculatorRealRechargeVmg = $this->calculatorRealRecharge($criteria2,'vmg','vmg', $str_condition); 
                $calculatorRealRechargeAppota = $this->calculatorRealRecharge($criteria2,'appota','appota', $str_condition);  
                $totalCard = $calculatorRealRechargeNL['totalCard'] + $calculatorRealRechargeFibo['totalCard'] + $calculatorRealRechargeVmg['totalCard'] + $calculatorRealRechargeAppota['totalCard'];
                $totalSms = $calculatorRealRechargeNL['totalSms'] + $calculatorRealRechargeFibo['totalSms'] + $calculatorRealRechargeVmg['totalSms']+ $calculatorRealRechargeAppota['totalSms'];
                $totalReal = $calculatorRealRechargeNL['totalReal'] + $calculatorRealRechargeFibo['totalReal'] + $calculatorRealRechargeVmg['totalReal'] + $calculatorRealRechargeAppota['totalReal'];
                $optConfigNL = $calculatorRealRechargeNL['optConfig']; 
                $optConfigFibo = $calculatorRealRechargeFibo['optConfig']; 
                $optConfigVmg = $calculatorRealRechargeVmg['optConfig'];
                $optConfigAppota = $calculatorRealRechargeAppota['optConfig'];
                $optionConfigSmsNL = $calculatorRealRechargeNL['optConfigSms'];
                $optionConfigSmsFibo = $calculatorRealRechargeFibo['optConfigSms'];
                $optionConfigSmsVmg = $calculatorRealRechargeVmg['optConfigSms'];
                $optionConfigSmsAppota = $calculatorRealRechargeAppota['optConfigSms'];
            }
            
            $return['items'] = array();
            $return['items']['totalCard'] = $totalCard;            
            $return['items']['totalSms'] = $totalSms;
            $return['items']['totalReal'] = $totalReal; 
            //$criteria->select = 'sum(recharge.value) AS total';                        
            
            $total = Recharge::model()->count($criteria);              
            if(isset($query))
                $rechargeModel = Yii::app()->db->createCommand($query)->queryAll();              
            $return['code'] = 0;
            $return['message'] = "Thành công";            
            $return['items']['count'] = 0;
            $return['items']['typeGoldStatistic'] = 'Số tiền nạp vào hệ thống';
            $return['items']['partnerGoldStatistic'] = $partnerGoldStatistic;
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['totalPage'] = 0;
            if (($total) % $limit > 0) {
                $return['items']['totalPage'] = ($total - ($total % $limit)) / $limit;
            } else
                $return['items']['totalPage'] = $total / $limit - 1;
            $return['items']['detail'] = array();
            $startDate = strtotime($startDate);
            $endDate = strtotime($endDate);
            $days = array();
            $return['items']['detailList'] = array();           
            while ($startDate <= $endDate) {  
                $day = date('Y-m-d', $startDate); 
                $return['items']['detail'][$day] = 0;                 
                foreach($rechargeModel as $item ){
                    $date = date('Y-m-d',strtotime($item['create_time']));                    
                    if($day == $date){
                        $return['items']['count'] += $item['total_recharge'];
                        $return['items']['detail'][$day] = $item['total_recharge'];
                    }    
                }                     
                $return['items']['detailList'][$day] = array();
                $startDate += strtotime('+1 day', 0);
            }                        
            
            $start_id = $page * $limit;            
            $criteria2->order = "create_time DESC";            
            $criteria2->offset = $start_id;
            $criteria2->limit = $limit;
            $criteria2->condition = $str_condition;
            $rechargeList = Recharge::model()->findAll($criteria2);             
            if (!empty($rechargeList)) {
                $index = 0;
                $day_before = date('Y-m-d',strtotime($rechargeList[0]->create_time));
                foreach ($rechargeList as $items) {                        
                    $day_curr = date('Y-m-d', strtotime($items->create_time));                    
                    if($day_curr < $day_before){
                        $day_before = $day_curr;
                        $index = 0;
                    }
                    $return['items']['detailList'][$day_curr][$index]['id'] = $items->id;   
                    $return['items']['detailList'][$day_curr][$index]['Kenhnap'] = $items->rechargeType->rechargeMethodProvider->name;
                    $return['items']['detailList'][$day_curr][$index]['NhaMang'] = $items->rechargeType->telco->name;
                    $return['items']['detailList'][$day_curr][$index]['username'] = $items->user->username;                   
                    $return['items']['detailList'][$day_curr][$index]['Sotienkhachhang'] = $items->value;
                    if($items->rechargeType->rechargeMethodProvider->rechargeMethod->code_identifier == "sms")
                        $return['items']['detailList'][$day_curr][$index]['Sotienthucnhan'] = $items->rechargeType->real_value; 
                    else{                       
                        if(!$provider){
                            if($items->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'nganluong')
                                $return['items']['detailList'][$day_curr][$index]['Sotienthucnhan'] = $items->rechargeType->real_value * ((100 - $optConfigNL[$items->rechargeType->telco->code_identifier]) / 100); 
                            if($items->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'vmg')
                                $return['items']['detailList'][$day_curr][$index]['Sotienthucnhan'] = $items->rechargeType->real_value * ((100 - $optConfigVmg[$items->rechargeType->telco->code_identifier]) / 100); 
                            if($items->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'appota')
                                $return['items']['detailList'][$day_curr][$index]['Sotienthucnhan'] = $items->rechargeType->real_value * ((100 - $optConfigAppota[$items->rechargeType->telco->code_identifier]) / 100); 
                        }else{
                            if($provider == 'nganluong' || $provider == 'vmg' || $provider == 'appota')
                                $return['items']['detailList'][$day_curr][$index]['Sotienthucnhan'] = $items->rechargeType->real_value * ((100 - $optConfig[$items->rechargeType->telco->code_identifier]) / 100); 
                        }
                    }
                    $return['items']['detailList'][$day_curr][$index]['gold'] = $items->gold;
                    if($partner->code_identifier == "esimo"){                        
                        $partner_id = Yii::app()->request->getParam('partner_id');
                        if(isset($partner_id)){
                            if($partner_id != 0){
                                $return['items']['detailList'][$day_curr][$index]['esimo'] = $items->value - ($items->value * $items->ratio_partner);
                                $return['items']['detailList'][$day_curr][$index]['partner'] = $items->value * $items->ratio_partner;
                            }else{
                                $return['items']['detailList'][$day_curr][$index]['esimo'] = $items->value * $items->ratio_partner;
                                $return['items']['detailList'][$day_curr][$index]['partner'] = 0;
                            }
                        }else{ 
                            if($partner_id != 0){
                                $return['items']['detailList'][$day_curr][$index]['esimo'] = $items->value - ($items->value * $items->ratio_partner);                                
                            }else{
                                $return['items']['detailList'][$day_curr][$index]['esimo'] = $items->value * $items->ratio_partner;                                
                            }                            
                        }
                    }else{
                        $return['items']['detailList'][$day_curr][$index]['partner'] = $items->value * $items->ratio_partner;
                    }
                    
                    $return['items']['detailList'][$day_curr][$index]['create_time'] = $items->create_time;
                    $index ++;
                }
            }                         
        }
        echo json_encode($return);
    }

    public function actionGetTotalRecharge() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $partnerGoldStatistic = Yii::app()->request->getParam('partnerGoldStatistic');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            $type = Yii::app()->request->getParam('rechargeType');
            $page = Yii::app()->request->getParam('page');
            $provider = Yii::app()->request->getParam('provider');
            if(!isset($page))
                $page = 0;
            $limit = Yii::app()->request->getParam('limit');            
            $criteria = new CDbCriteria;            
            $criteria->alias = 'recharge';            
            if ($partnerGoldStatistic != 0 && isset($partnerGoldStatistic)) {
                $criteria->addCondition("recharge_type.id = '" . $partnerGoldStatistic . "'");
            };
            
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                           
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }                 
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';            
                $criteria->addCondition('recharge.create_time >= "' . $startTime . '"'); 
            };
            if (isset($endDate)) {
                $endTime = $endDate.' 23:59:59';      
                $criteria->addCondition('recharge.create_time <= "' . $endTime . '"'); 
            };
            $criteria->join = 'INNER JOIN recharge_type on recharge.recharge_type_id = recharge_type.id';
            $str = "";
            if(!empty($type)){                               
               $criteria->addCondition('recharge_type.type="'.$type.'"'); 
               $str = ' AND recharge_type.type="'.$type.'"';
            }
                        
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;            
            $partner = $user->getAdminPartner();               
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){        
                        $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);
                        
                         $query = 'select user_id, username, sum(value) as total, sum(gold) as total_gold, `date` 
                                from 
                                    (select `recharge`.user_id, `user`.username, recharge.`value`, recharge.`gold`, date(recharge.`create_time`) as `date` from `recharge` 
                                        inner join `user_partner` on `user_partner`.user_id = `recharge`.user_id 
                                        inner join `user` on `user`.id = `recharge`.user_id 
                                        inner join `recharge_type` on `recharge_type`.id = `recharge`.recharge_type_id 
                                        where date(`recharge`.`create_time`) between "'.$startTime.'" and "'.$endTime.'" AND `user_partner`.partner = "'.$partner_id.'" '.$str.' order by `recharge`.create_time) as tbl                                            
                                group by user_id, `date`;';
                    }else{
                         $query = 'select user_id, username, sum(value) as total, sum(gold) as total_gold, `date` 
                                from 
                                    (select user.username, recharge.user_id, recharge.`value`, recharge.`gold`, date(recharge.`create_time`) as `date` from `recharge`  
                                    inner join `user` on `user`.id = `recharge`.user_id 
                                    inner join `recharge_type` on `recharge_type`.id = `recharge`.recharge_type_id 
                                        where date(`recharge`.`create_time`) between "'.$startTime.'" and "'.$endTime.'" '.$str.'  order by `recharge`.create_time) as tbl                                 
                                group by user_id, `date`;';
                    }                    
                }else{          
                    $criteria->join .= ' INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id);
                    $query = 'select user_id,username, sum(value) as total, sum(gold) as total_gold, `date` 
                                from 
                                    (select `recharge`.user_id, `user`.username, recharge.`value`, recharge.`gold`, date(recharge.`create_time`) as `date` from `recharge` 
                                        inner join `user_partner` on `user_partner`.user_id = `recharge`.user_id
                                        inner join `user` on `user`.id = `recharge`.user_id 
                                        inner join `recharge_type` on `recharge_type`.id = `recharge`.recharge_type_id 
                                        where date(`recharge`.`create_time`) between "'.$startTime.'" and "'.$endTime.'" AND `user_partner`.partner = "'.$partner->id.'" '.$str.' order by `recharge`.create_time) as tbl                                            
                                group by user_id, `date`;';                                       
                }
            }  
            $criteria->select = 'recharge.user_id';
            $criteria->group = 'recharge.user_id';
            $countUser = Recharge::model()->count($criteria);
            if(!isset($countUser))
                $countUser = 0;
                        
            if(isset($query))
                $rechargeModel = Yii::app()->db->createCommand($query)->queryAll();  
            $total = count($rechargeModel);            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['count'] = $countUser;
            $return['items']['typeGoldStatistic'] = 'Số tài khoản nạp tiền';
            $return['items']['partnerGoldStatistic'] = $partnerGoldStatistic;
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['totalPage'] = 0;
            if (($total) % $limit > 0) {
                $return['items']['totalPage'] = ($total - ($total % $limit)) / $limit;
            } else
                $return['items']['totalPage'] = $total / $limit - 1;
            $return['items']['detail'] = array();
            $startDate = strtotime($startDate);
            $endDate = strtotime($endDate);
            $days = array();
            $return['items']['detailList'] = array();           
            while ($startDate <= $endDate) {  
                $day = date('Y-m-d', $startDate); 
                $return['items']['detail'][$day] = 0;                 
                foreach($rechargeModel as $item ){
                    $date = date('Y-m-d',strtotime($item['date']));                    
                    if($day == $date){                        
                        $return['items']['detail'][$day] ++;
                    } 
                    
                }                     
                $return['items']['detailList'][$day] = array();
                $startDate += strtotime('+1 day', 0);
            }
            function sortFunction( $a, $b ) {
                return strtotime($b["date"]) - strtotime($a["date"]);
            }
            usort($rechargeModel, "sortFunction");  
            
            $start_id = $page * $limit;                      
            if (!empty($rechargeModel)) {
                $index = 0;                
                $day_before = date('Y-m-d',strtotime($rechargeModel[0]['date']));
                foreach ($rechargeModel as $i => $items) {     
                    if($i >= ($page * $limit)){
                        $day_curr = date('Y-m-d', strtotime($items['date']));                    
                        if($day_curr < $day_before){
                            $day_before = $day_curr;
                            $index = 0;
                        }
                        $return['items']['detailList'][$day_curr][$index]['user_id'] = $items['user_id'];
                        $return['items']['detailList'][$day_curr][$index]['username'] = $items['username'];                                              
                        $return['items']['detailList'][$day_curr][$index]['Sotienkhachhang'] = $items['total'];
                        $return['items']['detailList'][$day_curr][$index]['gold'] = $items['total_gold'];                    

                        $return['items']['detailList'][$day_curr][$index]['date'] = $items['date'];                    
                        $index ++;
                        if($i >= (($page +1) * $limit))
                            break;
                    }
                }
            }                         
        }
        echo json_encode($return);
    }

    public function actionGetTimeRecharge() {        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $partnerGoldStatistic = Yii::app()->request->getParam('partnerGoldStatistic');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            $type = Yii::app()->request->getParam('rechargeType');
            $page = Yii::app()->request->getParam('page');
            if(!isset($page))
                $page = 0;
            $limit = Yii::app()->request->getParam('limit');
            if(!isset($limit))
                $limit = 30;
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                               
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            $criteria = new CDbCriteria;
            $criteria2 = new CDbCriteria;
            $criteria->alias = 'recharge';
            $criteria2->alias = 'recharge';

            if ($partnerGoldStatistic != 0 && isset($partnerGoldStatistic)) {
                $criteria->addCondition("recharge_type.id = '" . $partnerGoldStatistic . "'");
                $rechargeTypeName = RechargeType::model()->find("id = '".$partnerGoldStatistic."'");
                $return['items']['partnerGoldStatistic'] = $rechargeTypeName->name;
            };
            if (isset($startDate)) {
                $startTime =$startDate.' 00:00:00';
                $criteria->addCondition("recharge.create_time >= '" . $startTime . "'");
                $criteria2->addCondition("recharge.create_time >= '" . $startTime . "'");
            };
            if (isset($endDate)) {
                $endTime =$endDate. ' 23:59:59';
                $criteria->addCondition("recharge.create_time <= '" . $endTime . "'");
                $criteria2->addCondition("recharge.create_time <= '" . $endTime . "'");
            };
            $criteria->join = 'INNER JOIN recharge_type on recharge.recharge_type_id = recharge_type.id';  
            $criteria2->join = 'INNER JOIN recharge_type on recharge.recharge_type_id = recharge_type.id';  
            $str ="";
            if(!empty($type)){
                $criteria->addCondition ('recharge_type.type="'.$type.'"');
                $criteria2->addCondition ('recharge_type.type="'.$type.'"');
                $str = ' AND recharge_type.type="'.$type.'"';
            }
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;       
            $partner = $user->getAdminPartner();            
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){
                        $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);   

                        $criteria2->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                        $criteria2->addCondition('user_partner.partner = '.$partner_id);                        
                        $query='SELECT recharge.create_time, count(recharge.id) as number_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'"'.$str.' 
                            AND user_partner.partner = "'.$partner_id.'"     
                            GROUP BY CAST(recharge.create_time AS DATE);';
                    }else{
                       $query='SELECT recharge.create_time, count(recharge.id) as number_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'"'.$str.'                            
                            GROUP BY CAST(recharge.create_time AS DATE);';
                    }

                }else{
                    $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id);  

                    $criteria2->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = recharge.user_id';
                    $criteria2->addCondition('user_partner.partner = '.$partner->id); 

                    $query='SELECT recharge.create_time, count(recharge.id) as number_recharge
                            FROM recharge
                            INNER JOIN user_partner on user_partner.user_id = recharge.user_id
                            INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id 
                            WHERE recharge.create_time > "'.$startTime.'" AND recharge.create_time <= "'.$endTime.'"'.$str.' 
                            AND user_partner.partner = "'.$partner->id.'"     
                            GROUP BY CAST(recharge.create_time AS DATE);';                        
                }
            }

            $total = Recharge::model()->count($criteria);        
            $recharges=Yii::app()->db->createCommand($query)->queryAll();     
            if ($total > 0) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                $return['items']['count'] = $total;
                $return['items']['page'] = $page;           
                $return['items']['typeGoldStatistic'] = 'Số lượng giao dịch nạp tiền';
//                if(isset($partnerGoldStatistic))
//                    $return['items']['partnerGoldStatistic'] = $rechargeTypeName->rechargeType->name;;
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['totalPage'] = 0;
                if (count($total) % $limit > 0) {
                    $return['items']['totalPage'] = ($total - ($total % $limit)) / $limit;
                } else
                    $return['items']['totalPage'] = $total / $limit - 1;
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();
                $return['items']['detailList'] = array();           
                while ($startDate <= $endDate) {  
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    foreach($recharges as $recharge ){
                        $date = date('Y-m-d',strtotime($recharge['create_time']));                    
                        if($day == $date){
                            $return['items']['detail'][$day] = $recharge['number_recharge'];
                        }    
                    }                
                    $return['items']['detailList'][$day] = array();
                    $startDate += strtotime('+1 day', 0);
                }   

                if ($partnerGoldStatistic != 0 && isset($partnerGoldStatistic)) {   
                    $criteria2->addCondition("recharge_type_id = '" . $partnerGoldStatistic . "'");
                };
                $start_id = $page * $limit;
                $criteria2->offset = $start_id;
                $criteria2->limit = $limit;
                $criteria2->order = "create_time DESC";
                $rechargeList = Recharge::model()->findAll($criteria2);             
                if (!empty($rechargeList)) {
                    $index = 0;
                    $day_before = date('Y-m-d',strtotime($rechargeList[0]->create_time));
                    foreach ($rechargeList as $items) {                        
                        $day_curr = date('Y-m-d', strtotime($items->create_time));                    
                        if($day_curr < $day_before){
                            $day_before = $day_curr;
                            $index = 0;
                        }
                        $return['items']['detailList'][$day_curr][$index]['id'] = $items->id;
                        $return['items']['detailList'][$day_curr][$index]['user_id'] = $items->user->id;                   
                        $return['items']['detailList'][$day_curr][$index]['username'] = $items->user->username;                   
                        $return['items']['detailList'][$day_curr][$index]['value'] = $items->value;
                        $return['items']['detailList'][$day_curr][$index]['gold'] = $items->gold;
                        $return['items']['detailList'][$day_curr][$index]['ratio'] = $items->ratio;
                        $return['items']['detailList'][$day_curr][$index]['Kenhnap'] = $items->rechargeType->rechargeMethodProvider->name;
                        $return['items']['detailList'][$day_curr][$index]['NhaMang'] = $items->rechargeType->telco->name;                        
                        $return['items']['detailList'][$day_curr][$index]['create_time'] = $items->create_time;
                        $index ++;
                    }
                }            
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn Recharge nào";
            }
        }
        
        echo json_encode($return);
    }
    
    

    public function actionGetTotalGame() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if ($gameStatistic != 0) {
            $criteria->addCondition("game_id = '" . $gameStatistic . "'");
        };
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("timestamp > '" . $startDate . "'");
        };
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("timestamp < '" . $endDate . "'");
        };
//        $criteria->group = 'transaction.user_id';
//        $criteria
        $game = GameLog::model()->count($criteria);
        if ($game > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Tổng số ván chơi';
            $return['items']['gameStatistic'] = $gameStatistic;
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = $game;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn Game nàooo";
        }
        echo json_encode($return);
    }

    public function actionGetTotalTimeLogin() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("timestamp > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("timestamp < '" . $endDate . "'");
        }
        $criteria->addCondition("game_action_id = 38");
        $countUser = UserGameAction::model()->count($criteria);
        if ($countUser > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['count'] = $countUser;
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Total time login';
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = $countUser;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn User nàooo";
        }
        echo json_encode($return);
    }

    public function actionGetTotalTimeLoginOld() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("timestamp > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("timestamp < '" . $endDate . "'");
        }
        $criteria->addCondition("action_id = 1");
        $countUser = UserAction::model()->count($criteria);
        if ($countUser > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['count'] = $countUser;
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Total time login';
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = $countUser;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn User nàooo";
        }
        echo json_encode($return);
    }

    public function actionGetNumberUserLogin() {        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {   
            $gameStatistic = Yii::app()->request->getParam('gameStatistic');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                           
                    $return['items']['typeStatistic'] = 'New User';
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            
            $criteria = new CDbCriteria;
            $criteria->alias = 'user_game_action';
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';        
                $criteria->addCondition("user_game_action.timestamp > '" . $startTime . "'");
            }
            if (isset($endDate)) {
                $endTime = $endDate. ' 23:59:59';
                $criteria->addCondition("user_game_action.timestamp < '" . $endTime . "'");
            }
            
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;       
            $partner = $user->getAdminPartner();            
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){
                        $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user_game_action.user_id';
                        $criteria->addCondition('user_partner.partner = '.$partner_id);                          

                        $query = "select 
                                        date(user_game_action.`timestamp`) as `date`,
                                        count(DISTINCT user_game_action.user_id) as number_action
                                    from
                                        `user_game_action`
                                    inner join user_partner on user_partner.user_id = user_game_action.user_id    
                                    where
                                        `user_game_action`.`game_action_id` = 38
                                            AND user_game_action.`timestamp` > '".$startTime."'
                                            AND user_game_action.`timestamp` < '".$endTime."'
                                            AND user_partner.partner = '".$partner_id."'      
                                    group by `date`
                                            ";                        
                    }else{
                       $query = "select 
                                    date(`timestamp`) as `date`,
                                    count(DISTINCT user_id) as number_action
                                from
                                    `user_game_action`
                                where
                                    `user_game_action`.`game_action_id` = 38
                                        AND `timestamp` > '".$startTime."'
                                        AND `timestamp` < '".$endTime."'
                                group by `date`
                                        ";
                    }

                }else{
                    $criteria->join = 'INNER JOIN user_partner as user_partner on user_partner.user_id = user_game_action.user_id';
                    $criteria->addCondition('user_partner.partner = '.$partner->id);                     

                    $query = "select 
                                    date(user_game_action.`timestamp`) as `date`,
                                    count(DISTINCT user_game_action.user_id) as number_action
                                from
                                    `user_game_action`
                                inner join user_partner on user_partner.user_id = user_game_action.user_id    
                                where
                                    `user_game_action`.`game_action_id` = 38
                                        AND user_game_action.`timestamp` > '".$startTime."'
                                        AND user_game_action.`timestamp` < '".$endTime."'
                                        AND user_partner.partner = '".$partner->id."'      
                                group by `date`
                                        ";                         
                }
            }
                                    
            $criteria->join .= ' INNER JOIN game_action as game_action on game_action.id = user_game_action.game_action_id';
            $criteria->addCondition('game_action.code_identifier = "login"');              
            $criteria->group = 'user_game_action.user_id';
            $totalUser = UserGameAction::model()->findAll($criteria);
            $countUser = count($totalUser);                        
                                        
            $user_game_actions = Yii::app()->db->createCommand($query)->queryAll();                                                
            if ($countUser > 0) {
                $index = 0;
                $total = 0;
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['count'] = $countUser;
                $return['items'] = array();
                $return['items']['typeStatistic'] = 'Number User login';
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['count'] = $countUser;
                
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();                 
                while ($startDate <= $endDate) {  
                    $index ++;
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    foreach($user_game_actions as $user_game_action ){
                        $date = date('Y-m-d',strtotime($user_game_action['date']));                    
                        if($day == $date){
                            $total += $user_game_action['number_action'];
                            $return['items']['detail'][$day] = $user_game_action['number_action'];
                        }    
                    }                                    
                    $startDate += strtotime('+1 day', 0);
                }
                $return['items']['tb'] = ceil($total/$index);
            } else {
                $return['code'] = 1;
                $return['message'] = "Không có action nào";
            }
        }
        echo json_encode($return);
    }

    public function actionGetNumberUserLoginOld() {
        $return = array();
        $gameStatistic = Yii::app()->request->getParam('gameStatistic');
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $criteria = new CDbCriteria;
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("timestamp > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("timestamp < '" . $endDate . "'");
        }
        $criteria->addCondition("action_id = 1");
        $criteria->group = 'user_id';
        $totalUser = UserAction::model()->findAll($criteria);
        $countUser = count($totalUser);
        if ($countUser > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['count'] = $countUser;
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Number User login';
            $return['items']['startDate'] = $startDate;
            $return['items']['endDate'] = $endDate;
            $return['items']['count'] = $countUser;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn User nàooo";
        }
        echo json_encode($return);
    }

    public function actionGetAllUser() {
        $return = array();
        $countUser = User::model()->count();
        if ($countUser > 0) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            $return['items']['typeStatistic'] = 'Number All User';
            $return['items']['count'] = $countUser;
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tai User nàooo";
        }
        echo json_encode($return);
    }

    public function actionGetWorldMap() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $worldMap = WorldMap::model()->findAll();
        if (!empty($worldMap)) {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($worldMap as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['node_name'] = $items->node_name;
                $return['items'][$index]['client_scene'] = $items->client_scene;
                $return['items'][$index]['display_name'] = $items->display_name;
                $return['items'][$index]['maximum_players'] = $items->maximum_players;
                $return['items'][$index]['parent_id'] = $items->parent_id;
                $return['items'][$index]['num_robot_allowed'] = $items->num_robot_allowed;
                $return['items'][$index]['robot_level'] = $items->robot_level;
                $return['items'][$index]['num_children_default'] = $items->num_children_default;
                $return['items'][$index]['node_plugin_handle'] = $items->node_plugin_handle;
                $return['items'][$index]['game_type'] = $items->game_type;
                $return['items'][$index]['game_details'] = $items->game_details;
                $return['items'][$index]['app_id'] = $items->app_id;
                $return['items'][$index]['icon'] = $items->icon;
                $return['items'][$index]['logo'] = $items->logo;
                $return['items'][$index]['description'] = $items->description;
                $return['items'][$index]['min_money'] = $items->min_money;
                $return['items'][$index]['client_version_supported_virtual_room'] = $items->client_version_supported_virtual_room;
                $return['items'][$index]['default_game_config'] = $items->default_game_config;
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn World Map nàooo";
        }
        //}
        echo json_encode($return);
    }

    public function actionCreateWorldMap() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
//            $id = Yii::app()->request->getParam('id');
            $node_name = Yii::app()->request->getParam('node_name');
            $client_scene = Yii::app()->request->getParam('client_scene');
            $display_name = Yii::app()->request->getParam('display_name');
            $maximum_players = Yii::app()->request->getParam('maximum_players');
            $parent_id = Yii::app()->request->getParam('parent_id');
            $num_robot_allowed = Yii::app()->request->getParam('num_robot_allowed');
            $robot_level = Yii::app()->request->getParam('robot_level');
            $num_children_default = Yii::app()->request->getParam('num_children_default');
            $node_plugin_handle = Yii::app()->request->getParam('node_plugin_handle');
            $game_type = Yii::app()->request->getParam('game_type');
            $game_details = Yii::app()->request->getParam('game_details');
            $app_id = Yii::app()->request->getParam('app_id');
            $icon = Yii::app()->request->getParam('icon');
            $logo = Yii::app()->request->getParam('logo');
            $description = Yii::app()->request->getParam('description');
            $min_money = Yii::app()->request->getParam('min_money');
            $client_version_supported_virtual_room = Yii::app()->request->getParam('client_version_supported_virtual_room');
            $default_game_config = Yii::app()->request->getParam('default_game_config');
            if (!isset($node_name) || !isset($app_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new WorldMap();
                if (isset($node_name)) {
                    $model->node_name = $node_name;
                }
                if (isset($client_scene)) {
                    $model->client_scene = $client_scene;
                }
                if (isset($display_name)) {
                    $model->display_name = $display_name;
                }
                if (isset($maximum_players)) {
                    $model->maximum_players = $maximum_players;
                }
                if (isset($parent_id)) {
                    $model->parent_id = $parent_id;
                }
                if (isset($num_robot_allowed)) {
                    $model->num_robot_allowed = $num_robot_allowed;
                }
                if (isset($robot_level)) {
                    $model->robot_level = $robot_level;
                }
                if (isset($num_children_default)) {
                    $model->num_children_default = $num_children_default;
                }
                if (isset($node_plugin_handle)) {
                    $model->node_plugin_handle = $node_plugin_handle;
                }
                if (isset($game_type)) {
                    $model->game_type = $game_type;
                }
                if (isset($game_details)) {
                    $model->game_details = $game_details;
                }
                if (isset($icon)) {
                    $model->icon = $icon;
                }
                if (isset($logo)) {
                    $model->logo = $logo;
                }
                if (isset($description)) {
                    $model->description = $description;
                }
                if (isset($min_money)) {
                    $model->min_money = $min_money;
                }
                if (isset($client_version_supported_virtual_room)) {
                    $model->client_version_supported_virtual_room = $client_version_supported_virtual_room;
                }
                if (isset($default_game_config)) {
                    $model->default_game_config = $default_game_config;
                }
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {

                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateWorldMap() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $node_name = Yii::app()->request->getParam('node_name');
            $client_scene = Yii::app()->request->getParam('client_scene');
            $display_name = Yii::app()->request->getParam('display_name');
            $maximum_players = Yii::app()->request->getParam('maximum_players');
            $parent_id = Yii::app()->request->getParam('parent_id');
            $num_robot_allowed = Yii::app()->request->getParam('num_robot_allowed');
            $robot_level = Yii::app()->request->getParam('robot_level');
            $num_children_default = Yii::app()->request->getParam('num_children_default');
            $node_plugin_handle = Yii::app()->request->getParam('node_plugin_handle');
            $game_type = Yii::app()->request->getParam('game_type');
            $game_details = Yii::app()->request->getParam('game_details');
            $app_id = Yii::app()->request->getParam('app_id');
            $icon = Yii::app()->request->getParam('icon');
            $logo = Yii::app()->request->getParam('logo');
            $description = Yii::app()->request->getParam('description');
            $min_money = Yii::app()->request->getParam('min_money');
            $client_version_supported_virtual_room = Yii::app()->request->getParam('client_version_supported_virtual_room');
            $default_game_config = Yii::app()->request->getParam('default_game_config');
            if (!isset($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = WorldMap::model()->findByPk($id);
                if (isset($model)) {
                    $model = new WorldMap();
                    if (isset($node_name)) {
                        $model->node_name = $node_name;
                    }
                    if (isset($client_scene)) {
                        $model->client_scene = $client_scene;
                    }
                    if (isset($display_name)) {
                        $model->display_name = $display_name;
                    }
                    if (isset($maximum_players)) {
                        $model->maximum_players = $maximum_players;
                    }
                    if (isset($parent_id)) {
                        $model->parent_id = $parent_id;
                    }
                    if (isset($num_robot_allowed)) {
                        $model->num_robot_allowed = $num_robot_allowed;
                    }
                    if (isset($robot_level)) {
                        $model->robot_level = $robot_level;
                    }
                    if (isset($num_children_default)) {
                        $model->num_children_default = $num_children_default;
                    }
                    if (isset($node_plugin_handle)) {
                        $model->node_plugin_handle = $node_plugin_handle;
                    }
                    if (isset($game_type)) {
                        $model->game_type = $game_type;
                    }
                    if (isset($game_details)) {
                        $model->game_details = $game_details;
                    }
                    if (isset($icon)) {
                        $model->icon = $icon;
                    }
                    if (isset($logo)) {
                        $model->logo = $logo;
                    }
                    if (isset($description)) {
                        $model->description = $description;
                    }
                    if (isset($min_money)) {
                        $model->min_money = $min_money;
                    }
                    if (isset($client_version_supported_virtual_room)) {
                        $model->client_version_supported_virtual_room = $client_version_supported_virtual_room;
                    }
                    if (isset($default_game_config)) {
                        $model->default_game_config = $default_game_config;
                    }
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = 'ID truyền vào không đúng';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteWorldMap() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = WorldMap::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionGetAdsLumo() {
        $return = array();
        $game_id = Yii::app()->request->getParam('game_id', NULL);
        $partner_id = Yii::app()->request->getParam('partner_id');
        if (isset($game_id) && isset($partner_id)) {
            $model = Ads::model()->findAll("game_id=:game_id AND partner_id=:partner_id", array(':game_id'=>$game_id, ':partner_id'=>$partner_id));
        }elseif($game_id){
            $model = Ads::model()->findAll("game_id=:game_id", array(':game_id'=>$game_id));
        }elseif($partner_id){
            $model = Ads::model()->findAll("partner_id=:partner_id", array(':partner_id'=>$partner_id));
        } else {
            $model = Ads::model()->findAll();
        }
        if (empty($model)) {
            $return['code'] = 1;
            $return['message'] = "Không có quảng cáo nào!";
        } else {            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['id'] = $item->id;
                $return['items'][$index]['game_id'] = $item->game_id;
                $return['items'][$index]['partner_id'] = $item->partner_id;
                $return['items'][$index]['partner_name'] = $item->partner->name;
                $return['items'][$index]['type'] = $item->type;
                $return['items'][$index]['index'] = $item->index;
                $return['items'][$index]['description'] = $item->description;
                $return['items'][$index]['scenes'] = $item->scenes;
                $return['items'][$index]['url'] = $item->url;
                $return['items'][$index]['image'] = $item->image;
            }            
        }
        echo json_encode($return);
    }

    public function actionLogin() {
        $return = array();
        $username = Yii::app()->request->getParam('username', NULL);
        $password = Yii::app()->request->getParam('password', NULL);
        if (isset($username)) {
            $model = User::model()->find("username='" . $username . "'");
            if (empty($model)) {
                $return['code'] = 2;
                $return['message'] = "Tên đăng nhập không đúng!";
            } else {
                if (!isset($password)) {
                    $return['code'] = 3;
                    $return['message'] = "Chưa nhập mật khẩu !";
                } else {
                    $userpassword = $model->createHash($password . $model->salt);
                    if ($userpassword == $model->password) {
                        $roles = UserRole::model()->findAll("user_id='" . $model->id . "'");
                        if (empty($roles)) {
                            $return['code'] = 4;
                            $return['message'] = "Not permission!";
                        } else {
                            foreach ($roles as $role) {
                                $currentRole = Role::model()->findByPk($role->role_id);
                                if($currentRole->name == "super admin"){
                                    $return['role'] = "super admin";
                                };
                                $permission = RolePermission::model()->findAll("role_id ='" . $role->role_id . "'");
                                foreach ($permission as $index) {
                                    $return['permission'][] = $index->permission_id;
                                    $permission_code = Permission::model()->findByPk($index->permission_id);
                                    $return['permissions'][] = $permission_code->code_identifier;
                                }
                            }
                            if (empty($return['permissions'])) {
                                $return['code'] = 5;
                                $return['message'] = "Not permission!";
                            } else {
                                $session = new CHttpSession();
                                $session->open();
                                $session['user'] = $model;
                                $ip = $_SERVER['REMOTE_ADDR'];
                                $accessToken = UserAccessToken::model()->createLoginAccessToken($model->id, $ip);
                                
                                // log login                                
                                $adminActionType = AdminActionType::model()->find("code_identifier='login'");
                                if(isset($adminActionType)){                                                                                         
                                    $adminActionLog = new AdminActionLog();
                                    $adminActionLog->type_id = $adminActionType->id;
                                    $adminActionLog->user_id = $model->id;
                                    $adminActionLog->content = "login";
                                    $adminActionLog->save();
                                }                                
                                
                                $session['accessToken'] = $accessToken;
                                Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);
                                $return['accessToken'] = $accessToken;
                                $return['username'] = $model->username;
                                $return['email'] = $model->email;
                                $return['birthday'] = $model->birthday;
                                $return['fullname'] = $model->fullname;
                                $return['mobile'] = $model->mobile;
                                $return['address'] = $model->address;
                                $return['gold'] = $model->gold;
                                $return['chip'] = $model->chip;
                                $return['avatar'] = $model->avatar;
                                $return['identity_card_number'] = $model->identity_card_number;
                                $return['code'] = 0;
                                $return['message'] = "Đăng nhập Admin thành công";
                            }
                        }
                    } else {
                        $return['code'] = 5;
                        $return['message'] = "Mật khẩu không đúng!";
                    }
                }
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Thiếu tên đăng nhập";
        }
        echo json_encode($return);
//        K::dump($return);
    }

    public function actionCheckAccessToken() {
        $return = array();
        $accessToken = Yii::app()->request->getParam('accessToken', NULL);
        if ($accessToken == NULL)
            return;
        if (UserAccessToken::model()->checkAccessToken($accessToken) == TRUE) {
            $return['code'] = 0;
            $return['message'] = 'accessToken correct';
            $aTk = UserAccessToken::model()->find('access_token=:accessToken', array(':accessToken' => $accessToken));
            $user_id = $aTk->user_id;
            $model = User::model()->findByPk($user_id);
            $roles = UserRole::model()->findAll("user_id='" . $model->id . "'");
            if (empty($roles)) {
                $return['code'] = 4;
                $return['message'] = "Not permission!";
            } else {
                foreach ($roles as $role) {
                    $currentRole = Role::model()->findByPk($role->role_id);
                    if($currentRole->name == "super admin"){
                        $return['role'] = "super admin";
                    };
                    $permission = RolePermission::model()->findAll("role_id ='" . $role->role_id . "'");
                    foreach ($permission as $index) {
                        $return['permission'][] = $index->permission_id;
                        $permission_code = Permission::model()->findByPk($index->permission_id);
                        $return['permissions'][] = $permission_code->code_identifier;
                    }
                    if($role->role_id == 5)
                        $return['role'] = $role->role->name;
                }
                if (empty($return['permissions'])) {
                    $return['code'] = 5;
                    $return['message'] = "Not permission!";
                } else {
                    $session = new CHttpSession();
                    $session->open();
                    $session['user'] = $model;
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $accessToken = UserAccessToken::model()->createLoginAccessToken($model->id, $ip);
                    
                    // log login                                
                    $adminActionType = AdminActionType::model()->find("code_identifier='login'");
                    if(isset($adminActionType)){                                                                                         
                        $adminActionLog = new AdminActionLog();
                        $adminActionLog->type_id = $adminActionType->id;
                        $adminActionLog->user_id = $model->id;
                        $adminActionLog->content = "login";
                        $adminActionLog->save();
                    }
                    
                    $session['accessToken'] = $accessToken;
                    Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);
                    $return['accessToken'] = $accessToken;
                    $return['code'] = 0;
                    $return['username'] = $model->username;
                    
                    $return['message'] = "Đăng nhập Admin thành công";
                }
            }
        } else {
            $return['code'] = 1;
            $return['message'] = 'accessToken incorrect!';
        }
        echo json_encode($return);
    }

    public function actionGetPermissionCategory() {
        $return = array();
        $model = PermissionCategory::model()->findAll();
        if (empty($model)) {
            $return['code'] = 1;
            $return['message'] = "Không có GameActionCategory nào";
            $return['items'] = array();
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['parent_id'] = $items->parent_id;
                $return['items'][$index]['name'] = $items->name;
                $return['items'][$index]['description'] = $items->description;
            }
        }

        echo json_encode($return);
    }

    public function actionDeleteRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Role::model()->findByPk($id);
                if (isset($model)) {
                    $rolePermission = RolePermission::model()->findAll('role_id ="' . $model->id . '"');
                    foreach ($rolePermission as $index) {
                        if (!$index->delete()) {
                            $return['code'] = 3;
                            $return['message'] = "Xóa Role Permission Lỗi";
                            echo json_encode($return);
                            die;
                        }
                    };
                    $userRole = UserRole::model()->findAll('role_id ="' . $model->id . '"');
                    foreach ($userRole as $index) {
                        if (!$index->delete()) {
                            $return['code'] = 3;
                            $return['message'] = "Xóa User Role Lỗi";
                            echo json_encode($return);
                            die;
                        }
                    }
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Permission::model()->findByPk($id);
                if (isset($model)) {
                    $rolePermission = RolePermission::model()->findAll('role_id ="' . $model->id . '"');
                    foreach ($rolePermission as $index) {
                        if (!$index->delete()) {
                            $return['code'] = 3;
                            $return['message'] = "Xóa Role Permission Lỗi";
                            echo json_encode($return);
                            die;
                        }
                    };
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePost() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = Post::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePostCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = PostCategory::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePermissionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $model = PermissionCategory::model()->findByPk($id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteRolePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $permission_id = Yii::app()->request->getParam('permission_id');
            $description = Yii::app()->request->getParam('description');
            if (!isset($role_id) || !isset($permission_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = RolePermission::model()->find("role_id = " . $role_id . " AND permission_id = " . $permission_id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteUserRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $user_id = Yii::app()->request->getParam('user_id');
            if (!isset($role_id) || !isset($user_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = UserRole::model()->find("role_id = " . $role_id . " AND user_id = " . $user_id);
                if (isset($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Object với ID truyền vào!";
                }
            }
        }
        echo json_encode($return);
    }


    

    public function actionCreatePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $category_id = Yii::app()->request->getParam('category_id');
            if (!isset($name) || !isset($code_identifier)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new Permission();
                $model->name = $name;
                $model->code_identifier = $code_identifier;
                $model->category_id = $category_id;
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreatePermissionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $parent_id = Yii::app()->request->getParam('parent_id');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new PermissionCategory();
                $model->name = $name;
                if (isset($parent_id))
                    $model->parent_id = $parent_id;
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $partner_id = Yii::app()->request->getParam('partner_id');            
            if (empty($name) || empty($partner_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new Role();
                $model->name = $name;
                $model->partner_id = $partner_id;
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            if (!isset($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Role::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $category_id = Yii::app()->request->getParam('category_id');
            if (!isset($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = Permission::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;
                    if (isset($description))
                        $model->description = $description;
                    if (isset($code_identifier))
                        $model->code_identifier = $code_identifier;
                    if (isset($category_id))
                        $model->category_id = $category_id;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePermissionCategory() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $parent_id = Yii::app()->request->getParam('parent_id');
            if (!isset($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = PermissionCategory::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;
                    if (isset($description))
                        $model->description = $description;
                    if (isset($code_identifier))
                        $model->code_identifier = $code_identifier;
                    if (isset($category_id))
                        $model->parent_id = $parent_id;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateRolePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $permission_id = Yii::app()->request->getParam('permission_id');
            $description = Yii::app()->request->getParam('description');
            if (!isset($role_id) || !isset($permission_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = RolePermission::model()->find("role_id = " . $role_id . " AND permission_id = " . $permission_id);
                if (isset($model)) {
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateUserRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $user_id = Yii::app()->request->getParam('user_id');
            $enabled = Yii::app()->request->getParam('enabled');
            if (!isset($role_id) || !isset($user_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = UserRole::model()->find("role_id = " . $role_id . " AND user_id = " . $user_id);
                if (isset($model)) {
                    if (isset($enabled))
                        $model->enabled = $enabled;
                    if ($model->save()) {
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateRolePermission() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $permission_id = Yii::app()->request->getParam('permission_id');
            $description = Yii::app()->request->getParam('description');
            if (!isset($role_id) || !isset($permission_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = RolePermission::model()->find("role_id = " . $role_id . " AND permission_id = " . $permission_id);

                $model = RolePermission::model()->find("role_id = " . $role_id . " AND permission_id = " . $permission_id);
                if (isset($model)) {
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {
                        $return['create'] = 0;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['create'] = 0;
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $model = new RolePermission();
                    $model->role_id = $role_id;
                    $model->permission_id = $permission_id;
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {
                        $return['create'] = 1;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['create'] = 1;
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionCreateUserRole() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $role_id = Yii::app()->request->getParam('role_id');
            $user_id = Yii::app()->request->getParam('user_id');
            $enabled = Yii::app()->request->getParam('enabled');
            if (!isset($role_id) || !isset($user_id) || !isset($enabled)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = UserRole::model()->find("role_id = " . $role_id . " AND user_id = " . $user_id);
                if (isset($model)) {
                    if (isset($enabled))
                        $model->enabled = $enabled;
                    if ($model->save()) {
                        $return['create'] = 0;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['create'] = 0;
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $model = new UserRole();
                    $model->role_id = $role_id;
                    $model->user_id = $user_id;
                    $model->enabled = $enabled;                    
                    $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;                                
                    $model->parent_id = $user->id;
                    if ($model->save()) {
                        $return['create'] = 1;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        Common::dump($model->errors);
                        $return['create'] = 1;
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    

    
    
    
    public function actionGetConfigurationClient(){        
        $return = array();
        $name = Yii::app()->request->getParam('name');
        if(isset($name)){
            $model = ConfigurationClient::model()->find('name="'.$name.'"');
            if(!isset($model)){
                $return['code'] = -1;
                $return['message'] = "Không tồn tại configuration";
            }else{
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['id'] = $model->id;
                $return['name'] = $model->name;                
                $return['description'] = $model->description;
            }
        }else{            
            $models = ConfigurationClient::model()->findAll();
            if(!isset($models)){
                $return['code'] = -1;
                $return['message'] = "Không tồn tại config nào";                
            }else{
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach($models as $index=>$item){
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['name'] = $item->name;                    
                    $return['items'][$index]['description'] = $item->description;
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionCreateConfigurationClient() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {            
            $name = Yii::app()->request->getParam('name');            
            $description = Yii::app()->request->getParam('description');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = new ConfigurationClient();                
                $model->name = $name;                                         
                if (isset($description))
                    $model->description = $description;
                if ($model->save()) {
//                    $electroServer = new ElectroServerBridge();
//                    $electroServer->send($electroServer->buildURLForAction('reset_db_configuration_client'));
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }                
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateConfigurationClient() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');            
            $description = Yii::app()->request->getParam('description');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = ConfigurationClient::model()->findByPk($id);
                if (isset($model)) {
                    if (isset($name))
                        $model->name = $name;                                        
                    if (isset($description))
                        $model->description = $description;
                    if ($model->save()) {                        
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 2;
                    $return['message'] = 'Không ton tai Configuration ID';
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionDeleteConfigurationClient() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {
                $configurationClientVersion = ConfigurationClientVersion::model()->find('configuration_client_id =' . $id);
                if (isset($configurationClientVersion)) {
                    $return['code'] = 3;
                    $return['message'] = "Bạn phải xóa trong bảng configurationClientVersion trước";
                } else {
                    $model = ConfigurationClient::model()->findByPk($id);
                    if (isset($model)) {
                        if ($model->delete()) {
                            $return['code'] = 0;
                            $return['message'] = "Thành công";
                        } else {
                            $return['code'] = 1;
                            $return['message'] = "Xóa dữ liệu thất bại";
                        }
                    } else {
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Object với ID truyền vào!";
                    }
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetConfigurationClientVersion() {
        $return = array();
        $configuration_client_id = Yii::app()->request->getParam('configuration_client_id');
        $game_version_id = Yii::app()->request->getParam('game_version_id');
        if (isset($configuration_client_id) && isset($configuration_client_id) && is_numeric($game_version_id) && is_numeric($game_version_id)) {
            $model = ConfigurationClientVersion::model()->findAll('configuration_client_id = ' . $configuration_client_id . ' AND game_version_id=' . $game_version_id);
        } elseif (isset($configuration_client_id) && is_numeric($configuration_client_id)) {
            $model = ConfigurationClientVersion::model()->findAll('configuration_client_id = ' . $configuration_client_id);
        } elseif (isset($game_version_id) && is_numeric($game_version_id)) {
            $model = ConfigurationClientVersion::model()->findAll('game_version_id = ' . $game_version_id);
        } else {
            $model = ConfigurationClientVersion::model()->findAll();
        }
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['game_version_id'] = $item->game_version_id;
                $return['items'][$index]['configuration_client_id'] = $item->configuration_client_id;
                $return['items'][$index]['value'] = $item->value;
            }
        }

        echo json_encode($return);
    }

    public function actionCreateConfigurationClientVersion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $game_version_id = Yii::app()->request->getParam('game_version_id');
            $configuration_client_id = Yii::app()->request->getParam('configuration_client_id');
            $value = Yii::app()->request->getParam('value');
            if (!isset($game_version_id) || !isset($configuration_client_id) || !is_numeric($game_version_id) || !is_numeric($configuration_client_id) || !isset($value)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $gameVersion = GameVersion::model()->findByPk($game_version_id);
                if (!isset($gameVersion)) {
                    $return['code'] = 3;
                    $return['message'] = "Khong ton tai gameversion";
                    echo json_encode($return);
                    die;
                }
                $configurationClient = ConfigurationClient::model()->findByPk($configuration_client_id);
                if (!isset($configurationClient)) {
                    $return['code'] = 4;
                    $return['message'] = "Khong ton tai Config";
                    echo json_encode($return);
                    die;
                }
                $model = new ConfigurationClientVersion();
                $model->game_version_id = $game_version_id;
                $model->configuration_client_id = $configuration_client_id;
                $model->value = $value;
                if ($model->save()) {                                          
                    $return['game_version_id'] = $model->game_version_id;
                    $return['configuration_client_id'] = $model->configuration_client_id;
                    $return['value'] = $model->value;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateConfigurationClientVersion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $configuration_client_id = Yii::app()->request->getParam('configuration_client_id');
            $game_version_id = Yii::app()->request->getParam('game_version_id');
            $value = Yii::app()->request->getParam('value');
            if (!isset($configuration_client_id) || !is_numeric($configuration_client_id) || !isset($game_version_id) || !is_numeric($game_version_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = ConfigurationClientVersion::model()->find('game_version_id=' . $game_version_id . ' AND configuration_client_id=' . $configuration_client_id);
                if (isset($model)) {
                    if (isset($value))
                        $model->value = $value;                                                            
                    if ($model->save()) {                        
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                } else {
                    $return['code'] = 2;
                    $return['message'] = 'Không ton tai Configuration Client Version';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteConfigurationClientVersion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $game_version_id = Yii::app()->request->getParam('game_version_id');
            $configuration_client_id = Yii::app()->request->getParam('configuration_client_id');
            if (!isset($game_version_id) || !is_numeric($game_version_id) || !isset($configuration_client_id) || !is_numeric($configuration_client_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $model = ConfigurationClientVersion::model()->find('game_version_id=' . $game_version_id . ' AND configuration_client_id=' . $configuration_client_id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Configuration Client Version";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetHelp() {
        $return = array();
        $id = Yii::app()->request->getParam('id');
        $game_id = Yii::app()->request->getParam('game_id');
        if (isset($id)) {
            $model = Help::model()->findByPk($id);            
        }elseif($game_id){
            $model = Help::model()->findAll('game_id='.$game_id);            
        }else {
            $model = Help::model()->findAll();            
        }
        if(!isset($model)){
            $return['code'] = 1;
            $return['message'] = 'Không tồn tại kết quả';
            echo json_encode($return);die;
        }
        if(isset($id)){            
            $return['code'] = 0;
            $return['message'] = 'Thành công';
            $return['id'] = $model->id;
            $return['title'] = $model->title;    
            $return['content'] = $model->content;
            $return['status'] = $model->status;
            $return['time'] = $model->time;           
        }else{            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['id'] = $item->id;
                $return['items'][$index]['game_id'] = $item->game_id;
                $return['items'][$index]['title'] = $item->title;
                $return['items'][$index]['content'] = $item->content;
                $return['items'][$index]['time'] = $item->time;
                $return['items'][$index]['status'] = $item->status;                
            }            
        }

        echo json_encode($return);
    }

    public function actionCreateHelp() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $game_id = Yii::app()->request->getParam('game_id');
            $title = Yii::app()->request->getParam('title');    
            $content = Yii::app()->request->getParam('content');
            $status = Yii::app()->request->getParam('status');
            if (!isset($game_id) || !isset($title) || !isset($content) || !isset($status) || !is_numeric($game_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $gameModel = Game::model()->findByPk($game_id);
                if(!isset($gameModel)){
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại game";
                    return json_encode($return);die;
                }
                $model = new Help();
                $model->game_id = $game_id;
                $model->title = $title;
                $model->content = $content;
                $model->status = $status;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateHelp() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $game_id = Yii::app()->request->getParam('game_id');
            $title = Yii::app()->request->getParam('title');    
            $content = Yii::app()->request->getParam('content');
            $status = Yii::app()->request->getParam('status');
            if (!isset($game_id) || !isset($title) || !isset($content) || !isset($status) || !is_numeric($game_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $gameModel = Game::model()->findByPk($game_id);
                if(!isset($gameModel)){
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại game";
                    return json_encode($return);die;
                }
                $model = Help::model()->findByPk($id);
                if (isset($title))
                    $model->title = $title;
                if (isset($game_id))
                    $model->game_id = $game_id;
                if (isset($content))
                    $model->content = $content;                                
                if (isset($status))
                    $model->status = $status;
                if ($model->save()) {
                    $json = array();
                    $modelHelp = Help::model()->findAll('game_id='.$model->game_id); 
                    $json['items'] = array();
                    foreach ($modelHelp as $index => $item) {
                        $json['items'][$index]['id'] = $item->id;
                        $json['items'][$index]['game_id'] = $item->game_id;
                        $json['items'][$index]['title'] = $item->title;
                        $json['items'][$index]['content'] =  $item->content;//"<p>1.&Ugrave; Xu&ocirc;ng -&Ugrave; Xu&ocirc;ng l&agrave; sau khi chia xong b&agrave;i tr&ecirc;n b&agrave;i người chơi kh&ocirc;ng c&oacute; cạ v&agrave; phỏm. -Độ ưu ti&ecirc;n t&iacute;nh từ c&aacute;i. -Kh&ocirc;ng phải l&agrave; người cầm c&aacute;i m&agrave; &Ugrave; Xu&ocirc;ng th&igrave; mất 5 lần Ch&iacute;p cược -Nếu người cầm c&aacute;i m&agrave; &Ugrave; Xu&ocirc;ng th&igrave; t&iacute;nh 10 lần Ch&iacute;p cược 2.X&agrave;o Khan -X&agrave;o Khan chỉ &aacute;p dụng khi chơi 4 người -X&agrave;o Khan l&agrave; chỉ 1 người c&oacute; phỏm hoặc tất cả m&oacute;m. -Trường hợp tất cả m&oacute;m th&igrave; người hạ đầu sẽ được t&iacute;nh X&agrave;o Khan. -X&agrave;o Khan t&iacute;nh 8 lần Ch&iacute;p cược -Trường Hợp đặc biệt : c&oacute; 4 người chơi A B C D , A ăn 1 c&acirc;y rồi tho&aacute;t &gt;&gt; A bị xử m&oacute;m.Kết th&uacute;c v&aacute;n tất cả đều m&oacute;m v&agrave; B hạ đầu th&igrave; B kh&ocirc;ng được t&iacute;nh X&agrave;o Khan mặc d&ugrave; tất cả đều m&oacute;m.V&igrave; A đ&atilde; ăn 1 c&acirc;y nhưng do tho&aacute;t n&ecirc;n bị xử m&oacute;m. 3.Chốt Chồng -Chốt chồng l&agrave; nh&acirc;n hệ số Ch&iacute;p cược cho c&acirc;y chốt khi ăn được c&acirc;y chốt lại cho người kh&aacute;c ăn c&acirc;y chốt. -V&iacute; dụ : A cho cho B ăn c&acirc;y chốt (c&acirc;y 1),B cho C ăn c&acirc;y chốt(c&acirc;y 2),C cho D ăn c&acirc;y chốt(c&acirc;y 3),D cho C ăn c&acirc;y chốt(c&acirc;y 4)&hellip; tất cả phải li&ecirc;n tiếp v&agrave; kh&ocirc;ng bị ngắt qu&atilde;ng &gt;&gt; C&acirc;y 1 nh&acirc;n hệ số 1 &gt;&gt; C&acirc;y 2 nh&acirc;n hệ số 2 &gt;&gt; C&acirc;y 3 nh&acirc;n hệ số 4 &gt;&gt; C&acirc;y 4 nh&acirc;n hệ số 8 4.Đền Chồng -Khi ăn c&acirc;y chốt chồng hệ số bao nhi&ecirc;u th&igrave; khi c&oacute; người &Ugrave; th&igrave; bị nh&acirc;n theo hệ số đ&oacute; 5.&Ugrave; Tr&ograve;n -&Ugrave; Tr&ograve;n l&agrave; khi &Ugrave; tr&ecirc;n b&agrave;i c&oacute; 10 c&acirc;y.Nếu c&oacute; bất cứ h&agrave;nh động n&agrave;o gửi b&agrave;i th&igrave; sẽ kh&ocirc;ng được t&iacute;nh l&agrave; &Ugrave; Tr&ograve;n. -&Ugrave; Tr&ograve;n t&iacute;nh 10 lần Ch&iacute;p cược.</p>";
                        $json['items'][$index]['time'] = $item->time;
                        $json['items'][$index]['status'] = $item->status;                
                    }                                        
                    $json['typeRealTime'] = "help";                       
                    $config_json = json_encode($json);
                    $electroServer = new ElectroServerBridge();
                    $data = array();           
                    $data['appId'] = $model->game_id;                                              
                    $data['config'] = $config_json;
                    $data['command'] = 'updateConfigClient';
                    $electroServer->sendPost($data);
                    
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteHelp() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {                
                $model = Help::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Help";
                }
            }
        }
        echo json_encode($return);
    }        
    
    public function actionGetPromotion() {
        $return = array();
        $id = Yii::app()->request->getParam('id');        
        if (isset($id)) {
            $model = Promotion::model()->findByPk($id);                    
        }else {
            $model = Promotion::model()->findAll();            
        }
        if(!isset($model)){
            $return['code'] = 1;
            $return['message'] = 'Không tồn tại kết quả';
            echo json_encode($return);die;
        }
        if(isset($id)){            
            $return['code'] = 0;
            $return['message'] = 'Thành công';
            $return['id'] = $model->id;
            $return['name'] = $model->name;
            $return['description'] = $model->description;
        }else{            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['id'] = $item->id;
                $return['items'][$index]['name'] = $item->name;
                $return['items'][$index]['description'] = $item->description;
            }            
        }

        echo json_encode($return);
    }

    public function actionCreatePromotion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');            
            $description = Yii::app()->request->getParam('description');
            if (!isset($name)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new Promotion();
                $model->name = $name;
                if(isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePromotion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $description = Yii::app()->request->getParam('description');            
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = Promotion::model()->findByPk($id);
                if (isset($name))
                    $model->name = $name;
                if (isset($description))
                    $model->description = $description;                
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePromotion() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else { 
                $promotionRuleModel = PromotionRule::model()->find('promotion_id=:promotion_id', array(':promotion_id'=>$id));
                if(isset($promotionRuleModel)){
                    $return['code'] = 4;
                    $return['message'] = "Bạn phải xóa promotion rule trước";
                    echo json_encode($return);die;
                }
                $promotionActive = PromotionActive::model()->find('promotion_id=:promotion_id', array(':promotion_id'=>$id));
                if(isset($promotionActive)){
                    $return['code'] = 5;
                    $return['message'] = "Bạn phải xóa promotion active trước";
                    echo json_encode($return);die;
                }
                $model = Promotion::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Help";
                }
            }
        }
        echo json_encode($return);
    }
    
     public function actionGetConstraintType() {
        $return = array();
        $id = Yii::app()->request->getParam('id');        
        if (isset($id)) {
            $model = ConstraintType::model()->findByPk($id);                    
        }else {
            $model = ConstraintType::model()->findAll();            
        }
        if(!isset($model)){
            $return['code'] = 1;
            $return['message'] = 'Không tồn tại kết quả';
            echo json_encode($return);die;
        }
        if(isset($id)){            
            $return['code'] = 0;
            $return['message'] = 'Thành công';
            $return['id'] = $model->id;
            $return['name'] = $model->name;
            $return['operator'] = $model->operator;
            $return['description'] = $model->description;
        }else{            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['id'] = $item->id;
                $return['items'][$index]['name'] = $item->name;
                $return['items'][$index]['operator'] = $item->operator;
                $return['items'][$index]['description'] = $item->description;
            }            
        }

        echo json_encode($return);
    }

    public function actionCreateConstraintType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');          
            $operator = Yii::app()->request->getParam('operator');
            $description = Yii::app()->request->getParam('description');
            if (!isset($name) || !isset($operator)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = new ConstraintType();
                $model->name = $name;
                $model->operator = $operator;
                if(isset($description))
                    $model->description = $description;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateConstraintType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $name = Yii::app()->request->getParam('name');
            $operator = Yii::app()->request->getParam('operator');
            $description = Yii::app()->request->getParam('description');            
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $model = ConstraintType::model()->findByPk($id);
                if (isset($name))
                    $model->name = $name;
                if (isset($operator))
                    $model->operator = $operator;
                if (isset($description))
                    $model->description = $description;                
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeleteConstraintType() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else { 
                $promotionRuleModel = PromotionRule::model()->find('constraint_type_id=:constraint_type_id', array(':constraint_type_id'=>$id));
                if(isset($promotionRuleModel)){
                    $return['code'] = 4;
                    $return['message'] = "Bạn phải xóa promotion rule trước";
                    echo json_encode($return);die;
                }                
                $model = ConstraintType::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại Constraint";
                }
            }
        }
        echo json_encode($return);
    }
        
    public function actionGetPromotionActive() {
        $return = array();
        $id = Yii::app()->request->getParam('id');     
        $promotion_id = Yii::app()->request->getParam('promotion_id');
        if (isset($id)) {
            $model = PromotionActive::model()->findByPk($id);               
        }elseif(isset($promotion_id)){    
            $model = PromotionActive::model()->find('promotion_id=:promotion_id',array(':promotion_id'=>$promotion_id));               
        }else {
            $model = PromotionActive::model()->findAll();            
        }
        if(!isset($model)){
            $return['code'] = 1;
            $return['message'] = 'Không tồn tại kết quả';
            echo json_encode($return);die;
        }
        if(isset($id)){            
            $return['code'] = 0;
            $return['message'] = 'Thành công';
            $return['id'] = $model->id;
            $return['promotion_id'] = $model->promotion_id;
            $return['start_time'] = $model->start_time;
            $return['end_time'] = $model->end_time;
        }else{            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {
                $return['items'][$index]['id'] = $item->id;
                $return['items'][$index]['promotion_id'] = $item->promotion_id;
                $return['items'][$index]['start_time'] = $item->start_time;
                $return['items'][$index]['end_time'] = $item->end_time;
            }            
        }

        echo json_encode($return);
    }

    public function actionCreatePromotionActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $promotion_id = Yii::app()->request->getParam('promotion_id');   
            $start_time = Yii::app()->request->getParam('start_time');
            $end_time = Yii::app()->request->getParam('end_time');
            if (!isset($promotion_id) || !isset($start_time) || !isset($end_time) || !is_numeric($promotion_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                
                $promotion = Promotion::model()->findByPk($promotion_id);
                if(!isset($promotion)){
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại promotion";
                    echo json_encode($return);die;
                }
                $model = new PromotionActive();
                $model->promotion_id = $promotion_id;                
                $model->start_time = $start_time;
                $model->end_time = $end_time;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdatePromotionActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            $promotion_id = Yii::app()->request->getParam('promotion_id');
            $start_time = Yii::app()->request->getParam('start_time');
            $end_time = Yii::app()->request->getParam('end_time');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                                
                $model = PromotionActive::model()->findByPk($id);
                if (isset($promotion_id)){
                    $promotion = Promotion::model()->findByPk($promotion_id);
                    if(!isset($promotion)){
                        $return['code'] = 3;
                        $return['message'] = "Không tồn tại Promotion";
                        echo json_encode($return);die;
                    }
                    $model->promotion_id = $promotion_id;
                }
                if(isset($start_time))
                    $model->start_time = $start_time;
                if(isset($end_time))
                    $model->end_time = $end_time;
                
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }

    public function actionDeletePromotionActive() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else { 
                $promotionLog = PromotionLog::model()->find('promotion_active_id=:promotion_active_id', array(':promotion_active_id'=>$id));
                if(isset($promotionLog)){
                    $return['code'] = 4;
                    $return['message'] = "Bạn phải xóa promotion log trước";
                    echo json_encode($return);die;
                }                
                $model = PromotionActive::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại PromotionActive";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetPromotionRule() {
        $return = array();        
        $promotion_id = Yii::app()->request->getParam('promotion_id');
        $game_action_id = Yii::app()->request->getParam('game_action_id');
        if (isset($promotion_id) && isset($game_action_id) && is_numeric($game_action_id) && is_numeric($promotion_id)) {
            $model = PromotionRule::model()->find('promotion_id=:promotion_id AND game_action_id=:game_action_id',array(':promotion_id'=>$promotion_id, ':game_action_id'=>$game_action_id));   
        }elseif(isset($promotion_id) && is_numeric($promotion_id)){    
            $model = PromotionRule::model()->find('promotion_id=:promotion_id',array(':promotion_id'=>$promotion_id));               
        }elseif(isset($game_action_id) && is_numeric($game_action_id)){
            $model = PromotionRule::model()->find('game_action_id=:game_action_id',array(':game_action_id'=>$game_action_id));               
        }else {
            $model = PromotionRule::model()->findAll();            
        }
        if(!isset($model)){
            $return['code'] = 1;
            $return['message'] = 'Không tồn tại kết quả';
            echo json_encode($return);die;
        }
        if(isset($id)){            
            $return['code'] = 0;
            $return['message'] = 'Thành công';            
            $return['promotion_id'] = $model->promotion_id;
            $return['game_action_id'] = $model->game_action_id;
            $return['constraint_type_id'] = $model->constraint_type_id;
            $return['value'] = $model->value;
            $return['timestamp'] = $model->timestamp;
        }else{            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $item) {                
                $return['items'][$index]['promotion_id'] = $item->promotion_id;
                $return['items'][$index]['game_action_id'] = $item->game_action_id;
                $return['items'][$index]['constraint_type_id'] = $item->constraint_type_id;
                $return['items'][$index]['value'] = $item->value;
                $return['items'][$index]['timestamp'] = $item->timestamp;
            }            
        }

        echo json_encode($return);
    }

    public function actionCreatePromotionRule() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $session = new CHttpSession();
        $session->open();
        $access_token = $session['accessToken'];
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $promotion_id = Yii::app()->request->getParam('promotion_id');   
            $game_action_id = Yii::app()->request->getParam('game_action_id');
            $constraint_type_id = Yii::app()->request->getParam('constraint_type_id');
            $value = Yii::app()->request->getParam('value');
            if (!isset($promotion_id) || !isset($game_action_id) || !isset($constraint_type_id) || !isset($value) || !is_numeric($promotion_id) || !is_numeric($game_action_id) || !is_numeric($constraint_type_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {                                 
                $promotion = Promotion::model()->findByPk($promotion_id);
                if(!isset($promotion)){
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại promotion";
                    echo json_encode($return);die;
                }
                $gameAction = GameAction::model()->findByPk($game_action_id);
                if(!isset($gameAction)){
                    $return['code'] = 4;
                    $return['message'] = "Không tồn tại GameAction";
                    echo json_encode($return);die;
                }
                
                $modelPromotionRule = PromotionRule::model()->find('promotion_id=:promotion_id AND game_action_id =:game_action_id', array(':promotion_id'=>$promotion_id, ':game_action_id'=>$game_action_id));
                if(isset($modelPromotionRule)){
                    $modelPromotionRule->contraint_type_id = $constraint_type_id;
                    $modelPromotionRule->value = $value;
                    if($modelPromotionRule->save()){
                        $return['id'] = $modelPromotionRule->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';                        
                    }else{
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';                        
                    }
                    echo json_encode($return);die;
                }
                $model = new PromotionRule();
                $model->promotion_id = $promotion_id;                
                $model->game_action_id = $game_action_id;
                $model->constraint_type_id = $constraint_type_id;
                $model->value = $value;
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }    

    public function actionDeletePromotionRule() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $promotion_id = Yii::app()->request->getParam('promotion_id');
            $game_action_id = Yii::app()->request->getParam('game_action_id');
            if (!isset($promotion_id) || !is_numeric($promotion_id) || !isset($game_action_id) || !is_numeric($game_action_id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {                                             
                $model = PromotionRule::model()->find('promotion_id=:promotion_id AND game_action_id =:game_action_id', array(':promotion_id'=>$promotion_id, ':game_action_id'=>$game_action_id));
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại PromotionRule";
                }
            }
        }
        echo json_encode($return);
    }    
    
    public function actionGetPromotionLog() {
        $return = array();
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');
        $promotion_active_id = Yii::app()->request->getParam('promotion_active_id');
        $promotion_id = Yii::app()->request->getParam('promotion_id');
        $game_id = Yii::app()->request->getParam('game_id');
        $user_id = Yii::app()->request->getParam('user_id');
        $page = Yii::app()->request->getParam('page');
        $limit = Yii::app()->request->getParam('limit');
        if(!isset($page))
            $page = 0;
        if(!isset($limit))
            $limit = 30;
        
        $criteria = new CDbCriteria;
        $criteria->alias = "promotion_log";
        $start = $page * $limit;
        $criteria->offset = $start;
        $criteria->limit = $limit;                
        
        $criteria->join = ' INNER JOIN promotion_active ON promotion_active.id = promotion_log.promotion_active_id';        
        $criteria->join .= ' INNER JOIN user_game_action ON promotion_log.user_game_action_id = user_game_action.id';
        $criteria->join .= ' INNER JOIN game_action ON user_game_action.game_action_id = game_action.id';                
        if (isset($startDate)) {
            $startDate .= ' 00:00:00';
            $criteria->addCondition("promotion_log.timestamp > '" . $startDate . "'");
        }
        if (isset($endDate)) {
            $endDate .= ' 23:59:59';
            $criteria->addCondition("promotion_log.timestamp < '" . $endDate . "'");
        }
        if(isset($promotion_active_id)){
            $criteria->addCondition("promotion_log.promotion_active_id=".$promotion_active_id);
        }else{
            if(isset($promotion_id)){
                $criteria->addCondition("promotion_active.promotion_id=".$promotion_id);
            }
        }
        if(isset($promotion_id)){
            $criteria->addCondition("promotion_active.promotion_id=".$promotion_id);
        }
        if(isset($user_id)){            
            $criteria->addCondition("user_game_action.user_id=".$user_id);
        }        
        if(isset($game_id)){
            $criteria->addCondition("game_action.game_id=".$game_id);
        }
        $model = PromotionLog::model()->findAll($criteria);
        
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['user_game_action_id'] = $items->user_game_action_id;
                $return['items'][$index]['promotion_active_id'] = $items->promotion_active_id;
                $return['items'][$index]['value'] = $items->value;
                $return['items'][$index]['timestamp'] = $items->timestamp;
                if(isset($items->userGameAction->gameAction->game))
                    $return['items'][$index]['game'] = $items->userGameAction->gameAction->game->name;
                $return['items'][$index]['username'] = $items->userGameAction->user->username;
                $return['items'][$index]['promotion'] = $items->promotionActive->promotion->name;
                $return['items'][$index]['start_time'] = $items->promotionActive->start_time;
                $return['items'][$index]['end_time'] = $items->promotionActive->end_time;
            }
            $criteria->offset = 0;
            $criteria->limit = 10000000;
            $total = PromotionLog::model()->count($criteria);
            if (count($total) % $limit > 0) {
                $return['count'] = ($total - ($total % $limit)) / $limit;
            } else
                $return['count'] = $total / $limit - 1;
        }

        echo json_encode($return);
    }
    
    public function actionPushNotification(){  
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {   
            $users = Yii::app()->request->getParam('users');
            $request = $_REQUEST;
            $push = new PushNotificationLog();
            $push->pushNotification($users, $request);
            $return['code'] = 0;
            $return['message'] = "Thành công";
        }
        echo json_encode($return);		
       
   }      
   
   public function actionGetGameServerLog(){
        $return = array();
//        $token = Yii::app()->request->getParam('accessToken');        
//        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
//            $return['code'] = -1;
//            $return['message'] = "Bạn không có quyền truy cập";
//        } else {                                
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');                         
            if(!isset($endDate) && !isset($startDate)){
                $startTime = new MongoDate(time()-24*3600);
                $endTime = new MongoDate();        
            }elseif(!isset($endDate) && isset($startDate)){
                $startTime = new MongoDate(strtotime($startDate));
                $endTime = new MongoDate();
                if($startTime > $endTime){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                               
                    $return['items']['startTime'] = $startTime;
                    $return['items']['endTime'] = $endTime;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $startTime = new MongoDate(strtotime($endDate)- 24*3600);
                $endTime = new MongoDate(strtotime($endDate));                               
            }else{
                $startTime = new MongoDate(strtotime($startDate));
                $endTime = new MongoDate(strtotime($endDate)+ 24*3600); 
            }   
                        
            $name = Yii::app()->request->getParam('name'); 
            if(isset($name)){                                           
              $criteria = new EMongoCriteria();                         
              $criteria2 = new EMongoCriteria;
              $criteria4 = new EMongoCriteria();               
              if (isset($startDate)) {                                          
                $criteria4->timestamp(">=",$startTime);                              
              }
              if (isset($endDate)) {                                         
                $criteria4->timestamp("<=",$endTime);
              }              
                            
              $criteria4->type("==",4);     
              $criteria4->sort('timestamp',EMongoCriteria::SORT_DESC)->limit(1)->offset(0);
              $model = MGameServerLog::model()->findAll($criteria4);               
              $curr = 0;
              $startTimeRestart = '';
              if(isset($model))
                foreach($model as $item){
                    if($item->value == $curr){
                      $curr = $item->value;   
                      $startTimeRestart = $item->timestamp;
                    }
                }                
              if($startTimeRestart == ''){
                  $criteria->timestamp(">=",$startTime); 
                  $criteria->timestamp("<=",$endTime);                  
              }else{       
                  $startTimeRestart = new MongoDate(strtotime($startTimeRestart));   
                  $criteria->timestamp(">=",$startTimeRestart); 
                  $criteria->timestamp("<=",$endTime);                   
              }                                

              $timeDiff = abs($endTime->sec - $startTime->sec);              

              $numberDays = $timeDiff/86400;  // 86400 seconds in one day

              // and you might want to convert to integer
              $numberDays = intval($numberDays);
                            
              $criteria->type('==',1);              
                            
              if($numberDays > 7){
                  //$criteria->id('==');    
              }
                                         
              $criteria2->type('==',1);
//              Common::dump($gameServerLog);die;
//              $criteria->order = 'game_server_log.id ASC';   
//              $criteria2->order = 'game_server_log.id DESC';                        
//              $criteria2->limit = 1;  
//              $criteria->select = 'game_server_log.value, game_server_log.timestamp';
              
              $criteria->sort('id',EMongoCriteria::SORT_ASC);
              $criteria2->sort('id',EMongoCriteria::SORT_DESC)->limit(1)->offset(0);
              $currCCUModel = MGameServerLog::model()->findAll($criteria2);                 
              $gameServerLog = MGameServerLog::model()->findAll($criteria);    
              
              $criteria3 = new CDbCriteria;
              $criteria3->addCondition("name = 'ccu_log_time_step'");
              $return['items'] = array();
              $return['items']['configLogCCU'] = Configuration::model()->find($criteria3)->value;
              
              /*$criteria5 = new EMongoCriteria;                           
              $criteria5->type('==',1);
              $criteria5->timestamp(">=",$startTime); 
              $criteria5->timestamp("<=",$endTime);
              $criteria5->sort('value',EMongoCriteria::SORT_DESC);
              $maxCCUModel = MGameServerLog::model()->findAll($criteria5);                         
               */    
              
//              $query = 'Select MAX(CAST(value AS SIGNED)) as max_ccu from game_server_log as g where g.type=1 AND g.timestamp > "'.$startTime.'" AND g.timestamp <= "'.$endTime.'"';
//              $maxCCUModel = Yii::app()->db->createCommand($query)->queryAll();                            
              
              if($gameServerLog){  
                  $totalCCUTB = 0;
                  $return['code'] = 0;
                  $return['message'] = "Thành công";
                  //$return['id'] = $currCCUModel['id'];
                  $return['type'] = $name;
                  if($currCCUModel)
                    $return['items']['currCCU'] = $currCCUModel[0]->value;    
                  $return['items']['maxCCU'] = 0;
                  $return['items']['typeStatistic'] = 'Game Server Log';                  
                  $return['items']['startDate'] = date('Y-m-d',$startTime->sec);
                  $return['items']['endDate'] = date('Y-m-d',$endTime->sec);
                  $return['items']['detail'] = array();
                  $tmp = array();
                  $tmp2 = array();                  
                  $day_before = date('Y-m-d',$gameServerLog[0]->timestamp->sec);                  
                  foreach($gameServerLog as $index => $item){  
                      if($item->value > $return['items']['maxCCU'])
                        $return['items']['maxCCU'] = $item->value;
                      $day_curr = date('Y-m-d', $item->timestamp->sec); 
                      if(intval($item->value) == 0){
                          if(isset($tmp[$index-1]))	
                              $startTimeServer = $tmp[$index-1];
                          else
                              $startTimeServer = $startTime->sec;                          
                          $endTimeServer = $item->timestamp->sec;                            
                          while ($startTimeServer <= $endTimeServer) {                               
                            $curr_date = date('Y-m-d H:i:s', $startTimeServer);                            
                            $return['items']['detail'][$curr_date] = 0;                                 
                            if($day_curr > $day_before){
                                $day_before = $day_curr;                            
                            }else{
                                if(!isset($tmp2[$day_curr])){
                                    $tmp2[$day_curr]['number'] = 0;
                                    $tmp2[$day_curr]['total'] = 0;
                                }
                                else{
                                    $tmp2[$day_curr]['number'] ++;
                                }
                            } 
                            $startTimeServer += strtotime('+1 minutes', 0);                                
                          }                          
                      }
                      $tmp[$index] = $item->timestamp->sec;
                      $return['items']['detail'][date('Y-m-d H:i:s',$item->timestamp->sec)] = intval($item->value);
                                         
                        if($day_curr > $day_before){
                            $day_before = $day_curr;                            
                        }else{
                            if(!isset($tmp2[$day_curr])){
                                $tmp2[$day_curr]['number'] = 0;
                                $tmp2[$day_curr]['total'] = 0;
                            }
                            else{
                                $tmp2[$day_curr]['number'] ++;
                                $tmp2[$day_curr]['total'] += intval($item->value);
                            }
                        }                        
                  }                  
                                    
                  $return['items']['detailList'] = array();
                  $tmp3 = 0;
                  $tmp4 = 0;
                  if(count($tmp2) > 0){
                    foreach($tmp2 as $index=> $item){
                        $return['items']['detailList'][$index] = ceil($item['total']/$item['number']);
                        $tmp3 += $item['total'];
                        $tmp4 += $item['number'];
                    }
                  }
                  $return['items']['tbCCU'] = ceil($tmp3/$tmp4);      
                        
              }else{
                  $return['code'] = 1;
                  $return['message'] = "Thất bại";             
              }
            }else{
                $return['code'] = -1;
                $return['message'] = "Thiếu dữ liệu nhập vào";
            }
        //}
        echo json_encode($return);
   }  
      
   public function actionGetGameServerLogBk(){
        $return = array();
//        $token = Yii::app()->request->getParam('accessToken');        
//        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
//            $return['code'] = -1;
//            $return['message'] = "Bạn không có quyền truy cập";
//        } else {                                
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');                         
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("1 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                               
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }
            $name = Yii::app()->request->getParam('name'); 
            if(isset($name)){                                           
              $criteria = new CDbCriteria;                         
              $criteria2 = new CDbCriteria;
              $criteria4 = new CDbCriteria(); 
              $criteria->alias = $criteria2->alias = 'game_server_log';
              if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';            
                $criteria4->addCondition("timestamp >= '" . $startTime . "'");                
                }
              if (isset($endDate)) {
                $endTime = $endDate. ' 23:59:59';
                $criteria4->addCondition("timestamp <='" . $endTime . "'");                
              }               
              
              $criteria4->order = 'timestamp DESC';
              $criteria4->limit = 1;
              $criteria4->addCondition("type = 4");
              $criteria4->addCondition("timestamp >= '" . $startTime . "'");   
              $criteria4->addCondition("timestamp <='" . $endTime . "'");   
              $model = GameServerLog::model()->findAll($criteria4);                     
              $curr = 0;
              $startTimeRestart = '';
              if(isset($model))
                foreach($model as $item){
                    if($item->value == $curr){
                      $curr = $item->value;   
                      $startTimeRestart = $item->timestamp;
                    }
                }                
              if($startTimeRestart == ''){
                  $criteria->addCondition("timestamp >= '" . $startTime . "'");   
                  $criteria->addCondition("timestamp <='" . $endTime . "'");   
              }else{                 
                  $criteria->addCondition("timestamp >= '" . $startTimeRestart . "'");   
                  $criteria->addCondition("timestamp <='" . $endTime . "'");   
              }    
              
              $startTimeStamp = strtotime($startTime);
              $endTimeStamp = strtotime($endTime);

              $timeDiff = abs($endTimeStamp - $startTimeStamp);

              $numberDays = $timeDiff/86400;  // 86400 seconds in one day

              // and you might want to convert to integer
              $numberDays = intval($numberDays);
              
              $criteria->join = $criteria2->join = 'INNER JOIN game_server_log_type as game_server_log_type on game_server_log.type = game_server_log_type.id ';
              $criteria->addCondition("game_server_log_type.name='".$name."'");
              if($numberDays > 7)
                  $criteria->addCondition('(game_server_log.id%2)=0');              
              $criteria2->addCondition("game_server_log_type.name='".$name."'");              
              $criteria->order = 'game_server_log.id ASC';   
              $criteria2->order = 'game_server_log.id DESC';                        
              $criteria2->limit = 1;  
              $criteria->select = 'game_server_log.value, game_server_log.timestamp';
              $currCCUModel = GameServerLog::model()->find($criteria2);   
              
              $gameServerLog = GameServerLog::model()->findAll($criteria);                
              $criteria3 = new CDbCriteria;
              $criteria3->addCondition("name = 'ccu_log_time_step'");
              $return['items'] = array();
              $return['items']['configLogCCU'] = Configuration::model()->find($criteria3)->value;
              
              $query = 'Select MAX(CAST(value AS SIGNED)) as max_ccu from game_server_log as g inner join game_server_log_type as t on t.id = g.type where t.name = "ccu" AND g.timestamp > "'.$startTime.'" AND g.timestamp <= "'.$endTime.'"';
              $maxCCUModel = Yii::app()->db->createCommand($query)->queryAll();                            
              
              if($gameServerLog){  
                  $totalCCUTB = 0;
                  $return['code'] = 0;
                  $return['message'] = "Thành công";
                  //$return['id'] = $currCCUModel['id'];
                  $return['type'] = $name;
                  if($currCCUModel)
                    $return['items']['currCCU'] = $currCCUModel['value'];
                  if($maxCCUModel)
                    $return['items']['maxCCU'] = $maxCCUModel[0]['max_ccu'];
                  $return['items']['typeStatistic'] = 'Game Server Log';                  
                  $return['items']['startDate'] = $startDate;
                  $return['items']['endDate'] = $endDate;
                  $return['items']['detail'] = array();
                  $tmp = array();
                  $tmp2 = array();
                  $day_before = date('Y-m-d',strtotime($gameServerLog[0]->timestamp));
                  foreach($gameServerLog as $index => $item){   
                      $day_curr = date('Y-m-d', strtotime($item->timestamp)); 
                      if($item->value == 0){
						if(isset($tmp[$index-1]))	
							$startTimeServer = strtotime($tmp[$index-1]);
						else
							$startTimeServer = strtotime($startTime);	
                          $endTimeServer = strtotime($item->timestamp);                            
                          while ($startTimeServer <= $endTimeServer) {                               
                            $curr_date = date('Y-m-d H:i:s', $startTimeServer);                            
                            $return['items']['detail'][$curr_date] = 0;                                 
                            if($day_curr > $day_before){
                                $day_before = $day_curr;                            
                            }else{
                                if(!isset($tmp2[$day_curr])){
                                    $tmp2[$day_curr]['number'] = 0;
                                    $tmp2[$day_curr]['total'] = 0;
                                }
                                else{
                                    $tmp2[$day_curr]['number'] ++;
                                }
                            } 
                            $startTimeServer += strtotime('+1 minutes', 0);                                
                          }                          
                      }
                      $tmp[$index] = $item->timestamp;
                      $return['items']['detail'][$item->timestamp] = $item->value;
                                         
                        if($day_curr > $day_before){
                            $day_before = $day_curr;                            
                        }else{
                            if(!isset($tmp2[$day_curr])){
                                $tmp2[$day_curr]['number'] = 0;
                                $tmp2[$day_curr]['total'] = 0;
                            }
                            else{
                                $tmp2[$day_curr]['number'] ++;
                                $tmp2[$day_curr]['total'] += $item->value;
                            }
                        }                        
                  }                  
                                    
                  $return['items']['detailList'] = array();
                  $tmp3 = 0;
                  $tmp4 = 0;
                  if(count($tmp2) > 0){
                    foreach($tmp2 as $index=> $item){
                        $return['items']['detailList'][$index] = ceil($item['total']/$item['number']);
                        $tmp3 += $item['total'];
                        $tmp4 += $item['number'];
                    }
                  }
                  $return['items']['tbCCU'] = ceil($tmp3/$tmp4);      
                        
              }else{
                  $return['code'] = 1;
                  $return['message'] = "Thất bại";             
              }
            }else{
                $return['code'] = -1;
                $return['message'] = "Thiếu dữ liệu nhập vào";
            }
        //}
        echo json_encode($return);
   }  
   
   public function actionGetReplyFeedback(){       
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $feedback_id = Yii::app()->request->getParam('feedback_id');
            if(!isset($feedback_id) || !is_numeric($feedback_id)){
                $return['code'] = -1;
                $return['message'] =  "Đầu vào không đúng";
                echo json_encode($return);die;
            }
            $model = ReplyFeedback::model()->findAll('feedback_id='.$feedback_id);
            if(isset($model)){
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach($model as $index => $item){
                    $return['items'][$index]['content'] = $item->content;
                    $return['items'][$index]['user_id'] = $item->user_id;
                    $return['items'][$index]['username'] = $item->user->username;
                    $return['items'][$index]['timestamp'] = $item->timestamp;
                }
            }
        }
        echo json_encode($return);        
   }
   
   public function actionCreateReplyFeedback(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $feedback_id = Yii::app()->request->getParam('feedback_id');
            $has_notification = Yii::app()->request->getParam('has_notification');
            $has_email = Yii::app()->request->getParam('has_email');
            $has_display_website = Yii::app()->request->getParam('has_display_website');
            if(!isset($feedback_id) || !is_numeric($feedback_id)){
                $return['code'] = -1;
                $return['message'] =  "Đầu vào không đúng";
                echo json_encode($return);die;
            }
            $feedbackModel = Feedback::model()->findByPk($feedback_id);
            if(!$feedbackModel){
                $return['code'] = -2;
                $return['message'] = "Không tồn tại Feedback";
                echo json_encode($return);die;
            }
            $content = Yii::app()->request->getParam('content');
            $content_notification = Yii::app()->request->getParam('content_notification');
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user; 
            $model = new ReplyFeedback();
            $model->content = $content;
            $model->content_notification = $content_notification;
            $model->user_id = $user->id;
            $model->feedback_id = $feedback_id;
            $model->has_notification = $has_notification;
            $model->has_email = $has_email;
            $model->has_display_website = $has_display_website;
            if($model->save()){
                $sendMessage = MUserMessage::model()->sendUserMessage($feedbackModel->user_id, strip_tags(trim(html_entity_decode($content, ENT_QUOTES, 'UTF-8'), "\xc2\xa0")),1);
                if($model->has_email == 1){
                    $userModel = User::model()->findByPk($feedbackModel->user_id); 
                    if(isset($userModel->email)){
                        $message = new YiiMailMessage;
                        //this points to the file test.php inside the view path
                        $message->view = "feedback";
                        $params = array('userinfo' => $userModel, 'content'=>$content);
                        $message->subject = 'Trả lời ý kiến phản hồi';
                        $message->setBody($params, 'text/html');
                        $message->addTo($userModel->email);
                        //$message->from = 'game@eposi.vn';
                        $message->setFrom(array('admin@esimo.vn' => 'Esimo.vn'));
                        $result = Yii::app()->mail->send($message);
                    }
                }
                if($model->has_notification == 1){
                    $content_notification = '{"title":"Esimo","badge":"1","alert":"Bạn vừa nhận được một phản hồi từ Esimo","message":"Bạn vừa nhận được một phản hồi từ Esimo"}';
                    $users = array();        
                    array_push($users, $feedbackModel->user_id);                    
                    if(!empty($users)){
                        $contentNotify = json_decode($content_notification, true);       
                        $push = new PushNotificationLog();
                        $push->pushNotification($users, $contentNotify);
                    }
                }
                $return['code'] = 0;
                $return['message'] = "Thành công";
            }else{
                $return['code'] = 1;
                $return['message'] = "Không thành công";
            }    
        }
        echo json_encode($return);
   }
   
   public function actionDeleteReplyFeedback(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {            
            $id = Yii::app()->request->getParam('id');
            if (!isset($id) || !is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu đầu vào không đúng";
            } else {                                             
                $model = ReplyFeedback::model()->findByPk($id);
                if (!empty($model)) {
                    if ($model->delete()) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                    } else {
                        $return['code'] = 1;
                        $return['message'] = "Xóa dữ liệu thất bại";
                    }
                } else {
                    $return['code'] = 3;
                    $return['message'] = "Không tồn tại";
                }
            }
        }
        echo json_encode($return);
   }
   
   public function actionGetPushNotification(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {  
            $id = Yii::app()->request->getParam('id');
            if(isset($id) && is_numeric($id)){
                $model = PushNotificationLog::model()->findByPk($id);                
            }else{
                $model = PushNotificationLog::model()->findAll();
            }
            if(!isset($model)){
                $return['code'] = -2;
                $return['message'] = 'Không tồn tại notification';
                echo json_encode($return);die;
            }            
            $return['code'] = 0;
            $return['message'] = "Thành công";
            if(!isset($id)){
                $return['items'] = array();
                foreach($model as $index => $item){
                    $return['items'][$index]['id'] = $item->id;
                    $return['items'][$index]['content'] = $item->content;
                    $return['items'][$index]['times'] = $item->times;
                    $return['items'][$index]['time_sent'] = $item->time_sent;
                    $return['items'][$index]['username'] = $item->user->username;
                    $return['items'][$index]['status'] = $item->status;
                    $return['items'][$index]['create_time'] = $item->create_time;
                    if(!empty($item->receiver_ids)){
                        $receiver_ids = explode(',',$item->receiver_ids);
                        foreach($receiver_ids as $index2 => $item2){
                            $return['items'][$index]['receiver'][$index2]['id'] = $item2;
                       }
                   }
                }
            }else{
                $return['id'] = $model->id;
                $return['content'] = $model->content;
                $return['times'] = $model->times;
                $return['time_sent'] = $model->time_sent;
                $return['username'] = $model->user->username;
                $return['status'] = $model->status;
                $return['receiver'] = array();
                if(!empty($model->receiver_ids)){
                    $receiver_ids = explode(',',$model->receiver_ids);
                    foreach($receiver_ids as $index => $item){
                         $return['receiver'][$index]['id'] = $item;
                         $user = User::model()->findByPk($item);
                         $return['receiver'][$index]['username'] = $user->username;
                    }
                }
                $return['create_time'] = $model->create_time;
            }                        
        }
        echo json_encode($return);
   }
      
     public function actionCreatePushNotification(){                    
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {            
            $time_sent = Yii::app()->request->getParam('time_sent');  
            $laster = true;
            if(empty($time_sent)){
                $laster = false;
                $time_sent = date('Y-m-d h:i:s');
            }
            $times = Yii::app()->request->getParam('times');
            $times = 1;
            $content = Yii::app()->request->getParam('content');
            $receiver_ids = Yii::app()->request->getParam('users');
            if(!isset($content) || !isset($times) || !isset($time_sent)){
                $return['code'] = -2;
                $return['message'] = "Thiếu dữ liệu đầu vào";
                echo json_encode($return);die;
            }
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user; 
            $model = new PushNotificationLog();
            $model->content = $content;
            $model->user_id = $user->id;
            $model->times = $times;
            $model->time_sent = $time_sent;
            if(isset($receiver_ids)){
                $receiver_ids = implode(',',$receiver_ids);
                $model->receiver_ids = $receiver_ids;
            }
            if(!$laster){
                $model->status = 1;
            }else{
                $model->status = 0;
            }    
            if($model->save()){  
                if($laster){
                    $file_path = "../../../../../../var/spool/cron/apache";                
                    // Open the file to get existing content
                    $current = file_get_contents($file_path);
                    // Append a new person to the file
                    $url = Yii::app()->createAbsoluteUrl('apiAdmin/runPushNotification?id='.$model->id);                
                    $tmp = explode(' ', $model->time_sent);                
                    $day = explode('-',$tmp[0]);
                    $time = explode(':',$tmp[1]);
                    $str = $time[1]." ".$time[0]." ".$day[2]." ".$day[1]." *";
                    $current .= $str." curl-job ".$url." "."\n";
                    // Write the contents back to the file
                    file_put_contents($file_path, $current);    
                }
                $return['code'] = 0;
                $return['message'] = "Thành công";
            }else{                
                $return['code'] = 1;
                $return['message'] = "Không thành công";
            }    
        }
        echo json_encode($return);
    } 
    
    public function actionUpdatePushNotification(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {       
            $id = Yii::app()->request->getParam('id');
            $sentNow = Yii::app()->request->getParam('sentNow');
            $notSent = Yii::app()->request->getParam('notSent');
            if(!isset($id) || !is_numeric($id)){
                $return['code'] = -1;
                $return['message'] = 'Thiếu dữ liệu đầu vào';
                echo json_encode($return);die;
            }
            $model = PushNotificationLog::model()->findByPk($id);
            if(!isset($model)){
                $return['code'] = -3;
                $return['message'] = 'Dữ liệu sai';
                echo json_encode($return);die;
            }                        
            
            if($model->status == 1){
                $return['code'] = -4;
                $return['message'] = 'Tin nhắn này đã gửi, bạn không được sửa';
                echo json_encode($return);die;
            }
            
            if($sentNow){                
                $this->redirect(Yii::app()->createUrl('apiAdmin/runPushNotification?id='.$id));
            }
            
            if($notSent) $model->status = -1;
            
            $content = Yii::app()->request->getParam('content');
            $receiver_ids = Yii::app()->request->getParam('users');
                        
            if(!isset($content)){
                $return['code'] = -2;
                $return['message'] = "Thiếu dữ liệu đầu vào";
                echo json_encode($return);die;
            }            
            $model->content = $content;                                    
            if(isset($receiver_ids)){
                $receiver_ids = implode(',',$receiver_ids);
                $model->receiver_ids = $receiver_ids;
            }
                        
            if($model->save()){                  
                $return['code'] = 0;
                $return['message'] = "Thành công";
            }else{          
                Common::dump($model->errors());die;
                $return['code'] = 1;
                $return['message'] = "Không thành công";
            }    
        }
        echo json_encode($return);
    }
    
    public function actionRunPushNotification(){        
        $id = Yii::app()->request->getParam('id');
        $model = PushNotificationLog::model()->findByPk($id);
        if(!isset($model) || $model->status != 0){
            exit();
        }        
        if(!empty($model->receiver_ids))
            $users = explode(',', $model->receiver_ids);
        else
            $users = array();        
        $content = json_decode($model->content, true);      
        $push = new PushNotificationLog();
        $push->pushNotification($users, $content);
        $model->status = 1;
        $model->save();
    }            
    
    // tăng tỉ giá nạp tiền. chạy cron tab.
    public function actionChangeRatioRecharge(){                
//        $model = RechargeType::model()->findAll('type="mobile_card"');        
//        foreach($model as $item){
//            $item->ratio = 1;
//            $item->save();
//        }
//        echo "Thành công với ratio là 1";
//        die;
    }
    
    public function actionTotalUserActiveTest(){        
        $arr = array();
        $arr2 = array();
        //user choi game trong thang 2
        $query = 'select u.id from `user` as u inner join user_game_action as g on g.user_id = u.id where u.create_time < "2014-02-28 23:59:59" AND u.is_virtual_player != 1 AND u.id !=0 AND g.game_action_id = 38 AND g.`timestamp` > "2014-02-01 00:00:00" AND g.`timestamp` < "2014-02-28 23:59:59" group by u.id';
        $results = Yii::app()->db->createCommand($query)->queryAll();
        foreach($results as $item){
            array_push($arr, $item['id']);
        }        
        $str = implode(',',$arr);
        $str = "(".$str.")";        
        // user khong choi game trong thang 2
        $query2 = 'select id from user where create_time < "2014-02-28 23:59:59" AND id NOT IN '.$str;        
        $results2 = Yii::app()->db->createCommand($query2)->queryAll();        
        foreach($results2 as $item){
            array_push($arr2, $item['id']);
        }
        $str2 = implode(',',$arr2);
        $str2 = "(".$str2.")";        
        $query3 = 'select u.id from `user` as u inner join user_game_action as g on g.user_id = u.id where u.create_time < "2014-02-28 23:59:59" AND u.is_virtual_player != 1 AND u.id !=0 AND g.game_action_id = 38 AND g.`timestamp` > "2014-01-01 00:00:00" AND g.`timestamp` < "2014-01-31 23:59:59" AND u.id IN '.$str2.' group by u.id ';
        $results3 = Yii::app()->db->createCommand($query3)->queryAll();       
        Common::dump(count($results3));die;
    }
    
    public function actionGetNewDevice() {        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {   
            $gameStatistic = Yii::app()->request->getParam('gameStatistic');
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                           
                    $return['items']['typeStatistic'] = 'New User';
                    $return['items']['startDate'] = $startDate;
                    $return['items']['endDate'] = $endDate;
                    echo json_encode($return);die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }            
            
            if (isset($startDate)) {
                $startTime =$startDate.' 00:00:00';                            
            };
            if (isset($endDate)) {
                $endTime =$endDate. ' 23:59:59';                
            };                        
            
            $user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;       
            $partner = $user->getAdminPartner();            
            if($partner){                    
                if($partner->code_identifier == 'esimo'){
                    $partner_id = Yii::app()->request->getParam('partner_id');
                    if(isset($partner_id)){                                                
                        $query = "select 
                                    `device_token`.*                                
                                from
                                    `device_token`
                                INNER JOIN user_device_token as user_device_token on user_device_token.device_token_id = device_token.id
                                INNER JOIN user_partner on user_partner.user_id = user_device_token.user_id    
                                where                        
                                        `device_token`.`timestamp` > '".$startTime."'
                                        AND `device_token`.`timestamp` < '".$endTime."'
                                        AND user_partner.partner = '".$partner_id."'    
                                        AND user_device_token.active = 1 
                                group by `device_token`.`token`
                                        ";                                                
                    }else{
                       $query = "select                                     
                                    *
                                from
                                    `device_token`
                                where                        
                                        `timestamp` > '".$startTime."'
                                        AND `timestamp` < '".$endTime."'  
                                group by `token`            
                                        ";
                    }

                }else{                                   
                    $query = "select 
                                    `device_token`.*
                                from
                                    `device_token`
                                INNER JOIN user_device_token as user_device_token on user_device_token.device_token_id = device_token.id
                                INNER JOIN user_partner on user_partner.user_id = user_device_token.user_id    
                                where                        
                                        `device_token`.`timestamp` > '".$startTime."'
                                        AND `device_token`.`timestamp` < '".$endTime."'
                                        AND user_partner.partner = '".$partner->id."' 
                                        AND user_device_token.active = 1  
                                group by `device_token`.`token`
                                        ";                       
                }
            }                                                                                                     
                                        
            $deviceTokenModel = Yii::app()->db->createCommand($query)->queryAll();  
            $countDevice = count($deviceTokenModel);
            if ($countDevice > 0) {
                $index = 0;
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['count'] = $countDevice;
                $return['items'] = array();
                $return['items']['typeStatistic'] = 'New Devices';
                $return['items']['startDate'] = $startDate;
                $return['items']['endDate'] = $endDate;
                $return['items']['count'] = $countDevice;
                
                $return['items']['detail'] = array();
                $startDate = strtotime($startDate);
                $endDate = strtotime($endDate);
                $days = array();                 
                while ($startDate <= $endDate) {  
                    $index ++;
                    $day = date('Y-m-d', $startDate); 
                    $return['items']['detail'][$day] = 0; 
                    foreach($deviceTokenModel as $item ){
                        $date = date('Y-m-d',strtotime($item['timestamp']));                    
                        if($day == $date){
                            $return['items']['detail'][$day] ++;
                        }    
                    }                                    
                    $startDate += strtotime('+1 day', 0);
                } 
                $return['items']['tb'] = ceil($countDevice/$index);
            } else {
                $return['code'] = 1;
                $return['message'] = "Không có device nào";
            }
        }
        echo json_encode($return);
    }     
    
    public function calculatorRealRecharge(CDbCriteria $criteria, $provider_sms, $provider_card, $condition = NULL){
        $return = array();      
       
        if($provider_sms == 'fibo' && $condition != NULL){   
             $condition .= " AND topup_provider.code_identifier = 'fibo'";
             $criteria->condition = $condition;       
        }
        if($provider_card == 'nganluong' && $condition != NULL){
             $condition .= " AND topup_provider.code_identifier = 'nganluong'";
             $criteria->condition = $condition;
        }
        if($provider_card == 'vmg' && $provider_sms == 'vmg' && $condition != NULL){
             $condition .= " AND topup_provider.code_identifier = 'vmg'";
             $criteria->condition = $condition;
        } 
        
        if($provider_card == 'appota' && $provider_sms == 'appota' && $condition != NULL){
             $condition .= " AND topup_provider.code_identifier = 'appota'";
             $criteria->condition = $condition;
        }
        
        $rechargeListAlls = Recharge::model()->findAll($criteria);  
        $totalSms = 0;
        $totalCard = array();
        if(isset($rechargeListAlls)){
            foreach($rechargeListAlls as $item){
                if($item->rechargeType->rechargeMethodProvider->rechargeMethod->code_identifier == "sms"){
                    $totalSms += $item->rechargeType->real_value;
                }else{
                    if(!isset($totalCard[$item->rechargeType->telco->code_identifier]))
                        $totalCard[$item->rechargeType->telco->code_identifier] = 0;                        
                    $totalCard[$item->rechargeType->telco->code_identifier] += $item->rechargeType->real_value;
                }
            }
        }                        
        $opt = 0;
        $totalCardAll = 0;
        if($provider_card == 'vmg'){
            if(count($totalCard) > 0){
                foreach($totalCard as $item){
                    $totalCardAll += $item;
                }
                if($totalCardAll < 100000000){
                    $opt = 0;
                }elseif($totalCardAll >=100000000 && $totalCardAll < 300000000){
                    $opt = 1;
                }elseif($totalCardAll >=300000000 && $totalCardAll < 700000000){
                    $opt = 2;
                }else{
                    $opt = 3;
                }
            }
        }
        if($provider_card == 'nganluong')
            $opt = 0;
        
         if($provider_card == 'appota')
            $opt = 0;
                

        $optConfig = array();
        $totalCardAllReal = 0;
        if($provider_card){            
            $configProvider = TopupProvider::model()->getConfig($provider_card);            
            if(!empty($configProvider)){                
                foreach($configProvider as $key => $item){
                    $tmps = explode(',', $item);                
                    $optConfig[$key] = $tmps[$opt];
                }
            }            
            if(count($totalCard) > 0){
               foreach($totalCard as $key => $item){
                   $totalCardAllReal += ((100 - $optConfig[$key]) * $item) /100; 
               }
            }
        }
        $optConfigSms = 0;
        if($provider_sms == 'vmg'){
            if($totalSms < 100000000){
                $totalSms = $totalSms*0.85;
                $optConfigSms = 0.85;
            }elseif($totalSms >=100000000 && $totalSms < 200000000){
                $totalSms = $totalSms*0.88;
                $optConfigSms = 0.88;
            }elseif($totalSms >=200000000 && $totalSms <500000000){
                $totalSms = $totalSms*0.89;
                $optConfigSms = 0.89;
            }elseif($totalSms >=500000000 && $totalSms <1500000000){
                $totalSms = $totalSms*0.9;
                $optConfigSms = 0.9;
            }else{
                $totalSms = $totalSms*0.92;
                $optConfigSms = 0.92;
            }
        }
        if($provider_sms == 'fibo'){
            $totalSms = $totalSms*0.8;
            $optConfigSms= 0.8;
        }
        
        if($provider_sms == 'appota'){
            $totalSms = $totalSms*0.65;
            $optConfigSms= 0.65;
        }
        
        if($provider_card == 'appota'){
            $totalCardAllReal = $totalCardAllReal * 0.65;
        }
        
        $return['totalCard'] = $totalCardAllReal;            
        $return['totalSms'] = $totalSms;
        $return['totalReal'] = $totalCardAllReal + $totalSms;       
        $return['optConfig'] = $optConfig;        
        $return['optConfigSms'] = $optConfigSms;        
        return $return;
    }
    
    public function actionUserRechargeExportExcel(){                     
        $startDate = Yii::app()->request->getParam('startDate');
        $endDate = Yii::app()->request->getParam('endDate');  
        $type = Yii::app()->request->getParam('rechargeType');        
        if(!isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime("7 days ago"));               
        }elseif(!isset($endDate) && isset($startDate)){
            $endDate = date('Y-m-d');
            $startDate = date("Y-m-d", strtotime($startDate));
            if($startDate > $endDate){
                die;
            }
        }elseif(isset($endDate) && !isset($startDate)){
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
        }else{
            $endDate = date('Y-m-d', strtotime($endDate));                
            $startDate = date("Y-m-d", strtotime($startDate));  
        }       
        $startTime = $startDate.' 00:00:00';
        $endTime = $endDate.' 23:59:59';
        $model = new Recharge('search');  // your model
        $model->unsetAttributes();  // clear any default values        
        //below is code we added for export search result to CSV file.
        $criteria=new CDbCriteria;     
        $criteria->alias = 'recharge';                  
        $criteria->join = 'INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id';            
        $criteria->join .= ' INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id';
        $criteria->join .= ' INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id ';       
        if (isset($startDate)) {
            $startTime = $startDate. ' 00:00:00';
            $criteria->addCondition('recharge.create_time >= "' . $startTime . '"');             
        };
        if (isset($endDate)) {
            $endTime = $endDate.' 23:59:59';
            $criteria->addCondition('recharge.create_time <= "' . $endTime . '"');             
        };
        if(!empty($type)){                               
           $criteria->addCondition('recharge_method.code_identifier="'.$type.'"');                       
        }
        
        $criteria->order = "recharge.create_time";
        $data = new CActiveDataProvider($model, array(
                'criteria'=>$criteria,
        ));        
        $this->widget('EExcelView', array(
                'id'=>'tool-grid',
                'dataProvider'=>$data,
                'title'=>'Report',
                'grid_mode'=>'export',
                'filename'=>'nap-tien-'.$startDate.'-den-'.$endDate,
                'exportType'=>'Excel2007',
                'autoWidth'=>true,
                'stream'=>TRUE, //make sure you output to browser
                'columns'=>array(                       
                    array(
                        'name' => 'id',            
                        'value'=>'$data->id'
                    ),  
                    array(
                        'header' => 'Kênh nạp',            
                        'value'=>'$data->rechargeType->rechargeMethodProvider->rechargeMethod->name',
                        'htmlOptions'=>array('align'=>'center')
                    ),
                    array(
                        'header' => 'Nhà mạng',            
                        'value'=>'$data->rechargeType->telco->name',
                        'htmlOptions'=>array('align'=>'center')
                    ),  
                    array(
                        'header' => 'Tài khoản',            
                        'value'=>'$data->user->username',                        
                    ),
                    array(
                        'header' => 'Số tiền khách hàng',            
                        'value'=>'$data->value',                        
                    ),   
                    array(
                        'header' => 'Số Gold',            
                        'value'=>'$data->gold'
                    ),
                    array(
                        'header' => 'Thời gian',            
                        'value'=>'$data->create_time'
                    ),
                    
                ),
        ));
        Yii::app()->end();
        //some people add Yii::app()->end(); here, but it's not necessary, see EExcelView.php source code.        
    }
    
    public function actionRechargeExportExcel(){
//        $return = array();
//        $token = Yii::app()->request->getParam('accessToken');        
//        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
//            $return['code'] = -1;
//            $return['message'] = "Bạn không có quyền truy cập";
//        } else {
            //$user = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user; 
            $startDate = Yii::app()->request->getParam('startDate');
            $endDate = Yii::app()->request->getParam('endDate');  
            $type = Yii::app()->request->getParam('rechargeType'); 
            $provider = Yii::app()->request->getParam('provider');
            if(!isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime("7 days ago"));               
            }elseif(!isset($endDate) && isset($startDate)){
                $endDate = date('Y-m-d');
                $startDate = date("Y-m-d", strtotime($startDate));
                if($startDate > $endDate){
                    die;
                }
            }elseif(isset($endDate) && !isset($startDate)){
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", (strtotime($endDate) - 7* 24 * 3600));                                
            }else{
                $endDate = date('Y-m-d', strtotime($endDate));                
                $startDate = date("Y-m-d", strtotime($startDate));  
            }       
            $startTime = $startDate.' 00:00:00';
            $endTime = $endDate.' 23:59:59';
            $model = new Recharge('search');  // your model
            $model->unsetAttributes();  // clear any default values        
            //below is code we added for export search result to CSV file.            
            $criteria=new CDbCriteria;     
            $criteria->alias = 'recharge';                  
            $criteria->join = 'INNER JOIN recharge_type on recharge_type.id = recharge.recharge_type_id';            
            $criteria->join .= ' INNER JOIN recharge_method_provider on recharge_type.recharge_method_provider_id = recharge_method_provider.id';
            $criteria->join .= ' INNER JOIN recharge_method on recharge_method_provider.recharge_method_id = recharge_method.id '; 
            $criteria->join .= ' INNER JOIN topup_provider on recharge_method_provider.topup_provider_id = topup_provider.id '; 
            if (isset($startDate)) {
                $startTime = $startDate. ' 00:00:00';
                $criteria->addCondition('recharge.create_time >= "' . $startTime . '"');             
            };
            if (isset($endDate)) {
                $endTime = $endDate.' 23:59:59';
                $criteria->addCondition('recharge.create_time <= "' . $endTime . '"');             
            };
            if(!empty($type)){                               
               $criteria->addCondition('recharge_method.code_identifier="'.$type.'"');                       
            }
            
            if(!empty($provider)){                               
               $criteria->addCondition('topup_provider.code_identifier="'.$provider.'"');                       
            }
            $return = array();
            
            if($provider == 'vmg'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria,'vmg','vmg');  
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig']; 
                $optionConfigSmsVmg = $calculatorRealRecharge['optConfigSms'];
                $return['optConfig'] = $optConfig;
                $return['optConfigSms'] = $optionConfigSmsVmg;
            }elseif($provider == 'fibo'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria,'fibo',''); 
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig'];    
                $optionConfigSmsFibo = $calculatorRealRecharge['optConfigSms'];
                $return['optConfig'] = $optConfig;
                $return['optConfigSms'] = $optionConfigSmsFibo;
            }elseif($provider == 'nganluong'){
                $calculatorRealRecharge = $this->calculatorRealRecharge($criteria,'','nganluong');                 
                $totalCard = $calculatorRealRecharge['totalCard'];
                $totalSms = $calculatorRealRecharge['totalSms'];
                $totalReal = $calculatorRealRecharge['totalReal'];
                $optConfig = $calculatorRealRecharge['optConfig'];   
                $optionConfigSmsNL = $calculatorRealRecharge['optConfigSms'];
                $return['optConfig'] = $optConfig;
                $return['optConfigSms'] = $optionConfigSmsNL;
            }else{
                $calculatorRealRechargeNL = $this->calculatorRealRecharge($criteria,'','nganluong');                 
                $calculatorRealRechargeFibo = $this->calculatorRealRecharge($criteria,'fibo','');
                $calculatorRealRechargeVmg = $this->calculatorRealRecharge($criteria,'vmg','vmg'); 
                $totalCard = $calculatorRealRechargeNL['totalCard'] + $calculatorRealRechargeFibo['totalCard'] + $calculatorRealRechargeVmg['totalCard'];
                $totalSms = $calculatorRealRechargeNL['totalSms'] + $calculatorRealRechargeFibo['totalSms'] + $calculatorRealRechargeVmg['totalSms'];
                $totalReal = $calculatorRealRechargeNL['totalReal'] + $calculatorRealRechargeFibo['totalReal'] + $calculatorRealRechargeVmg['totalReal'];
                $optConfigNL = $calculatorRealRechargeNL['optConfig']; 
                $optConfigFibo = $calculatorRealRechargeFibo['optConfig']; 
                $optConfigVmg = $calculatorRealRechargeVmg['optConfig']; 
                $optionConfigSmsNL = $calculatorRealRechargeNL['optConfigSms'];
                $optionConfigSmsFibo = $calculatorRealRechargeFibo['optConfigSms'];
                $optionConfigSmsVmg = $calculatorRealRechargeVmg['optConfigSms'];
                $return['optConfigNL'] = $optConfigNL;
                $return['optConfigFibo'] = $optConfigFibo;
                $return['optConfigVmg'] = $optConfigVmg;
                $return['optConfigSmsNL'] = $optionConfigSmsNL;
                $return['optConfigSmsFibo'] = $optionConfigSmsFibo;
                $return['optConfigSmsVmg'] = $optionConfigSmsVmg;
                
            }                        
            $return['totalCard'] = $totalCard;            
            $return['totalSms'] = $totalSms;
            $return['totalReal'] = $totalReal;                                    
            //$calculatorRealRecharge = $this->calculatorRealRecharge($criteria);                
            $model->total_card = $return;
            $model->provider = $provider;            
            
            $criteria->order = "recharge.create_time";            
            $data = new CActiveDataProvider($model, array(
                    'criteria'=>$criteria,
                    'pagination'=>array(
                          'pageSize'=>1000000000
                      )
                    
            ));              
            
            $this->widget('application.components.tlbExcelView', array(
                'id'                   => 'some-grid',
                'dataProvider'         => $data,
                'grid_mode'            => 'export', // Same usage as EExcelView v0.33
                'template'           => "{summary}\n{items}\n{exportbuttons}\n{pager}",
                'title'                => 'Nap-tien-'.$startDate.'-den-'.$endDate,
                'creator'              => '',//$user->username,
                'subject'              => mb_convert_encoding('Something important with a date in French: ' . utf8_encode(strftime('%e %B %Y')), 'ISO-8859-1', 'UTF-8'),
                'description'          => mb_convert_encoding('Etat de production généré à la demande par l\'administrateur (some text in French).', 'ISO-8859-1', 'UTF-8'),
                'lastModifiedBy'       => '',//$user->username,
                'sheetTitle'           => 'Nap-tien',
                'keywords'             => '',
                'category'             => '',
                'landscapeDisplay'     => true, // Default: false
                'A4'                   => true, // Default: false - ie : Letter (PHPExcel default)
                'pageFooterText'       => '&RThis is page no. &P of &N pages', // Default: '&RPage &P of &N'
                'automaticSum'         => true, // Default: false
                'decimalSeparator'     => ',', // Default: '.'
                'thousandsSeparator'   => '.', // Default: ','
                //'displayZeros'       => false,
                //'zeroPlaceholder'    => '-',
                'sumLabel'             => 'Totals:', // Default: 'Totals'
                'borderColor'          => '00FF00', // Default: '000000'
                'bgColor'              => 'FFFFFF', // Default: 'FFFFFF'
                'textColor'            => 'FF0000', // Default: '000000'
                'rowHeight'            => 45, // Default: 15
                'headerBorderColor'    => 'FF0000', // Default: '000000'
                'headerBgColor'        => 'CCCCCC', // Default: 'CCCCCC'
                'headerTextColor'      => '0000FF', // Default: '000000'
                'headerHeight'         => 20, // Default: 20
                'footerBorderColor'    => '0000FF', // Default: '000000'
                'footerBgColor'        => '00FFCC', // Default: 'FFFFCC'
                'footerTextColor'      => 'FF00FF', // Default: '0000FF'
                'footerHeight'         => 50, // Default: 20
                'zoomScale'            => 100, // Default: 100
                'columns'              => array(                       
                        array(
                            'name' => 'id',            
                            'value'=>'$data->id',
                            'footer'=> "Tổng:"
                        ),  
                        array(
                            'header' => 'Đối tác',            
                            'value'=>'$data->rechargeType->rechargeMethodProvider->topupProvider->name',   
                            'htmlOptions'=>array('align'=>'center')
                        ),  
                        array(
                            'header' => 'Kênh nạp',            
                            'value'=>'$data->rechargeType->rechargeMethodProvider->rechargeMethod->name',
                            'htmlOptions'=>array('align'=>'center')
                        ),
                        array(
                            'header' => 'Nhà mạng',            
                            'value'=>'$data->rechargeType->telco->name',
                            'htmlOptions'=>array('align'=>'center')
                        ),  
                        array(
                            'header' => 'Tài khoản',            
                            'value'=>'$data->user->username',                        
                        ),
                        array(
                            'header' => 'Số tiền khách hàng',            
                            'value'=>'$data->value',  
                            //'footer'=>$model->getTotalValue($data->getData()),                
                        ),  
                    
                        array(
                            'header' => 'Thực nhận về hệ thống',            
                            'value'=>function($data,$row){                                    
                                $calculatorRealRecharge = $row->dataProvider->model->total_card;   
                                $provider = $row->dataProvider->model->provider;
                                if($data->rechargeType->rechargeMethodProvider->rechargeMethod->code_identifier == "sms"){
                                    if(empty($provider)){
                                        if($data->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'vmg')
                                            return $data->rechargeType->real_value * $calculatorRealRecharge['optConfigSmsVmg']; 
                                        if($data->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'fibo')
                                            return $data->rechargeType->real_value * $calculatorRealRecharge['optConfigSmsFibo']; 
                                    }else{
                                        if($provider == 'vmg' || $provider == 'fibo'){
                                            return $data->rechargeType->real_value * $calculatorRealRecharge['optConfigSms']; 
                                        }
                                    }
                                }else{                                    
                                    if(empty($provider)){
                                        if($data->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'nganluong')
                                            return $data->rechargeType->real_value * ((100 - $calculatorRealRecharge['optConfigNL'][$data->rechargeType->telco->code_identifier]) / 100);                                 
                                        if($data->rechargeType->rechargeMethodProvider->topupProvider->code_identifier == 'vmg')
                                            return $data->rechargeType->real_value * ((100 - $calculatorRealRecharge['optConfigVmg'][$data->rechargeType->telco->code_identifier]) / 100);                                 
                                    }else{
                                        if($provider == 'vmg' || $provider == 'nganluong'){
                                            return $data->rechargeType->real_value * ((100 - $calculatorRealRecharge['optConfig'][$data->rechargeType->telco->code_identifier]) / 100);                                 
                                        }
                                    }
                                }
                            },  
                            //'footer'=>$calculatorRealRecharge['totalReal'],                
                        ),
                        array(
                            'header' => 'Quy đổi thành tiền ảo',            
                            'value'=>'$data->gold'
                        ),
                        array(
                            'header' => 'Thời gian',            
                            'value'=>'$data->create_time'
                        ),

                    ), // an array of your CGridColumns
            ));            
            Yii::app()->end();              
        //}
    }
    
    public function actionDownloadFeedback(){
//        $fileModel = File::model()->findByPk(1710);
//        $fileModel->getFile('image');
        $user = User::model()->findByPk(74);
        echo "<img src='".$user->getAvatar()."' height='205px' width='205px' alt='avatar'/>";
    }
    
    public function actionGetCache(){
        $memcache = Common::initMemcache();
        $mem = $memcache->get('key_memcache_httpd');
        //sort($mem);
        echo json_encode($mem);
    }
    
    public function actionGetContentCache(){
        $memcache = Common::initMemcache();
        $key = Yii::app()->request->getParam('mem_key');
        if($memcache->get($key))
            print_r($memcache->get($key));        
            
    }
    
    public function actionDeleteCache(){
        $memcache = Common::initMemcache();
        $return = array();
        $keys = Yii::app()->request->getParam('deleteList');
        if(!empty($keys) && is_array($keys)){
            $listMem = $memcache->get('key_memcache_httpd');
            foreach($keys as $key){                
                $memcache->delete($listMem[$key]);                    
                unset($listMem[$key]);                
            }
            $memcache->set('key_memcache_httpd',$listMem);
            $return['code'] = 0;
            $return['message'] = 'Thanh cong';
        }else{
            $return['code'] = 1;
            $return['message'] = 'Khong ton tai cache';
        }
        echo json_encode($return);
    }
    
    public function actionChangeServerMode(){
        $mode = Yii::app()->request->getParam('mode');
        $electroServer = new ElectroServerBridge();                
        $data = array();           
        $data['mode'] = $mode;                                                      
        $url = $electroServer -> buildURLForAction('change_server_mode', $data);
        $result = $electroServer->send($url);        
    }
    
    public function actionChangePassword(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $user_id = Yii::app()->request->getParam('user_id');
        $password = Yii::app()->request->getParam('password');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user = User::model()->findByPk($user_id);
            if(isset($user)){                  
                $like = '{"user_id":"'.$user_id.'","username":"'.$user->username.'"';
                $criteria = new CDbCriteria;
                $criteria->addCondition('type_id = 3 AND status = 0');
                $criteria->addSearchCondition('content', $like);
                $adminLog = AdminActionLog::model()->find($criteria);
                if(isset($adminLog)){
                    $return['code'] = 3;
                    $return['message'] = 'Bạn cần revert lại password trước khi muốn thay đổi tiếp';
                    echo json_encode($return);die;
                }
                
                $userAdmin = UserAccessToken::model()->find('access_token=:access_token',array(':access_token'=>$token))->user;
                $adminActionType = AdminActionType::model()->find("code_identifier='change_password'");
                if(isset($adminActionType)){
                    $tmp = array();                    
                    $tmp['user_id'] = $user->id;
                    $tmp['username'] = $user->username;
                    $tmp['user_password'] = $user->password;

                    $adminActionLog = new AdminActionLog();
                    $adminActionLog->type_id = $adminActionType->id;
                    $adminActionLog->user_id = $userAdmin->id;
                    $adminActionLog->content = json_encode($tmp);
                    $adminActionLog->status = 0;
                    $adminActionLog->save();
                }
                if($user->changePassword($password)){
                    $electroServer = new ElectroServerBridge(); 
                    $password = $user->createHash($password . $user->salt);
                    $result = $electroServer->sendUserInfo($user->id, 'password', $password);                    
                    $return['code'] = 0;
                    $return['message'] = 'Thanh cong';
                }
            }else{
                $return['code'] = 1;
                $return['message'] = 'Khong ton tai user';
            }
        }
        echo json_encode($return);
    }
    
    public function actionRevertPassword(){
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        $user_id = Yii::app()->request->getParam('user_id');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $user = User::model()->findByPk($user_id);
            if(isset($user)){
                $like = '{"user_id":"'.$user_id.'","username":"'.$user->username.'"';
                $criteria = new CDbCriteria;
                $criteria->addCondition('type_id = 3 AND status = 0');
                $criteria->addSearchCondition('content', $like);
                $adminLog = AdminActionLog::model()->find($criteria);                
                if($adminLog){
                    $content = json_decode($adminLog->content);
                    if($content){
                         $password_old = $content->user_password; 
                         $user->password = $password_old;
                         if($user->save()){
                             $adminLog->status = 1;
                             $adminLog->save();
                             
                             $electroServer = new ElectroServerBridge();                            
                             $result = $electroServer->sendUserInfo($user->id, 'password', $password_old);  
                             $return['code'] = 0;
                             $return['message'] = 'Thanh cong';
                         }
                    }      
                }else{
                    $return['code'] = 2;
                    $return['message'] = 'Password chua thay doi';
                }
            }else{
                $return['code'] = 1;
                $return['message'] = 'Khong ton tai user';
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetTopupProvider(){
        $return = array();        
        $model = TopupProvider::model()->findAll();
        if (!isset($model)) {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại kết quả";
        } else {
            $return['code'] = 0;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach ($model as $index => $items) {
                $return['items'][$index]['id'] = $items->id;
                $return['items'][$index]['name'] = $items->name;                
                $return['items'][$index]['description'] = $items->description;                
                $return['items'][$index]['config'] = $items->config;                
                $return['items'][$index]['code_identifier'] = $items->code_identifier;
            }
        }        
        
        echo json_encode($return);
    }
    
    //xu ly gian hang            

    public function actionGetCategoryProduct() {
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $category = CategoryProduct::model()->findAll();
            if (isset($category)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($category as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                    $return['items'][$index]['image'] = $items->image;
                    $return['items'][$index]['description'] = $items->description;
                    $return['items'][$index]['parent_id'] = $items->parent_id;
                    $return['items'][$index]['status'] = $items->status;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại danh mục nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $category = CategoryProduct::model()->findByPk($id);
                if (isset($category)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $category->id;
                    $return['name'] = $category->name;
                    $return['code_identifier'] = $category->code_identifier;
                    $return['description'] = $category->description;
                    $return['parent_id'] = $category->parent_id;
                    $return['status'] = $category->status;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại danh mục";
                }
            }
        }
        //}
        echo json_encode($return);
    }
    
    public function actionCreateCategoryProduct() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {            
            $name = Yii::app()->request->getParam('name');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $description = Yii::app()->request->getParam('description');            
            $parent_id = Yii::app()->request->getParam('parent_id');
            $status = Yii::app()->request->getParam('status');
            if (!isset($code_identifier) || !isset($status)) {
                $return['code'] = 2;
                $return['message'] = "Thiếu Dữ liệu nhập vào";
            } else {
                $model = new CategoryProduct();
                $model->name = $name;
                $model->code_identifier = $code_identifier;
                $model->status = $status;
                if (isset($description))
                    $model->description = $description;
                if (isset($parent_id))
                    $model->parent_id = $parent_id;
                else
                    $model->parent_id = 0;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/products/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }
                if ($model->save()) {
                    $return['id'] = $model->id;
                    $return['code'] = 0;
                    $return['message'] = 'Thành công';
                } else {
                    $return['code'] = 1;
                    $return['message'] = 'Không thành công';
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionUpdateCategoryProduct() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');       
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = CategoryProduct::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');
                if (isset($name))
                    $model->name = $name;
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                $parent_id = Yii::app()->request->getParam('parent_id');
                if (isset($parent_id))
                    $model->parent_id = $parent_id;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/post/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }    
                $status = Yii::app()->request->getParam('status');
                if (isset($status))
                    $model->status = $status;
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetProduct() {
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $product = Product::model()->findAll();
            if (isset($product)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach ($product as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['code_identifier'] = $items->code_identifier;
                    $return['items'][$index]['name'] = $items->name;
                    $return['items'][$index]['image'] = $items->image;
                    $return['items'][$index]['text_image'] = $items->image;
                    $return['items'][$index]['description'] = $items->description;                                        
                    $return['items'][$index]['status'] = $items->status;                    
                    $return['items'][$index]['gold'] = $items->gold;
                    $return['items'][$index]['chip'] = $items->chip;
                    $return['items'][$index]['chip_receiver'] = $items->chip_receiver;
                    $return['items'][$index]['qty'] = $items->quatity;
                    $return['items'][$index]['category_id'] = $items->category_product_id;
                    $return['items'][$index]['create_time'] = $items->create_time;
                    $return['items'][$index]['order'] = $items->order;
                }
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại product nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $product = Product::model()->findByPk($id);
                if (isset($product)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $product->id;
                    $return['name'] = $product->name;
                    $return['description'] = $product->description;
                    $return['image'] = $product->image;
                    $return['text_image'] = $product->image;
                    $return['gold'] = $product->gold;
                    $return['chip'] = $product->chip;
                    $return['chip_receiver'] = $product->chip_receiver;
                    $return['qty'] = $product->quatity;
                    $return['status'] = $product->status;
                    $return['create_time'] = $product->create_time;     
                    $return['category_id'] = $product->category_product_id;
                    $return['order'] = $product->order;
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại product nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }
    
    public function actionCreateProduct() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $name = Yii::app()->request->getParam('name');
            $code_identifier = Yii::app()->request->getParam('code_identifier');
            $category_id = Yii::app()->request->getParam('category_id');            
            $description = Yii::app()->request->getParam('description'); 
            $gold = Yii::app()->request->getParam('gold');
            $chip = Yii::app()->request->getParam('chip');
            $chip_receiver = Yii::app()->request->getParam('chip_receiver');
            $qty = Yii::app()->request->getParam('qty');
            $status = Yii::app()->request->getParam('status');     
            $order = Yii::app()->request->getParam('order');
            if (!isset($name) || !isset($code_identifier)) {
                $return['code'] = 2;
                $return['message'] = "Dữ liệu nhập vào không đúng";
            } else {
                $category = CategoryProduct::model()->findByPk($category_id);
                if (!isset($category)) {
                    $return['code'] = 3;
                    $return['message'] = "Danh mục không tồn tại";
                } else {
                    $model = new Product();
                    $model->code_identifier = $code_identifier;
                    $model->name = $name;
                    $model->description = $description;
                    if (!empty($_FILES['image'])) {
                        $filename = $_FILES['image']['name'];
                        $pos = strrpos($filename, '.');
                        $name = substr($filename, 0, $pos);
                        $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                        $name = Myvietstring::removeInvalidCharacter($name);
                        $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                        $ext = pathinfo($filename, PATHINFO_EXTENSION);
                        // $real_name = $name . '.' . $ext;
                        $name.= '-' . time();
                        $name.= '.' . $ext;
                        $fullpath = 'files/products/' . $name;
                        move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                        $model->image = $fullpath;
                    } else {
                        $return['code'] = 5;
                        $return['message'] = "Khong ton tai file";
                        echo json_encode($return);
                        die;
                    }                                                     
                    $model->status = $status;
                    $model->category_product_id = $category_id;                    
                    $model->gold = $gold;
                    $model->chip = $chip;
                    $model->chip_receiver = $chip_receiver;
                    $model->quatity = $qty;
                    $model->order = $order;
                    if ($model->save()) {
                        $return['id'] = $model->id;
                        $return['code'] = 0;
                        $return['message'] = 'Thành công';                       
                    } else {
                        $return['code'] = 1;
                        $return['message'] = 'Không thành công';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    public function actionUpdateProduct() {
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = Product::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {
                $name = Yii::app()->request->getParam('name');                
                if (isset($name))
                    $model->name = $name;
                if (!empty($_FILES['image'])) {
                    $filename = $_FILES['image']['name'];
                    $pos = strrpos($filename, '.');
                    $name = substr($filename, 0, $pos);
                    $name = strtolower(str_replace(' ', '-', Myvietstring::removeVietChars($name)));
                    $name = Myvietstring::removeInvalidCharacter($name);
                    $name = strlen(strlen($name) > 10) ? substr($name, 0, 10) : $name;
                    $ext = pathinfo($filename, PATHINFO_EXTENSION);
                    // $real_name = $name . '.' . $ext;
                    $name.= '-' . time();
                    $name.= '.' . $ext;
                    $fullpath = 'files/products/' . $name;
                    move_uploaded_file($_FILES['image']['tmp_name'], $fullpath);
                    $model->image = $fullpath;
                }
                
                $description = Yii::app()->request->getParam('description');
                if (isset($description))
                    $model->description = $description;
                
                $gold = Yii::app()->request->getParam('gold');
                if (isset($gold))
                    $model->gold = $gold;
                
                $chip = Yii::app()->request->getParam('chip');
                if (isset($chip)) {
                    $model->chip = $chip;
                }   
                
                $chip_receiver = Yii::app()->request->getParam('chip_receiver');
                if (isset($chip_receiver)) {
                    $model->chip_receiver = $chip_receiver;
                } 
                
                $qty = Yii::app()->request->getParam('qty');
                if (isset($qty)) {
                    $model->quatity = $qty;
                } 
                
                $status = Yii::app()->request->getParam('status');
                if (isset($status))
                    $model->status = $status;
                
                $order = Yii::app()->request->getParam('order');
                if(isset($order))
                    $model->order = $order;
                
                $category_id = Yii::app()->request->getParam('category_id');
                if (isset($category_id))
                    $model->category_product_id = $category_id;
                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";                    
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);
    } 
    
    public function actionGetOrder(){
        $id = Yii::app()->request->getParam('id');
        if (!isset($id)) {
            $limit = Yii::app()->request->getParam('limit');
            $page = Yii::app()->request->getParam('page');
            $product_name = Yii::app()->request->getParam('product_name');
            $username = Yii::app()->request->getParam('username');                        
            $startTime = Yii::app()->request->getParam('startTime');
            $endTime = Yii::app()->request->getParam('endTime'); 
            $order_status = Yii::app()->request->getParam('order_status');
            if(!isset($endTime) && !isset($startTime)){
                $endTime = date('Y-m-d H:i:s');
                $startTime = date("Y-m-d H:i:s", strtotime(time()- 7*24*3600));               
            }elseif(!isset($endTime) && isset($startTime)){
                $endTime = date('Y-m-d H:i:s');
                $startTime = date("Y-m-d H:i:s", strtotime($startTime));
                if($startTime > $endTime){
                    $return['code'] = -2;
                    $return['message'] = "Ngày bắt đầu lớn hơn ngày hiện tại";
                    $return['items'] = array();                                              
                    $return['items']['startTime'] = $startTime;
                    $return['items']['endTime'] = $endTime;
                    echo json_encode($return);die;
                }
            }elseif(isset($endTime) && !isset($startTime)){
                $endTime = date('Y-m-d H:i:s', strtotime($endTime));                         
                $startTime = date("Y-m-d H:i:s", (strtotime($endTime) - 7* 24 * 3600));                                
            }else{
                $endTime = date('Y-m-d H:i:s', strtotime($endTime));                
                $startTime = date("Y-m-d", strtotime($startTime));  
            }
            
            if(!isset($page))
                $page = 0;
            if(!isset($limit))
                $limit = 30;
            $criteria = new CDbCriteria();
            $start_id = $page * $limit;
            $criteria->alias = 'order';
            $criteria->join = 'INNER JOIN product ON order.product_id = product.id';
            $criteria->join .= ' INNER JOIN user ON order.user_id = user.id';                        
            if($product_name)
                $criteria->addSearchCondition('product.name', $product_name);
            if($username)
                $criteria->addSearchCondition('user.username', $username);
            
            if (isset($startTime)) {                     
                $criteria->addCondition("order.create_time >= '" . $startTime . "'");                
            }
            if (isset($endTime)) {                                             
                $criteria->addCondition("order.create_time <='" . $endTime . "'");                
            }    
            
            if(isset($order_status))
                $criteria->addCondition ('order.order_status ='. $order_status);
            
            $count = Order::model()->count($criteria);
            $criteria->offset = $start_id;
            $criteria->limit = $limit;
            $criteria->order = "order.create_time DESC";    
            $order = Order::model()->findAll($criteria);
            if (isset($order)) {
                $return['code'] = 0;
                $return['message'] = "Thành công";                
                if (($count) % $limit > 0) {
                    $return['totalPage'] = ($count - ($count % $limit)) / $limit;
                } else
                    $return['totalPage'] = $count / $limit - 1;
                $return['items'] = array();
                foreach ($order as $index => $items) {
                    $return['items'][$index]['id'] = $items->id;
                    $return['items'][$index]['order_code'] = $items->order_code;
                    $return['items'][$index]['username'] = $items->user->username;
                    $return['items'][$index]['product_image'] = $items->product->image;
                    $return['items'][$index]['product_name'] = $items->product->name;                                        
                    $return['items'][$index]['status'] = $items->status;                    
                    $return['items'][$index]['gold'] = $items->gold;
                    $return['items'][$index]['chip'] = $items->chip;
                    $return['items'][$index]['qty'] = $items->quatity;
                    $return['items'][$index]['comment'] = $items->comment;
                    $return['items'][$index]['phone'] = $items->phone;
                    $return['items'][$index]['email'] = $items->email;
                    $return['items'][$index]['address'] = $items->address;
                    $return['items'][$index]['order_status'] = $items->orderStatus->id;
                    $return['items'][$index]['order_status_name'] = $items->orderStatus->name;
                    $return['items'][$index]['create_time'] = $items->create_time;
                }
                
                function cmp($a, $b)
                {
                    return strcmp($a["order_status"], $b["order_status"]);
                }               
                
                usort($return['items'],"cmp");
            } else {
                $return['code'] = 1;
                $return['message'] = "Không tồn tại order nào";
            }
        } else {
            if (!is_numeric($id)) {
                $return['code'] = 2;
                $return['message'] = "Id nhập vào không phải là số";
            } else {
                $order = Order::model()->findByPk($id);
                if (isset($order)) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";
                    $return['id'] = $order->id;
                    $return['username'] = $order->user->username;
                    $return['product_name'] = $order->product->name;
                    $return['product_image'] = $order->product->image;
                    $return['qty'] = $order->quatity;
                    $return['gold'] = $order->gold;
                    $return['chip'] = $order->chip;    
                    $return['phone'] = $order->phone;
                    $return['email'] = $order->email;
                    $return['address'] = $order->address;
                    $return['comment'] = $order->comment;
                    $return['status'] = $order->status;
                    $return['order_status'] = $order->orderStatus->id;
                    $return['order_status_name'] = $order->orderStatus->name;
                    $return['create_time'] = $order->create_time;                    
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Không tồn tại order nào";
                }
            }
        }
        //}
        echo json_encode($return);
    }   
    
    public function actionUpdateOrder(){        
        $return = array();
        $token = Yii::app()->request->getParam('accessToken');        
        if (!isset($token) || !UserAccessToken::model()->checkAccessToken($token)) {
            $return['code'] = -1;
            $return['message'] = "Bạn không có quyền truy cập";
        } else {
            $id = Yii::app()->request->getParam("id");
            $model = Order::model()->findByPk($id);
            if (!isset($id) || !is_numeric($id) || !isset($model)) {
                $return['code'] = 2;
                $return['message'] = 'Dữ liệu nhập vào không đủ';
            } else {                
                $qty = Yii::app()->request->getParam('qty');
                if (isset($qty)) {
                    $model->quatity = $qty;
                } 
                
                $phone = Yii::app()->request->getParam('phone');
                if (isset($phone)) {
                    $model->phone = $phone;
                } 
                
                $email = Yii::app()->request->getParam('email');
                if (isset($email)) {
                    $model->email = $email;
                } 
                
                $address = Yii::app()->request->getParam('address');
                if (isset($address)) {
                    $model->address = $address;
                } 
                
                $status = Yii::app()->request->getParam('status');
                if (isset($status))
                    $model->status = $status;
                
                $order_status = Yii::app()->request->getParam('order_status');
                if (isset($order_status))
                    $model->order_status = $order_status;
                
                if ($model->save()) {
                    $return['code'] = 0;
                    $return['message'] = "Thành công";                    
                } else {
                    Common::dump($model->getErrors());die;
                    $return['code'] = 1;
                    $return['message'] = "Lưu dữ liệu thất bại";
                }
            }
        }
        echo json_encode($return);    
    }
    
    public function actionGetOrderStatus(){       
        $return = array();
        $orderStatus = OrderStatus::model()->findAll();
        if (isset($orderStatus)) {
            $return['message'] = "Thành công";
            $return['code'] = 0;
            $return['items'] = array();
            foreach ($orderStatus as $index => $items) {
                $return['items'][$index]['id'] = $items->id;                
                $return['items'][$index]['name'] = $items->name;                
                $return['items'][$index]['description'] = $items->description;                                                        
            }
        } else {
            $return['code'] = 1;
            $return['message'] = "Không tồn tại trạng thái đơn hàng nào";
        }
        echo json_encode($return);
    }
    
    public function actionRemoveTransaction(){
        $startTime = '2014-06-01 00:00:00';
        $endTime = '2014-08-30 00:00:00';
        $startTime = new MongoDate(strtotime($startTime));
        $endTime = new MongoDate(strtotime($endTime));
        $criteria = new EMongoCriteria();    
                
        $criteria->timestamp(">=",$startTime);
        $criteria->timestamp("<=",$endTime);        
        $transaction = MGameLog::model()->deleteAll($criteria);  
        Common::dump($transaction);
    }
        
}
