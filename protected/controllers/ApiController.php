<?php
require_once "curlhelper.php";

class ApiController extends Controller {

    protected  $app_id = '553026998169096';
    protected  $secret_key = 'b43e4d98cbc2633a2ce0acd13d71fdcf';
    public $layout='blank';    
    // Uncomment the following methods and override them if needed
    /*
      public function filters()
      {
      // return the filter configuration for this controller, e.g.:
      return array(
      'inlineFilterName',
      array(
      'class'=>'path.to.FilterClass',
      'propertyName'=>'propertyValue',
      ),
      );
      }
     * */
    public function actions() {
        // return external action classes, e.g.: 
        return array(
            'rechargesmsvmg' => array(
                'class' => 'CWebServiceAction',
            ),
            'rechargesmsnganluong' => array(
                'class' => 'CWebServiceAction',
            ),
            'rechargesmsonepay' => array(
                'class' => 'CWebServiceAction',
            ),    
            'rechargesmsappota' => array(
                'class' => 'CWebServiceAction',
            ),
        );
    }
    
    public function actionIndex() {                
        $criteria = new CDbCriteria;
        $criteria->addCondition('category_id = 9 AND status = 1');  
        $criteria->limit = 5;
        $criteria->order = "create_time DESC";
        $model = Post::model()->findAll($criteria);
        if(isset($model)){
            $this->render('index', array('model'=>$model));                
        }
    }
    
    public function actionLikeFacebook(){                         
        if($_POST){            
            $facebook_id = Yii::app()->request->getParam('facebook_id');
            $username = Yii::app()->request->getParam('username');
            if ($facebook_id != NULL) {
                $model = User::model()->find('username =:username',array(':username'=>$username));                    
                if ($model) {                         
                     $model->facebook_id = $facebook_id;                     
                     if($model->save()){
                         if(!$model->checkUserGameAction('like_facebook')){
                            $userGameActionId = $model->userGameAction('like_facebook');                    
                            if($userGameActionId){
                                $bonus = $model->bonusChipByLikeFacebook($userGameActionId);
                                $electroServer = new ElectroServerBridge();
                                $result = $electroServer->sendUserInfo($model->id, 'chip', $bonus);
                                UserMessage::model()->sendUserMessage($model->id,"Bạn vừa nhận được ".number_format($bonus, 0, ',', '.')." chip từ hệ thống Esimo !",1);
                                $this->redirect(Yii::app()->createUrl('api/getPost'));
                            }
                        }else{
                            MUserMessage::model()->sendUserMessage($model->id,"Bạn tài khoản của bạn đã được cộng tiền khi like Fanpage Esimo trên facebook !",1);
                        }
                     }                                          
                     $this->redirect(Yii::app()->createAbsoluteUrl('api/getPost'));
                } 
            } else {
                $this->redirect(Yii::app()->createUrl('api/getPost'));
            }
        }
        
        $fbAppArray = array(
            'appId'  => $this->app_id,
            'secret' => $this->secret_key,
            'cookie' => true
        );               
        $fbAppObj = new Facebook( $fbAppArray );              
        $signedRequest = $fbAppObj->getSignedRequest();
                
        if($signedRequest == NULL)
            $signedRequest['user_id'] = $fbAppObj->getUser();                   
        if($signedRequest && $signedRequest['user_id'] != 0){             
            $user = User::model()->find('facebook_id=:facebook_id',array(':facebook_id'=>$signedRequest['user_id']));             
            if(isset($user)){ 
                if(!$user->checkUserGameAction('like_facebook')){
                    $userGameActionId = $user->userGameAction('like_facebook');                    
                    if($userGameActionId){
                        $bonus = $user->bonusChipByLikeFacebook($userGameActionId);
                        $electroServer = new ElectroServerBridge();
                        $result = $electroServer->sendUserInfo($user->id, 'chip', $bonus);
                        MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($bonus, 0, ',', '.')." chip từ hệ thống Esimo !",1);
                        $this->redirect(Yii::app()->createUrl('api/getPost'));                        
                    }
                }else{
                    MUserMessage::model()->sendUserMessage($user->id,"Bạn tài khoản của bạn đã được cộng tiền khi like Fanpage Esimo trên facebook !",1);
                    //$this->redirect(Yii::app()->createUrl('api/getPost'));    
                }
            }else{
                $session = new CHttpSession();
                $session->open();
                if($session['user']){
                    $user = $session['user'];
                    $this->render('like_facebook', array('facebook_id'=>$signedRequest['user_id'], 'username'=>$user->username));
                }else{
                    $this->redirect(Yii::app()->createUrl('api/getPost'));
                }
            }
        }        
    
    }
    
    public function actionInviteFacebook(){      
        if($_POST){
            $username = Yii::app()->request->getParam('username');            
            $facebook_id = Yii::app()->request->getParam('facebook_id');
            if ($username != NULL && $facebook_id != NULL) {
                $model = User::model()->find('username =:username',array(':username'=>$username));                
                if ($model) {                         
                     $model->facebook_id = $facebook_id;                     
                     if($model->save()){
                         $userGameActionId = $model->userGameAction('invite_facebook');                    
                         if($userGameActionId){
                            $bonus = $model->bonusChipByInviteFacebook($userGameActionId);
                            $electroServer = new ElectroServerBridge();
                            $result = $electroServer->sendUserInfo($model->id, 'chip', $bonus);
                            MUserMessage::model()->sendUserMessage($model->id,"Bạn vừa nhận được ".number_format($bonus, 0, ',', '.')." chip từ hệ thống Esimo !",1);
                            $this->redirect(Yii::app()->createUrl('api/getPost'));
                         }
                     }
                                          
                     $this->redirect(Yii::app()->createAbsoluteUrl('api/getPost'));
                } 
            } else {
                $this->redirect(Yii::app()->createUrl('api/getPost'));
            }
        }
        $friends = Yii::app()->request->getParam('to');         
        if($friends == 'undefined')
            $this->redirect(Yii::app()->createUrl('api/getPost'));
        
        $fbAppArray = array(
            'appId'  => $this->app_id,
            'secret' => $this->secret_key,
            'cookie' => true
        );       
        
        $fbAppObj = new Facebook( $fbAppArray );                    
        $signedRequest = $fbAppObj->getSignedRequest();        
        if($signedRequest == NULL){           
            $signedRequest['user_id'] = $fbAppObj->getUser(); 
            if($signedRequest['user_id'] == 0){
                $loginUrl   = $fbAppObj->getLoginUrl(
                                    array(
                                        'scope'         => 'publish_stream'
                                    )
                                );
                header('location: '.$loginUrl);
            }
        }            
                     
        if(isset($friends)){
            $friends = explode(',', $friends);
            $criteria = new CDbCriteria;
            $criteria->alias = "promotion_rule";        
            $criteria->join = ' INNER JOIN promotion ON promotion_rule.promotion_id = promotion.id';
            $criteria->join .= ' INNER JOIN game_action ON promotion_rule.game_action_id = game_action.id';
            $criteria->addCondition('promotion.id = 4 AND game_action.code_identifier = "invite_facebook"');
            $promotionRule = PromotionRule::model()->find($criteria);
            if(isset($promotionRule)){
                $tmp = json_decode($promotionRule->value);
                if($tmp){
                    $value = $tmp->number;                    
                }else{
                    $value = 0;
                }
            }else{
                $value = 0;
            }                
            if($value > 0 && count($friends) >= $value){                
                $user = User::model()->find('facebook_id=:facebook_id',array(':facebook_id'=>$signedRequest['user_id']));                     
                if(isset($user)){                    
                    $userGameActionId = $user->userGameAction('invite_facebook');                    
                    if($userGameActionId){
                        $bonus = $user->bonusChipByInviteFacebook($userGameActionId);
                        $electroServer = new ElectroServerBridge();
                        $result = $electroServer->sendUserInfo($user->id, 'chip', $bonus);
                        UserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($bonus, 0, ',', '.')." chip từ hệ thống Esimo !",1);
                        $this->redirect(Yii::app()->createUrl('api/getPost'));
                    }                    
                }else{                       
                    $session = new CHttpSession();
                    $session->open();
                    if($session['user']){
                        $user = $session['user'];
                        $this->render('like_facebook', array('facebook_id'=>$signedRequest['user_id'], 'username'=>$user->username));
                    }else{
                        $this->redirect(Yii::app()->createUrl('api/getPost'));
                    }                        
                }
            }else{
                UserMessage::model()->sendUserMessage($user->id,"Bạn phải mời được nhiều hơn ".$value." người",1);
            }
        }
        
    }           

    public function actionGetAccessToken() {
        $type = Yii::app()->request->getParam('type', NULL);
        $type = str_replace("'", "", $type);
        $ip = $_SERVER['REMOTE_ADDR'];
        $userId = null;
        $return = array();
        $electroServer = new ElectroServerBridge();    
        if ($type == 0) {
            $appId = Yii::app()->request->getParam('appId');
            $username = Yii::app()->request->getParam('username', NULL);
            $password = Yii::app()->request->getParam('password', NULL);
            $user = User::model()->getUser($username, $password);
            if ($user !== NULL) {
                $userId = $user->id;
                $accessToken = UserAccessToken::model()->createAccessToken($userId, $appId, $ip);
                Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);
                $return['code'] = 1;
                $return['message'] = 'Đăng nhập thành công';
                $return['accessToken'] = $accessToken;
                $return['username'] = $username;
                $return['userid'] = $user->id;
                $electroServer->sendAccessToken($user->id, $username, $accessToken); 
            } else {
                $return['code'] = -1;
                $return['message'] = 'Tên đăng nhập và mật khẩu không hợp lệ';
            }
        }
        // Get user profile
        if ($type == 'facebook') {
            $fbAccessToken = Yii::app()->request->getParam('accessToken');
            $fbAccessToken = str_replace("'", "", $fbAccessToken);
            if ($fbAccessToken != NULL) {
                $facebook = new Facebook(array(
                    'appId' => $this->app_id,
                    'secret' => $this->secret_key,
                ));
                $facebook->setAccessToken($fbAccessToken);
//                $userProfile = $facebook->api('/me?field=email,first_name,last_name,name,username,id');
                $userProfile = $facebook->api('/me');
            }
        }
        if ($type == 'google') {
            $accessToken = Yii::app()->request->getParam('accessToken');
            $accessToken = str_replace("'", "", $accessToken);
            $apiUrl = 'https://www.googleapis.com/oauth2/v1/userinfo?access_token=' . $accessToken;
            $apiCurl = curl_init($apiUrl);
            curl_setopt($apiCurl, CURLOPT_RETURNTRANSFER, TRUE);
            $jsonUserProfile = curl_exec($apiCurl);
            $userProfile = json_decode($jsonUserProfile, JSON_FORCE_OBJECT);
        }
        // End get user profile
        if ($type == 'google' || $type == 'facebook') {
            if ($userProfile['id'] != NULL) { //Nếu đã lấy được thông tin tài khoản
                $user = User::model()->getUserWithOpenId($type, $userProfile['id']);
                if ($user != NULL) { // Nếu đã tạo tài khoản
//                    echo 'da co tk';
                    $accessToken = UserAccessToken::model()->createAccessToken($user->id, $appId, $ip);
                    Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);
                    $return['code'] = 1;
                    $return['message'] = 'Đăng nhập thành công';
                    $return['accessToken'] = $accessToken;
                    $return['username'] = $user->username;
                    $electroServer->sendAccessToken($user->id, $user->username, $accessToken); 
                } else {    // Nếu chưa đăng ký tài khoản
                    $session = new CHttpSession();
                    $session->open();
                    $session['openIdType'] = $type;
                    $session['profile'] = $userProfile;
                    $return['code'] = -2;
                    $return['message'] = 'Tài khoản chưa đăng ký';
                    if (isset($userProfile['username']))
                        $suggestUser = $userProfile['username'];
                    else {
                        $parts = explode("@", $userProfile['email']);
                        $suggestUser = $parts[0];
                    }
                    $suggestUser = str_replace(".", '', $suggestUser);
                    $suggestUser = strtolower($suggestUser);
                    $return['suggestUser'] = User::model()->suggestUser($suggestUser);
                }
            } else {  //Nếu chưa lấy đc thông tin tk (Accesstoken gửi lên ko đúng):
                $return['code'] = -1;
                $return['message'] = 'Accesstoken không đúng';
            }
        }
        if ($type == 'register') {  // Đăng ký lần với openid
            $session = new CHttpSession();
            $session->open();
            $provider = $session['openIdType'];
            $userProfile = $session['profile'];
            $username = Yii::app()->request->getParam('username');
            $password = Yii::app()->request->getParam('password');
            $appId = Yii::app()->request->getParam('appId');
            if (!$provider && !$userProfile && !$username && !$password) {
                $return['code'] = -3;
                $return['message'] = 'Thiếu biến';
            } else {
                if (preg_match('/[^a-zA-Z0-9_-]/', $username))
                {
                    $return['code'] = -6;
                    $return['message'] = 'Tên tài khoản không được phép chứa các kí tự đặc biệt';
                    echo json_encode($return);die;
                } 
                $check = User::model()->checkUsernameExist($username);
                if($check){
                    $return['code'] = -5;
                    $return['message'] = 'Tài khoản đã tồn tại trong hệ thống';    
                    echo json_encode($return);die;
                }                                
                $user = User::model()->registerWithOpenId($provider, $userProfile, $username, $password);
                if ($user) {
                    $accessToken = UserAccessToken::model()->createAccessToken($user->id, $appId, $ip);
                    //add user partner
                    $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');
                    $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
                    if(!isset($partnerModel)){
                        $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
                    }
                    $partner_id = $partnerModel->id;                    
                    $userPartnerModel = new UserPartner();
                    $userPartnerModel->user_id = $user->id;
                    $userPartnerModel->partner = $partner_id;
                    $userPartnerModel->save();
                    
                    Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);
                    $return['code'] = 1;
                    $return['message'] = 'Đăng ký thành công';
                    $return['accessToken'] = $accessToken;
                    $return['username'] = $username;
                    $return['userid'] = $user->id;
                    $electroServer->sendAccessToken($user->id, $username, $accessToken); 
                } else {
                    $return['code'] = -4;
                    $return['message'] = 'Đăng ký thất bại';
                }
            }
        }
        echo json_encode($return);
        exit();
        Yii::app()->end();
    }

//    public function actionGetUserInfomation (){
//        $type = Yii::app()->request->getParam('accessToken', NULL);
//        
//    }

    public function actionChangeUserInformation() {
        $type = Yii::app()->request->getParam('type');
        $data = Yii::app()->request->getParam('data');
        $return = array();
        if ($type == NULL || $data == NULL) {
            $return['code'] = -1;
            $return['message'] = 'Biến nhập vào không đầy đủ';
        } else {
            $userInfo = json_decode($data, JSON_FORCE_OBJECT);            
            $user = User::model()->find('username=:username', array(':username' => $userInfo['username']));
            if ($user == NULL) {
                $return['code'] = -2;
                $return['message'] = 'Username và password không đúng';
            } else {
                $electroServer = new ElectroServerBridge();
                //$electroServer2 = new ElectroServerBridge('210.211.102.121');
                if ($type == 'changeAvatar') { // Change Avatar
                    $image = $userInfo['avatar'];
                    $file = File::model()->saveBinaryFile($image, 'user', TRUE);
                    if ($file) {
                        $file->resizeImage(NULL, NULL, 205, 205);
                        $user->avatar = $file->id;
                        if ($user->save()) {
                            $return['code'] = 1;
                            $return['message'] = 'Thay đổi avatar thành công';
                            $return['avatarLink'] = $user->getAvatar();
                            $electroServer->sendUserInfo($user->id, 'avatar', $user->getAvatar());                                                         
                        }
                    }
                }
                if ($type == 'changeInfomation') {// Change Avatar
                    $user->first_name = $userInfo['first_name'];
                    $user->last_name = $userInfo['last_name'];
                    $user->middle_name = $userInfo['middle_name'];
                    $user->gender = $userInfo['gender'];
                    $user->address = $userInfo['address'];
                    $user->birthday = $userInfo['birthday'];
                    if ($user->save()) {
                        $electroServer->sendUserInfo($user->id, 'firstName', $user->first_name); 
                        $electroServer->sendUserInfo($user->id, 'lastName', $user->last_name);
                        $electroServer->sendUserInfo($user->id, 'middleName', $user->middle_name);
                        $electroServer->sendUserInfo($user->id, 'gender', $user->gender);
                        $electroServer->sendUserInfo($user->id, 'address', $user->address);
                        $electroServer->sendUserInfo($user->id, 'birthday', $user->birthday);                       
                        
                        $return['code'] = 1;
                        $return['message'] = 'Thay đổi thông tin thành công';
                        unset($user->password);
                        unset($user->salt);
                        unset($user->yahoo_id);
                        unset($user->twitter_id);
                        unset($user->google_id);
                        unset($user->facebook_id);
                        unset($user->role);
                        $return['user'] = $user->attributes;
                    } else {
                        $return['code'] = -1;
                        $return['message'] = 'Thay đổi thông tin thất bại';
                    }
                }
                if ($type == 'changePassword') {// Change Avatar
                    if(!$user->checkPassword($userInfo['password'])){
                        $return['code'] = -3;
                        $return['message'] = 'Bạn nhập sai mật khẩu';
                        echo json_encode($return);die;
                    }
                    if($userInfo['newPassword'] != $userInfo['renewPassword']){
                        $return['code'] = -3;
                        $return['message'] = 'Nhập lại mật khẩu không khớp';
                        echo json_encode($return);die;
                    }
                    if($user->changePassword($userInfo['newPassword'])){
                        $password = $user->createHash($userInfo['newPassword'] . $user->salt);
                        $electroServer->sendUserInfo($user->id, 'password', $password);
                        //$electroServer2->sendUserInfo($user->id, 'password', $password);
                        $return['code'] = 1;
                        $return['message'] = 'Đổi mật khẩu thành công!';                        
                    } else {
                        $return['code'] = -3;
                        $return['messsage'] = 'Đổi mật khẩu sai';
                    }
                }
                if ($type == 'changeInformaionSpecial') {
                    if (!isset($user->email)) {
                        $model = User::model()->find('email=:email', array(':email' => $userInfo['email']));
                        if (isset($model)) {
                            $return['code_email'] = -1;
                        } else {
                            $return['code_email'] = 1;
                            $user->email = $userInfo['email'];
                        }
                    } else {
                        $return['code_email'] = 0;
                    }
                    if (!isset($user->identity_card_number)) {
                        $model = User::model()->find('identity_card_number=:identity_cart_number', array(':identity_cart_number' => $userInfo['identity_card_number']));
                        if (isset($model)) {
                            $return['code_cmt'] = -1;
                        } else {
                            $return['code_cmt'] = 1;
                            $user->identity_card_number = $userInfo['identity_card_number'];
                        }
                    } else {
                        $return['code_cmt'] = 0;
                    }
                    if (!isset($user->mobile)) {
                        $model = User::model()->find('mobile=:mobile', array(':mobile' => $userInfo['mobile']));
                        if (isset($model)) {
                            $return['code_mobile'] = -1;
                        } else {
                            $return['code_mobile'] = 1;
                            $user->mobile = $userInfo['mobile'];
                        }
                    } else {
                        $return['code_mobile'] = 0;
                    }
                    if ($user->save()) {
                        $electroServer->sendUserInfo($user->id, 'email', $user->email);
                        $electroServer->sendUserInfo($user->id, 'mobile', $user->mobile);
                        $electroServer->sendUserInfo($user->id, 'identityCardNumber', $user->identity_card_number);                        
                                            
                        $return['code'] = 1;
                        $return['message'] = 'Thay đổi thông tin thành công';
                        unset($user->password);
                        unset($user->salt);
                        $return['user'] = $user->attributes;
                    } else {
                        $return['code'] = -1;
                        $return['message'] = 'Thay đổi thông tin thất bại';
                    }
                }
            }
        }
        echo json_encode($return);
    }

    /**
     * @param string $str
     * @return string the value of $str
     * @soap
     */
    public function getSoap($str) {
        return $str;
    }

    public function actionQuickRegister() {
        $username = Yii::app()->request->getParam('username');
        $password = Yii::app()->request->getParam('password');
        $email = Yii::app()->request->getParam('email');
        $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');
        $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
        if(!isset($partnerModel)){
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
        }
        $partner_id = $partnerModel->id;
        $return = array();        
        if (User::model()->find('username=:username', array(':username' => $username))) {
            $return['code'] = -2;
            $return['message'] = 'User này đã tồn tại';
            echo json_encode($return);
            exit();
        }
        $user = new User();
        $user->username = $username;
        $user->repeat_password = $password;
        $user->init_password = $password;         
        if(isset($email))
            $user->email = $email;
        $error = $user->checkNotIn($username);
        if (preg_match('/[^a-zA-Z0-9_]/', $username) || $error == 1)
        {
            $return['code'] = -2;
            $return['message'] = 'Tên tài khoản không được phép chứa các kí tự đặc biệt';
            echo json_encode($return);die;
        }        
        $user_id = $user->register();        
        $content = "Bạn vừa đăng kí trên Xóm cờ bạc";
        $template = Constants::getTemplate();    
        // save Mongodb
        $template_content = $this->renderPartial('application.modules.admin.views.message.' . $template['register'], array('content' => $content, 'username' => $username), true);
        $sendMessage = MUserMessage::model()->sendUserMessage($user_id, strip_tags(trim(html_entity_decode($template_content, ENT_QUOTES, 'UTF-8'), "\xc2\xa0")),1);        
        //save mysql
        //$message = $this->sendMessage($user_id, $content, $template['register']);
        if ($user_id) {            
            //add user partner      
                                        
            $userPartnerModel = new UserPartner();
            $userPartnerModel->user_id = $user_id;
            $userPartnerModel->partner = $partner_id;
            $userPartnerModel->save();
            
            $return['code'] = 1;
            $return['message'] = 'Đăng ký thành công';
        } else {
            $return['code'] = -1;
            $return['message'] = 'Đăng ký không thành công';
        }
        echo json_encode($return);die;
    }

    public function actionForgotPassword() {
        $return = array();
        $email = Yii::app()->request->getParam('email', NULL);
        if (!$email) {
            $return['code'] = -1;
            $return['message'] = 'Lỗi gửi mail';
            echo json_encode($return);
            exit();
        }
        $user = User::model()->find('email=:email', array(':email' => $email));
        if ($user == NULL) {
            $return['code'] = -2;
            $return['message'] = 'Email này không tồn tại';
            echo json_encode($return);
            exit();
        }
        $userId = $user->id;
        $ip = $_SERVER['REMOTE_ADDR'];        
        $accessToken = UserAccessToken::model()->createAccessToken($userId, '', $ip);
        
        $user->password = MyFunction::rand_string(8);
        $user->access_token = $accessToken;
        $message = new YiiMailMessage;
        //this points to the file test.php inside the view path
        $message->view = "index";
        $params = array('userinfo' => $user);
        $message->subject = 'Lấy lại mật khẩu trên Esimo';
        $message->setBody($params, 'text/html');
        $message->addTo($email);
        //$message->from = 'game@eposi.vn';
        $message->setFrom(array('admin@esimo.vn' => 'Esimo.vn'));
        $result = Yii::app()->mail->send($message);
        if ($result) {
            $return['code'] = 1;
            $return['message'] = 'Gửi email thành công, vui lòng vào email của bạn để lấy lại mật khẩu';
        } else {
            $return['code'] = -1;
            $return['message'] = 'Gửi email không thành công';
        }
        echo json_encode($return);
    }

    public function actionGameError() {
        $return = array();           
        $app_id = Yii::app()->request->getParam('app_id');
        $game_version = Yii::app()->request->getParam('game_version', NULL);
        $user_id = Yii::app()->request->getParam('user_id', NULL);
        $username = Yii::app()->request->getParam('username', NULL);
        $scene = Yii::app()->request->getParam('scene', NULL);
        $detail = Yii::app()->request->getParam('detail', NULL);
        $error = Yii::app()->request->getParam('error', NULL);
        $environment = Yii::app()->request->getParam('environment', NULL);
        $debug = Yii::app()->request->getParam('debug_log', NULL);         

        if ($game_version == NULL || $user_id == NULL || !is_numeric($user_id)) {
            $return['code'] = -1;
            $return['message'] = 'Có lỗi xảy ra';
            echo json_encode($return);
            exit();
        } 
        $game_error = new MGameError();       
        $game_error->app_id = intval($app_id);
        $game_error->users[0] = new MGameErrorUser;
        $game_error->users[0]->user_id = intval($user_id);        
        $game_error->users[0]->username = $username;
        $game_error->game_version = $game_version;
        $game_error->scene = $scene;
        $game_error->detail = $detail;
        $game_error->error = $error;
        $game_error->environment = $environment;
        $game_error->time = new MongoDate();
                //date('Y-m-d H:i:s');      
        if(isset($debug)){        
            $filename = $username.'-'.$app_id.'-'.date('Y-m-d').'-'.date('H:i:s').'.txt';
            $game_error->debug_log = $filename;
            
            $filePath = 'files/gameerror/'.$filename;
            file_put_contents($filePath, base64_decode($debug));
        }

        if ($game_error->save()) {
            $return['code'] = 1;
            $return['message'] = 'Insert thành công';
        } else {
            $return['code'] = -1;
            $return['message'] = 'Insert không thành công';
        }
        echo json_encode($return);
    }

    public function actionFeedback() {
        $return = array();
        $user_id = Yii::app()->request->getParam('user_id', NULL);
        $game_id = Yii::app()->request->getParam('game_id', NULL);
        $content = Yii::app()->request->getParam('content', NULL);
        $title = Yii::app()->request->getParam('title', NULL);
        $file = Yii::app()->request->getParam('file',NULL);
        $model = new Feedback();
        if (!$user_id || !$content || !is_numeric($user_id)) {
            $return['code'] = -1;
            $return['message'] = "Có lỗi xảy ra";
            echo json_encode($return);
            exit();
        }
        if (!$game_id)
            $game_id = 1;
        if(isset($file)){
            //$file = base64_encode($file);
            $fileModel = File::model()->saveBinaryFile($file, 'feedback', TRUE);            
            $model->file = $fileModel->id;
        }
        $userModel = User::model()->findByPk($user_id);
        if(!isset($userModel)){
            $return['code'] = -2;
            $return['message'] = "User không tồn tại";
            echo json_encode($return);die;
        }
        $model->user_id = $user_id;
        $model->content = $content;
        $model->game_id = $game_id;
        if(!isset($title))
            $title = "Câu hỏi của ". $userModel->username;
        $model->title = $title;
        if ($model->save()) {
            $return['code'] = 1;
            $return['message'] = "Lưu thành công";
        } else {
            $return['code'] = -1;
            $return['message'] = "Lưu không thành công";
        }
        echo json_encode($return);
    }

    public function actionGameRule() {
        $return = array();
        $config = Configuration::model()->find('name=:name', array(':name' => 'on_chip'));
        if ($config) {
            $return['code'] = 1;
            $return['on_chip'] = $config->value;
            $return['message'] = "Thành công";
        } else {
            $return['code'] = -1;
            $return['message'] = "Có lỗi";
        }
        echo json_encode($return);
    }

    public function actionHelp() {
        $memcache = Common::initMemcache();
        $return = array();        
        $game_id = Yii::app()->request->getParam('game_id');        
        $gameModel =  Game::model()->findByPk($game_id);
        if (!isset($gameModel)){
            $return['code'] = -2;
            $return['message'] = "Không tồn tại game";
            echo json_encode($return);die;
        }
        $arr_key = array('info_help',$game_id);
        $key_memcache = Common::build_key_memcache($arr_key);
        if($memcache && $memcache->get($key_memcache)){
            Common::add_key_memcache($key_memcache);
            echo $memcache->get($key_memcache);
        }else{   
            $model = Help::model()->findAll('game_id=:game_id AND status=:status ORDER BY id ASC', array(':game_id' => $game_id,':status' => 1));
            if (!empty($model)) {
                $return['code'] = 1;
                $return['message'] = 'Thành công';
                $return['help'] = array();
                foreach ($model as $index => $item) {
                    $return['help'][$index]['id'] = $item->id;
                    $return['help'][$index]['game_id'] = $item->game_id;
                    $return['help'][$index]['title'] = $item->title;
                    $return['help'][$index]['content'] = $item->content;
                    $return['help'][$index]['time'] = $item->time;
                }
            } else {
                $return['code'] = -1;
                $return['message'] = "Không có dữ liệu nào";
            }
            if($memcache){
                Common::add_key_memcache($key_memcache);
                $memcache->set($key_memcache,  json_encode($return), 0,604800);
            }
            echo json_encode($return);
        }
    }        

    public function actionGetMessage() {
        $return = array();
        $return['code'] = 1;
        $return['message'] = 'Thành công';
        echo json_encode($return);die;
        $time = Yii::app()->request->getParam('time', NULL);
        $time_sent = date("Y-m-d H:i:s", strtotime($time));
        $receiver = Yii::app()->request->getParam('receiver', NULL);
        $sender = Yii::app()->request->getParam('sender', NULL);
        if (!is_numeric($receiver) || $receiver == NULL || !is_numeric($sender) || $sender == NULL) {
            $return['code'] = -2;
            $return['message'] = "Có lỗi";
            echo json_encode($return);
            exit();
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition('receiver=' . $receiver);
        $criteria->addCondition('sender=' . $sender);
        $criteria->addCondition('status=1');
        if ($time != NULL)
            $criteria->addCondition("time_sent > '" . $time_sent . "'");
        $model = UserMessage::model()->findAll($criteria);
        if ($model) {
            $return['code'] = 1;
            $return['message'] = 'Thành công';
            $return['item'] = array();
            foreach ($model as $index => $item) {
                $return['item'][$index]['id'] = $item->id;
                $return['item'][$index]['sender'] = $item->sender;
                $return['item'][$index]['sender_name'] = $item->sender_name;
                $return['item'][$index]['content'] = $item->content;
                $return['item'][$index]['time_sent'] = $item->time_sent;
                $return['item'][$index]['read'] = $item->read;
            }
        } else {
            $return['code'] = -1;
            $return['message'] = "Có lỗi";
        }
        echo json_encode($return);
    }

    public function sendMessage($user_id, $content = '', $template) {
        $return = array();
        $model = User::model()->findByPk($user_id);
        if (!$model) {
            return false;
        }
        $message = new UserMessage();        
        $message->sender_name = 'system';
        $modelSystem = User::model()->find('username=:username',array(':username'=>'system'));
        if(!$modelSystem)
            return false;
        $message->sender = $modelSystem->id;
        $message->receiver = $user_id;
        $message->receiver_name = $model->username;
        $template_content = $this->renderPartial('application.modules.admin.views.message.' . $template, array('content' => $content, 'username' => $model->username), true);
        $message->content = $template_content;
        $message->time_sent = date('Y-m-d H:i:s');
        $message->status = 1;
        if ($message->save()) {
            /* $url = 'http://localhost:8889/es/?type=json&user_id=';
              $ch = curl_init($url);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
              $result = curl_exec($ch); */
            return true;
        } else {
            return false;
        }
    }

    public function actionGetConfig() {
        $return = array();        
        $name = Yii::app()->request->getParam('name', NULL);
        if (isset($name)) {
            $model = Configuration::model()->find("name=:name", array(':name'=>$name));
        } else {
            $model = Configuration::model()->findAll();
        }
        if (empty($model)) {
            $return['code'] = -1;
            $return['message'] = "Không tìm thấy cấu hình";
        } else {		
            $version = Yii::app()->request->getParam('version', NULL);
//            if(isset($version) && (($name == 'allow_recharge_phom') && ($version=="1.2.0.4434")) || (($name == 'allow_recharge_tlmn') && ($version=="1.2.0.4327"))) {
//            	$return = array('code'=>1,'message'=>'Thành công', 'value'=>'{"ios":0,"android":0,"editor":0,"webplayer":0,"pc":0}');
//            } else 
            if (isset($name)) {
                $return['code'] = 1;
                $return['message'] = "Thành công";
                $return['value'] = $model->value;
            } else {
                $return['code'] = 1;
                $return['message'] = "Thành công";
                $return['item'] = array();
                foreach ($model as $index => $item) {
                    $return['item'][$index]['name'] = $item->name;
                    $return['item'][$index]['value'] = $item->value;
                }
            }
        }
        echo json_encode($return);
    }
    
    public function actionGetConfigurationClient(){
        $memcache = Common::initMemcache();
        $return = array();        
        $code_game = Yii::app()->request->getParam('code_game');
        $code_platform = Yii::app()->request->getParam('code_platform');
        $core_version = Yii::app()->request->getParam('core_version');
        $build_version = Yii::app()->request->getParam('build_version');
        $code_revision = Yii::app()->request->getParam('code_revision');                     
        $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');            
        if(!isset($code_game) || !isset($code_platform) || !isset($core_version) || !isset($build_version) || !isset($code_revision)){
            $return['code'] = -2;
            $return['message'] = 'Thong tin dau vao khong dau du';
            echo json_encode($return);die;           
        }
        $code_game = strtolower($code_game);
        $arr_key = array('configuration_client',$code_game,$code_platform,$core_version,$build_version,$code_revision,$partner_code_identifier);
        $key_memcache = Common::build_key_memcache($arr_key);
        if($memcache && $memcache->get($key_memcache)){
            Common::add_key_memcache($key_memcache);
            echo $memcache->get($key_memcache);
        }else{        
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
            if(!isset($partnerModel)){                 
                $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
            }
            $partner_id = $partnerModel->id;                

            $criteria = new CDbCriteria;
            $criteria->alias = "game_version";        
            $criteria->join = ' INNER JOIN game_platform ON game_version.game_platform_id = game_platform.id';
            $criteria->join .= ' INNER JOIN game ON game.id = game_platform.game_id';
            $criteria->join .= ' INNER JOIN platform ON platform.id = game_platform.platform_id';
            $criteria->addCondition('platform.code_identifier =:code_platform');// AND game.code_identifier=:code_game AND game_version.core_version =:core_version AND game_version.build_version =:build_version AND game_version.code_revision =:code_revision', 'params'=>array(':code_platform'=>$code_platform, ':code_game'=>$code_game, ':core_version'=>$core_version, ':build_version'=>$build_version, ':code_revision'=>$code_revision)));        
            $criteria->addCondition('game.code_identifier=:code_game');
            $criteria->addCondition('game_version.core_version =:core_version');
            $criteria->addCondition('game_version.build_version =:build_version');
            $criteria->addCondition('game_version.code_revision =:code_revision');
            $criteria->addCondition('game_version.partner_id =:partner_id');
            $criteria->params = array(':code_platform'=>$code_platform, ':code_game'=>$code_game, ':core_version'=>$core_version, ':build_version'=>$build_version, ':code_revision'=>$code_revision, ':partner_id'=>$partner_id);
            $gameVersion = GameVersion::model()->find($criteria);
            if(!isset($gameVersion)){
                $return['code'] = -1;
                $return['message'] = "Không tìm thấy gameversion tương ứng";
            }else{
                $criteria1 = new CDbCriteria;
                $criteria1->alias = "configuration_client_version";        
                $criteria1->join  = ' INNER JOIN configuration_client ON configuration_client.id = configuration_client_version.configuration_client_id';
                $criteria1->join .= ' INNER JOIN game_version ON game_version.id = configuration_client_version.game_version_id';                
                if (isset($name)) {                            
                    $criteria1->addCondition('game_version.id =:game_version_id AND configuration_client.name=:name');                       
                    $criteria1->params = array(':name'=>$name, ':game_version_id'=>$gameVersion->id);
                    $model = ConfigurationClientVersion::model()->find($criteria1);
                } else {                
                    $criteria1->addCondition('game_version.id =:game_version_id');  
                    $criteria1->params = array(':game_version_id'=>$gameVersion->id);
                    $model = ConfigurationClientVersion::model()->findAll($criteria1);                
                }
                if (empty($model)) {
                    $return['code'] = 1;
                    $return['message'] = "Không tìm thấy cấu hình";
                } else {		
                    if(isset($name)) {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                        $return['name'] = $model->configurationClient->name;
                        $return['value'] = $model->value;
                        $return['create_time'] = $model->create_time;
                    } else {
                        $return['code'] = 0;
                        $return['message'] = "Thành công";
                        $return['items'] = array();
                        foreach ($model as $index => $item) {
                            $return['items'][$index]['name'] = $item->configurationClient->name;
                            $return['items'][$index]['value'] = $item->value;
                            $return['items'][$index]['create_time'] = $item->create_time;
                        }
                    }
                }
            }
            if($memcache){
                Common::add_key_memcache($key_memcache);
                $memcache->set($key_memcache,  json_encode($return),0,604800);
            }
            echo json_encode($return);
        }
    }

    public function actionCheckVersion() {
        $memcache = Common::initMemcache();
        $return = array();
        $version = Yii::app()->request->getParam('version', NULL);
        $full_version = Yii::app()->request->getParam('offical_version', NULL);
        $game_id = Yii::app()->request->getParam('game_id', NULL);
        $platform = Yii::app()->request->getParam('device', NULL);
        $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');           
        if (empty($game_id) || empty($platform) || !is_numeric($game_id)) {            
            $return['code'] = -1;
            $return['message'] = "Dữ liệu đầu vào không đúng";                
            echo json_encode($return);die;            
        }                             
        $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
        if(!isset($partnerModel)){                 
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
        }
        $partner_id = $partnerModel->id;

        $gameModel = Game::model()->findByPk($game_id);
        if (!isset($gameModel)) {
            $return['code'] = -1;
            $return['message'] = "Không tìm thấy game";
            echo json_encode($return);die;
        }
        $arr_key = array('current_version',$game_id,$platform,$partner_code_identifier);
        $key_memcache = Common::build_key_memcache($arr_key);
        if($memcache && $memcache->get($key_memcache)){
            Common::add_key_memcache($key_memcache);
            $model = $memcache->get($key_memcache);
        }else{   
            $model = $gameModel->getCurrentVersion($platform, $partner_id); 
            if($memcache){
                Common::add_key_memcache($key_memcache);
                $memcache->set($key_memcache, $model,0, 604800);
            }
        }
        //Common::dump($model->gamePlatform->market_url;);die;        
        if (!$model) {
            $return['code'] = -1;
            $return['message'] = "Không tìm thấy version hiện tại";
        } else {
            $update = false;
            if(isset($full_version)){
                $full_version = explode('.', $full_version);
                if(count($full_version) > 3){
                    if(floatval($full_version[0].".".$full_version[1]) < floatval($model->core_version))
                        $update = true;
                    if(floatval($full_version[0].".".$full_version[1]) == floatval($model->core_version) && $full_version[2] < $model->build_version)
                        $update = true;
                    if(floatval($full_version[0].".".$full_version[1]) == floatval($model->core_version) && $full_version[2] == $model->build_version && $full_version[3] < $model->code_revision)
                        $update = true;
                }
            }
            if(isset($version) && !isset($full_version)){
                if ($model->code_revision > $version) {
                    $update = true;
                }
            }
            if ($update) {
                if ($model->force_update == 1) {
                    $return['code'] = 1;
                    $return['message'] = "Cập nhật phiên bản mới(bắt buộc)";
                    $return['core_version'] = $model->core_version;
                    $return['build_version'] = $model->build_version;
                    $return['code_version_build'] = $model->code_revision;
                    if($partner_id == 1)
                        $return['link'] = $model->gamePlatform->market_url;
                    else
                        $return['link'] = $model->link_update;
                } elseif($model->force_update == 2) {
                    $return['code'] = 2;
                    $return['message'] = "Cập nhật phiên bản mới(không bắt buộc)";
                    $return['core_version'] = $model->core_version;
                    $return['build_version'] = $model->build_version;
                    $return['code_version_build'] = $model->code_revision;
                    if($partner_id == 1)
                        $return['link'] = $model->gamePlatform->market_url;
                    else {
                        $return['link'] = $model->link_update;
                    }
                }else{
                    $return['code'] = 3;
                    $return['message'] = "Không cập nhật";
                    $return['core_version'] = $model->core_version;
                    $return['build_version'] = $model->build_version;
                    $return['code_version_build'] = $model->code_revision;                    
                    //$return['link'] = $model->gamePlatform->market_url;
                }
            } else {
                $return['code'] = 0;
                $return['message'] = "Chưa có phiên bản mới";
            }
        }        
        echo json_encode($return);
        
    }

    public function actionRechargeBK() {                                
        $return = array();
        $username = Yii::app()->request->getParam('username');
        $soseri = Yii::app()->request->getParam('txtSoSeri');
        $sopin = Yii::app()->request->getParam('txtSoPin');
        $type_card = Yii::app()->request->getParam('select_method');                  
        
        if (!isset($username) || !isset($soseri) || !isset($sopin) || !isset($type_card)) {
            $return['code'] = -2;
            $return['message'] = "Bạn cần nhập đầu đủ thông tin";
            echo json_encode($return);
            exit();
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition('username =:username');  
        $criteria->params = array(':username'=>$username);
        $user = User::model()->find($criteria);
        if (!isset($user)) {
            $return['code'] = -1;
            $return['message'] = "User không tồn tại";
            echo json_encode($return);
            exit();
        }

        Common::includeTheCaoMobile();
        //Tiến hành kết nối thanh toán Thẻ cào.
        $call = new MobiCard();
        $rs = new Result();
        $ref_code = 'NapTien-' . $username . '-' . date('His-dmY');
        $rs = $call->CardPay($sopin, $soseri, $type_card, $ref_code, $username, "0934571589", "thanhnguyendinh89@gmail.com");                  
        if ($rs->error_code == '00') {
            // Ghi log
            $rechargeCardLog = new RechargeCardLog();
            $rechargeCardLog->pin = $sopin;
            $rechargeCardLog->seri = $soseri;
            $rechargeCardLog->card_type = $type_card;
            $rechargeCardLog->username = $user->username;
            $rechargeCardLog->value = $rs->card_amount;
            $rechargeCardLog->save();            
            // Cập nhật data tại đây
            $amount = $rs->card_amount;            
            switch ($type_card) {
                case 'VIETTEL': {
                        $type = $this->getRechargeTypeOfCard('viettel',$amount);
                        $view_type = 'Viettel';
                        break;
                    }
                case 'VNP': {
                        $type = $this->getRechargeTypeOfCard('vinaphone',$amount);
                        $view_type = 'Vinaphone';
                        break;
                    }
                case 'VMS': {
                        $type = $this->getRechargeTypeOfCard('mobifone',$amount);
                        $view_type = 'Mobifone';
                        break;
                    }
                case 'VCOIN': {
                        $type = $this->getRechargeTypeOfCard('vcoin',$amount);
                        $view_type = 'Vcoin';
                        break;
                    }
                case 'GATE': {
                        $type = $this->getRechargeTypeOfCard('gate',$amount);
                        $view_type = 'Gate';
                        break;
                    }
            }
            $userGameActionId = $user->userGameAction('recharge');
            $userGold = $user->rechargeMoney($amount, $type);
            if ($userGold) {    
                $flag = false;
//                $criteria2 = new CDbCriteria();
//                $criteria2->alias = 'user_game_action';
//                $criteria2->select = 'user_game_action.id,user_game_action.version';
//                $criteria2->addCondition('user_game_action.user_id = '.$user->id.' AND user_game_action.game_action_id = 38');
//                $criteria2->order = 'user_game_action.timestamp DESC';
//                $userGameAction = UserGameAction::model()->find($criteria2);                   
//                if(isset($userGameAction)){
//                    $full_version = $userGameAction->version;                    
//                    if(isset($full_version)){                        
//                        $full_version = explode('.', $full_version);
//                        if(count($full_version) > 3){
//                            if(floatval($full_version[0].".".$full_version[1]) < floatval(2.2))
//                                $flag = true;
//                            if(floatval($full_version[0].".".$full_version[1]) == floatval(2.3) && $full_version[2] < 3)
//                                $flag = true;
//                            if(floatval($full_version[0].".".$full_version[1]) == floatval(2.2) && $full_version[2] == 3 && $full_version[3] < 4148)
//                                $flag = true;
//                        }
//                    }                    
//                }                
                if($flag == true){                       
                    $userChip = $user->convertGoldByChip($userGold);
                    $userChipBonus = 0;
                    if($userGameActionId)
                        $userChipBonus = $user->bonusChips($userGameActionId,$userGold);

                    $totalChip = $userChip + $userChipBonus;
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'chip', $totalChip);                                      

                    MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($totalChip, 0, ',', '.')." chip từ hệ thống Esimo qua thẻ cào ".$view_type."!",1);
                    $return['code'] = 1;
                    $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($totalChip, 0, ',', '.') . ' Chip  vào tài khoản của bạn';                 
                }else{                       
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'gold', $userGold);                                                                                                     
                    MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." gold từ hệ thống Esimo qua thẻ cào ".$view_type."!",1);
                    $return['code'] = 1;
                    $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($userGold, 0, ',', '.') . ' Gold  vào tài khoản của bạn';                 
                }
                
            } else {
                $return['code'] = 0;
                $return['message'] = 'Nạp tiền thất bại';
            }
        } else {
            $return['code'] = -3;
            $return['message'] = $rs->error_message;
        }
        echo json_encode($return);
    }
    
    public function getRechargeTypeOfCard($code, $amount){           
        $criteria = new CDbCriteria;
        $criteria->alias = 'recharge_type';
        $criteria->join = "INNER JOIN telco on telco.id = recharge_type.telco_id";
        $criteria->join .= " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
        $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
        $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
        $criteria->addCondition('telco.code_identifier="'.$code.'" AND recharge_method.code_identifier = "mobile_card" AND topup_provider.code_identifier="pandapay" AND recharge_method_provider.code='.$amount);          
        $rechargeType = RechargeType::model()->find($criteria);
        if(isset($rechargeType))
            $type =  $rechargeType->id;        
        else 
            $type = 0;
        return $type;
    }
    
    public function actionRecharge(){           
        $return = array();
        $username = Yii::app()->request->getParam('username');
        $serial = Yii::app()->request->getParam('txtSoSeri');
        $pin = Yii::app()->request->getParam('txtSoPin');
        $card_type = Yii::app()->request->getParam('select_method');  
        if (!isset($username) || !isset($serial) || !isset($pin) || !isset($card_type)) {
            $return['code'] = -2;
            $return['message'] = "Bạn cần nhập đầu đủ thông tin";
            echo json_encode($return);
            exit();
        }
        $criteria = new CDbCriteria;
        $criteria->addCondition('username =:username');  
        $criteria->params = array(':username'=>$username);
        $user = User::model()->find($criteria);
        if (!isset($user)) {
            $return['code'] = -1;
            $return['message'] = "User không tồn tại";
            echo json_encode($return);
            exit();
        }
        if($card_type == 'VIETTEL') $card_type = 'VTT';
        if($card_type == 'VCOIN') $card_type = 'VTC';
        if($card_type == 'GATE') $card_type = 'FPT';                              
                            
        $partner_id = '1013';   
        $merchant_id = '100007';
        $secret_key = 'b9b267f230149f11fbb597e892c1aaf2';       
        $curlHelper = new cURL();
        $url = 'http://connect.pandapay.net/CardCharge.asmx/Charging';
        $data = array(
            'partner_id' => $partner_id,
            'merchant_id' => $merchant_id,
            'telco'       => $card_type,            
            'usernameClient' => $username,
            'mobileClient' => '',      
            'emailClient'=>'',
            'code'            => $pin,            
            'serial'         => $serial,
            'valid' => md5($partner_id.'|'.$merchant_id.'|'.$serial.'|'.$card_type.'|'.$secret_key),
            'secret_key'    => $secret_key
        );                        
        $result = $curlHelper->post($url,$data);
        $response = new SimpleXMLElement($result);
        $obj = json_decode($response[0]);        
//        $obj->ErrCode = '00';        
//        $obj->data = 10000;
        if ($obj->ErrCode == '00') {
            // Ghi log
            $rechargeCardLog = new RechargeCardLog();
            $rechargeCardLog->pin = $pin;
            $rechargeCardLog->seri = $serial;
            $rechargeCardLog->card_type = $card_type;
            $rechargeCardLog->username = $user->username;
            $rechargeCardLog->value = $obj->data;
            $rechargeCardLog->save();
            // Cập nhật data tại đây
            $amount = $obj->data;            
            switch ($card_type) {
                case 'VTT': {
                        $type = $this->getRechargeTypeOfCard('viettel',$amount);
                        $view_type = 'Viettel';
                        break;
                    }
                case 'VNP': {
                        $type = $this->getRechargeTypeOfCard('vinaphone',$amount);
                        $view_type = 'Vinaphone';
                        break;
                    }
                case 'VMS': {
                        $type = $this->getRechargeTypeOfCard('mobifone',$amount);
                        $view_type = 'Mobifone';
                        break;
                    }
                case 'VTC': {
                        $type = $this->getRechargeTypeOfCard('vcoin',$amount);
                        $view_type = 'Vcoin';
                        break;
                    }
                case 'FPT': {
                        $type = $this->getRechargeTypeOfCard('gate',$amount);
                        $view_type = 'Gate';
                        break;
                    }
            }
            $userGold = $user->rechargeMoney($amount, $type);
            $userGameActionId = $user->userGameAction('recharge');
            if ($userGold) {
                $electroServer = new ElectroServerBridge();
                $electroServer->sendUserInfo($user->id, 'gold', $userGold);                                                                                                     
                MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." gold từ hệ thống Gamedautri qua thẻ cào ".$view_type."!",1);
                $return['code'] = 1;
                $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($userGold, 0, ',', '.') . ' Gold  vào tài khoản của bạn';                                                 
                                                
            } else {
                $return['code'] = 0;
                $return['message'] = 'Nạp tiền thất bại';
            }
        } else {
            $return['code'] = -3;
            $return['ErrCode'] = $obj->ErrCode;
            $return['message'] = $obj->ErrMessage;
        }
        echo json_encode($return);                         
    }        

    public function actionRechargeSMS() {
        //require_once('fibosmsconfig.php'); // Khai báo dùng thư viện của Fibo 
        Common::includeFiboSMS();
        $fibo = new FiboSMS();
        $fibo->CheckRequest();        //Hàm kiể m tra request, đảm bảo yêu cầu xử lý đến từ  server củ a Fibo
        // Lấy nội dung tin nhắn 
        $message = $_REQUEST['message']; // Nội dung tin 
        $phone = $_REQUEST['phone']; // số điện thoại của KH 
        $service = $_REQUEST['service']; // mã dịch vụ 
        $port = $_REQUEST['port']; // đầu số 
        $main = $_REQUEST['main']; //keyword 
        $sub = $_REQUEST['sub']; // prefix         
        // Hết lấy nội dung tin nhắn 
        $md5id = md5(uniqid(rand(), true)); // id duy nhất để gán cho tin trả về 
        
        // tren site that esimo.vn
        if ($port == '8577' || $port == '8677' || $port == '8777') {// kiểm tra xem có đúng đầu số không? 
            $telco = $this->findTelcoSms($phone);           
            $message = strtoupper($message);
            $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
            if ($tmp[0] == 'ESIMO' && // phần thứ nhất chứa keyword 
                    @$tmp[1] == 'NAP' //phần thứ 2 chứa Prefix 
            ) {
                switch ($port) {                        
                    case '8577': {
                            $amount = 5000;
                            $type = $this->getRechargeTypeOfSms($telco, '8577');    
                            break;
                        }
                    case '8677': {
                            $amount = 10000;
                            $type = $this->getRechargeTypeOfSms($telco, '8677');                             
                            break;
                        }
                    case '8777': {
                            $amount = 15000;
                            $type = $this->getRechargeTypeOfSms($telco, '8777');    
                            break;
                        }
                }                     
                // luu log sms
                $mo = new MoSmsLog();
                $mo->user_id = $phone;
                $mo->service_id = $port;
                $mo->command_code = $main;
                $mo->message = $message;
                $mo->request_id = $service;
                $mo->save();

                $criteria = new CDbCriteria;
                $criteria->addCondition('username =:username');  
                $criteria->params = array(':username'=>$tmp[2]);
                $user = User::model()->find($criteria);                
                if (isset($user)) {
                    if($type != 0){
                        $userGold = $user->rechargeMoney($amount, $type);
                        if ($userGold) {
                            $flag = false;
                            $criteria2 = new CDbCriteria();
                            $criteria2->alias = 'user_game_action';
                            $criteria2->select = 'user_game_action.id,user_game_action.version';
                            $criteria2->addCondition('user_game_action.user_id = '.$user->id.' AND user_game_action.game_action_id = 38');
                            $criteria2->order = 'user_game_action.timestamp DESC';
                            $userGameAction = UserGameAction::model()->find($criteria2);                   
                            if(isset($userGameAction)){
                                $full_version = $userGameAction->version;                    
                                if(isset($full_version)){                        
                                    $full_version = explode('.', $full_version);
                                    if(count($full_version) > 3){
                                        if(floatval($full_version[0].".".$full_version[1]) < floatval(2.2))
                                            $flag = true;
                                        if(floatval($full_version[0].".".$full_version[1]) == floatval(2.3) && $full_version[2] < 3)
                                            $flag = true;
                                        if(floatval($full_version[0].".".$full_version[1]) == floatval(2.2) && $full_version[2] == 3 && $full_version[3] < 4148)
                                            $flag = true;
                                    }
                                }

                            } 
                            if($flag == true){                                                        
                                $userChip = $user->convertGoldByChip($userGold);
                                $electroServer = new ElectroServerBridge();
                                $return = $electroServer->sendUserInfo($user->id, 'chip', $userChip);

                                $electroServer2 = new ElectroServerBridge('210.211.102.121');
                                $electroServer2->sendUserInfo($user->id, 'chip', $userChip); 

                                MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Esimo qua đầu số ".$port."!",1);                                                          
                                // tra ve khi thanh cong
                                echo ' <ClientResponse> 
                                        <Message> 
                                            <PhoneNumber>' . $phone . '</PhoneNumber> 
                                            <Message>Ban da nap ' . $userChip . ' chip vao tai khoan ' . $user->username . ' </Message> 
                                            <SMSID>' . $md5id . '</SMSID> 
                                            <ServiceNo>' . $service . '</ServiceNo> 
                                        </Message> 
                                </ClientResponse>';
                            }else{
                                $electroServer = new ElectroServerBridge();
                                $return = $electroServer->sendUserInfo($user->id, 'gold', $userGold);                                                        
                                MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." gold từ hệ thống Esimo qua đầu số ".$port."!",1);
                                // tra ve khi thanh cong
                                echo ' <ClientResponse> 
                                        <Message> 
                                            <PhoneNumber>' . $phone . '</PhoneNumber> 
                                            <Message>Ban da nap ' . $userGold . ' gold vao tai khoan ' . $user->username . ' </Message> 
                                            <SMSID>' . $md5id . '</SMSID> 
                                            <ServiceNo>' . $service . '</ServiceNo> 
                                        </Message> 
                                </ClientResponse>';
                            }
                            
                        } else {
                            // loi trong qua trinh xu ly phia esimo
                            echo ' <ClientResponse> 
                                    <Message> 
                                        <PhoneNumber>' . $phone . '</PhoneNumber> 
                                        <Message>Co loi trong qua trinh xu ly, vui long lien he CSKH Esimo 01262500000 </Message> 
                                        <SMSID>' . $md5id . '</SMSID> 
                                        <ServiceNo>' . $service . '</ServiceNo> 
                                    </Message> 
                            </ClientResponse>';
                        }                        
                    }else{
                        // khong xac dinh duoc hinh thuc nap tien
                        echo ' <ClientResponse> 
                                <Message> 
                                    <PhoneNumber>' . $phone . '</PhoneNumber> 
                                    <Message>So dien thoai khong hop le, vui long lien he CSKH Esimo 01262500000 </Message> 
                                    <SMSID>' . $md5id . '</SMSID> 
                                    <ServiceNo>' . $service . '</ServiceNo> 
                                </Message> 
                        </ClientResponse>';
                    }
                } else {
                    // khong ton tai tai khoan nap tien
                    echo ' <ClientResponse> 
                                <Message> 
                                    <PhoneNumber>' . $phone . '</PhoneNumber> 
                                    <Message>Tai khoan khong ton tai, vui long lien he CSKH Esimo 01262500000 </Message> 
                                    <SMSID>' . $md5id . '</SMSID> 
                                    <ServiceNo>' . $service . '</ServiceNo> 
                                </Message> 
                        </ClientResponse>';
                }
            } else {// nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp 
                echo ' <ClientResponse> 
                            <Message> 
                                <PhoneNumber>' . $phone . '</PhoneNumber> 
                                <Message>Noi dung tin nhan khong hop le. Vui long goi tin nhan theo noi dung ESIMO NAP <Ten dang nhap></Message> 
                                <SMSID>' . $md5id . '</SMSID> 
                                <ServiceNo>' . $service . '</ServiceNo>
                            </Message> 
                        </ClientResponse>';
            }            
        } else { 
            //trường hợp nhắn sai đầu số 
            echo ' <ClientResponse> 
                        <Message> 
                            <PhoneNumber>' . $phone . '</PhoneNumber> 
                            <Message>Ban da nhap sai dau so</Message> 
                            <SMSID>' . $md5id . '</SMSID> 
                            <ServiceNo>' . $service . '</ServiceNo> 
                        </Message> 
                    </ClientResponse>';
        }
        
    }   
    
    public function getRechargeTypeOfSms($code, $key){           
        $criteria = new CDbCriteria;
        $criteria->alias = 'recharge_type';
        $criteria->join = "INNER JOIN telco on telco.id = recharge_type.telco_id";
        $criteria->join .= " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
        
        $criteria->addCondition('telco.code_identifier="'.$code.'"  AND recharge_method_provider.code='.$key);          
        $rechargeType = RechargeType::model()->find($criteria);
        if(isset($rechargeType))
            $type =  $rechargeType->id;        
        else 
            $type = 0;
        return $type;
    }
    
    public function findTelcoSms($phone){
        $telcoConfigs = Configuration::model()->getConfig('mobile_detect');    
        $telco = "viettel";
        if(isset($telcoConfigs)){
            $sub_phone4 = substr($phone, 0, 4);
            $sub_phone5 = substr($phone, 0, 5);            
            foreach($telcoConfigs as $key => $item){
                $tmps = explode(',', $item);
                foreach($tmps as $tmp){
                    if($sub_phone4 == $tmp || $sub_phone5 == $tmp){
                        $telco = $key;
                    }                            
                }
            }
        }
        return $telco;
    }

    public function actionGetAds() {  
        $memcache = Common::initMemcache();
        $return = array();
        $game_id = Yii::app()->request->getParam('game_id', NULL);
        $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');  
        $arr_key = array('info_ads',$game_id,$partner_code_identifier);
        $memcache_key = Common::build_key_memcache($arr_key); 
        if($memcache && $memcache->get($memcache_key)){           
            echo $memcache->get($memcache_key);           
        }else{
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
            if(!isset($partnerModel)){                 
                $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
            }
            $partner_id = $partnerModel->id;   
            if (isset($game_id)) {
                $model = Ads::model()->findAll("game_id=:game_id AND partner_id=:partner_id", array(':game_id'=>$game_id, ':partner_id'=>$partner_id ));
            } else {
                $model = Ads::model()->findAll("partner_id =:partner_id", array(':partner_id'=>$partner_id));
            }
            if (empty($model)) {
                $return['code'] = -1;
                $return['message'] = "Không có quảng cáo nào!";
            } else {
                if (isset($game_id)) {
                    $return['code'] = 1;
                    $return['message'] = "Thành công";
                    $return['item'] = array();
                    foreach ($model as $index => $item) {
                        $return['item'][$index]['name'] = $item->id;
                        $return['item'][$index]['partner_id'] = $item->partner_id;
                        $return['item'][$index]['type'] = $item->type;
                        $return['item'][$index]['index'] = $item->index;
                        $return['item'][$index]['description'] = $item->description;
                        $return['item'][$index]['scenes'] = $item->scenes;
                        $return['item'][$index]['url'] = $item->url;
                        $return['item'][$index]['image'] = $item->image;
                    }
                } else {
                    $return['code'] = 1;
                    $return['message'] = "Thành công";
                    $return['item'] = array();
                    foreach ($model as $index => $item) {
                        $return['item'][$index]['name'] = $item->id;
                        $return['item'][$index]['partner_id'] = $item->partner_id;
                        $return['item'][$index]['type'] = $item->type;
                        $return['item'][$index]['index'] = $item->index;
                        $return['item'][$index]['description'] = $item->description;
                        $return['item'][$index]['scenes'] = $item->scenes;
                        $return['item'][$index]['url'] = $item->url;
                        $return['item'][$index]['image'] = $item->image;
                    }
                }
            }
            if($memcache){
                Common::add_key_memcache($memcache_key);
                $memcache->set($memcache_key,  json_encode($return)); 
            }
            echo json_encode($return);
       }
   }
   
   public function actionGetPartner(){   
       $memcache = Common::initMemcache();
       if( $memcache && $memcache->get('info_partner_default')){    
            Common::add_key_memcache('info_partner_default');
            echo $memcache->get('info_partner_default');           
       }else{                         
            $return = array();
            $models = Partner::model()->findAll();
            if(empty($models)){
               $return['code'] = -1;
               $return['message'] = "Không tồn tại partner nào";
            }else{
                $return['code'] = 1;
                $return['message'] = "Thành công";
                $return['items'] = array();
                foreach($models as $index => $model){
                   $return['items'][$index]['id'] = $model->id;
                   $return['items'][$index]['code_identifier'] = $model->code_identifier;
                   $return['items'][$index]['name'] = $model->name;
                   $return['items'][$index]['address'] = $model->address;
                   $return['items'][$index]['tel'] = $model->tel;
                   $return['items'][$index]['mobile'] = $model->mobile;
                   $return['items'][$index]['email'] = $model->email;
                   $return['items'][$index]['website'] = $model->website;
                }
            }
            if($memcache){
                Common::add_key_memcache('info_partner_default');
                $memcache->set('info_partner_default',  json_encode($return));   
            }
            echo json_encode($return);
       }
   }

   public function actionGetInfoRecharge(){ 
        $memcache = Common::initMemcache();
        $return = array();        
        $code_game = Yii::app()->request->getParam('code_game');
        $code_platform = Yii::app()->request->getParam('code_platform');
        $core_version = Yii::app()->request->getParam('core_version');
        $build_version = Yii::app()->request->getParam('build_version');
        $code_revision = Yii::app()->request->getParam('code_revision');                       
        $partner_code_identifier = Yii::app()->request->getParam('partner_code_identifier');   
        if(!isset($code_game) || !isset($code_platform) || !isset($core_version) || !isset($build_version) || !isset($code_revision)){
            $return['code'] = -2;
            $return['message'] = "Thiếu tham số";    
            echo json_encode($return);die;            
        } 
        $arr_key = array('info_recharge',$code_game,$code_platform,$core_version,$build_version,$code_revision,$partner_code_identifier);
        $memcache_key = Common::build_key_memcache($arr_key);            
        if($memcache && $memcache->get($memcache_key)){
            Common::add_key_memcache($memcache_key);  
            echo $memcache->get($memcache_key);           
        }else{                             
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>$partner_code_identifier));
            if(!isset($partnerModel)){                 
                $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
            }
            $partner_id = $partnerModel->id;
                 
            $sms_provider = "sms_recharge_provider";
            $card_provider = "card_recharge_provider";

            $criteria = new CDbCriteria;
            $criteria->alias = "game_version";        
            $criteria->join = ' INNER JOIN game_platform ON game_version.game_platform_id = game_platform.id';
            $criteria->join .= ' INNER JOIN game ON game.id = game_platform.game_id';
            $criteria->join .= ' INNER JOIN platform ON platform.id = game_platform.platform_id';
            $criteria->addCondition('platform.code_identifier =:code_platform');// AND game.code_identifier=:code_game AND game_version.core_version =:core_version AND game_version.build_version =:build_version AND game_version.code_revision =:code_revision', 'params'=>array(':code_platform'=>$code_platform, ':code_game'=>$code_game, ':core_version'=>$core_version, ':build_version'=>$build_version, ':code_revision'=>$code_revision)));        
            $criteria->addCondition('game.code_identifier=:code_game');
            $criteria->addCondition('game_version.core_version =:core_version');
            $criteria->addCondition('game_version.build_version =:build_version');
            $criteria->addCondition('game_version.code_revision =:code_revision');
            $criteria->addCondition('game_version.partner_id =:partner_id');
            $criteria->params = array(':code_platform'=>$code_platform, ':code_game'=>$code_game, ':core_version'=>$core_version, ':build_version'=>$build_version, ':code_revision'=>$code_revision, ':partner_id'=>$partner_id);
            $gameVersion = GameVersion::model()->find($criteria);
            if(!isset($gameVersion)){
                $return['code'] = -1;
                $return['message'] = "Không tìm thấy gameversion tương ứng";                
            }else{
                $criteria1 = new CDbCriteria;
                $criteria1->alias = "configuration_client_version";        
                $criteria1->join  = ' INNER JOIN configuration_client ON configuration_client.id = configuration_client_version.configuration_client_id';
                $criteria1->join .= ' INNER JOIN game_version ON game_version.id = configuration_client_version.game_version_id';                                                     
                $criteria1->addCondition('game_version.id =:game_version_id AND configuration_client.name= "'.$sms_provider.'"');                       
                $criteria1->params = array(':game_version_id'=>$gameVersion->id);
                $modelConfig1 = ConfigurationClientVersion::model()->find($criteria1);      

                $criteria2 = new CDbCriteria;
                $criteria2->alias = "configuration_client_version";        
                $criteria2->join  = ' INNER JOIN configuration_client ON configuration_client.id = configuration_client_version.configuration_client_id';
                $criteria2->join .= ' INNER JOIN game_version ON game_version.id = configuration_client_version.game_version_id';                                                     
                $criteria2->addCondition('game_version.id =:game_version_id AND configuration_client.name= "'.$card_provider.'"');                       
                $criteria2->params = array(':game_version_id'=>$gameVersion->id);
                $modelConfig2 = ConfigurationClientVersion::model()->find($criteria2);      
                $tmp1 = array();
                $tmp2 = array();
                if(isset($modelConfig1)){            
                    $tmp1 = explode(',', $modelConfig1->value);                        
                }
                if(isset($modelConfig2)){
                    $tmp2 = explode(',', $modelConfig2->value);            
                }       
                $return = $this->displayInfoRecharge($tmp1,$tmp2);  
            }
           if($memcache){ 
                Common::add_key_memcache($memcache_key);     
                $memcache->set($memcache_key,json_encode($return));
           }
           echo json_encode($return);     
       }
   }   
   
   public function displayInfoRecharge($provider_sms=array(), $provider_card=array()){
        $return = array();        
//        if(empty($provider_sms))
//            $provider_sms = array('vmg');
//        if(empty($provider_card))
//            $provider_card = array('viettel','mobifone','vinaphone','vcoin','gate');
        $str = '';
        $count = count($provider_sms);        
        if($count > 0){
            foreach($provider_sms as $j => $p){       
                if($j < ($count-1))
                    $str .= '"'.$p.'",';
                else
                    $str .= '"'.$p.'"';
            }
        }         
        $criteria = new CDbCriteria;
        $criteria->alias = "recharge_type";
        $criteria->join = "INNER JOIN recharge_method_provider on recharge_method_provider.id = recharge_type.recharge_method_provider_id";
        $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
        $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
        $criteria->addCondition('recharge_method.code_identifier = "sms" AND topup_provider.code_identifier IN ('.$str.') AND recharge_method_provider.status = 1 ');            
        $criteria->group = "recharge_method_provider.id";
        $models = RechargeType::model()->findAll($criteria); 
                
        //$models = RechargeType::model()->findAll('(type="sms" OR type="mobile_card") AND status = 1');
        $return['code'] = 1;
        $return['message'] = "Thành công";
        $return['items'] = array();  
        $ratio = Configuration::model()->getConfig('gold_by_chip');
        $i=0;
        if(isset($models)){
            foreach ($models as $index => $items) {
                $return['items'][$index]['type'] = $items->rechargeMethodProvider->rechargeMethod->code_identifier;
                $return['items'][$index]['code'] = $items->rechargeMethodProvider->name;
                $return['items'][$index]['code_value'] = $items->value;
                $return['items'][$index]['provider'] = $items->rechargeMethodProvider->topupProvider->code_identifier;
                $return['items'][$index]['value'] = ceil($items->value * $items->ratio) * $ratio; 
                $return['items'][$index]['gold_value'] = ceil($items->value * $items->ratio); 
                $return['items'][$index]['template'] = $items->template; 
                $return['items'][$index]['image'] = $items->image; 
                $i = $index;
            }
        }        
        $str2 = '';
        $count2 = count($provider_card);
        if($count2 > 0){
            foreach($provider_card as $j => $p){       
                if($j < ($count2-1))
                    $str2 .= '"'.$p.'",';
                else
                    $str2 .= '"'.$p.'"';
            }
        }                    
        $criteria2 = new CDbCriteria;
        $criteria2->alias = "recharge_type";
        $criteria2->join = "INNER JOIN recharge_method_provider on recharge_method_provider.id = recharge_type.recharge_method_provider_id";
        $criteria2->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
        $criteria2->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
        $criteria2->join .= " INNER JOIN telco on telco.id = recharge_type.telco_id";
        $criteria2->addCondition('recharge_method.code_identifier = "mobile_card" AND topup_provider.code_identifier="nganluong" AND telco.code_identifier IN ('.$str2.') AND recharge_type.status=1');            
        //$criteria2->group = "telco.id";
        $models2 = RechargeType::model()->findAll($criteria2);     
        if(isset($models2)){
            foreach ($models2 as $index => $items) {
                if($i > 0)
                    $index2 = $index+$i+1;
                else
                    $index2 = $index;
                $return['items'][$index2]['type'] = $items->rechargeMethodProvider->rechargeMethod->code_identifier;
                $return['items'][$index2]['code'] = $items->rechargeMethodProvider->name;
                $return['items'][$index2]['code_value'] = $items->value;
                $return['items'][$index2]['provider'] = $items->telco->code_identifier;
                $return['items'][$index2]['value'] = ceil($items->value * $items->ratio) * $ratio; 
                $return['items'][$index2]['gold_value'] = ceil($items->value * $items->ratio); 
                $return['items'][$index2]['template'] = $items->template; 
                $return['items'][$index2]['image'] = $items->image; 
            }
        }
        return $return;
   }
   
   public function actionGetCurrentVersionFacebook(){       
       $return = array();
       $game =  Yii::app()->request->getParam('game');
       $arr = explode('.', $game);
       if(is_array($arr)){
           $gameModel = Game::model()->find('code_identifier=:code_identifier',array(':code_identifier'=>$arr[0]));
           if(!isset($gameModel)){
               $return['code'] = -1;
               $return['message'] = "Không tồn tại game";
               echo json_encode($return);die;
           }
           $gameVersion = $gameModel->getCurrentVersion('fb');
           if(!$gameVersion){
               $return['code'] = -2;
               $return['message'] = "Không tồn tại game current version";
               echo json_encode($return);die;
           }else{               
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($gameModel->name.'-'.$gameVersion->getCurrentVersion()));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($gameVersion->file_path));
                ob_clean();
                flush();
                readfile($gameVersion->file_path);               
           }
       }       
   }  
   
   public function actionGetGame(){       
       $return = array();
       $platform = Yii::app()->request->getParam('platform');
       
        $criteria = new CDbCriteria;
        $criteria->alias = "game_platform";        
        $criteria->join = ' INNER JOIN game ON game_platform.game_id = game.id';            
        $criteria->join .= ' INNER JOIN platform ON platform.id = game_platform.platform_id';
        if(isset($platform)){
            $criteria->addCondition('platform.code_identifier =:code_platform');            
            $criteria->params = array(':code_platform'=>$platform);
        }
        $gamePlatform = GamePlatform::model()->findAll($criteria);           
        if(isset($gamePlatform)){
            $return['code'] = 1;
            $return['message'] = "Thành công";
            $return['items'] = array();
            foreach($gamePlatform as $index => $item){
                $return['items'][$index]['platform'] = $item->platform->name;
                $return['items'][$index]['game'] = $item->game->name;
                $return['items'][$index]['image'] = Yii::app()->createAbsoluteUrl($item->game->image);
                $return['items'][$index]['bundle'] = $item->bundle_id;
                $return['items'][$index]['url'] = $item->market_url;
            }
        }else{
            $return['code'] = 0;
            $return['message'] = 'Khong ton tai game';
        }       
       echo json_encode($return);
   }
   
   public function actionGetPolicy(){
       $return = array();
       $update_time = Yii::app()->request->getParam('update_time');              
       $policy = Post::model()->find('category_id=2');       
       if(isset($policy)){
           if(isset($update_time) && strtotime($update_time) < strtotime($policy->create_time)){
                $return['code'] = 2;
                $return['message'] = 'Khong update';
                echo json_encode($return);die;
            }
           $return['code'] = 1;
           $return['message'] = 'Thanh cong';
           $return['policy'] = $policy->content;
           $return['update_time'] = $policy->create_time;
       }else{
           $return['code'] = 0;
           $return['message'] = 'Khong ton tai';
       }
       echo json_encode($return);
       
   }         
   
   /**
     * @param string the userid of the rechargesmsvmg
     * @param string the serviceid of the rechargesmsvmg
     * @param string the commandcode of the rechargesmsvmg
     * @param string the message of the rechargesmsvmg
     * @param string the requestid of the rechargesmsvmg
     * @return string the rechargesmsvmg messageReceiver
     * @soap
     */
   
    public function getMO($userid,$serviceid,$commandcode,$message,$requestid) {
        return;
            $mo = new MoSmsLog();
            $mo->user_id = $userid;
            $mo->service_id = $serviceid;
            $mo->command_code = $commandcode;
            $mo->message = $message;
            $mo->request_id = $requestid;
            $mo->save();

            $message = strtoupper($message);  
            $commandcode = strtoupper($commandcode);              
            $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
            if ($tmp[0] == 'ESIMO' && // phần thứ nhất chứa keyword 
                $commandcode == 'ESIMO'    
            ) {
                if($serviceid == "8179"){                    
                    if(isset($tmp[1]) && $tmp[1] == 'MK')
                        $message = '{"message":"Tai khoan ESIMO dang ky voi so dien thoai nay da duoc cap mat khau moi la : 111111 . Chi tiet lien he : 01262500000","messageType":1}';
                    else
                        $message = '{"message":"Cu phap sai. Vui long soan tin: ESIMO MK gui 8179. Chi tiet lien he : 01262500000","messageType":2}';
                }elseif($serviceid == "8279"){ 
                    if(isset($tmp[1]) && $tmp[1] == 'KH')  
                        $message = '{"message":"Tai khoan da duoc kich hoat tren he thong ESIMO voi so dien thoai nay. Chi tiet lien he : 01262500000","messageType":1}';                                        
                    else
                        $message = '{"message":"Cu phap sai. Vui long soan tin: ESIMO KH gui 8279. Chi tiet lien he : 01262500000","messageType":2}';                                                                
                }elseif($serviceid == '8579' || $serviceid == '8679' || $serviceid == '8779'  || $serviceid == "8379" || $serviceid == "8479"){
                    $telco = $this->findTelcoSms($userid);
                    switch ($serviceid) {  
                        case '8379': {
                            $amount = 3000;
                            $type = $this->getRechargeTypeOfSms($telco, '8379');  
                            break;
                        }
                        case '8479': {
                            $amount = 4000;
                            $type = $this->getRechargeTypeOfSms($telco, '8479');   
                            break;
                        }
                        case '8579': {
                                $amount = 5000;
                                $type = $this->getRechargeTypeOfSms($telco, '8579');   
                                break;
                            }
                        case '8679': {
                                $amount = 10000;
                                $type = $this->getRechargeTypeOfSms($telco, '8679'); 
                                break;
                            }
                        case '8779': {
                                $amount = 15000;
                                $type = $this->getRechargeTypeOfSms($telco, '8779');   
                                break;
                            }
                    }
                    $criteria = new CDbCriteria;
                    $criteria->addCondition('username =:username');  
                    $criteria->params = array(':username'=>$tmp[1]);
                    $user = User::model()->find($criteria);                
                    if (isset($user)) {
                        if($type != 0){
                            $userGold = $user->rechargeMoney($amount, $type);
                            if ($userGold) {
                                $userChip = $user->convertGoldByChip($userGold);
                                $electroServer = new ElectroServerBridge();
                                $electroServer->sendUserInfo($user->id, 'chip', $userChip);
                                MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Esimo qua đầu số ".$serviceid."!",1);                           
                                $message = '{"message":"Ban da nap '.$userChip.' CHIP vao tai khoan '.$user->username.' tren he thong game ESIMO. Chuc ban choi vui ve va gap nhieu may man !","messageType":1}';                                                                                              
                            } else {
                                $message = '{"message":"Gap loi trong qua trinh nap tien. Chi tiet lien he : 01262500000","messageType":2}';                                                                                               
                            }
                        }else{
                            $message = '{"message":"Khong xac dinh duoc dau so! Chi tiet lien he : 01262500000","messageType":2}';
                        }
                    }else{
                        $message = '{"message":"Username cua ban khong ton tai! Chi tiet lien he : 01262500000","messageType":2}';                               
                    }
                }
            }        

            return $message;
        }  
        
        /**
        * @param string the message of the rechargesmsvht
        * @param string the phone of the rechargesmsvht
        * @param string the service of the rechargesmsvht
        * @param string the port of the rechargesmsvht
        * @param string the main of the rechargesmsvht
        * @return string the rechargesmsvht messagemt
        * @soap
        */        
        public function getMoVht($message,$phone,$service,$port,$main){      
            $return = array();
            //require_once('fibosmsconfig.php'); // Khai báo dùng thư viện của Fibo 
           Common::includeVhtSMS();
           $vht = new VhtSMS();
           $vht->CheckRequest();        //Hàm kiể m tra request, đảm bảo yêu cầu xử lý đến từ  server củ a Fibo                 
           // Hết lấy nội dung tin nhắn            
           
           if ($port == '6589' || $port == '6689' || $port == '6789') {// kiểm tra xem có đúng đầu số không? 
               $telco = $this->findTelcoSms($phone);           
               $message = strtoupper($message);
               $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
               if ($tmp[0] == 'ESIMO' // phần thứ nhất chứa keyword                       
               ) {
                   switch ($port) {                        
                       case '6589': {
                               $amount = 5000;
                               $type = $this->getRechargeTypeOfSms($telco, '6589');    
                               break;
                           }
                       case '6689': {
                               $amount = 10000;
                               $type = $this->getRechargeTypeOfSms($telco, '6689');                             
                               break;
                           }
                       case '6789': {
                               $amount = 15000;
                               $type = $this->getRechargeTypeOfSms($telco, '6789');    
                               break;
                           }
                   }                     
                   // luu log sms
                   $mo = new MoSmsLog();
                   $mo->user_id = $phone;
                   $mo->service_id = $port;
                   $mo->command_code = $main;
                   $mo->message = $message;
                   $mo->request_id = $service;
                   $mo->save();

                   $criteria = new CDbCriteria;
                   $criteria->addCondition('username =:username');  
                   $criteria->params = array(':username'=>$tmp[1]);
                   $user = User::model()->find($criteria);                
                   if (isset($user)) {
                       if($type != 0){
                           $userGold = $user->rechargeMoney($amount, $type);
                           if ($userGold) {
                               $userChip = $user->convertGoldByChip($userGold);
                               $electroServer = new ElectroServerBridge();
                               $result = $electroServer->sendUserInfo($user->id, 'chip', $userChip);
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Esimo qua đầu số ".$port."!",1);
                               // tra ve khi thanh cong
                               $return['message'] = 'Ban da nap ' . $userChip . ' chip vao tai khoan ' . $user->username;
                               $return['IsSuccess'] = true;
                           } else {
                               // loi trong qua trinh xu ly phia esimo
                               $return['message'] = 'Co loi trong qua trinh xu ly, vui long lien he CSKH Esimo 01262500000';
                               $return['IsSuccess'] = false;
                           }                        
                       }else{
                           // khong xac dinh duoc hinh thuc nap tien
                           $return['message'] = 'So dien thoai khong hop le, vui long lien he CSKH Esimo 01262500000';
                           $return['IsSuccess'] = false;
                       }
                   } else {
                       // khong ton tai tai khoan nap tien
                       $return['message'] = 'Tai khoan khong ton tai, vui long lien he CSKH Esimo 01262500000';
                       $return['IsSuccess'] = false;                       
                   }
               } else {// nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp 
                   $return['message'] = 'Noi dung tin nhan khong hop le. Vui long soan tin nhan theo noi dung ESIMO <Ten dang nhap>';
                   $return['IsSuccess'] = false;  
               }            
           } else { 
               //trường hợp nhắn sai đầu số 
               $return['message'] = 'Ban da nhap sai dau so';
               $return['IsSuccess'] = false;                 
           }
           return json_encode($return);            
        }   
        
        /**
        * @param string the message of the rechargesmsnganluong
        * @param string the phone of the rechargesmsnganluong
        * @param string the service of the rechargesmsnganluong
        * @param string the port of the rechargesmsnganluong
        * @param string the main of the rechargesmsnganluong
        * @return string the rechargesmsnganluong messagemtNganluong
        * @soap
        */        
        public function getMoNganluong($message,$phone,$service,$port,$main){                
           $return = array();            
           
           if ($port == '8533' || $port == '8633' || $port == '8733') {// kiểm tra xem có đúng đầu số không? 
               $moSmsLogModel = MoSmsLog::model()->find('request_id=:request_id', array(':request_id'=>$service));
               if(isset($moSmsLogModel)) return;
               $telco = $this->findTelcoSms($phone);           
               $message = strtoupper($message);
               $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
               if ($tmp[0] == 'CHV' && // phần thứ nhất chứa keyword 
                    @$tmp[1] == 'NAP' //phần thứ 2 chứa Prefix 
                ) {
                   switch ($port) {                        
                       case '8533': {
                               $amount = 5000;
                               $type = $this->getRechargeTypeOfSms($telco, '8533');    
                               break;
                           }
                       case '8633': {
                               $amount = 10000;
                               $type = $this->getRechargeTypeOfSms($telco, '8633');                             
                               break;
                           }
                       case '8733': {
                               $amount = 15000;
                               $type = $this->getRechargeTypeOfSms($telco, '8733');    
                               break;
                           }
                   }                     
                   // luu log sms
                   $mo = new MoSmsLog();
                   $mo->user_id = $phone;
                   $mo->service_id = $port;
                   $mo->command_code = $main;
                   $mo->message = $message;
                   $mo->request_id = $service;
                   $mo->save();

                   $criteria = new CDbCriteria;
                   $criteria->addCondition('username =:username');  
                   $criteria->params = array(':username'=>$tmp[2]);
                   $user = User::model()->find($criteria);                
                   if (isset($user)) {
                       if($type != 0){
                           $userGold = $user->rechargeMoney($amount, $type);
                           if ($userGold) {
                               /*
                               $userChip = $user->convertGoldByChip($userGold);
                               $electroServer = new ElectroServerBridge();
                               $result = $electroServer->sendUserInfo($user->id, 'chip', $userChip);
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Chieuvuong qua đầu số ".$port."!",1);
                               // tra ve khi thanh cong
                               $return['message'] = 'Ban da nap ' . $userChip . ' chip vao tai khoan ' . $user->username;
                               $return['IsSuccess'] = true;
                               */
                               $electroServer = new ElectroServerBridge();
                               $electroServer->sendUserInfo($user->id, 'gold', $userGold);                                                                                                     
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." gold từ hệ thống Chieuvuong qua đầu số ".$port."!",1);
                               $return['IsSuccess'] = true;
                               $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($userGold, 0, ',', '.') . ' Gold  vào tài khoản của bạn';                                                
                           } else {
                               // loi trong qua trinh xu ly phia esimo
                               $return['message'] = 'Co loi trong qua trinh xu ly, vui long lien he CSKH Chieuvuong 01698486249';
                               $return['IsSuccess'] = false;
                           }                        
                       }else{
                           // khong xac dinh duoc hinh thuc nap tien
                           $return['message'] = 'So dien thoai khong hop le, vui long lien he CSKH Chieuvuong 01698486249';
                           $return['IsSuccess'] = false;
                       }
                   } else {
                       // khong ton tai tai khoan nap tien
                       $return['message'] = 'Tai khoan khong ton tai, vui long lien he CSKH Chieuvuong 01698486249';
                       $return['IsSuccess'] = false;                       
                   }
               } else {// nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp 
                   $return['message'] = 'Noi dung tin nhan khong hop le. Vui long soan tin nhan theo noi dung ESIMO <Ten dang nhap>';
                   $return['IsSuccess'] = false;  
               }            
           } else { 
               //trường hợp nhắn sai đầu số 
               $return['message'] = 'Ban da nhap sai dau so';
               $return['IsSuccess'] = false;                 
           }
           return json_encode($return);            
        }           
        
        /**
        * @param string the message of the rechargesmsonepay
        * @param string the phone of the rechargesmsonepay
        * @param string the service of the rechargesmsonepay
        * @param string the port of the rechargesmsonepay
        * @param string the main of the rechargesmsonepay
        * @return string the rechargesmsonepay messagemtOnepay
        * @soap
        */        
        public function getMoOnepay($message,$phone,$service,$port,$main){                
           $return = array();            
           
           if ($port == '8598' || $port == '8698' || $port == '8798') {// kiểm tra xem có đúng đầu số không? 
               $moSmsLogModel = MoSmsLog::model()->find('request_id=:request_id', array(':request_id'=>$service));
               if(isset($moSmsLogModel)) return;
               $telco = $this->findTelcoSms($phone);           
               $message = strtoupper($message);
               $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
               if ($tmp[0] == 'DT') { // phần thứ nhất chứa keyword                     
                   switch ($port) {                        
                       case '8598': {
                               $amount = 5000;
                               $type = $this->getRechargeTypeOfSms($telco, '8598');    
                               break;
                           }
                       case '8698': {
                               $amount = 10000;
                               $type = $this->getRechargeTypeOfSms($telco, '8698');                             
                               break;
                           }
                       case '8798': {
                               $amount = 15000;
                               $type = $this->getRechargeTypeOfSms($telco, '8798');    
                               break;
                           }
                   }                     
                   // luu log sms
                   $mo = new MoSmsLog();
                   $mo->user_id = $phone;
                   $mo->service_id = $port;
                   $mo->command_code = $main;
                   $mo->message = $message;
                   $mo->request_id = $service;
                   $mo->save();

                   $criteria = new CDbCriteria;
                   $criteria->addCondition('username =:username');  
                   $criteria->params = array(':username'=>$tmp[1]);
                   $user = User::model()->find($criteria);                
                   if (isset($user)) {
                       if($type != 0){
                           $userGold = $user->rechargeMoney($amount, $type);
                           if ($userGold) {
                               /*
                               $userChip = $user->convertGoldByChip($userGold);
                               $electroServer = new ElectroServerBridge();
                               $result = $electroServer->sendUserInfo($user->id, 'chip', $userChip);
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Chieuvuong qua đầu số ".$port."!",1);
                               // tra ve khi thanh cong
                               $return['message'] = 'Ban da nap ' . $userChip . ' chip vao tai khoan ' . $user->username;
                               $return['IsSuccess'] = true;
                               */
                               $electroServer = new ElectroServerBridge();
                               $electroServer->sendUserInfo($user->id, 'gold', $userGold);                                                                                                     
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." gold từ hệ thống Game đấu trí qua đầu số ".$port."!",1);
                               $return['IsSuccess'] = true;
                               $return['message'] = 'Ban da nap ' . number_format($amount, 0, ',', '.') . ' VND' . ' tuong duong voi ' . number_format($userGold, 0, ',', '.') . ' Gold  vao tai khoan cua ban';                                                
                           } else {
                               // loi trong qua trinh xu ly phia esimo
                               $return['message'] = 'Co loi trong qua trinh xu ly, vui long lien he CSKH Gamedautri co bac 0972304369';
                               $return['IsSuccess'] = false;
                           }                        
                       }else{
                           // khong xac dinh duoc hinh thuc nap tien
                           $return['message'] = 'So dien thoai khong hop le, vui long lien he CSKH Gamedautri co bac 0972304369';
                           $return['IsSuccess'] = false;
                       }
                   } else {
                       // khong ton tai tai khoan nap tien
                       $return['message'] = 'Tai khoan khong ton tai, vui long lien he CSKH Gamedautri co bac 0972304369';
                       $return['IsSuccess'] = false;                       
                   }
               } else {// nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp 
                   $return['message'] = 'Noi dung tin nhan khong hop le. Vui long soan tin nhan theo noi dung DT <Ten dang nhap>';
                   $return['IsSuccess'] = false;  
               }            
           } else { 
               //trường hợp nhắn sai đầu số 
               $return['message'] = 'Ban da nhap sai dau so, vui long lien he CSKH Gamedautri co bac 0972304369';
               $return['IsSuccess'] = false;                 
           }
           return json_encode($return);            
        }
        
        public function actionSaveDeviceToken(){            
            $return = array();
            $deviceToken = Yii::app()->request->getParam('deviceToken');
            $platform = Yii::app()->request->getParam('platform');
            if(!$deviceToken || !$platform){
                $return['code'] = -2;
                $return['message'] = 'Thiếu dữ liệu';
                echo json_encode($return);die;
            }
            if($platform == "android_official")
                $platform = "android";
            if($platform == "ios_official")
                $platform = "ios";
            $deviceTokenModel = DeviceToken::model()->find('token = "'.$deviceToken.'"');
            if(isset($deviceTokenModel)){
                $return['code'] = -1;
                $return['message'] = "Đã tồn tại device token";
                echo json_encode($return);die;
            }
            $query = "call `save_device_token`('".$deviceToken."', '".$platform."')";
            $result = Yii::app()->db->createCommand($query)->execute();
            if($result){
                $return['code'] = 1;
                $return['message'] = 'Thành công';
            }else{
                $return['code'] = 0;
                $return['message'] = 'Không thành công';
            }
            echo json_encode($return);
        }
        
        public function actionGetEvent(){
            $return = array();
            $criteria = new CDbCriteria;
            $criteria->addCondition('category_id = 9');  
            $criteria->limit = 5;
            $criteria->order = "create_time DESC";
            $model = Post::model()->findAll($criteria);
            if(isset($model)){
                $return['code'] = 0;
                $return['message'] = 'Thành công';
                $return['items'] = array();
                foreach($model as $index => $item){
                    $return['items'][$index]['title'] = $item->title;
                    $return['items'][$index]['image'] = Yii::app()->createAbsoluteUrl($item->main_image);
                    $return['items'][$index]['url'] = Yii::app()->createAbsoluteUrl('bai-viet/'.$item->slug);
                }
            }else{
                $return['code'] = 1;
                $return['message'] = 'Không thành công';
            }
            echo json_encode($return);
        }
        
        public function actionGetPost(){
            //echo "Tính năng đang được phát triển";die;
            $criteria = new CDbCriteria;
            $criteria->addCondition('category_id = 9 AND status = 1');  
            $criteria->limit = 5;
            $criteria->order = "create_time DESC";
            $model = Post::model()->findAll($criteria);
            if(isset($model)){
                $this->render('event', array('model'=>$model));                
            }
        }
        
        public function actionPushNotification(){
            $return = array();
            $serverIP=$_SERVER['HTTP_X_REAL_IP'];
            if($serverIP != "210.211.102.97"){
                $return['code'] = -1;
                $return['message'] = "Ban khong co quyen truy cap";
            }else{                
                $request = $_REQUEST;
                $push = new PushNotificationLog();
                $push->pushNotificationByDevice($request);
                $return['code'] = 0;
                $return['message'] = "Thanh cong";
            }
            echo json_encode($return);
        }                             
        
        public function actionGetUserFromBlogGame(){            
            $return = array();
            $email = Yii::app()->request->getParam('email');
            $token = Yii::app()->request->getParam('token');
            $appId = Yii::app()->request->getParam('appId');
            if(!isset($email) || !isset($token) || !isset($appId)){
                $return['code'] = 3;
                $return['message'] = "Thieu du lieu nhap vao";                
                echo json_encode($return);die;
            }
            $user = User::model()->find('email=:email',array(':email'=>$email));
            if(!isset($user)){
                $return['code'] = 0;
                $return['message'] = "Tai khoan khong ton tai";                                
            }else{
                $curlHelper = new cURL();
                $url = 'http://bloggame.vn/mobile/tkcheck/';
                $data = array();           
                $data['email'] = $email;                                              
                $data['utoken'] = $token;                 
                //$data['gameid'] = 18;                
                $result = $curlHelper->post($url, $data);
                $obj = json_decode($result);
                if($obj->ecode == 1010){
                    $return['code'] = 1;
                    $return['message'] = 'Thanh cong';
                    $ip = $_SERVER['REMOTE_ADDR'];
                    $userId = $user->id;                    
                    $accessToken = UserAccessToken::model()->createAccessToken($userId, $appId, $ip);
                    $return['accessToken'] = $accessToken;
                }else{
                    $return['code'] = 2;
                    $return['message'] = $obj->msg;
                }
            }
            echo json_encode($return);
        }
                
        public function actionExchangeMoneyFromBlogGame(){
            $return = array();
            $email = Yii::app()->request->getParam('email');
            $gameid = Yii::app()->request->getParam('gameid');
            $transaction = Yii::app()->request->getParam('transaction');
            $money = Yii::app()->request->getParam('money');
            $bloggame_coin = Yii::app()->request->getParam('bloggame_coin');
            $token = Yii::app()->request->getParam('token');
            if(!isset($email) || !isset($gameid) || !isset($transaction) || !isset($money) || !isset($bloggame_coin)){
                $return['code'] = 3;
                $return['message'] = 'Thieu tham so';
                echo json_encode($return);die;
            }
            $curlHelper = new cURL();
            $url = 'http://bloggame.vn/mobile/ctransaction/';
            $data = array();           
            $data['email'] = $email;                                              
            $data['gameid'] = $gameid;  
            $data['transaction'] = $transaction;
            $data['money'] = $money;
            $data['bloggame_coin'] = $bloggame_coin;
            //$data['gameid'] = 18;                
            $result = $curlHelper->post($url, $data);
            $obj = json_decode($result);            
            if($obj->ecode == 1010){
                $criteria = new CDbCriteria;
                $criteria->addCondition('email =:email');  
                $criteria->params = array(':email'=>$email);
                $user = User::model()->find($criteria);                
                if (isset($user)) {
                    $type = $this->getRechargeTypeOfBlogGame($money);
                    $amount = $money;
                    $userGold = $user->rechargeMoney($amount, $type);
                    if ($userGold) {
                        //$userChip = $user->convertGoldByChip($userGold);
                        $electroServer = new ElectroServerBridge();
                        $electroServer->sendUserInfo($user->id, 'gold', $userGold);
                        UserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userGold, 0, ',', '.')." golds từ hệ thống Esimo qua BlogGame!",1);                           
                        $return['code'] = 1;
                        $return['message'] = 'Thanh cong';
                        $data2 = array();           
                        $data2['email'] = $email;                                                                      
                        $data2['utoken'] = $token;
                        //$data['gameid'] = 18;
                        $url2 = 'http://bloggame.vn/mobile/getmoney/';
                        $curlHelper2 = new cURL();
                        $result2 = $curlHelper2->post($url2, $data2);            
                        $obj2 = json_decode($result2);   
                        if($obj2->ecode == 1010){
                            $return['money'] = $obj2->money; 
                        }else{
                            $return['money'] = 0; 
                        }
                    } else {
                        $return['code'] = 5;
                        $return['message'] = 'Loi trong qua trinh xu ly';                                                                                              
                    }
                    
                }else{
                    $return['code'] = 4;
                    $return['message'] = 'User khong ton tai';                              
                }                
            }else{
                $return['code'] = 2;
                $return['message'] = $obj->msg;
            }
            echo json_encode($return);
        }
        
        public function getRechargeTypeOfBlogGame($amount){           
            $criteria = new CDbCriteria;
            $criteria->alias = 'recharge_type';            
            $criteria->join = " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
            $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
            $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
            $criteria->addCondition('recharge_method.code_identifier = "bloggame" AND topup_provider.code_identifier="bloggame" AND recharge_method_provider.code='.$amount);          
            $rechargeType = RechargeType::model()->find($criteria);
            if(isset($rechargeType))
                $type =  $rechargeType->id;        
            else 
                $type = 0;
            return $type;
        }
        
        public function actionGetInfoBlogGame(){
            $return = array();
            $email = Yii::app()->request->getParam('email');
            $token = Yii::app()->request->getParam('token');
            $criteria = new CDbCriteria;
            $criteria->alias = "recharge_type";
            $criteria->join = "INNER JOIN recharge_method_provider on recharge_method_provider.id = recharge_type.recharge_method_provider_id";
            $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
            $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
            $criteria->addCondition('recharge_method.code_identifier = "bloggame" AND topup_provider.code_identifier = "bloggame" AND recharge_method_provider.status = 1 ');            
            $criteria->group = "recharge_method_provider.id";
            $models = RechargeType::model()->findAll($criteria);
            if(!isset($models)){
                $return['code'] = 0;
                $return['message'] = "Không tồn tại dữ liệu";
                echo json_encode($return);die;
            }            
            $return['code'] = 1;
            $return['message'] = "Thành công";
            
            $curlHelper = new cURL();
            $url = 'http://bloggame.vn/mobile/getmoney/';
            $data = array();           
            $data['email'] = $email;                                                                      
            $data['utoken'] = $token;
            //$data['gameid'] = 18;                
            $result = $curlHelper->post($url, $data);            
            $obj = json_decode($result);   
            if($obj->ecode == 1010){
                $return['money'] = $obj->money; 
            }else{
                $return['money'] = 0; 
            }
            $return['items'] = array();  
            $ratio = Configuration::model()->getConfig('gold_by_chip');            
            foreach ($models as $index => $items) {
                $return['items'][$index]['type'] = $items->rechargeMethodProvider->rechargeMethod->code_identifier;
                $return['items'][$index]['code'] = $items->rechargeMethodProvider->name;
                $return['items'][$index]['code_value'] = $items->rechargeMethodProvider->code;
                $return['items'][$index]['provider'] = $items->rechargeMethodProvider->topupProvider->code_identifier;
                $return['items'][$index]['value'] = ceil($items->value * $items->ratio) * $ratio; 
                $return['items'][$index]['template'] = $items->template; 
                $return['items'][$index]['image'] = $items->image;                 
            }
            echo json_encode($return);
        }
        
        public function actionGetUser(){
            $return = array();
            $username = Yii::app()->request->getParam('username');
            $user = User::model()->find('username=:username', array(':username'=>$username));
            if(empty($user)){
                $return['code'] = 0;
                $return['message'] = 'User khong ton tai';
            }else{
                $return['code'] = 1;
                $return['message'] = 'Thanh cong';
                $session = new CHttpSession();
                $session->open();
                $session['user'] = $user;
            }
            echo json_encode($return);
        }  
        
        
                        
        public function actionRechargeAppotaCard(){
            $return = array();
            $username = Yii::app()->request->getParam('username');
            $soseri = Yii::app()->request->getParam('txtSoSeri');
            $sopin = Yii::app()->request->getParam('txtSoPin');
            $type_card = Yii::app()->request->getParam('select_method');                  

            if (!isset($username) || !isset($soseri) || !isset($sopin) || !isset($type_card)) {
                $return['code'] = -2;
                $return['message'] = "Bạn cần nhập đầu đủ thông tin";
                echo json_encode($return);
                exit();
            }
            $criteria = new CDbCriteria;
            $criteria->addCondition('username =:username');  
            $criteria->params = array(':username'=>$username);
            $user = User::model()->find($criteria);
            if (!isset($user)) {
                $return['code'] = -1;
                $return['message'] = "User không tồn tại";
                echo json_encode($return);
                exit();
            }
            
            //Tiến hành kết nối thanh toán Thẻ cào.
            if($type_card == 'VIETTEL') $nhamang = 'viettel';
            if($type_card == 'VNP') $nhamang = 'vinaphone';
            if($type_card == 'VMS') $nhamang = 'mobifone';
            if($type_card == 'VCOIN') $nhamang = 'vcoin';
            if($type_card == 'GATE') $nhamang = 'fpt';
            
            $curlHelper = new cURL();
            $url = 'http://nkduong.com/index.php/game/thecao/';
            $data = array();           
            $data['mathe'] = $sopin;                                              
            $data['seri'] = $soseri;     
            $data['nhamang'] = $nhamang;
            $data['username'] = $username;
            $data['email'] = 'thanhnguyendinh89@gmail.com';                           
            $result = $curlHelper->post($url, $data);            
            $obj = json_decode($result);              
            if ($obj->status == 'success') {
                // Ghi log
                $rechargeCardLog = new RechargeCardLog();
                $rechargeCardLog->pin = $sopin;
                $rechargeCardLog->seri = $soseri;
                $rechargeCardLog->card_type = $type_card;
                $rechargeCardLog->username = $user->username;
                $rechargeCardLog->value = $obj->money;
                $rechargeCardLog->save();            
                // Cập nhật data tại đây
                $amount = $obj->money;            
                switch ($type_card) {
                    case 'VIETTEL': {
                            $type = $this->getRechargeTypeOfAppota('viettel',$amount);
                            $view_type = 'Viettel';
                            break;
                        }
                    case 'VNP': {
                            $type = $this->getRechargeTypeOfAppota('vinaphone',$amount);
                            $view_type = 'Vinaphone';
                            break;
                        }
                    case 'VMS': {
                            $type = $this->getRechargeTypeOfAppota('mobifone',$amount);
                            $view_type = 'Mobifone';
                            break;
                        }
                    case 'VCOIN': {
                            $type = $this->getRechargeTypeOfAppota('vcoin',$amount);
                            $view_type = 'Vcoin';
                            break;
                        }
                    case 'GATE': {
                            $type = $this->getRechargeTypeOfAppota('gate',$amount);
                            $view_type = 'Gate';
                            break;
                        }
                }
                $userGameActionId = $user->userGameAction('recharge');
                $userGold = $user->rechargeMoney($amount, $type);
                if ($userGold) {
                    $userChip = $user->convertGoldByChip($userGold);
                    $userChipBonus = 0;
                    if($userGameActionId)
                        $userChipBonus = $user->bonusChips($userGameActionId,$userGold);

                    $totalChip = $userChip + $userChipBonus;
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'chip', $totalChip);     
                    
//                    $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                    $electroServer2->sendUserInfo($user->id, 'chip', $totalChip); 

                    MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($totalChip, 0, ',', '.')." chip từ hệ thống Esimo qua thẻ cào ".$view_type."!",1);
                    $return['code'] = 1;
                    $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($totalChip, 0, ',', '.') . ' Chip  vào tài khoản của bạn';
                } else {
                    $return['code'] = 0;
                    $return['message'] = 'Nạp tiền thất bại';
                }
            } else {
                $return['code'] = -3;
                $return['message'] = $obj->message;
            }
            echo json_encode($return);
        }      
        
         /**
        * @param string the message of the rechargesmsappota
        * @param string the phone of the rechargesmsappota
        * @param string the service of the rechargesmsappota
        * @param string the port of the rechargesmsappota
        * @param string the main of the rechargesmsappota
        * @param string the username of the rechargesmsappota
        * @return string the rechargesmsappota messagemt
        * @soap
        */        
        public function getMoAppota($message,$phone,$service,$port,$main,$username){      
           $return = array();            
           Common::includeAppotaSMS();
           $appota = new AppotaSMS();             
           $appota->CheckRequest();        //Hàm kiể m tra request, đảm bảo yêu cầu xử lý đến từ  server củ a Fibo                 
           // Hết lấy nội dung tin nhắn            
           
           if ($port == '8588' || $port == '8688' || $port == '8788') {// kiểm tra xem có đúng đầu số không? 
               $telco = $this->findTelcoSms($phone);           
               $message = strtoupper($message);
               $tmp = explode(" ", $message); //cắt nội dung tin ra làm 3 phần 
               if ($tmp[0] == 'OTA' // phần thứ nhất chứa keyword                       
               ) {
                   switch ($port) {                        
                       case '8588': {
                               $amount = 5000;
                               $type = $this->getRechargeTypeOfSms($telco, '8588');    
                               break;
                           }
                       case '8688': {
                               $amount = 10000;
                               $type = $this->getRechargeTypeOfSms($telco, '8688');                             
                               break;
                           }
                       case '8788': {
                               $amount = 15000;
                               $type = $this->getRechargeTypeOfSms($telco, '8788');    
                               break;
                           }
                   }                     
                   // luu log sms
                   $mo = new MoSmsLog();
                   $mo->user_id = $phone;
                   $mo->service_id = $port;
                   $mo->command_code = $main;
                   $mo->message = $message;
                   $mo->request_id = $service;
                   $mo->save();

                   $criteria = new CDbCriteria;
                   $criteria->addCondition('username =:username');  
                   $criteria->params = array(':username'=>$username);
                   $user = User::model()->find($criteria);                
                   if (isset($user)) {
                       if($type != 0){
                           $userGold = $user->rechargeMoney($amount, $type);
                           if ($userGold) {
                               $userChip = $user->convertGoldByChip($userGold);
                               $electroServer = new ElectroServerBridge();
                               $result = $electroServer->sendUserInfo($user->id, 'chip', $userChip);
                               MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($userChip, 0, ',', '.')." chip từ hệ thống Esimo qua đầu số ".$port."!",1);
                               
//                            $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                            $electroServer2->sendUserInfo($user->id, 'chip', $userChip); 
                               // tra ve khi thanh cong
                               $return['message'] = 'Ban da nap ' . $userChip . ' chip vao tai khoan ' . $user->username;
                               $return['IsSuccess'] = true;
                           } else {
                               // loi trong qua trinh xu ly phia esimo
                               $return['message'] = 'Co loi trong qua trinh xu ly, vui long lien he CSKH Esimo 01262500000';
                               $return['IsSuccess'] = false;
                           }                        
                       }else{
                           // khong xac dinh duoc hinh thuc nap tien
                           $return['message'] = 'So dien thoai khong hop le, vui long lien he CSKH Esimo 01262500000';
                           $return['IsSuccess'] = false;
                       }
                   } else {
                       // khong ton tai tai khoan nap tien
                       $return['message'] = 'Tai khoan khong ton tai, vui long lien he CSKH Esimo 01262500000';
                       $return['IsSuccess'] = false;                       
                   }
               } else {// nếu sai thi trả về hướng dẫn nhắn lại cho đúng cú pháp 
                   $return['message'] = 'Noi dung tin nhan khong hop le. Vui long soan tin nhan theo noi dung ESIMO <Ten dang nhap>';
                   $return['IsSuccess'] = false;  
               }            
           } else { 
               //trường hợp nhắn sai đầu số 
               $return['message'] = 'Ban da nhap sai dau so';
               $return['IsSuccess'] = false;                 
           }
           return json_encode($return);            
        }  
        
        public function getRechargeTypeOfAppota($code, $amount){           
            $criteria = new CDbCriteria;
            $criteria->alias = 'recharge_type';
            $criteria->join = "INNER JOIN telco on telco.id = recharge_type.telco_id";
            $criteria->join .= " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
            $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
            $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
            $criteria->addCondition('telco.code_identifier="'.$code.'" AND recharge_method.code_identifier = "mobile_card" AND topup_provider.code_identifier="appota" AND recharge_method_provider.code='.$amount);          
            $rechargeType = RechargeType::model()->find($criteria);
            if(isset($rechargeType))
                $type =  $rechargeType->id;        
            else 
                $type = 0;
            return $type;
        }
        
        public function actionUpdateInfoRechargeAppota(){
            $return = array();
            $username = Yii::app()->request->getParam('username');
            $user = User::model()->find('username =:username', array(':username'=>$username));
            if(!isset($user)){
                $return['code'] = -1;
                $return['message'] = 'Khong ton tai username';
                echo json_encode($return);die;
            }
            $curlHelper = new cURL();
            $url = 'http://nkduong.com/index.php/game/getSMS/'.$username;            
            $data = array();                                                           
            $result = $curlHelper->post($url, $data);            
            $obj = json_decode($result);     
            if($obj->status == true){
                $return['code'] = 1;
                $return['message'] = 'thanh cong';      
                $return['items'] = array();
                foreach($obj->data->options as $index => $item){                    
                    $return['items'][$index]['sms'] = $item->sms;
                    $return['items'][$index]['send'] = $item->send;
                    $return['items'][$index]['amount'] = $item->amount;
                    $return['items'][$index]['currency'] = $item->currency;
                }
            }else{
                $return['code'] = 0;
                $return['message'] = 'that bai';
            }
            echo json_encode($return);
        }   
        
        public function actionRechargeSMSAppotaEsimo(){            
            $phone = Yii::app()->request->getParam('phone');
            $message = Yii::app()->request->getParam('message');
            $port = Yii::app()->request->getParam('port');
            echo $message;
        }
        
        public function actionRechargeCardAppotaEsimo(){
            $return = array();
            $username = Yii::app()->request->getParam('username');
            $soseri = Yii::app()->request->getParam('txtSoSeri');
            $sopin = Yii::app()->request->getParam('txtSoPin');
            $type_card = Yii::app()->request->getParam('select_method');                  

            if (!isset($username) || !isset($soseri) || !isset($sopin) || !isset($type_card)) {
                $return['code'] = -2;
                $return['message'] = "Bạn cần nhập đầu đủ thông tin";
                echo json_encode($return);
                exit();
            }
            $criteria = new CDbCriteria;
            $criteria->addCondition('username =:username');  
            $criteria->params = array(':username'=>$username);
            $user = User::model()->find($criteria);
            if (!isset($user)) {
                $return['code'] = -1;
                $return['message'] = "User không tồn tại";
                echo json_encode($return);
                exit();
            }
            
            //Tiến hành kết nối thanh toán Thẻ cào.
            if($type_card == 'VIETTEL') $nhamang = 'viettel';
            if($type_card == 'VNP') $nhamang = 'vinaphone';
            if($type_card == 'VMS') $nhamang = 'mobifone';
            if($type_card == 'VCOIN') $nhamang = 'vcoin';
            if($type_card == 'GATE') $nhamang = 'fpt';
            
            $curlHelper = new cURL();
            $url = 'https://api.appota.com/payment/inapp_card?api_key=XXX&lang=LANG';
            $data = array();           
            $data['card_code'] = $sopin;                                              
            $data['card_serial'] = $soseri;     
            $data['vendor'] = $nhamang;            
            $data['direct'] = 1;                           
            $result = $curlHelper->post($url, $data);            
            $obj = json_decode($result);              
            if ($obj->status == true && $obj->error_code == 0) {
                // Ghi log
                $rechargeCardLog = new RechargeCardLog();
                $rechargeCardLog->pin = $sopin;
                $rechargeCardLog->seri = $soseri;
                $rechargeCardLog->card_type = $type_card;
                $rechargeCardLog->username = $user->username;
                $rechargeCardLog->value = $obj->money;                
                //$rechargeCardLog->transaction_id = $obj->data->transaction_id;
                
                $rechargeCardLog->save();            
                // Cập nhật data tại đây
                $amount = $obj->data->amount;            
                switch ($type_card) {
                    case 'VIETTEL': {
                            $type = $this->getRechargeTypeOfAppotaEsimo('viettel',$amount);
                            $view_type = 'Viettel';
                            break;
                        }
                    case 'VNP': {
                            $type = $this->getRechargeTypeOfAppotaEsimo('vinaphone',$amount);
                            $view_type = 'Vinaphone';
                            break;
                        }
                    case 'VMS': {
                            $type = $this->getRechargeTypeOfAppotaEsimo('mobifone',$amount);
                            $view_type = 'Mobifone';
                            break;
                        }
                    case 'VCOIN': {
                            $type = $this->getRechargeTypeOfAppotaEsimo('vcoin',$amount);
                            $view_type = 'Vcoin';
                            break;
                        }
                    case 'GATE': {
                            $type = $this->getRechargeTypeOfAppotaEsimo('gate',$amount);
                            $view_type = 'Gate';
                            break;
                        }
                }
                $userGameActionId = $user->userGameAction('recharge');
                $userGold = $user->rechargeMoney($amount, $type);
                if ($userGold) {
                    $userChip = $user->convertGoldByChip($userGold);
                    $userChipBonus = 0;
                    if($userGameActionId)
                        $userChipBonus = $user->bonusChips($userGameActionId,$userGold);

                    $totalChip = $userChip + $userChipBonus;
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendUserInfo($user->id, 'chip', $totalChip);     
                    
//                    $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                    $electroServer2->sendUserInfo($user->id, 'chip', $totalChip); 

                    MUserMessage::model()->sendUserMessage($user->id,"Bạn vừa nhận được ".number_format($totalChip, 0, ',', '.')." chip từ hệ thống Esimo qua thẻ cào ".$view_type."!",1);
                    $return['code'] = 1;
                    $return['message'] = 'Bạn đã nạp ' . number_format($amount, 0, ',', '.') . ' VNĐ' . ' tương đương với ' . number_format($totalChip, 0, ',', '.') . ' Chip  vào tài khoản của bạn';
                } else {
                    $return['code'] = 0;
                    $return['message'] = 'Nạp tiền thất bại';
                }
            } else {
                $return['code'] = -3;
                $return['message'] = $obj->message;
            }
            echo json_encode($return);
        }
        
        public function getRechargeTypeOfAppotaEsimo($code, $amount){           
            $criteria = new CDbCriteria;
            $criteria->alias = 'recharge_type';
            $criteria->join = "INNER JOIN telco on telco.id = recharge_type.telco_id";
            $criteria->join .= " INNER JOIN recharge_method_provider on recharge_method_provider.id=recharge_type.recharge_method_provider_id";
            $criteria->join .= " INNER JOIN recharge_method on recharge_method.id = recharge_method_provider.recharge_method_id";
            $criteria->join .= " INNER JOIN topup_provider on topup_provider.id = recharge_method_provider.topup_provider_id";
            $criteria->addCondition('telco.code_identifier="'.$code.'" AND recharge_method.code_identifier = "mobile_card" AND topup_provider.code_identifier="appota_esimo" AND recharge_method_provider.code='.$amount);          
            $rechargeType = RechargeType::model()->find($criteria);
            if(isset($rechargeType))
                $type =  $rechargeType->id;        
            else 
                $type = 0;
            return $type;
        }
        
        public function actionGetInfoRechargeAppotaEsimo(){
            $return = array();
            $username = Yii::app()->request->getParam('username');
            $user = User::model()->find('username =:username', array(':username'=>$username));
            if(!isset($user)){
                $return['code'] = -1;
                $return['message'] = 'Khong ton tai username';
                echo json_encode($return);die;
            }
            $curlHelper = new cURL();
            $url = 'https://api.appota.com/payment/inapp_sms?api_key=XXX&lang=LANG'.$username;            
            $data = array();                                                           
            $result = $curlHelper->post($url, $data);            
            $obj = json_decode($result);     
            if($obj->status == true){
                $return['code'] = 1;
                $return['message'] = 'thanh cong';      
                $return['items'] = array();
                foreach($obj->data->options as $index => $item){                    
                    $return['items'][$index]['sms'] = $item->sms;
                    $return['items'][$index]['send'] = $item->send;
                    $return['items'][$index]['amount'] = $item->amount;
                    $return['items'][$index]['currency'] = $item->currency;
                }
            }else{
                $return['code'] = 0;
                $return['message'] = 'that bai';
            }
            echo json_encode($return);
        }
        
        // xu ly gian hang, qua tang
        public function actionShop(){            
            $username = Yii::app()->request->getParam('username');
            $accessToken = Yii::app()->request->getParam('accessToken');                
            
            $userAccessToken = UserAccessToken::model()->find('access_token =:token',array(':token'=>$accessToken));
            if(!isset($userAccessToken)){
                echo "Bạn không có quyền truy cập";die;
            }
                
            $user = User::model()->findByPk($userAccessToken->user_id);            
            if(!isset($user)){               
                echo "Chức năng đang được phát triển";die;
            }
            
            $categoryProduct = CategoryProduct::model()->findAll();
            $arrCategory = array();
            foreach($categoryProduct as $index=> $category){
                if($category->parent_id == 0){
                     $arrCategory[$category->id] = array();   
                     $arrCategory[$category->id]['info'] = $category;
                }else{                    
                    if(!isset($arrCategory[$category->parent_id])){
                        $arrCategory[$category->parent_id] = array();                                            
                    }
                    
                    if(!isset($arrCategory[$category->parent_id]['childrens'])){
                        $arrCategory[$category->parent_id]['childrens'] = array();    
                    }
                    
                    array_push($arrCategory[$category->parent_id]['childrens'], $category);                        
                }                    
            } 
            
            $arrProduct = array();            
            $criteria = new CDbCriteria();
            $criteria->alias = 'product';
            $criteria->order = 'product.order ASC';
            $criteria->addCondition('product.status = 1');
            $products = Product::model()->findAll($criteria);
            foreach($products as $p){
                if(!isset($arrProduct[$p->category_product_id]))
                    $arrProduct[$p->category_product_id] = array();
                array_push($arrProduct[$p->category_product_id], $p);
            }                        
            
            $this->render('shop', array('categoryProduct'=>$arrCategory,'product'=>$arrProduct, 'user'=>$user, 'accessToken'=>$accessToken));
        }     
        
        public function actionCheckout(){
            $return = array();
            $product_id = Yii::app()->request->getParam('product_id');
            $qty = Yii::app()->request->getParam('qty');
            $phone = Yii::app()->request->getParam('phone');
            $email = Yii::app()->request->getParam('email');
            $address = Yii::app()->request->getParam('address');
            $comment = Yii::app()->request->getParam('comment');
            $accessToken = Yii::app()->request->getParam('accessToken'); 
            if (!isset($accessToken) || !UserAccessToken::model()->checkAccessToken($accessToken)) {
                echo "Bạn không có quyền truy cập";die;
            }
            $username = Yii::app()->request->getParam('username');
            
            $product = Product::model()->findByPk($product_id);
            if(!isset($product) || $qty < 1 || !is_numeric($qty) || ($product->status != 1))
                $this->redirect(Yii::app()->createUrl('api/shop'));
            
            if($qty > $product->quatity){
                $return['code'] = 2;
                $return['message'] = 'Số lượng bạn chọn vượt quá số lượng sản phẩm đang có !';
                echo json_encode($return);die;
            } 
            
             if($qty < 1){
                $return['code'] = 11;
                $return['message'] = 'Số lượng bạn chọn phải lớn hơn 1 !';
                echo json_encode($return);die;
            } 
            
            if(empty($phone)){
                $return['code'] = 7;
                $return['message'] = 'Bạn cần nhập số điện thoại';
                echo json_encode($return);die;
            }                        
            
//            if(empty($email)){
//                $return['code'] = 8;
//                $return['message'] = 'Bạn cần nhập địa chỉ email';
//                echo json_encode($return);die;
//            }
            
            if(!is_numeric($phone)){
                $return['code'] = 9;
                $return['message'] = 'Số điện thoại của bạn không hợp lệ';
                echo json_encode($return);die;
            }
            
            if($product->categoryProduct->code_identifier == 'doi_chip'){
                $return['code'] = 10;
                $return['message'] = 'Sản phẩm thanh toán của bạn không hợp lệ';
                echo json_encode($return);die;
            }
                        
            if($username){                
                $userModel = User::model()->find('username =:username AND active=:active', array(':username'=>$username, ':active'=>0));
                if(isset($userModel)){
                    if($product->gold > 0){
                        if($userModel->gold > $product->gold){
                            $userModel->gold -= $product->gold;                                 
                        }else{
                            $return['code'] = 3;
                            $return['message'] = 'Số gold của bạn không đủ';
                            echo json_encode($return);die;
                        }                    
                    }                        
                    if($product->chip > 0){
                        if($userModel->chip > $product->chip){
                            $userModel->chip -= $product->chip;                           
                        }else{
                            $return['code'] = 4;
                            $return['message'] = 'Số chip của bạn không đủ';
                            echo json_encode($return);die;
                        }  
                    }
                    
                    $order = new Order();
                    $order->user_id = $userModel->id;
                    $order->product_id = $product->id;
                    $order->quatity = $qty;
                    $order->gold = $product->gold;
                    $order->chip = $product->chip;
                    $order->email = $email;
                    $order->phone = $phone;
                    $order->address = $address;
                    $order->comment = $comment;
                    $order->status = -1;
                    $order->order_status = 1;                    
                    
                    if($order->save()){                    
                        if($userModel->save()){
                            $product->quatity -= $qty;
                            $product->save();
                            $order->status = 1;
                            $order->order_code = "ESIMO-".$order->id;
                            $order->save();                                                                                  
                            
                            //call electroserver                            
                            $electroServer = new ElectroServerBridge();
                            if($product->chip > 0){    
                                $transaction = new Transaction();  
                                $transaction->type = 14;
                                $transaction->value = $product->chip;
                                $transaction->user_id = $userModel->id;
                                $transaction->save();
                                $returnElectroChip = $electroServer->sendUserInfo($userModel->id, 'chip', - ($product->chip));
                            }
                            if($product->gold > 0){
                                $transaction = new Transaction();  
                                $transaction->type = 15;
                                $transaction->value = $product->gold;
                                $transaction->user_id = $userModel->id;
                                $transaction->save();
                                $returnElectroGold = $electroServer->sendUserInfo($userModel->id, 'gold', - ($product->gold));
                            }
                            
//                           $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                           $electroServer2->sendUserInfo($user->id, 'chip', $userChip);                             
                            
                            MUserMessage::model()->sendUserMessage($userModel->id,"Bạn đã mua sản phẩm thành công tại gian hàng của Esimo! Chăm sóc khách hàng của chúng tôi sẽ liên hệ với bạn trong vòng 24h. Cảm ơn bạn.",0);                                                       
                            
                            $return['code'] = 0;
                            $return['message'] = 'Bạn đã mua sản phẩm thành công tại gian hàng của Esimo! Chăm sóc khách hàng của chúng tôi sẽ liên hệ với bạn trong vòng 24h. Cảm ơn bạn.';                           
                            echo json_encode($return);
                        }else{
                            $order->status = -1;
                            $order->save();
                            $return['code'] = 5;
                            $return['message'] = 'Có lỗi trong quá trình xử lý user';
                            echo json_encode($return);die;
                        }                        
                    }else{                                            
                        $return['code'] = 6;
                        $return['message'] = 'Có lỗi trong quá trình xử lý đơn hàng';
                        echo json_encode($return);die;
                    }
                }else{
                    $this->redirect(Yii::app()->createUrl('api/shop'));
                }                                    
            }else{
                $this->redirect(Yii::app()->createUrl('api/shop'));
            }
        }
        
        public function actionCheckoutGoldToChip(){
            $return = array();
            $product_id = Yii::app()->request->getParam('product_id');
            $accessToken = Yii::app()->request->getParam('accessToken'); 
            if (!isset($accessToken) || !UserAccessToken::model()->checkAccessToken($accessToken)) {
                echo "Bạn không có quyền truy cập";die;
            }
            $username = Yii::app()->request->getParam('username');            
            $product = Product::model()->findByPk($product_id);
            if(!isset($product) || ($product->status != 1))
                $this->redirect(Yii::app()->createUrl('api/shop'));
            
            if($product->categoryProduct->code_identifier != 'doi_chip'){     
                $return['code'] = 2;
                $return['message'] = 'Sản phẩm thanh toán của bạn không hợp lệ';
                echo json_encode($return);die;
            }
            if($username){                
                $userModel = User::model()->find('username =:username AND active=:active', array(':username'=>$username, ':active'=>0));
                if(isset($userModel)){
                    if($product->gold > 0){
                        if($userModel->gold < $product->gold){                            
                            $return['code'] = 3;
                            $return['message'] = 'Số gold của bạn không đủ';
                            echo json_encode($return);die;
                        }                    
                    }                       
                    
                    $electroServer = new ElectroServerBridge();
                    $returnElectroGold = $electroServer->sendUserInfo($userModel->id, 'gold', -($product->gold));
                    $chipReceiver = $userModel->convertGoldByChip($product->gold); 
                    $returnElectro = $electroServer->sendUserInfo($userModel->id, 'chip', $chipReceiver);
                    MUserMessage::model()->sendUserMessage($userModel->id,"Bạn đã mua thành công ".number_format($chipReceiver, 0, ',', '.')." chip từ  Esimo!",0);                                                    
                    $return['code'] = 0;
                    $return['message'] = 'Bạn đã mua gói chip thành công';                           
                    echo json_encode($return);
                    
                }
            }else{
                $return['code'] = 1;
                $return['message'] = 'User không tồn tại';                           
                echo json_encode($return);
            }
        }
        
        public function actionTransferMoney(){                             
            $escrow_timeout = Yii::app()->request->getParam('escrow_timeout');
            $escrow_timeout = isset($escrow_timeout) ? $escrow_timeout : 0;
            $token = Yii::app()->request->getParam('token');
                        
            $uri = 'http://kiemthu.baokim.vn';
            $url = '/transactions/rest/transfer_api/merchant_create';
            $dataPost = array();           
            //$dataPost['to_account_id'] = '111';                                              
            $dataPost['email'] = 'dev.baokim@bk.vn';     
            $dataPost['phone_no'] = '84934571589';            
            $dataPost['amount'] = 1000; //số tiền chuyển
            $dataPost['transaction_mode_id'] = 2; //chế độ chuyển(1: trực tiếp, 2: an toàn), default: 1
            $dataPost['escrow_timeout'] = $escrow_timeout; // số ngày tạm giữ
            $dataPost['description'] = 'co gi hot'; // nội dung chuyển tiền
            $dataPost['fee_payer'] = 1; // chỉ định tài khoản chịu phí bảo kim (1: tk ghi nợ, 2: tk ghi có), default: 1
            $dataPost['token'] = $token;
            $dataGet = array();
            
            $method = 'POST';            
            $privateKey = $_SERVER['DOCUMENT_ROOT'].'/protected/components/baokim.pem';            
            $signature = Common::makeBaoKimAPISignature($method,$url,$dataGet,$dataPost,$privateKey);            
            
            $url_signature = $uri.$url.'?signature='.$signature;
            $curl = curl_init($url_signature);
            $username = 'merchant';
            $password = '1234';
            curl_setopt_array($curl, array(
                    CURLOPT_POST=>true,
                    CURLOPT_HEADER=>false,
                    CURLINFO_HEADER_OUT=>true,
                    CURLOPT_TIMEOUT=>50,
                    CURLOPT_RETURNTRANSFER=>true,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTPAUTH=>CURLAUTH_DIGEST|CURLAUTH_BASIC,
                    CURLOPT_USERPWD=>$username.':'.$password,
                    CURLOPT_POSTFIELDS=>$dataPost
            ));
            $data = curl_exec($curl);	
            $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $error = curl_error($curl);
            $result = json_decode($data,true);
            Common::dump($result);die;
            $obj = json_decode($result);
        }
}
