<?php
Yii::app()->theme = Common::getTheme();

class DownloadController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function init() {
        $cs = Yii::app()->clientScript;
        $cs->registerScriptFile('/js/jRating.jquery.js');
        $cs->registerCssFile('/css/jRating.jquery.css');
    }

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {        
        $this->redirect(Yii::app()->createUrl('download/detail?id=7'));
        // renders the view file 'protected/views/game/index.php'
        // using the default layout 'protected/views/layouts/main.php'                  
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game Dautri, Tải game đấu trí miễn phí về di động.', 'Description');
        Yii::app()->clientScript->registerMetaTag('phom, tien len mien nam, chan, game, Gamedautri', 'Keywords');		
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game dau tri, Tải game Đấu trí miễn phí về di động.',null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('images/Logo_v.jpg')),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game dautri, Tải game Dautri miễn phí về di động.',null,null,array('property'=>'og:description'));               
        $dataGameFeature = new CActiveDataProvider('Game', array(
                    'criteria' => array(
                        'condition' => 'status = 1', //.Yii::app()->user->getUser()->id,                        				                                                                
                    ),
                ));        
        $this->render('index', array(
            'dataGameFeature' => $dataGameFeature,            
        ));
    }

    public function actionDetail() {
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game Gamedautri, Tải game Gamedautri miễn phí về di động.', 'Description');
        Yii::app()->clientScript->registerMetaTag('phom, tien len mien nam, chan, game, dautri', 'Keywords');		
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game Gamedautri, Tải game Gamedautri miễn phí về di động.',null,null,array('property'=>'og:title'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('images/Logo_v.jpg')),null,null,array('property'=>'og:image'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag('Gamedautri, game Gamedautri, Tải game Gamedautri miễn phí về di động.',null,null,array('property'=>'og:description'));               
        
        $id = Yii::app()->request->getParam('id', NULL);        
        if(!isset($id) || !is_numeric($id))
            $this->redirect(Yii::app()->homeUrl);
        $model = Game::model()->findByPk($id);
        if(!isset($model))
            $this->redirect(Yii::app()->homeUrl);        
        
        $gameVersionModel = $model->getCurrentVersionDetail();            
        
        if (empty($gameVersionModel)) {
            Common::alert('Game chưa sẵn sàng,vui lòng quay lại sau!');
        }                        
        
        $this->render('detail', array('model' => $model, 'gameVersionModel' => $gameVersionModel));
        
    }

    public function actionVote() {
        $aResponse['error'] = false;
        $aResponse['message'] = '';

        // ONLY FOR THE DEMO, YOU CAN REMOVE THIS VAR
        $aResponse['server'] = '';
        // END ONLY FOR DEMO


        if (isset($_POST['action'])) {
            if (htmlentities($_POST['action'], ENT_QUOTES, 'UTF-8') == 'rating') {
                /*
                 * vars
                 */
                $id = intval($_POST['idBox']);
                $rate = floatval($_POST['rate']);

                // YOUR MYSQL REQUEST HERE or other thing :)
                /*
                 *
                 */

                // if request successful
                $success = true;
                // else $success = false;
                // json datas send to the js file
                if ($success) {
                    $aResponse['message'] = 'Your rate has been successfuly recorded. Thanks for your rate :)';

                    // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
                    $aResponse['server'] = '<strong>Success answer :</strong> Success : Your rate has been recorded. Thanks for your rate :)<br />';
                    $aResponse['server'] .= '<strong>Rate received :</strong> ' . $rate . '<br />';
                    $aResponse['server'] .= '<strong>ID to update :</strong> ' . $id;
                    // END ONLY FOR DEMO

                    echo json_encode($aResponse);
                } else {
                    $aResponse['error'] = true;
                    $aResponse['message'] = 'An error occured during the request. Please retry';

                    // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
                    $aResponse['server'] = '<strong>ERROR :</strong> Your error if the request crash !';
                    // END ONLY FOR DEMO


                    echo json_encode($aResponse);
                }
            } else {
                $aResponse['error'] = true;
                $aResponse['message'] = '"action" post data not equal to \'rating\'';

                // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
                $aResponse['server'] = '<strong>ERROR :</strong> "action" post data not equal to \'rating\'';
                // END ONLY FOR DEMO


                echo json_encode($aResponse);
            }
        } else {
            $aResponse['error'] = true;
            $aResponse['message'] = '$_POST[\'action\'] not found';

            // ONLY FOR THE DEMO, YOU CAN REMOVE THE CODE UNDER
            $aResponse['server'] = '<strong>ERROR :</strong> $_POST[\'action\'] not found';
            // END ONLY FOR DEMO


            echo json_encode($aResponse);
        }
//        $point = Yii::app()->request->getParam('point', NULL);
//        $gameId = Yii::app()->request->getParam('id', NULL);
//        if ($point == NULL || $gameId == NULL)
//            echo '0';
//        else {
//            $game = Game::model()->findByPk($gameId);
//            $vote = $game->vote($point);
//            if ($vote)
//                echo $vote;
//            else
//                echo '0';
//        }
    }

    //Download file from database
    public function actionDownloadFile() {
        $id = Yii::app()->request->getParam('file', NULL);
        $return = array();
        //Nếu ko có biến truyền vào
        if ($id == NULL || $id <= 0) {
            $return['code'] = -1;
            $return['message'] = 'Error input variable';
        } else {
            $file = file::model()->findByPk($id);
            if ($file == NULL)
                $return = array(
                    'code' => 0,
                    'message' => 'File does not exist'
                );
            else if (!$file->checkDownload())
                $return = array(
                    'code' => -2,
                    'message' => 'Can not download file, please check permission'
                );
            else {
                $file->download();
                echo '1';
                return;
            }
        }
        echo json_encode($return);
    }

    //Get image to display 
    public function actionGetImageFile() {
        $id = Yii::app()->request->getParam('file', NULL);
        if ($id != NULL)
            $file = File::model()->findByPk($id);
        else
            return;
        if (!$file->getFile('image'))
            return;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionDownloadInServer() {
        $returnValue = array();
        $type = Yii::app()->request->getParam('type', NULL);
        $downloadFile = Yii::app()->request->getParam('downloadFile');
        if ($type == 'ajax') {
            $gameVersionModel = GameVersion::model()->findByPk($downloadFile);
            //$model = GameVersionPartner::model()->find('game_version_id='.$downloadFile.' AND partner_id = 1');
            if ($gameVersionModel != NULL) {
                $oldCountDownload = $gameVersionModel->download_times;
                $gameVersionModel->download_times = $oldCountDownload + 1;
                if ($gameVersionModel->save()) {
                    $file = $_SERVER["DOCUMENT_ROOT"].'/'.$gameVersionModel->file_path;
                    if(!file_exists($file))
                        Common::alert('File không tồn tại', Yii::app()->createUrl('download/detail?id=7'));
                    $quoted = sprintf('"%s"', addcslashes(basename($file), '"\\'));
                    $size   = filesize($file);                    
                    
                    header('Content-Description: File Transfer');
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename=gamedautri-'.$gameVersionModel->getCurrentVersion().'.'.pathinfo($file, PATHINFO_EXTENSION)); 
                    header('Content-Transfer-Encoding: binary');
                    header('Connection: Keep-Alive');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . $size);
                    readfile($file);
                    exit();
                    //$this->redirect(Yii::app()->createUrl('download/detail?id=7'));
                    $returnValue['code'] = 1;
                    //$returnValue['redirect'] = '';
                    //$returnValue['redirect'] = Yii::app()->createAbsoluteUrl($gameVersionModel->file_path);
                } else {                    
                    $returnValue['code'] = -1;
                }
            } else {
                $returnValue['code'] = -2;
            }
        }
        echo json_encode($returnValue);
    }     
    
    public function actionGame(){
        if(Yii::app()->theme->name != 'mobile')
            $this->redirect (Yii::app()->homeUrl);
        $id = Yii::app()->request->getParam('id');
        if(!isset($id) || !is_numeric($id)){
            $this->redirect(Yii::app()->homeUrl);  
        }
        $model = Game::model()->findByPk($id); 
        if(!isset($model))
            $this->redirect(Yii::app()->homeUrl);          
        if(Common::isRunningiOS()){        
            if($model->getCurrentVersion('ios')){                     
                $this->downloadLog('ios');
                $this->redirect($model->getCurrentVersion('ios')->gamePlatform->market_url);
            }
        }
        elseif(Common::isRunningAndroidOS()){                
            if($model->getCurrentVersion('android')){                
                $this->downloadLog('android');
                $this->redirect("market://details?id=".$model->getCurrentVersion('android')->gamePlatform->bundle_id);
            }
        }else{                
            $this->redirect (Yii::app()->homeUrl);  
        }                              
    }
    
    public function downloadLog($platform){ 
        $downloadLog = DownloadLog::model()->find('create_time =:create_time AND platform =:platform',array(':create_time'=>date('Y-m-d'), ':platform'=>$platform));
        if(isset($downloadLog)){
            $count = $downloadLog->count + 1;
            $downloadLog->count = $count;
            $downloadLog->save();
        }else{
            $downloadLog = new DownloadLog();
            $downloadLog->platform = $platform;
            $downloadLog->count = 1;
            $downloadLog->create_time = date('Y-m-d');
            $downloadLog->save();
        }
    }

}