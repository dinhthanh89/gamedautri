<?php
Yii::app()->theme = Common::getTheme();

class PlayController extends Controller {

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public $layout = 'main';
    
    public function actionIndex() {              
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php' 
        $this->seoPage('Game Đấu Trí');
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('play')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('files/games/gameLogo7.png')),null,null,array('property'=>'og:image'));
        $gameModel = Game::model()->find('code_identifier="esimo"');        
        if(!isset($gameModel))
            $this->redirect(Yii::app()->homeUrl);
        $gameVersion = $gameModel->getCurrentVersion('web');             
        if(!$gameVersion)
            $this->redirect(Yii::app()->homeUrl);
                
        if(Common::isRunningMobile())
        {                        
            if(Common::isRunningiOS())
                $this->redirect($gameModel->getCurrentVersion('ios')->gamePlatform->market_url);
            elseif(Common::isRunningAndroidOS()){                
                $this->redirect($gameModel->getCurrentVersion('android')->gamePlatform->market_url);                        
            }else{
                echo "Thiết bị của bạn không được hỗ trợ";die;
            }           
        }    
        $this->render('index', array('gameVersion'=>$gameVersion));
    }
    
    public function actionPhom(){
        $this->seoPage('Phỏm');       
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('play/phom')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('files/games/gameLogo1.png')),null,null,array('property'=>'og:image'));        
        $gameModel = Game::model()->find('code_identifier="phom"');
        if(!isset($gameModel))
            $this->redirect(Yii::app()->createUrl('play'));
        $gameVersion = $gameModel->getCurrentVersion('web');            
        if(Common::isRunningMobile())
        {                        
            if(Common::isRunningiOS())
                $this->redirect($gameModel->getCurrentVersion('ios')->gamePlatform->market_url);
            elseif(Common::isRunningAndroidOS()){                
                $this->redirect($gameModel->getCurrentVersion('android')->gamePlatform->market_url);                        
            }else{
                echo "Thiết bị của bạn không được hỗ trợ";die;
            }
           
        }    
        $this->render('phom', array('gameVersion'=>$gameVersion));
    }
    
    public function actionTlmn(){        
        $this->seoPage('Tiến lên miền nam');  
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('play/tlmn')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('files/games/gameLogo2.png')),null,null,array('property'=>'og:image'));        
        $gameModel = Game::model()->find('code_identifier="tlmn"');
        if(!isset($gameModel))
            $this->redirect(Yii::app()->createUrl('play'));
        $gameVersion = $gameModel->getCurrentVersion('web'); 
                
        if(Common::isRunningMobile())
        {                        
            if(Common::isRunningiOS())
                $this->redirect($gameModel->getCurrentVersion('ios')->gamePlatform->market_url);
            elseif(Common::isRunningAndroidOS()){                
                $this->redirect($gameModel->getCurrentVersion('android')->gamePlatform->market_url);                        
            }else{
                echo "Thiết bị của bạn không được hỗ trợ";die;
            }
           
        }    
        $this->render('tlmn', array('gameVersion'=>$gameVersion));
    }
    
    public function actionChan(){
        $this->seoPage('Chắn');
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('play/chan')),null,null,array('property'=>'og:url'));
        Yii::app()->clientScript->registerMetaTag((Yii::app()->createAbsoluteUrl('files/games/gameLogo3.png')),null,null,array('property'=>'og:image'));
        $gameModel = Game::model()->find('code_identifier="chan"');
        if(!isset($gameModel))
            $this->redirect(Yii::app()->createUrl('play'));
        $gameVersion = $gameModel->getCurrentVersion('web'); 
                
        if(Common::isRunningMobile())
        {                        
            if(Common::isRunningiOS())
                $this->redirect($gameModel->getCurrentVersion('ios')->gamePlatform->market_url);
            elseif(Common::isRunningAndroidOS()){                
                $this->redirect($gameModel->getCurrentVersion('android')->gamePlatform->market_url);                        
            }else{
                echo "Thiết bị của bạn không được hỗ trợ";die;
            }
           
        }    
        $this->render('chan', array('gameVersion'=>$gameVersion));
    }              
    
    public function seoPage($game){
        $this->pageTitle = "Chơi game ".$game." Online | đánh ".$game." Online";		
        Yii::app()->clientScript->registerMetaTag('Chơi game '.$game.' Online, đánh '.$game.' Online, '.$game.' online, game online, đánh bài, bài lá esimo.vn cổng trò chơi trực tuyến hàng đầu tại Việt Nam', 'Description');
        Yii::app()->clientScript->registerMetaTag('danh '.$game.', choi '.$game.' ', 'Keywords');	        
        Yii::app()->clientScript->registerMetaTag('Chơi game '.$game.' Online | đánh '.$game.' Online',null,null,array('property'=>'og:title'));        
        Yii::app()->clientScript->registerMetaTag('Chơi game '.$game.' Online, đánh '.$game.' Online, '.$game.' online, game online, đánh bài, bài lá esimo.vn cổng trò chơi trực tuyến hàng đầu tại Việt Nam',null,null,array('property'=>'og:description'));
    }        

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }        

}