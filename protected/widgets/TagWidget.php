<?php

class TagWidget extends CWidget {
    
    public function run() {
        // Get news            
        $criteria = new CDbCriteria();
        $criteria->alias = 'post';
        $criteria->distinct=true;
        $criteria->select = 'meta_keyword';
        $criteria->limit=5;
        $models = Post::model()->findAll($criteria);
        $this->render('tag', array(
            'models'=>$models   
        ));
    }
}
?>
