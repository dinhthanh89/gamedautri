<?php

class ContactWidget extends CWidget {
       
    public function run() {
        // Get contact
        $models = Configuration::model()->getConfig('support_contact');        
        $this->render('contact', array(
            'models'=>$models,            
        ));
    }
}
?>
