<?php

class NewsWidget extends CWidget {
    
    public function run() {
        // Get news            
        $models = Post::model()->findAll('category_id =:category_id AND status=:status ORDER BY create_time DESC LIMIT 0,3', array(':category_id'=>8, ':status'=>1));        
        
        $this->render('news', array(
            'models'=>$models   
        ));
    }
}
?>
