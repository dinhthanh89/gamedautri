<?php

class PostWidget extends CWidget {

    public $category_id;
    public function run() {  
        $guide = Post::model()->find('status = 1 AND category_id != 1 AND category_id != 2 AND category_id != 8 ORDER BY create_time');
        // Get news
        $models = Post::model()->findAll('category_id != 1 AND category_id != 2 AND category_id != 8 AND status=:status ORDER BY create_time DESC LIMIT 0,5', array(':status'=>1));        

        $this->render('post', array(
            'models'=>$models,
            'guide'=>$guide
        ));
    }
}
?>
