<?php

class NganLuongWidget extends CWidget {

    public function run() {
        // Connect Ngan luong
        Common::includeNganLuong();                
        $inputs = array(
            'receiver' => NL_RECEIVER,
            'order_code' => 'DH-' . date('His-dmY'),
            'return_url' => Yii::app()->createAbsoluteUrl('site/paymentsuccess'),
            'cancel_url' => '',
            'language' => 'vn'
        );

        $link_checkout = '';
        $obj = new NL_MicroCheckout(NL_MERCHANT_ID, NL_MERCHANT_PASS, URL_WS);
        $result = $obj->setExpressCheckoutDeposit($inputs);
        if ($result != false) {
            if ($result['result_code'] == '00') {
                $link_checkout = $result['link_checkout'];
                $link_checkout = str_replace('micro_checkout.php?token=', 'index.php?portal=checkout&page=micro_checkout&token_code=', $link_checkout);
            } else {
                die('Ma loi ' . $result['result_code'] . ' (' . $result['result_description'] . ') ');
            }
        } else {
        //    die('Loi ket noi toi cong thanh toan ngan luong');
        }// End ngan luong.   
        
        $this->render('nganluong', array('link_checkout'=>$link_checkout));
    }
}
?>
