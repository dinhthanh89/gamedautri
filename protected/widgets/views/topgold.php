<?php if(isset($models)):?>
<?php foreach($models as $model):?>
<div class="top-chip">
    <img width="100px" height="100px" src="<?php if(isset($model->userAvatar->path) && file_exists(Yii::app()->createUrl($model->userAvatar->path))) echo Yii::app()->createUrl($model->userAvatar->path); else echo Yii::app()->createUrl('files/user/default_avatar'); ?>"/>
    <div class="username-b"><?php echo $model->username;?></div>
    <div class="img-chip"><?php echo Yii::app()->format->formatNumber($model->chip);?><img class="chip" src="<?php echo Yii::app()->createUrl('css/images/chip.png') ?>" /></div>
</div>
<?php endforeach;?>
<?php endif; ?>

