<div class="page-in-border-3"></div>
<div class="question">
    <?php if($title == TRUE):?>
    <div class="question-title">HỎI ĐÁP</div>
    <?php endif;?>
    <div class="question-content" style="<?php if($title != TRUE) echo "text-align:left" ?>">
        <form method="POST" action="<?php echo Yii::app()->createUrl(Common::getCurrentUrl()); ?>">
            <p>Nếu các câu hỏi trên chưa thỏa mãn được thắc mắc của bạn, bạn có thể đặt câu hỏi trực tiếp cho chúng tôi.</p>
            <div class="form-input">
                <div><label>Nội dung:</label><textarea name="content" id="question_content"></textarea></div>
                <div class="question-submit"><input type="submit" value="Gửi"></div>
            </div>
        </form>
    </div>
</div>
<div class="diaglog-wraper" id="dialog-wrapper-login" style="display:none">
    <div class="diaglog" style="width: 486px; height: 296px;">
        <div class="title">Hoàn tất gửi hỏi đáp<a onclick="dialog_close();"></a></div>
        <div class="dialog-content login-group">
            <div class="content-left">                
                <div class="login-title">Đăng nhập để gửi hồi đáp</div>
                <ul class="login-input">                        
                    <li>                        
                        <input type="text" id="question_username" class="input register-input" name="username" value="" placeholder="Tên đăng nhập"/>                           
                    </li>
                    <li>                        
                        <input type="password" id="question_password" class="input register-input" name="password" value="" placeholder="Mật khẩu"/>
                    </li>
                </ul>
                <div class="form-button-question">
                    <a class="link-forgot-password">Đăng ký</a>
                    <a class="button" onclick="ajaxLoginPopup($('#question_username').val(), $('#question_password').val(), $('#question_content').html());">Xác nhận</a>
                </div>
                <div class="quick-register">
                    <span class="login-text">Đăng nhập nhanh</span>
                    <span><a class="login-gmail" href="<?php echo Yii::app()->createAbsoluteUrl('/site/GoogleLogin'); ?>"></a></span>
                    <span><a class="login-fb" href="<?php echo Yii::app()->createAbsoluteUrl('/site/FacebookLogin'); ?>"></a></span>
                </div>
            </div>
            <div class="content-right">
                <div class="login-title">Hoặc điền địa chỉ email để gửi hồi đáp</div>
                <ul class="login-input">                        
                    <li>                        
                        <input type="text" class="input register-input" name="email" value="" placeholder="Địa chỉ email"/>                           
                    </li>                    
                </ul>
                <div class="form-button-question">                    
                    <a class="button">Xác nhận</a>
                </div>
            </div>
        </div>                                        
    </div>
</div>
<script type="text/javascript">
    $(".question-submit").click(function(){  
        <?php if (!Yii::app()->user->is_login()): ?>
        $('#dialog-wrapper-login').show();
        //alert('Bạn cần đăng nhập để gửi phản hồi!');
         event.preventDefault();         
         <?php endif;?>
    })    
</script>
