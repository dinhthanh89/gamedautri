
<div class="comment-items">
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $dataComment,       
        'itemView' => '_item_comment',
        'emptyText' => 'Không có bình luận nào',
        'summaryText' => 'Trang {page} / {pages}',                
    ));
    ?>
</div>
<?php 
$userCurrent = Yii::app()->user->getUser();
if ( $userCurrent != NULL): ?>            
    <form class="panel-item">
        <textarea></textarea>
        <div class="submit-comment-out">
            <input  class="submit-comment" type="submit" value="Trả lời"/>
        </div>
    </form>    
<?php endif; ?>