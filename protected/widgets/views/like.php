<div id="fb-root"></div>                
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=328563347242091";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="fb-like" style="margin-right: 25px !important" data-href="<?php echo Yii::app()->createAbsoluteUrl($data) ?>" data-width="450" data-layout="button_count" data-show-faces="false" data-send="false"></div>
<div class="g-plusone" data-size="Medium" data-href="<?php echo Yii::app()->createAbsoluteUrl($data) ?>"></div>
<!-- Place this render call where appropriate -->
<script type="text/javascript">
    (function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/plusone.js';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();
</script>
<a href="<?php echo Yii::app()->createAbsoluteUrl($data) ?>" class="twitter-share-button" data-lang="en">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>                        