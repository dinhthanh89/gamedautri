<div class="content-item-header"><i class="fa fa-star-o fa-2x"></i><a class="news-event" href="<?php echo Yii::app()->createUrl('news') ?>">TIN TỨC - SỰ KIỆN</a></div>
<div class="content-item-main">
    <?php if(isset($models)): ?>
    <?php $top = $models[0]; ?>
    <a href="<?php echo $top->getDetailLink() ?>">
        <img width="220px" height="165px" src="<?php echo Yii::app()->createUrl($top->main_image) ?>"/>
        <div class="item">
            <div class="item-title"><?php echo $top->title ?></div>
            <div class="item-time"><?php echo $top->create_time ?></div>
            <div class="item-sapo"><?php echo MyFunction::shorten_string($top->sapo, 20) ?></div>
        </div>
    </a>
    <?php endif;?>
</div>
<div class="content-item-main">
    <?php if(isset($models) && isset($models[1])): ?>    
    <?php $top = $models[1]; ?>
    <a href="<?php echo $top->getDetailLink() ?>">
        <img width="220px" height="165px" src="<?php echo Yii::app()->createUrl($top->main_image) ?>"/>
        <div class="item">
            <div class="item-title"><?php echo $top->title ?></div>
            <div class="item-time"><?php echo $top->create_time ?></div>
            <div class="item-sapo"><?php echo MyFunction::shorten_string($top->sapo, 20) ?></div>
        </div>
    </a>
    <?php endif;?>
</div>
<div class="content-item-main">
    <?php if(isset($models) && isset($models[2])): ?>
    <?php $top = $models[2]; ?>
    <a href="<?php echo $top->getDetailLink() ?>">
        <img width="220px" height="165px" src="<?php echo Yii::app()->createUrl($top->main_image) ?>"/>
        <div class="item">
            <div class="item-title"><?php echo $top->title ?></div>
            <div class="item-time"><?php echo $top->create_time ?></div>
            <div class="item-sapo"><?php echo MyFunction::shorten_string($top->sapo, 20) ?></div>
        </div>
    </a>
    <?php endif;?>
</div>
<div class="content-item-main">
    <?php if(isset($models) && isset($models[3])): ?>
    <?php $top = $models[3]; ?>
    <a href="<?php echo $top->getDetailLink() ?>">
        <img width="220px" height="165px" src="<?php echo Yii::app()->createUrl($top->main_image) ?>"/>
        <div class="item">
            <div class="item-title"><?php echo $top->title ?></div>
            <div class="item-time"><?php echo $top->create_time ?></div>
            <div class="item-sapo"><?php echo MyFunction::shorten_string($top->sapo, 20) ?></div>
        </div>
    </a>
    <?php endif;?>
</div>
