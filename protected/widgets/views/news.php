<div class="footer-item-header"><a href="<?php echo Yii::app()->createUrl('news') ?>">TIN TỨC - SỰ KIỆN</a></div>
<div class="footer-item-main">
    <?php if(isset($models)): ?>
    <?php $top = $models[0]; ?>
    <a href="<?php echo $top->getDetailLink() ?>">
        <img width="80px" height="80px" src="<?php echo Yii::app()->createUrl($top->main_image) ?>"/>
        <div class="item">
            <div class="item-title"><?php echo $top->title ?></div>
            <div class="item-sapo"><?php echo MyFunction::shorten_string($top->sapo, 20) ?></div>
        </div>
    </a>
    <?php endif;?>
</div>
<div class="footer-item-bottom">
    <ul class="footer-ul-style-dis">
        <?php if(isset($models)): ?>
        <?php foreach($models as $index => $model):?>
        <?php if($index > 0):?>
        <li><span></span><a href="<?php echo $model->getDetailLink(); ?>"><?php echo $model->title; ?></a></li>
        <?php endif;?>
        <?php endforeach; ?>
        <?php endif;?>
    </ul>
</div> 