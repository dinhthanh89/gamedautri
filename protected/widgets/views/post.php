<div class="footer-item-header"><a href="<?php  echo Yii::app()->createUrl('huong-dan') ?>">HƯỚNG DẪN</a></div>
<div class="footer-item-main">
    <ul class="footer-ul-style-dis">
        <?php if(isset($models)): ?>
        <?php foreach($models as $model):?>
        <li><span></span><a href="<?php echo $model->getDetailLink() ?>"><?php echo $model->title?></a></li>
        <?php endforeach;?>
        <?php endif;?>        
    </ul>                                
</div> 