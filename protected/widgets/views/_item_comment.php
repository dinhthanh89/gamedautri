<?php $user = $data->user;?>
<div class="comment-item">
    <div class="comment-item-left">
        <img class="product-panel-item-img" width="50px" height="50px" src="<?php echo $user->getAvatar() ?>">        
    </div>
    <div class="comment-item-right">
        <div class="color-FF9805"><span class="font-bold"><?php echo $user->fullname ?></span><span class="static-label">đã bình luận:</span></div>
        <div><?php echo date('Y-m-d H:i', strtotime($data->create_time)) ?></div>                  
        <div class="color-FF9805"><?php K::echoWithXssFilter($data->content) ?></div>        
    </div>           
</div>