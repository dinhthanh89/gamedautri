<?php

class QuestionWidget extends CWidget {
    public $title;   
    public function run() {
        $title = $this->title;                           
        $this->render('question', array(
            'title'=>$title,            
        ));
    }
}
?>
