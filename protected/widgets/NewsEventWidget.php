<?php

class NewsEventWidget extends CWidget {
    
    public function run() {
        // Get news            
        $models = Post::model()->findAll('category_id =:category_id AND status=:status ORDER BY create_time DESC LIMIT 0,5', array(':category_id'=>8, ':status'=>1));        
        
        $this->render('newsevent', array(
            'models'=>$models   
        ));
    }
}
?>
