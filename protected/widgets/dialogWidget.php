<?php

class dialogWidget extends CWidget {

    public $title;
    public $content;

    public function init() {
        
    }

    public function run() {
        ?>
        <div class="diaglog-wraper">
            <div class="diaglog">
                <div class="title">
                    <?php echo $this->title; ?>
                </div>
                <div class="dialog-content">
                    <?php echo $this->content; ?>
                </div>
                <div class="ok-wrapper">
                    <div class="ok" onclick="dialog_close();"></div>
                </div>
            </div>
        </div>
        <?php
    }

}
?>
