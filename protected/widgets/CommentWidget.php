<?php

class CommentWidget extends CWidget {
    public $id;
    public function run() {             
        $dataComment = new CActiveDataProvider('UserComment', array(
                'criteria' => array(
                    'condition' => 'type = 1 AND status=1 AND object_id = ' . $this->id,
                    'order' => 'create_time ASC',                                                              
                )                
            ));                  
        $this->render('comment', array(
            'dataComment'=>$dataComment  
        ));
    }
}
?>
