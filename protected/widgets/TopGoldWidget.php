<?php

class TopGoldWidget extends CWidget {
    
    public function run() {
        // Get topgold    
        
             
        $criteria = new CDbCriteria();
        $criteria->alias = 'user';
        $criteria->select = 'file.path, user.chip, user.username, user.avatar';
        $criteria->join = 'INNER JOIN file ON file.id = user.avatar';
        $criteria->addCondition('is_virtual_player = 0');
        $criteria->order = 'user.chip DESC';
        $criteria->limit = 5;
        $models = User::model()->findAll($criteria);     
        $this->render('topgold', array(
            'models'=>$models   
        ));
    }
}
?>
