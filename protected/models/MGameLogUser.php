<?php
class MGameLogUser extends EMongoEmbeddedDocument
    {
      public $user_id;      
      public $username;
      
      public function rules()
      {
        return array(
          array('user_id,username', 'length', 'max'=>255),          
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'user_id'=>'UserId',  
          'username'=>'Username',
        );
      }            
      
    }