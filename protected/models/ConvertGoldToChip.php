<?php

/**
 * This is the model class for table "convert_gold_to_chip".
 *
 * The followings are the available columns in table 'convert_gold_to_chip':
 * @property integer $id
 * @property string $gold
 * @property double $ratio
 * @property integer $transaction_id 
 */
class ConvertGoldToChip extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConvertGoldToChip the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'convert_gold_to_chip';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('gold, ratio, transaction_id', 'required'),
			array('transaction_id', 'numerical', 'integerOnly'=>true),
			array('ratio', 'numerical'),
			array('gold', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, gold, ratio, transaction_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'gold' => 'Gold',
			'ratio' => 'Ratio',
			'transaction_id' => 'Transaction',			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('gold',$this->gold,true);
		$criteria->compare('ratio',$this->ratio);
		$criteria->compare('transaction_id',$this->transaction_id);		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}