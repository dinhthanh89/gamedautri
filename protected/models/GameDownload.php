<?php

/**
 * This is the model class for table "game_download".
 *
 * The followings are the available columns in table 'game_download':
 * @property integer $id
 * @property integer $game_id 
 * @property integer $count_download
 * @property string $store_link
 * @property string $device
 * @property string $description
 * @property string $server_link
 *
 * The followings are the available model relations:
 * @property Game $game
 * @property GameVersion[] $gameVersions
 */
class GameDownload extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GameDownload the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game_download';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, game_id, device', 'required'),
			array('id, game_id, count_download', 'numerical', 'integerOnly'=>true),
			array('store_link, device', 'length', 'max'=>256),
			array('description, server_link', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, game_id, count_download, store_link, device, description, server_link', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'gameVersions' => array(self::HAS_MANY, 'GameVersion', 'game_download_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'game_id' => 'Game',			
			'count_download' => 'Count Download',
			'store_link' => 'Store Link',
			'device' => 'Device',
			'description' => 'Description',
			'server_link' => 'Server Link',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('game_id',$this->game_id);		
		$criteria->compare('count_download',$this->count_download);
		$criteria->compare('store_link',$this->store_link,true);
		$criteria->compare('device',$this->device,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('server_link',$this->server_link,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getCurrentVersionByDevice(){                                   
            return GameVersion::model()->getCurrentVersion($this->id);          
        }
}