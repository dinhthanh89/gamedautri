<?php

/**
 * This is the model class for table "game_version".
 *
 * The followings are the available columns in table 'game_version':
 * @property integer $id
 * @property integer $game_platform_id 
 * @property string $core_version
 * @property string $build_version
 * @property string $code_version_build
 * @property string $size
 * @property string $create_date
 * @property string $modified_date
 * @property integer $enabled 
 * @property integer $visible
 * @property string $file_path
 *
 * The followings are the available model relations:
 * @property File $file
 * @property GameDownload $gameDownload
 */
class GameVersion extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return GameVersion the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'game_version';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('game_platform_id, core_version, build_version, code_revision, visible, enabled', 'required'),
            array('game_platform_id, visible, enabled', 'numerical', 'integerOnly' => true),
            array('core_version, build_version, code_revision, size', 'length', 'max' => 256),
            array('create_date, file_path', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, game_platform_id, core_version, build_version, code_revision, size, create_date,enabled, visible, file_path', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(            
            'gamePlatform' => array(self::BELONGS_TO, 'GamePlatform', 'game_platform_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'game_platform_id' => 'Game Platform',
            'file_id' => 'File',
            'core_version' => 'Core Version',
            'build_version' => 'Build Version',
            'code_' => 'Code Revision',
            'size' => 'Size',
            'create_date' => 'Create Date',            
            'enabled' => 'Enabled',            
            'visible' => 'Visible',
            'file_path' => 'File Path',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('game_platform_id', $this->game_platform_id);        
        $criteria->compare('core_version', $this->core_version, true);
        $criteria->compare('build_version', $this->build_version, true);
        $criteria->compare('code_revision', $this->code_revision, true);
        $criteria->compare('size', $this->size, true);
        $criteria->compare('create_date', $this->create_date, true);        
        $criteria->compare('enabled', $this->enabled);        
        $criteria->compare('visible', $this->visible);
        $criteria->compare('file_path', $this->file_path, true);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }

    public function getSize() {
        $size = (round(($this->size * 10) /1024))/10;
        return $size." MB";
    }
    
    public function getCurrentVersion(){        
        return $this->core_version.".".$this->build_version;
    }

}