<?php

/**
 * This is the model class for table "product".
 *
 * The followings are the available columns in table 'product':
 * @property integer $id
 * @property integer $category_product_id
 * @property string $name
 * @property string $code_identifier
 * @property string $description
 * @property string $image
 * @property integer $gold
 * @property integer $chip
 * @property integer $quatity
 * @property integer $status
 */
class Product extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Product the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category_product_id, code_identifier', 'required'),
			array('category_product_id, gold, chip, quatity, status', 'numerical', 'integerOnly'=>true),
			array('name, image', 'length', 'max'=>256),
			array('code_identifier', 'length', 'max'=>56),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, category_product_id, name, code_identifier, description, image, gold, chip, quatity, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                     'categoryProduct' => array(self::BELONGS_TO, 'CategoryProduct', 'category_product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category_product_id' => 'Category Product',
			'name' => 'Name',
			'code_identifier' => 'Code Identifier',
			'description' => 'Description',
			'image' => 'Image',
			'gold' => 'Gold',
			'chip' => 'Chip',
			'quatity' => 'Quatity',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('category_product_id',$this->category_product_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code_identifier',$this->code_identifier,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('chip',$this->chip);
		$criteria->compare('quatity',$this->quatity);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}