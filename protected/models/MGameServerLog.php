<?php
class MGameServerLog extends EMongoDocument
    {      
      public $type;
      public $value;
      public $description;
      public $timestamp;   
      public $max_ccu;
 
      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }
 
      // This method is required!
      public function getCollectionName()
      {
        return 'game_server_log';
      }
 
      public function rules()
      {
        return array(
                    
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'id'  => 'Id',
          'type'   => 'Type',
          'value'   => 'Value',
          'description' => 'Description',
          'timestamp' => 'Timestamp'  
        );
      }
            
    }