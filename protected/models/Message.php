<?php

/**
 * This is the model class for table "user_message".
 *
 * The followings are the available columns in table 'user_message':
 * @property interger $id
 * @property interger $sender
 * @property string $sender_name
 * @property interger $receiver
 * @property string $receiver_name
 * @property string $content
 * @property datetime $time_sent
 * @property interger $read 
 * @property interger $status
 *
 * The followings are the available model relations:
 * @property User $id 
 */
class Message extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Message the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_message';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('sender, sender_name, receiver, receiver_name, content, time_sent, read, status', 'required'),
            array('sender, receiver, read, status', 'numerical', 'integerOnly' => true),            
            array('content', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, sender, sender_name, receiver, receiver_name, content, time_sent, read, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(   
            'user' => array(self::BELONGS_TO, 'User', 'sender'),
            'userReceiver' => array(self::BELONGS_TO, 'User', 'receiver'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'sender' => 'Sender',
            'sender_name' => 'Sender Name',            
            'receiver' => 'Receiver',
            'receiver_name' => 'Receiver Name',
            'content' => 'Content',
            'read' => 'Read',
            'status' => 'Status'
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sender', $this->sender, true);
        $criteria->compare('sender_name', $this->sender_name, true);
        $criteria->compare('receiver', $this->receiver, true);
        $criteria->compare('receiver_name', $this->receiver_name);
        $criteria->compare('content', $this->content);
        $criteria->compare('time_sent', $this->time_sent);
        $criteria->compare('read', $this->read);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                ));
    }
    
    public function getInbox($receiver) {                
        $sends = self::model()->findAll('receiver=:receiver', array(':receiver' => $receiver));        
        if ($sends == NULL)
            return NULL;
        else{
            $groups = array();
            foreach($sends as $item){
                $key = $item->sender;
                $groups[$key][] = $item;                     
            }
            return $groups;
        }
            
    }
    
    public function getReceiver($sender) {                
        $inboxs = self::model()->findAll('sender=:sender', array(':sender' => $sender));        
        if ($inboxs == NULL)
            return NULL;
        else
            return $inboxs;
    }
    
    

}