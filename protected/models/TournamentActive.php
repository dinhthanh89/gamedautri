<?php

/**
 * This is the model class for table "tournament_active".
 *
 * The followings are the available columns in table 'tournament_active':
 * @property integer $id
 * @property integer $tournament_id
 * @property string $start_time_registration
 * @property string $end_time_registration
 * @property string $start_time
 * @property string $end_time
 * @property string $config
 * @property string $result
 * @property integer $max_players
 * @property integer $registration_winner
 */
class TournamentActive extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TournamentActive the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tournament_active';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tournament_id, start_time_registration, config', 'required'),
			array('tournament_id, max_players, registration_winner', 'numerical', 'integerOnly'=>true),
			array('end_time_registration, start_time, end_time, result', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, tournament_id, start_time_registration, end_time_registration, start_time, end_time, config, result, max_players, registration_winner', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tournament_id' => 'Tournament',
			'start_time_registration' => 'Start Time Registration',
			'end_time_registration' => 'End Time Registration',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'config' => 'Config',
			'result' => 'Result',
			'max_players' => 'Max Players',
			'registration_winner' => 'Registration Winner',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tournament_id',$this->tournament_id);
		$criteria->compare('start_time_registration',$this->start_time_registration,true);
		$criteria->compare('end_time_registration',$this->end_time_registration,true);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('config',$this->config,true);
		$criteria->compare('result',$this->result,true);
		$criteria->compare('max_players',$this->max_players);
		$criteria->compare('registration_winner',$this->registration_winner);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}