<?php

/**
 * This is the model class for table "recharge".
 *
 * The followings are the available columns in table 'recharge':
 * @property integer $id
 * @property integer $user_id
 * @property integer $value
 * @property integer $gold
 * @property integer $recharge_type_id
 * @property double $ratio
 * @property string $create_time
 * @property integer $status
 */
class Recharge extends CActiveRecord
{
        public $total;
        public $total_card;
        public $provider;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Recharge the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recharge';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, value, gold, recharge_type_id, ratio', 'required'),
			array('user_id, value, gold, recharge_type_id, status', 'numerical', 'integerOnly'=>true),
			array('ratio', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, value, gold, recharge_type_id, ratio, create_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'rechargeType' => array(self::BELONGS_TO, 'RechargeType', 'recharge_type_id'),
                    'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'value' => 'Value',
			'gold' => 'Gold',
			'recharge_type_id' => 'Recharge Type',
			'ratio' => 'Ratio',
			'create_time' => 'Create Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('value',$this->value);
		$criteria->compare('gold',$this->gold);
		$criteria->compare('recharge_type_id',$this->recharge_type_id);
		$criteria->compare('ratio',$this->ratio);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function getTotalValue($records)
        {
            $data=0;
            foreach($records as $record)
                $data+=$record->value;
            return $data;
        } 
        
        public function getTotalGold($records)
        {
            $data=0;
            foreach($records as $record)
                $data+=$record->gold;
            return $data;
        }
        
        public function getTotalRealValue($records)
        {            
            $data=0;
            foreach($records as $record)
                $data+=$record->rechargeType->real_value;
            return $data;
        }
}