<?php

/**
 * This is the model class for table "user_access_token".
 *
 * The followings are the available columns in table 'user_access_token':
 * @property integer $id
 * @property integer $user_id
 * @property string $access_token
 * @property string $last_time
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $user
 */
class UserAccessToken extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserAccessToken the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_access_token';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, access_token', 'required'),
            array('user_id, status', 'numerical', 'integerOnly' => true),
            array('access_token', 'length', 'max' => 256),
            array('access_token', 'unique'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, user_id, access_token, last_time, status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'user_id' => 'User',
            'access_token' => 'Access Token',
            'last_time' => 'Last Time',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('access_token', $this->access_token, true);
        $criteria->compare('last_time', $this->last_time, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function createAccessToken($userId, $appId, $ip) {
        $accessToken = new UserAccessToken('new');

        $accessToken->user_id = $userId;
        date_default_timezone_set('Asia/Bangkok');
        $time = date('Y-m-d H:i:s', time());
        $randomString = MyFunction::rand_string(1);
        $token = hash('sha512', $userId . $appId . $ip . $time . $randomString);
        $accessToken->access_token = $token;
        $accessToken->last_time = $time;
        if ($accessToken->validate()) {
            if ($accessToken->save())
                return $token;
            else
                return FALSE;
        }
        else
            return -1;
    }

    public function createLoginAccessToken($userId, $ip) {
        $accessToken = new UserAccessToken('new');

        $accessToken->user_id = $userId;
        date_default_timezone_set('Asia/Bangkok');
        $time = date('Y-m-d H:i:s', time());
        $randomString = MyFunction::rand_string(1);
        $token = hash('sha512', $userId . $ip . $time . $randomString);
        $accessToken->access_token = $token;
        $accessToken->last_time = $time;
        if ($accessToken->validate()) {
            if ($accessToken->save())
                return $token;
            else
                return FALSE;
        }
        else
            return -1;
    }

    /**
     * 
     * @param string $accessToken
     * @return bollean
     */
    public function checkAccessToken($accessToken) {
        $aTk = UserAccessToken::model()->find('access_token=:accessToken', array(':accessToken' => $accessToken));
        if ($aTk != NULL) {
            $time = date_format(new DateTime($aTk->last_time), 'U');
            $lifeTime = 30 * 24 * 60 * 60;
            if (time() < ($lifeTime + $time)) {
                $newTime = date('Y-m-d H:i:s', time());
                $aTk->last_time = $newTime;
                if ($aTk->save())
                    return TRUE;
            } else {
                return FALSE;
            }
        }
        else
            return FALSE;
    }
    public function checkForgotPasswordAccessToken($accessToken) {
        $aTk = UserAccessToken::model()->find('access_token=:accessToken', array(':accessToken' => $accessToken));
        if ($aTk != NULL) {
            $time = date_format(new DateTime($aTk->last_time), 'U');
            $lifeTime = 5 * 24 * 60 * 60;
            if (time() < ($lifeTime + $time)) {
//                $newTime = date('Y-m-d H:i:s', time()-$lifeTime);
//                $aTk->last_time = $newTime;
//                if ($aTk->save())
                return TRUE;
            } else {
                return FALSE;
            }
        }
        else
            return FALSE;
    }    

    /**
     * 
     * @return Bollean
     */
    public function checkExpire() {
        $lifeTime = 30 * 24 * 60 * 60;
        $time = date_format(new DateTime($this->last_time), 'U');
        if (time() < ($lifeTime + $time))
            return TRUE;
        else
            return FALSE;
    }   

}