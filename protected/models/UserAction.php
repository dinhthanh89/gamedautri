<?php

/**
 * This is the model class for table "user_action".
 *
 * The followings are the available columns in table 'user_action':
 * @property integer $id
 * @property integer $user_id
 * @property integer $action_id
 * @property string $timestamp
 * @property string $value
 * @property string $enviroment
 * @property string $version
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Action $action
 */
class UserAction extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('action_id', 'required'),
			array('user_id, action_id', 'numerical', 'integerOnly'=>true),
			array('version', 'length', 'max'=>255),
			array('timestamp, value, enviroment', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, action_id, timestamp, value, enviroment, version', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'action' => array(self::BELONGS_TO, 'Action', 'action_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'action_id' => 'Action',
			'timestamp' => 'Timestamp',
			'value' => 'Value',
			'enviroment' => 'Enviroment',
			'version' => 'Version',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('action_id',$this->action_id);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('enviroment',$this->enviroment,true);
		$criteria->compare('version',$this->version,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}