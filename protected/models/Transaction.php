<?php

/**
 * This is the model class for table "transaction".
 *
 * The followings are the available columns in table 'transaction':
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string $value
 * @property integer $game_log_id
 * @property string $timestamp
 *
 * The followings are the available model relations:
 * @property GameLog $gameLog
 * @property User $user
 */
class Transaction extends CActiveRecord
{
        public $total;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Transaction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'transaction';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, type, game_log_id', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>20),						
			array('timestamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, type, value, game_log_id, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gameLog' => array(self::BELONGS_TO, 'GameLog', 'game_log_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'type' => 'Type',
			'value' => 'Value',
			'game_log_id' => 'Game Log',			
			'timestamp' => 'Thời gian',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('game_log_id',$this->game_log_id);		
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
         public function searchUser($user_id, $type)
	{                        
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.                
		$criteria = new CDbCriteria(array(                                                    
                                'condition'=>'user_id='.$user_id.' AND type = '.$type,
                        ));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));                
	}  
}