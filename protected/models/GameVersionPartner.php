<?php

/**
 * This is the model class for table "game_version_partner".
 *
 * The followings are the available columns in table 'game_version_partner':
 * @property integer $game_version_id
 * @property integer $partner_id
 * @property integer $download_times
 */
class GameVersionPartner extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GameVersionPartner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game_version_partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('game_version_id, partner_id', 'required'),
			array('game_version_id, partner_id, download_times', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('game_version_id, partner_id, download_times', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'game_version_id' => 'Game Version',
			'partner_id' => 'Partner',
			'download_times' => 'Download Times',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('game_version_id',$this->game_version_id);
		$criteria->compare('partner_id',$this->partner_id);
		$criteria->compare('download_times',$this->download_times);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}