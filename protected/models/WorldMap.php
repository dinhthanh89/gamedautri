<?php

/**
 * This is the model class for table "world_map".
 *
 * The followings are the available columns in table 'world_map':
 * @property integer $id
 * @property string $node_name
 * @property integer $client_scene
 * @property string $display_name
 * @property integer $maximum_players
 * @property integer $parent_id
 * @property integer $num_robot_allowed
 * @property integer $robot_level
 * @property integer $num_children_default
 * @property string $node_plugin_handle
 * @property string $game_type
 * @property string $game_details
 * @property integer $app_id
 * @property string $icon
 * @property string $logo
 * @property string $description
 * @property integer $min_money
 * @property string $client_version_supported_virtual_room
 * @property string $default_game_config
 *
 * The followings are the available model relations:
 * @property WorldMap $parent
 * @property WorldMap[] $worldMaps
 */
class WorldMap extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WorldMap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'world_map';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('node_name', 'required'),
			array('client_scene, maximum_players, parent_id, num_robot_allowed, robot_level, num_children_default, app_id, min_money', 'numerical', 'integerOnly'=>true),
			array('node_name, node_plugin_handle, game_type', 'length', 'max'=>45),
			array('display_name, client_version_supported_virtual_room', 'length', 'max'=>255),
			array('game_details, icon, logo, description, default_game_config', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, node_name, client_scene, display_name, maximum_players, parent_id, num_robot_allowed, robot_level, num_children_default, node_plugin_handle, game_type, game_details, app_id, icon, logo, description, min_money, client_version_supported_virtual_room, default_game_config', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'WorldMap', 'parent_id'),
			'worldMaps' => array(self::HAS_MANY, 'WorldMap', 'parent_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'node_name' => 'Node Name',
			'client_scene' => 'Client Scene',
			'display_name' => 'Display Name',
			'maximum_players' => 'Maximum Players',
			'parent_id' => 'Parent',
			'num_robot_allowed' => 'Num Robot Allowed',
			'robot_level' => 'Robot Level',
			'num_children_default' => 'Num Children Default',
			'node_plugin_handle' => 'Node Plugin Handle',
			'game_type' => 'Game Type',
			'game_details' => 'Game Details',
			'app_id' => 'App',
			'icon' => 'Icon',
			'logo' => 'Logo',
			'description' => 'Description',
			'min_money' => 'Min Money',
			'client_version_supported_virtual_room' => 'Client Version Supported Virtual Room',
			'default_game_config' => 'Default Game Config',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('node_name',$this->node_name,true);
		$criteria->compare('client_scene',$this->client_scene);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('maximum_players',$this->maximum_players);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('num_robot_allowed',$this->num_robot_allowed);
		$criteria->compare('robot_level',$this->robot_level);
		$criteria->compare('num_children_default',$this->num_children_default);
		$criteria->compare('node_plugin_handle',$this->node_plugin_handle,true);
		$criteria->compare('game_type',$this->game_type,true);
		$criteria->compare('game_details',$this->game_details,true);
		$criteria->compare('app_id',$this->app_id);
		$criteria->compare('icon',$this->icon,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('min_money',$this->min_money);
		$criteria->compare('client_version_supported_virtual_room',$this->client_version_supported_virtual_room,true);
		$criteria->compare('default_game_config',$this->default_game_config,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}