<?php

/**
 * This is the model class for table "buddies".
 *
 * The followings are the available columns in table 'buddies':
 * @property integer $id
 * @property integer $user1
 * @property integer $user2
 * @property string $time_request
 * @property string $time_accept
 * @property integer $user2_accept
 */
class Buddies extends CActiveRecord
{
        const STATUS_ACCEPT=1;
	const STATUS_REJECT=2;
	const STATUS_WAIT=3;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Buddies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'buddies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user1, user2, time_accept', 'required'),
			array('user1, user2, user2_accept', 'numerical', 'integerOnly'=>true),
			array('time_request', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user1, user2, time_request, time_accept, user2_accept', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'User', 'user1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user1' => 'User1',
			'user2' => 'User2',
			'time_request' => 'Time Request',
			'time_accept' => 'Time Accept',
			'user2_accept' => 'User2 Accept',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user1',$this->user1);
		$criteria->compare('user2',$this->user2);
		$criteria->compare('time_request',$this->time_request,true);
		$criteria->compare('time_accept',$this->time_accept,true);
		$criteria->compare('user2_accept',$this->user2_accept);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}