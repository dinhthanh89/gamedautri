<?php

/**
 * This is the model class for table "game_action".
 *
 * The followings are the available columns in table 'game_action':
 * @property integer $id
 * @property string $name
 * @property integer $parent_id
 * @property integer $point
 * @property string $code_identifier
 * @property string $description
 * @property integer $game_id
 *
 * The followings are the available model relations:
 * @property GameActionCategory $parent
 * @property Game $game
 * @property UserPlayInTournamentMatch[] $userPlayInTournamentMatches
 */
class GameAction extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GameAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, parent_id', 'required'),
			array('parent_id, point, game_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('description', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, parent_id, point, description, game_id, code_identifier','safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'parent' => array(self::BELONGS_TO, 'GameActionCategory', 'parent_id'),
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'userPlayInTournamentMatches' => array(self::MANY_MANY, 'UserPlayInTournamentMatch', 'user_game_action(game_action_id, user_play_in_tournament_match_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Parent',
			'point' => 'Point',
			'description' => 'Description',
			'game_id' => 'Game',
                        'code_identifier' => 'Code Identifier'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('point',$this->point);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('game_id',$this->game_id);
		$criteria->compare('code_identifier',$this->code_identifier);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}