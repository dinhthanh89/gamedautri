<?php

/**
 * This is the model class for table "push_notification_log".
 *
 * The followings are the available columns in table 'push_notification_log':
 * @property integer $id
 * @property string $content
 * @property integer $times
 * @property string $time_sent
 * @property integer $user_id
 * @property integer $status
 * @property string $create_time
 */
class PushNotificationLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PushNotificationLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'push_notification_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('content, time_sent, user_id, status', 'required'),
			array('times, user_id, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, content, times, time_sent, user_id, status, create_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'content' => 'Content',
			'times' => 'Times',
			'time_sent' => 'Time Sent',
			'user_id' => 'User',
			'status' => 'Status',
			'create_time' => 'Create Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('times',$this->times);
		$criteria->compare('time_sent',$this->time_sent,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('create_time',$this->create_time,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function pushNotification($users=NULL, $request){       
            Common::includeApns(); 
            Common::includeGcm();     	   
            $registatoin_ids = array(); 
            $registatoin_ids_ios = array(); 	   
            $message = new ApnsPHP_Message();

            if(!empty($users)){                  
                $str = implode(',', $users);                
                $query = "select d.* from user_device_token as u inner join device_token as d on d.id = u.device_token_id where u.active = 1 AND u.user_id IN (".$str.") group by d.token";
                $devices = Yii::app()->db->createCommand($query)->queryAll(); 
            }else{
                $query = "select * from device_token group by token";
                $devices = Yii::app()->db->createCommand($query)->queryAll();
            }	
            $index = 0;
            $registatoin_ids[$index] = array();
            $index2 = 0;
            $registatoin_ids_ios[$index2] = array();
            foreach($devices as $device){
                if($device['platform'] == 'ios' || $device['platform'] == "ios_official"){
                    array_push($registatoin_ids_ios[$index2], $device['token']);
                    if(count($registatoin_ids_ios[$index2]) > 220){
                        $index2++;
                        $registatoin_ids_ios[$index2] = array();
                    }
                    //$message->addRecipient($device['token']);
                }
                if($device['platform'] == 'android' || $device['platform'] == "android_official"){	                                 
                    array_push($registatoin_ids[$index], $device['token']);
                    if(count($registatoin_ids[$index]) > 800){
                        $index++;
                        $registatoin_ids[$index] = array();
                    }
                }
            }               

            // push for ios  
            if(count($registatoin_ids_ios[0]) > 0){

                // Adjust to your timezone
                date_default_timezone_set('Asia/Ho_Chi_Minh');

                // Report all PHP errors
                error_reporting(-1);	

                // Instanciate a new ApnsPHP_Push object
                $push = new ApnsPHP_Push(
                        ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
                        'protected/components/apns/dist_apns_chan.pem'
                );              

                // Set the Root Certificate Autority to verify the Apple remote peer
                $push->setRootCertificationAuthority('protected/components/apns/entrust_root_certification_authority.pem');

                // Connect to the Apple Push Notification Service
                $push->connect();                                        	 

                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("message");

                $message -> setSound();
                $message -> setText("Bạn nhận được một tin nhắn từ hệ thống Esimo");
                $message -> setBadge(0);             
                foreach ($request as $key => $value) {                                
                    if ($key == "device_token" || $key == 'accessToken' || $key == 'content' || $key == 'users' || $key == "message" || $key == "title") continue;                  
                    switch ($key) {
                        case 'alert':
                            $message->setText($value);
                            break;
                        case 'badge':
                            $message->setBadge((int) $value);
                            break;
                        case 'sound':
                            $message->setSound($value);
                            break;
                        default:
                            $message->setCustomProperty($key, $value);
                            break;
                    }
                }            

                foreach($registatoin_ids_ios as $item){
                    $message->setRecipients($item);                     
                    // Add the message to the message queue
                    // Set the expiry value to 30 seconds
                    $message->setExpiry(30);                        
                    echo("push message: ".$message -> getPayload()."\r\n");   
                    $push->add($message);                

                    // Send all messages in the message queue
                    $push->send(); 
                    $message->setRecipients(array());
                }

                // Disconnect from the Apple Push Notification Service
                $push->disconnect();

                // Examine the error message container
                $aErrorQueue = $push->getErrors();
                if (!empty($aErrorQueue)) {
                        //var_dump($aErrorQueue);
                } 
            }                    
            //push for android
            if(count($registatoin_ids[0]) > 0){                   
                $title = "Esimo";     
                $messageAndroid = array();
                foreach ($request as $key => $value) {
                    if ($key == "device_token" || $key == 'accessToken' || $key == 'content' || $key == 'badge' || $key == 'content' || $key == 'message' || $key == 'users') continue;
                    $messageAndroid[$key] = $value;
                }
                if (!isset($messageAndroid["title"])) {
                    $messageAndroid["title"] = $title;
                }                  
                foreach($registatoin_ids as $item){                                        
                    $gcm = new GCM();
                    $result = $gcm -> send_notification($item, $messageAndroid);                    
                }
            }
       }
       
       
        public function pushNotificationByDevice($request){       
            Common::includeApns(); 
            Common::includeGcm();     	  
            if(empty($request['deviceToken']) || empty($request['alert']) || empty($request['platform']))
                return false;
            // push for ios  
            if($request['platform'] == "ios"){
                 $message = new ApnsPHP_Message($request['deviceToken']); 
                // Adjust to your timezone
                date_default_timezone_set('Asia/Ho_Chi_Minh');

                // Report all PHP errors
                error_reporting(-1);	

                // Instanciate a new ApnsPHP_Push object
                $push = new ApnsPHP_Push(
                        ApnsPHP_Abstract::ENVIRONMENT_PRODUCTION,
                        'protected/components/apns/dist_apns_chan.pem'
                );              

                // Set the Root Certificate Autority to verify the Apple remote peer
                $push->setRootCertificationAuthority('protected/components/apns/entrust_root_certification_authority.pem');

                // Connect to the Apple Push Notification Service
                $push->connect();                                        	 

                // Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
                // over a ApnsPHP_Message object retrieved with the getErrors() message.
                $message->setCustomIdentifier("message");

                $message -> setSound();
                $message -> setText("Bạn nhận được một tin nhắn từ hệ thống Esimo");
                $message -> setBadge(0);             
                foreach ($request as $key => $value) {                                
                    if ($key == "deviceToken" || $key == "platform") continue;                  
                    switch ($key) {
                        case 'alert':
                            $message->setText($value);
                            break;
                        case 'badge':
                            $message->setBadge((int) $value);
                            break;
                        case 'sound':
                            $message->setSound($value);
                            break;
                        default:
                            $message->setCustomProperty($key, $value);
                            break;
                    }
                }            
                                                    
                // Add the message to the message queue
                // Set the expiry value to 30 seconds
                $message->setExpiry(30);                        
                echo("push message: ".$message -> getPayload()."\r\n");   
                $push->add($message);                

                // Send all messages in the message queue
                $push->send(); 
                $message->setRecipients(array());
                

                // Disconnect from the Apple Push Notification Service
                $push->disconnect();

                // Examine the error message container
                $aErrorQueue = $push->getErrors();
                if (!empty($aErrorQueue)) {
                        //var_dump($aErrorQueue);
                } 
            }          
            //push for android
            if($request['platform'] == "android"){                        
                $title = "Esimo";     
                $messageAndroid = array();
                foreach ($request as $key => $value) {
                    if ($key == "deviceToken" || $key == 'badge' || $key == 'message') continue;
                    $messageAndroid[$key] = $value;
                }
                if (!isset($messageAndroid["title"])) {
                    $messageAndroid["title"] = $title;
                }                    
                $gcm = new GCM();
                $registatoin_ids = array($request['deviceToken']);
                $result = $gcm -> send_notification($registatoin_ids, $messageAndroid);                
            }
        }
       
       
}