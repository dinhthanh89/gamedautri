<?php

/**
 * This is the model class for table "game".
 *
 * The followings are the available columns in table 'game':
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $description
 * @property string $rule 
 * @property string $url
 * @property string $vote 
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property GameDownload[] $gameDownloads
 * @property GameImageLink[] $gameImageLinks
 */
class Game extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Game the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'game';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, status', 'required'),
            array('category_id, status', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 256),
            array('description, rule, url, vote', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, category_id, description, rule, url, vote, status, code_identifier', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'gamePlatform' => array(self::HAS_MANY, 'GamePlatform', 'game_id'),                        
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category',
            'description' => 'Description',
            'rule' => 'Rule',            
            'url' => 'Url',
            'vote' => 'Vote',            
            'status' => 'Status',
            'code_identifier' => 'Code Identifier',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('category_id', $this->category_id);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('rule', $this->rule, true);        
        $criteria->compare('url', $this->url, true);
        $criteria->compare('vote', $this->vote, true);        
        $criteria->compare('status', $this->status);
        $criteria->compare('code_identifier', $this->code_identifier);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function getCurrentVersion($platform_name = "web", $partner_id = 1) {            
        $platformModel = Platform::model()->find('code_identifier=:code_identifier', array(':code_identifier'=>$platform_name));
        if(!isset($platformModel))
            return FALSE;
        $gamePlatformModel = GamePlatform::model()->find('game_id=:game_id AND platform_id=:platform_id', array(':game_id'=>$this->id, ':platform_id'=>$platformModel->id));           
        if(!isset($gamePlatformModel))
            return FALSE;
        $criteria = new CDbCriteria;
        $criteria->addCondition('game_platform_id =:game_platform_id');        
        $criteria->addCondition('enabled = 1 AND partner_id = '.$partner_id);
        $criteria->params = array(':game_platform_id'=>$gamePlatformModel->id);        
        $version = GameVersion::model()->find($criteria);
        if (!isset($version))
            return FALSE;                    
        return $version;
    }
    
    public function getCountDownload(){
        $total = 0;
        $criteria = new CDbCriteria;
        $criteria->alias = "game_version";         
        $criteria->join .= ' INNER JOIN game_platform ON game_version.game_platform_id = game_platform.id';
        $criteria->join .= ' INNER JOIN game ON game.id = game_platform.game_id';
        $criteria->addCondition(' game_version.partner_id = 1 AND game.id = '.$this->id);        
        $models = GameVersion::model()->findAll($criteria);
        if(!isset($models))
            return FALSE;
        foreach($models as $model){
            $total += $model->download_times;
        }
        return $total;
    }
    
    public function getCurrentVersionDetail(){
        $criteria = new CDbCriteria;
        $criteria->alias = "game_version";        
        $criteria->join .= ' INNER JOIN game_platform ON game_version.game_platform_id = game_platform.id';
        $criteria->join .= ' INNER JOIN game ON game.id = game_platform.game_id';
        $criteria->join .= ' INNER JOIN platform ON platform.id = game_platform.platform_id';
        $criteria->addCondition(' (platform.code_identifier = "ios" OR platform.code_identifier="android") AND game_version.enabled = 1 AND game_version.visible = 1 AND partner_id = 1 AND game.id='.$this->id);
        $models = GameVersion::model()->findAll($criteria);        
        if(!isset($models))
            return FALSE;
        else
            return $models;
    }   
}