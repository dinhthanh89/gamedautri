<?php

/**
 * This is the model class for table "user_message".
 *
 * The followings are the available columns in table 'user_message':
 * @property integer $id
 * @property integer $sender
 * @property integer $receiver
 * @property string $content
 * @property string $time_sent
 * @property integer $read
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property User $receiver0
 * @property User $sender0
 */
class UserMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sender, receiver, content, time_sent', 'required'),
			array('sender, receiver, read, status', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, sender, receiver, content, time_sent, read, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'receiver0' => array(self::BELONGS_TO, 'User', 'receiver'),
			'sender0' => array(self::BELONGS_TO, 'User', 'sender'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender' => 'Sender',
			'receiver' => 'Receiver',
			'content' => 'Content',
			'time_sent' => 'Time Sent',
			'read' => 'Read',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sender',$this->sender);
		$criteria->compare('receiver',$this->receiver);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('time_sent',$this->time_sent,true);
		$criteria->compare('read',$this->read);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function sendUserMessage($userID,$content,$showPopup){
            $message = new UserMessage();
            $user = User::model()->find('username = "system"');
            $message->sender = $user->id;
            $message->sender_name = $user->username;
            $user = User::model()->findByPk($userID);
            $message->receiver_name = $user->username;
            $message->receiver = $userID;
            $message->content = $content;
            $message->read = 0;
            $message->time_sent = date('Y-m-d H:i:s', time());
            $message->status = 1;
            if(!isset($showPopup)){
                $showPopup = 0;
            }
            if ($message->save()) {
                $electroServer = new ElectroServerBridge();
                $electroServer->sendUserMessage($user->username,$content, $showPopup);
                return true;
            } else {
                return false;
            }
        }
        
        public function sendMessageForAll($content,$showPopup){
//            $criteria = new CDbCriteria();
//            $criteria->condition = 'id != 0';
//            $model = User::model()->findAll($criteria);
//            $error = 0;
//            $values = array();
//            foreach ($model as $user) {
//                $str = '(0, "system", ' . $user->id . ', "' . $user->username . '","' . $content . '","' . date("Y-m-d H:i:s") . '", 1 )';
//                array_push($values, $str);
//            }
//            $values = implode(',', $values);
//            $sql = 'INSERT INTO user_message (`sender`, `sender_name`, `receiver`, `receiver_name`, `content`, `time_sent`, `status`) VALUES ' . $values;
//            $command = Yii::app()->db->createCommand($sql);
//            if ($command->execute()) {
                
                if(!isset($showPopup)){
                    $showPopup = 0;
                }
                $electroServer = new ElectroServerBridge();
                $electroServer->sendMessage($content, $showPopup);
                return true;
//            } else {
//                return false;
//            }
        }
        
        
            
}