<?php

/**
 * This is the model class for table "user".
 * * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $avatar
 * @property string $first_name
 * @property string $last_name
 * @property string $middle_name
 * @property string $birthday
 * @property integer $gender
 * @property string $identity_card_number
 * @property string $mobile
 * @property string $address
 * @property integer $facebook_id
 * @property integer $google_id
 * @property integer $yahoo_id
 * @property integer $twitter_id
 * @property integer $user_money
 * @property string $salt
 * @property date $create_time
 * @property string $gold
 * @property string $chip
 * @property string $repeat_password 
 * @property string $init_password   
 * @property string $access_token
 * The followings are the available model relations:
 * @property UserAction[] $userAction
 * The followins are the user defined
 */
class User extends CActiveRecord {

// use to confirm password
    public $repeat_password;
    public $init_password;
    public $access_token;
    public $avatarLink;
    protected $saltLength = 32;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('create_time', 'match', 'pattern' => '/^([0-9]{2,4})-([0-1][0-9])-([0-3][0-9]) (?:([0-2][0-9]):([0-5][0-9]):([0-5][0-9]))?$/'),           
            array('email', 'length', 'max' => 128),
            array('username', 'length', 'max' => 32),            
            array('gold, chip', 'length', 'max' => 20),
            // The following rule is used by search()
            // Please remove those attributes that should not be searched.
            array('id, username, first_name, last_name, middle_name, gender,identity_card_number, mobile, user_money, email, repeat_password, init_password,  gold, chip', 'safe', 'on' => 'search'),
            // Rules of dev
            array('email', 'email', 'message' => 'Email không hợp lệ'),
            //array('email', 'unique', 'on' => 'register', 'message' => 'Email đã đăng ký'),
            // Rules of register
            array('repeat_password', 'compare', 'compareAttribute' => 'init_password', 'message' => 'Password gõ lại không khớp', 'on' => 'register'),
            array('username, init_password, repeat_password', 'required', 'message' => 'Không được để trống trường này', 'on' => 'register'),
            array('username, identity_card_number, facebook_id, google_id, yahoo_id, twitter_id', 'unique', 'on' => 'register'),           
            // Rules of login
            array('username, init_password', 'required', 'message' => 'Không được để trống trường này', 'on' => 'login'),
            // Rules of insert
            array('repeat_password, init_password', 'required', 'on' => 'insert'),
            array('email', 'required', 'on' => 'forgotPassword', 'message' => 'Không được để trống trường này'),
            array('email', 'email', 'on' => 'forgotPassword', 'message' => 'Email không hợp lệ'),
            array('username, password', 'required', 'on' => 'login', 'message'=>'Không được để trống trường này'),
            array('username','match', 'not' => true,'on'=>'register', 'pattern' => '/[^a-zA-Z0-9_]/','message' => 'Tên tài khoản không được phép chứa các kí tự đặc biệt'),
            array('username', 'notIn', 'on' => 'register', 'message'=>'Tên username không hợp lệ'),
            array('username, password', 'required', 'on' => 'apiUpdateInfo'),
            array('username, init_password, repeat_password', 'required', 'on' => 'registerOpenId', 'message' => 'Không được để trống trường này'),
            array('username', 'unique', 'on' => 'registerOpenId'),
            array('username', 'length', 'min' => 4, 'max' => 32, 'tooShort' => 'Tên đăng nhập quá ngắn (4-32 ký tự)', 'tooLong' => 'Tên đăng nhập quá dài (4-32 ký tự)'),
            array('init_password', 'length', 'min' => 6, 'max' => 32, 'tooShort' => 'Mật khẩu quá ngắn (6-32 ký tự)', 'tooLong' => 'Mật khẩu quá dài (6-32 ký tự)'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'userAction' => array(self::HAS_MANY, 'UserAction', 'user_id'),
            'accessToken' => array(self::HAS_MANY, 'UserAccessToken', 'user_id'),
            'userAvatar' => array(self::BELONGS_TO, 'File', 'avatar'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'username' => 'Username',
            'email' => 'Email',
            'password' => 'Password',
            'init_password' => 'Password',
            'repeat_password' => 'Repeat password',
            'salt' => 'Salt',
            'gold' => 'Gold',
            'chip' => 'Chip',
            'first_name' => 'Tên',
            'last_name' => 'Họ',
            'middle_name' => 'Tên đệm',
            'mobile' => 'Số điện thoại',
            'identity_card_number' => 'Số chứng minh thư',
            'facebook_id' => 'Facebook ID',
            'google_id' => 'Google ID',
            'yahoo_id' => 'Yahoo ID',
            'twitter_id' => ' Twitter ID',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('username', $this->username, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('salt', $this->salt, true);
        $criteria->compare('gold', $this->gold, true);
        $criteria->compare('chip', $this->chip, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function unique($attribute, $params) {
        if (isset($params['message']))
            $error = $params['message'];
        else
            $error = $this->getLabel($attribute) . ' đã tồn tại';
        if ($this->find($attribute . '=:value', array(':value' => $this->$attribute))) {
            $this->addError($attribute, $error);
        }
    }

    /**
     * 
     * @param type $attribute
     * @param type $params
     */
    public function notIn($attribute, $params) {        
        //Patterns
        $error = $this->checkNotIn($this->getValue($attribute));                   
        if ($error == 1) {
            if (isset($params['message']))
                $error = $params['message'];
            else
                $error = $this->getLabel($attribute) . ' không hợp lệ';
            $this->addError($attribute, $error);
        }
    }
    
    public function checkNotIn($value){
        $error = ''; 
        $patterns = explode(",", Configuration::model()->getConfig('denied_username'));                    
        foreach ($patterns as $patern) {            
            if (preg_match($patern, $value)) {
                $error = 1;
                break;
            }
        }
        return $error;
    }

    public function createSalt() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()!@#$%^&*()!@#$%^&*()";
        $str = '';
        $size = strlen($chars);
        for ($i = 0; $i < $this->saltLength; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    public function getLabel($attribute) {
        $labels = $this->attributeLabels();
        return $labels[$attribute];
    }        
    
    public function getValue($attribute){
        return $this->attributes[$attribute];
    }

    public function is_email_exists() {
        if (User::model()->find('email=:email', array(':email' => $this->email)))
            return TRUE;
        else
            return FALSE;
    }

    public function is_user_exists() {
        if (User::model()->find('username=:username', array(':username' => $this->username)))
            return TRUE;
        else
            return FALSE;
    }

    public function register() {
        //Kiểm tra điều kiện user và password giống nhau
        if ($this->init_password != $this->repeat_password)
            return FALSE;
        //Tạo salt
        $salt = MyFunction::rand_string(32);
        $this->salt = $salt;
        //Tạo password hash và lưu thành password
        $this->password = hash('sha512', $this->init_password . $salt);
        //Set gold
        $this->gold = 0;
        //Set chip
        $this->chip = 0;
        date_default_timezone_set('Asia/Bangkok');
        $this->create_time = date('Y-m-d H:i:s', time());
        //Nếu lưu thành công
        if (!$this->validate())
            return FALSE;
        if ($this->save()) {                                    
            $model = Configuration::model()->find('name="user_chip_register"');
            $chip = (int) $model->value;
            $this->addChipToUser($this->id,$chip);
            $this->changeChipSystem($chip, 'minus');
            return $this->primaryKey;
        }
        else{
            return FALSE;          
        }
    }
    
    public function addChipToUser($user_id,$chip){            
        if(!is_int($chip))
            return false;        
        $user = self::model()->findByPk($user_id);
        $currentChip = $user->chip;        
        if($chip == 0){
            return true;
        }else{
            $transaction = new Transaction();
            $transaction->user_id = $user->id;
            $transaction->value = $chip;
            $transaction->type = 12;
            $transaction->save();                        
            
            $user->chip = $currentChip + $chip;
            if($user->save()){
                return true;
            }
            else
                return false;
        }
    }
    
    public function changeChipSystem($chip, $type){
        $system = self::model()->find('username="system"');
        $currentSystemChip = $system->chip;
        if($type == 'add')
            $system->chip = $currentSystemChip + $chip;        
        if($type == 'minus')
            $system->chip = $currentSystemChip - $chip;                
        $system->save();
    }
    
    public function changeGoldSystem($gold, $type){
        $system = self::model()->find('username="system"');
        $currentSystemChip = $system->gold;
        if($type == 'add')
            $system->gold = $currentSystemChip + $gold;        
        if($type == 'minus')
            $system->gold = $currentSystemChip - $gold;                
        $system->save();
    }
    
    public function getUserUpdated() {
        $user = self::model()->currentUser();
        if ($user == NULL)
            return NULL;
        else {
            $newUser = self::model()->findByPk($user->id);
            $newUser->updateUserToSession();
            return $newUser;
        }
    }
    
    public function updateUserToSession($remember = FALSE) {
        $session = new CHttpSession();
        $session->open();
        if ($remember)
            $session->timeout = 20 * 3600 * 24;
        $user = $this->model()->findByPk($this->id);
        if (!isset($user))
            return FALSE;
        else
            $session['user'] = $user;
        return TRUE;
    }

    /**
     * Dang nhap va set session cho viec dang nhap user
     * 
     */
    public function login($username = NULL, $password = NULL) {
        // Login with user
        if (!$username && !$password) {
            if ($this->model()->exists()) {
                $session = new CHttpSession;
                $session->open();
                $session['user'] = $this;  
                $ip = $_SERVER['REMOTE_ADDR'];
                $accessToken = UserAccessToken::model()->createLoginAccessToken($this->id, $ip);
                $session['accessToken'] = $accessToken;     
                Yii::app()->request->cookies['accessToken'] = new CHttpCookie('accessToken', $accessToken);                                
                
                return TRUE;
            }
            else
                return FASLE;
        }
        // Check user
        else {
            $user = User::model()->getUser($username, $password);
            if ($user == NULL)
                return FALSE;
            else {                                
                return $user->login();                
            }
        }
    }
    
    public function loginForum($username,$password){
        // login forum
        Common::includeBridgeVBB();
        $forum = new vBulletin_Bridge();                                
        $userdata = array();
        $userdata['username'] = $username;
        $userdata['password'] = $password;                          
        $userInfo = $forum->checkUser($username);                           
        if($userInfo){
            $errmsg = $forum->login($userdata);
        }else{
            $userEsimo = User::model()->find("username='".$username."'");
            if(isset($userEsimo)){
                if(isset($userEsimo->email)){
                    $userdata['email'] = $userEsimo->email;
                    $forum->register_newuser($userdata);
                    $errmsg = $forum->login($userdata);
                }else{
                    $session = new CHttpSession;
                    $session->open();
                    $session['username_esimo'] = $username;
                    $session['password_esimo'] = $password;                
                    $session['login_esimo'] = 1;
                }
            }
        }
    }

    /**
     * 
     * @param string $username
     * @param string $password
     * @return string $status 
     */
    public function getUser($username, $password) {
        $user = User::model()->find('username=:username', array(':username' => $username));
        if ($user == NULL)
            return NULL;
        else
            $salt = $user->salt;    
        $hashPassword = hash('sha512', $password . $salt);
        if ($hashPassword == $user->password)
            return $user;
        else
            return NULL;
    }

    /**
     * 
     * @param type $password
     * @return boolean
     */
    public function checkPassword($password) {
        if ($this->getUser($this->username, $password))
            return TRUE;
        else
            return FALSE;
    }

    /**
     * 
     * @param type string
     * @return string hash 
     */
    public function createHash($string) {
        return hash('sha512', $string);
    }

    /**
     * 
     * @param type new password
     * @return boolean
     */
    public function changePassword($newPassword) {
        $newHashPassword = self::model()->createHash($newPassword . $this->salt);
        $this->password = $newHashPassword;
        if ($this->save()) {
            // change password forum
            //Common::includeBridgeVBB();
            $userdata = array();
            $userdata['username'] = $this->username;
            $userdata['password'] = $newPassword;   
//            $forum = new vBulletin_Bridge();                    
//            $forum->update_user($userdata);
            return TRUE;
        }
        return FALSE;
    }

    /**
     * 
     * @param type infomation User
     * @return boolean
     */
    public function islogin() {
        $session = new CHttpSession();
        $session->open();
        if ($session['user'] == NULL)
            return FALSE;
        else
            return TRUE;
    }

    public function changeInformation($info) {
        if ($info['firstname'] && $info['middlename'] && $info['lastname']) {
            $this->first_name = $info['firstname'];
            $this->middle_name = $info['middlename'];
            $this->last_name = $info['lastname'];
        }
        if ($info['birthday'])
            $this->birthday = $info['birthday'];
        if ($info['gender']) 
            $this->gender = $info['gender'];
        if ($info['email'])
            $this->email = $info['email'];
        if ($info['cmt'])
            $this->identity_card_number = $info['cmt'];
        if ($info['phone'])
            $this->mobile = $info['phone'];
        if ($info['address'])
            $this->address = $info['address'];
        if (!$this->validate())
            return false;
        else {
            $this->save();
            return $this;
        }
    }

    public function getOpenIdUser($type, $id) {
        if ($type == 1)
            $attribute = 'facebook_id';
        if ($type == 2)
            $attribute = 'google_id';
        if ($type == 3)
            $attribute = 'yahoo_id';
        if ($type == 4)
            $attribute = 'twitter_id';
        $user = User::model()->find($attribute . '=:value', array(':value' => $id));
        return $user;
    }

    /**
     * return Avatar
     */
    public function getAvatar() {
        if ($this->avatar != NULL) {
            $file = File::model()->findByPk($this->avatar);
            if ($file)
                return $file->getDirectImageLink();
        } else {
            $url = Configuration::model()->getConfig('default_user_avatar');
            if (preg_match('/^http:\/\/*/', $url)) {
                return $url;
            }
            else
                return Yii::app()->createAbsoluteUrl($url);
        }
    }

    /**
     * return String Fullname
     */
    public function getFullName($type = NULL) {
        if($this->last_name == NULL && $this->middle_name == NULL && $this->first_name == NULL)
            return $this->username;
        switch ($type) {
            default: return $this->last_name . ' ' . $this->middle_name . ' ' . $this->first_name;
        }
    }

    /**
     * return String Gender
     */
    public function getGender() {
        if ($this->gender == 'male')
            return 'Nam';
        elseif ($this->gender == 'female')
            return 'Nữ';
        else
            return '';
    }

    /**
     * return String Address
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * return String Chip
     */
    public function getChip() {        
        require_once "curlhelper.php";
        $curlHelper = new cURL();
        $url = 'http://chieuvuong.com:1903/get_user_info?users='.$this->username;
        //$url = 'http://beta.esimo.vn:1903/get_user_info?users='.$this->username;
        //$url = 'http://10.0.2.58:1903/get_user_info?users='.$this->username;
                
        $result = $curlHelper->get($url);
        if(isset($result)){
            $result = json_decode($result);    
            if(isset($result->user_infos[0]))
                return Yii::app()->format->formatNumber($result->user_infos[0]->chip);
            else {
                return Yii::app()->format->formatNumber($this->chip);
            }
        }else
            return Yii::app()->format->formatNumber($this->chip);
    }

    /**
     * return String Gold
     */
    public function getGold() {
        require_once "curlhelper.php";
        $curlHelper = new cURL();
        $url = 'http://chieuvuong.com:1903/get_user_info?users='.$this->username;
        //$url = 'http://beta.esimo.vn:1903/get_user_info?users='.$this->username;
        //$url = 'http://10.0.2.58:1903/get_user_info?users='.$this->username;
                
        $result = $curlHelper->get($url);
        if(isset($result)){
            $result = json_decode($result);    
            if(isset($result->user_infos[0]))
                return Yii::app()->format->formatNumber($result->user_infos[0]->gold);
            else {
                return Yii::app()->format->formatNumber($this->gold);
            }
        }else
            return Yii::app()->format->formatNumber($this->gold);        
    }

    public function checkUserWithOpenId($provider, $userProviderId) {
        if ($provider == 'facebook') {
            $user = $this->model()->find('facebook_id=:id', array(':id' => $userProviderId));
//            var_dump($user) ;
        }
        if ($provider == 'google') {
            $user = $this->model()->find('google_id=:id', array(':id' => $userProviderId));
        }
        if ($provider == 'twitter') {
            $user = $this->model()->find('twitter_id=:id', array(':id' => $userProviderId));
        }
        if ($user == NULL)
            return FALSE;
        else
            return TRUE;
    }

    public function checkEmailExist($email) {
        $user = $this->model()->find('email=:email', array(':email' => $email));
        if ($user == NULL)
            return FALSE;
        else
            return TRUE;
    }
    
    public function checkUsernameExist($username) {
        $user = $this->model()->find('username=:username', array(':username' => $username));
        if ($user == NULL)
            return FALSE;
        else
            return TRUE;
    }

    public function getUserWithOpenId($provider, $userProviderId) {
        if ($provider == 'facebook') {
            $user = $this->model()->find('facebook_id = :id', array(':id' => $userProviderId));
        }
        if ($provider == 'google') {
            $user = $this->model()->find('google_id=:id', array(':id' => $userProviderId));
        }
        if ($provider == 'twitter') {
            $user = $this->model()->find('twitter_id=:id', array(':id' => $userProviderId));
        }
        return $user;
    }

    public function loginWithOpenId($provider, $userProviderId) {
        if ($provider == 'facebook') {
            $user = $this->model()->find('facebook_id=:id', array(':id' => $userProviderId));
        }
        if ($provider == 'google') {
            $user = $this->model()->find('google_id=:id', array(':id' => $userProviderId));
        }
        if ($provider == 'twitter') {
            $user = $this->model()->find('twitter_id=:id', array(':id' => $userProviderId));
        }
        if ($user == NULL)
            return FALSE;
        else {
            $user->login();
            return $user;
        }
    }

    public function getUserFromUsernameAndPassword($username, $password) {
        $user = User::model()->find('username=:username', array(':username' => $username));
        if ($user == NULL)
            return NULL;
        else
            $salt = $user->salt;
        $hashPassword = self::model()->createHash($password . $salt);
        //echo $hashPassword . '<br>';
        //echo $user->password;
        if ($hashPassword == $user->password)
            return $user;
        else
            return NULL;
    }

    public function mergeOpenId($username, $password, $type, $id) {
        $user = self::model()->getUserFromUsernameAndPassword($username, $password);
        $provideIdVriable = $type . '_id';
        if ($user == NULL)
            return FALSE;
        if (isset($user->$provideIdVriable))
            return FALSE;
        else
            $user->$provideIdVriable = $id;
        if ($user->save()) {
            //var_dump($user->login(1));
            if ($user->login(1)) {
                return TRUE;
            } else {
                return FALSE;
            }            
        }
        else
            return FALSE;
    }

    public function registerWithOpenId($provider, $userProfile, $username, $password) {
        $user = new User('registerWithOpenId');
        if ($provider == 'facebook') {
            $user->facebook_id = $userProfile['id'];
            if (isset($userProfile['first_name']))
                $user->first_name = $userProfile['first_name'];
            if (isset($userProfile['last_name']))
                $user->last_name = $userProfile['last_name'];
            if (isset($userProfile['middle_name']))
                $user->middle_name = $userProfile['middle_name'];
            if (isset($userProfile['email']))
                if (!$this->checkEmailExist($userProfile['email'])) {
                    $user->email = $userProfile['email'];
                }
            $user->salt = $this->createSalt();
            $user->username = $username;
            $user->password = $this->createHash($password . $user->salt);
            if (!$user->validate()) {                
                return NULL;
            }
            if ($user->save()) {
                $model = Configuration::model()->find('name="user_chip_register"');
                $chip = (int) $model->value;
                $this->addChipToUser($user->id,$chip);    
                $this->changeChipSystem($chip, 'minus');
                return $user;
            }
            else
                return NULL;
        }
        if ($provider == 'google') {
            $user->google_id = $userProfile['id'];
            if (isset($userProfile['given_name']))
                $user->first_name = $userProfile['given_name'];
            if (isset($userProfile['family_name']))
                $user->last_name = $userProfile['family_name'];
            if (isset($userProfile['middle_name']))
                $user->middle_name = $userProfile['middle_name'];
            if (!$this->checkEmailExist($userProfile['email']))
                $user->email = $userProfile['email'];
            $user->salt = $this->createSalt();
            $user->username = $username;
            $user->password = $this->createHash($password . $user->salt);
            if (!$user->validate()) {
//                echo 'validate that bai';
                return FALSE;
            }
            if ($user->save()){                                                                
                $model = Configuration::model()->find('name="user_chip_register"');
                $chip = (int) $model->value;
                $this->addChipToUser($user->id,$chip);
                $this->changeChipSystem($chip, 'minus');
                return $user;
            }
            else
                return FALSE;
        }
    }

    public function suggestUser($username) {
        $i = 0;
        $user = self::model()->find('username=:username', array(':username' => $username));        
        if (!isset($user)) {
            $username = $username;
        } else {
            $username = $user->username . '_1';
        }
        return $username;
    }

    public function currentUser() {
        $session = new CHttpSession();
        $session->open();
        if (isset($session['user'])) {
            $user = $session['user'];
            $newUser = self::model()->findByPk($user->id);
            $session['user'] = $newUser;
            return $newUser;
        }
        else
            return NULL;
    }

    public function getCurrentUserName() {
        if ($this->currentUser())
            return $this->currentUser()->username;
        else
            return NULL;
    }
    
    public function convertGoldByChip($gold){
        $user = User::model()->findByPk($this->id);
        if($user->gold < $gold)
           return FALSE;
        $ratio = Configuration::model()->getConfig('gold_by_chip');       
        $transaction = new Transaction();
        $transaction->user_id = $user->id;
        $transaction->value = intval($gold * $ratio);
        $transaction->type = 10;
        if($transaction->save())
        {
            $convertGoldToChip = new ConvertGoldToChip();
            $convertGoldToChip->transaction_id = $transaction->id;
            $convertGoldToChip->gold = $gold;
            $convertGoldToChip->ratio = $ratio;
            if($convertGoldToChip->save()){
                $user->gold = $user->gold - $gold;
                $user->chip = $user->chip + $transaction->value;                
                if($user->save()){
                    $this->changeChipSystem($transaction->value, 'add');
                    return $transaction->value;
                }
                else
                    return FALSE;
            }else{                
                return FALSE;
            }
        }else{            
            return FALSE;
        }
    }
    
    public function rechargeMoney($money, $type = 1) {        
        $user = User::model()->findByPk($this->id);       
        
        $rechargeType = RechargeType::model()->findByPk($type);    
        
        $userPartnerModel =  UserPartner::model()->find('user_id=:user_id',array(':user_id'=>$this->id));
        if(isset($userPartnerModel))
            $partnerModel = Partner::model()->find('id=:partner_id',array(':partner_id'=>$userPartnerModel->partner)); 
        if(!isset($partnerModel)){
            $partnerModel = Partner::model()->find('code_identifier=:partner_code_identifier',array(':partner_code_identifier'=>'esimo'));
        }                        
        
        $recharge = new Recharge();
        $recharge->user_id = $user->id;
        $recharge->recharge_type_id = $type;
        $recharge->ratio = $rechargeType->ratio;
        $recharge->ratio_partner = $partnerModel->ratio;
        $recharge->value = $money;
        $recharge->gold = ceil($money * $rechargeType->ratio);
        $recharge->status = 1;
        if($recharge->save()){
            
            $currentMoney = $user->gold;       
            if (!$currentMoney)
                $currentMoney = 0;
            try {                
                $user->gold = $currentMoney + $recharge->gold;                
                $user->save();
                return $recharge->gold;
            } catch (Exception $e) {                        
                return FALSE;
            }
        }else{            
            return FALSE;
        }                                        
    }
    
    public function getAdminPartner(){
        $userRoleModel = UserRole::model()->find('user_id=:user_id',array(':user_id'=>$this->id));
        if(!isset($userRoleModel))
            return False;
        $partnerModel = Partner::model()->findByPk($userRoleModel->role->partner_id);
        if(!isset($partnerModel))
            return FALSE;
        return $partnerModel;
    }   
    
    public function checkUserGameAction($name){
        $criteria=new CDbCriteria;     
        $criteria->alias = 'user_game_action';                  
        $criteria->join = 'INNER JOIN game_action on game_action.id = user_game_action.game_action_id';  
        $criteria->addCondition('game_action.code_identifier="'.$name.'" AND user_id='.$this->id);
        $userGameAction = UserGameAction::model()->find($criteria);                
        if($userGameAction) return TRUE;
        else return FALSE;
    }    
    
    public function userGameAction($name){        
        $gameAction = GameAction::model()->find('code_identifier="'.$name.'"');        
        if(isset($gameAction)){
            $userGameAction = new UserGameAction();
            $userGameAction->user_id = $this->id;
            $userGameAction->game_action_id = $gameAction->id;
            if($userGameAction->save())
                return $userGameAction->id;
            else
                return FALSE;
        }else{
            return FALSE;
        }
    }
    
    public function bonusChips($user_game_action_id,$userGold){        
        // promotion
        $userChipBonus = 0;        
        $promotionRule = $this->getPromotionRule('recharge',2);
        if(isset($promotionRule)){
            $value = $promotionRule->value;
        }else{
            return $userChipBonus;
        }
        $bonus = $userGold * $value;         
        $promotionActive = $this->getPromotionActive($promotionRule->promotion_id);
        if($promotionActive){
            $promotion_active_id = $promotionActive->id;
            $userChipBonus = $this->addPromotionChip($user_game_action_id, $promotion_active_id, $bonus);        
        }
        
        return $userChipBonus;
        
    }
    
    public function bonusChipByLikeFacebook($user_game_action_id){      
        // promotion
        $userChipBonus = 0;        
        $promotionRule = $this->getPromotionRule('like_facebook',3);
        if(isset($promotionRule)){
            $value = $promotionRule->value;
        }else{
            return $userChipBonus;
        }
        $bonus = $value;         
        $promotionActive = $this->getPromotionActive($promotionRule->promotion_id);
        if($promotionActive){
            $promotion_active_id = $promotionActive->id;
            $userChipBonus = $this->addPromotionChip($user_game_action_id, $promotion_active_id, $bonus);        
        }
        
        return $userChipBonus;                
    }
    
    public function bonusChipByInviteFacebook($user_game_action_id){     
        
        // promotion
        $userChipBonus = 0;        
        $promotionRule = $this->getPromotionRule('invite_facebook',4);
        if(isset($promotionRule)){
            $tmp = json_decode($promotionRule->value);
            if($tmp){
                $value = $tmp->value;                    
            }else{
                $value = 0;
            }
        }else{
            return $userChipBonus;
        }
        $bonus = $value;         
        $promotionActive = $this->getPromotionActive($promotionRule->promotion_id);
        if($promotionActive){
            $promotion_active_id = $promotionActive->id;
            $userChipBonus = $this->addPromotionChip($user_game_action_id, $promotion_active_id, $bonus);        
        }
        return $userChipBonus; 
                
    }
    
    public function bonusChipsByLogin($user_game_action_id){
        // promotion
        $userChipBonus = 0;        
        $promotionRule = $this->getPromotionRule('login',5);           
        if(isset($promotionRule)){
            $tmp = json_decode($promotionRule->value);
            $userModel = User::model()->findByPk($this->id);
            if($tmp){
                if(strtotime($userModel->create_time) < strtotime($tmp->user_create_time))            
                    $value = $tmp->value;                    
                else
                    $value = 0;
            }else{
                $value = 0;
            }            
        }else{
            return $userChipBonus;
        }           
        $bonus = $value;        
        $promotionActive = $this->getPromotionActive($promotionRule->promotion_id);
        if($promotionActive){
            $promotion_active_id = $promotionActive->id;            
            $userChipBonus = $this->addPromotionChip($user_game_action_id, $promotion_active_id, $bonus);        
        }
        
        return $userChipBonus;
    }
    
    public function addPromotionChip($user_game_action_id,$promotion_active_id,$value){
        if($value == 0) return $value;
        $promotionLog = new PromotionLog();
        $promotionLog->user_game_action_id = $user_game_action_id;
        $promotionLog->promotion_active_id = $promotion_active_id;
        $promotionLog->value = $value;        
        if($promotionLog->save()){
            $userModel = User::model()->findByPk($this->id);
            $currentMoney = $userModel->gold;       
            if (!$currentMoney)
                $currentMoney = 0;                                              
            $userModel->gold = $currentMoney + $value;                
            $userModel->save();                                    

            $userChipBonus = $userModel->convertGoldByChip($value);            
            return $userChipBonus;
        }else{
            return 0;
        }    
        
    }
    
    public function getPromotionRule($game_action, $promotion_id){
        // promotion                
        $gameAction = GameAction::model()->find('code_identifier="'.$game_action.'"');        
        if(isset($gameAction)){
            $promotionActive = $this->getPromotionActive($promotion_id);
            if(isset($promotionActive)){
                $criteria2 = new CDbCriteria;
                $criteria2->alias = "promotion_rule";        
                $criteria2->join = ' INNER JOIN promotion ON promotion_rule.promotion_id = promotion.id';
                $criteria2->join .= ' INNER JOIN game_action ON promotion_rule.game_action_id = game_action.id';
                $criteria2->addCondition('promotion.id = '.$promotion_id.' AND game_action.code_identifier = "'.$game_action.'"');
                $promotionRule = PromotionRule::model()->find($criteria2);
                return $promotionRule;               
            }else{
                return false;
            }
        }else{
            return false;
        }        
    }        
    
    public function getPromotionActive($promotion_id){
        $time = date('Y-m-d H:i:s');
        $criteria = new CDbCriteria;
        $criteria->alias = "promotion_active";        
        $criteria->join = ' INNER JOIN promotion ON promotion_active.promotion_id = promotion.id';        
        $criteria->addCondition('promotion.id = '.$promotion_id.' AND promotion_active.start_time < "'.$time.'" AND promotion_active.end_time > "'.$time.'"');
        $promotionActive = PromotionActive::model()->cache(300)->find($criteria);
        if(isset($promotionActive))
            return $promotionActive;
        else
            return false;
    }
}

?>