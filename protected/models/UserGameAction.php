<?php

/**
 * This is the model class for table "user_game_action".
 *
 * The followings are the available columns in table 'user_game_action':
 * @property integer $id
 * @property integer $user_id
 * @property integer $game_action_id
 * @property string $enviroment
 * @property string $version
 * @property string $timestamp
 * @property string $value
 *
 * The followings are the available model relations:
 * @property PromotionActive[] $promotionActives
 * @property GameAction $gameAction
 * @property User $user
 * @property UserPlayInTournamentMatch[] $userPlayInTournamentMatches
 */
class UserGameAction extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserGameAction the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_game_action';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, game_action_id', 'required'),
			array('user_id, game_action_id', 'numerical', 'integerOnly'=>true),
			array('enviroment, version', 'length', 'max'=>1000),
			array('value', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, game_action_id, enviroment, version, timestamp, value', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'promotionActives' => array(self::MANY_MANY, 'PromotionActive', 'promotion_log(user_game_action_id, promotion_active_id)'),
			'gameAction' => array(self::BELONGS_TO, 'GameAction', 'game_action_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'userPlayInTournamentMatches' => array(self::MANY_MANY, 'UserPlayInTournamentMatch', 'user_tournament_match_game_action(user_game_action_id, user_play_in_tournament_match_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'game_action_id' => 'Game Action',
			'enviroment' => 'Enviroment',
			'version' => 'Version',
			'timestamp' => 'Timestamp',
			'value' => 'Value',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('game_action_id',$this->game_action_id);
		$criteria->compare('enviroment',$this->enviroment,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('timestamp',$this->timestamp,true);
		$criteria->compare('value',$this->value,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}