<?php

/**
 * This is the model class for table "recharge_card_log".
 *
 * The followings are the available columns in table 'recharge_card_log':
 * @property integer $id
 * @property string $pin
 * @property string $seri
 * @property string $username
 * @property string $card_type
 * @property integer $value
 * @property string $timestamp
 */
class RechargeCardLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RechargeCardLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'recharge_card_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('pin, seri, username, card_type, value', 'required'),
			array('value', 'numerical', 'integerOnly'=>true),
			array('pin, seri, username, card_type', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, pin, seri, username, card_type, value, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pin' => 'Pin',
			'seri' => 'Seri',
			'username' => 'Username',
			'card_type' => 'Card Type',
			'value' => 'Value',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pin',$this->pin,true);
		$criteria->compare('seri',$this->seri,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('card_type',$this->card_type,true);
		$criteria->compare('value',$this->value);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}