<?php
class MTransaction extends EMongoDocument
    {
      public $id;
      public $user_id;
      public $username;
      public $type;
      public $game_log_id;
      public $value;
 
      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }
 
      // This method is required!
      public function getCollectionName()
      {
        return 'transaction';
      }
 
      public function rules()
      {
        return array(
                    
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'id'  => 'Id',
          'user_id'   => 'User Id',
          'username'   => 'Username',
          'type' => 'Type',
          'game_log_id'=> 'Game Log Id',
          'value'=> 'Value'
        );
      }
    }