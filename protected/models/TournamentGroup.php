<?php

/**
 * This is the model class for table "tournament_group".
 *
 * The followings are the available columns in table 'tournament_group':
 * @property integer $id
 * @property string $display_name
 * @property integer $tournament_active_id
 * @property integer $tournament_round_id
 * @property integer $max_player
 * @property string $start_time
 * @property string $end_time
 * @property integer $num_players_continuous
 * @property integer $max_matchs
 */
class TournamentGroup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TournamentGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tournament_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('display_name, tournament_active_id, num_players_continuous', 'required'),
			array('tournament_active_id, tournament_round_id, max_player, num_players_continuous, max_matchs', 'numerical', 'integerOnly'=>true),
			array('display_name', 'length', 'max'=>256),
			array('start_time, end_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, display_name, tournament_active_id, tournament_round_id, max_player, start_time, end_time, num_players_continuous, max_matchs', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'display_name' => 'Display Name',
			'tournament_active_id' => 'Tournament Active',
			'tournament_round_id' => 'Tournament Round',
			'max_player' => 'Max Player',
			'start_time' => 'Start Time',
			'end_time' => 'End Time',
			'num_players_continuous' => 'Num Players Continuous',
			'max_matchs' => 'Max Matchs',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('display_name',$this->display_name,true);
		$criteria->compare('tournament_active_id',$this->tournament_active_id);
		$criteria->compare('tournament_round_id',$this->tournament_round_id);
		$criteria->compare('max_player',$this->max_player);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('end_time',$this->end_time,true);
		$criteria->compare('num_players_continuous',$this->num_players_continuous);
		$criteria->compare('max_matchs',$this->max_matchs);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}