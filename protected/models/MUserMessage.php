<?php
class MUserMessage extends EMongoDocument
    {      
      public $sender;
      public $receiver;
      public $content;
      public $timestamp;      
 
      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }
 
      // This method is required!
      public function getCollectionName()
      {
        return 'user_message';
      }
 
      public function rules()
      {
        return array(
                    
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'id'  => 'Id',
          'sender'   => 'Sender',
          'receiver'   => 'Receiver',
          'status' => 'Status'
        );
      }
      
//      public function embeddedDocuments()
//      {
//        return array(
//          // property name => embedded document class name
//          'users'=>'MGameLogUser'
//        );
//      }            
       
        public function behaviors()
        {
          return array(
            array(
              'class'=>'ext.YiiMongoDbSuite.extra.EEmbeddedArraysBehavior',
              'arrayPropertyName'=>'sender', // name of property
              'arrayDocClassName'=>'MUserMessageSender' // class name of documents in array
            ),
            array(
              'class'=>'ext.YiiMongoDbSuite.extra.EEmbeddedArraysBehavior',
              'arrayPropertyName'=>'receiver', // name of property
              'arrayDocClassName'=>'MUserMessageReceiver' // class name of documents in array
            ),  
          );
        }
        
        public function sendUserMessage($userID,$content,$showPopup){
            $message = new MUserMessage();
            $user = User::model()->find('username = "system"');
            $message->sender[0] = new MUserMessageSender;
            $message->sender[0]->user_id = intval($user->id);
            $message->sender[0]->username = $user->username;
            $userReceiver = User::model()->findByPk($userID);
            $message->receiver[0] = new MUserMessageReceiver;
            $message->receiver[0]->user_id = intval($userReceiver->id);
            $message->receiver[0]->username = $userReceiver->username;
            $message->receiver[0]->read = 0;
            $message->receiver[0]->status = 1;
            $message->content = $content;           
            $message->timestamp = new MongoDate();                   
            if(!isset($showPopup)){
                $showPopup = 0;
            }
            if ($message->save()) {                
                $electroServer = new ElectroServerBridge();
                $electroServer->sendUserMessage($userReceiver->username,$content, $showPopup, $message['_id']->{'$id'});
                
//                $electroServer2 = new ElectroServerBridge('210.211.102.121');
//                $electroServer2->sendUserMessage($userReceiver->username,$content, $showPopup, $message['_id']->{'$id'});
                return true;
            } else {
                return false;
            }
        }
        
        public function sendMessageForAll($content,$showPopup){            
            $message = new MUserMessage();
            $user = User::model()->find('username = "system"');
            $message->sender[0] = new MUserMessageSender;
            $message->sender[0]->user_id = intval($user->id);
            $message->sender[0]->username = $user->username;
            /*
            $criteria = new CDbCriteria();
            $criteria->condition = 'id != 0';                    
            $criteria->select= "id,username";
            $model = User::model()->findAll($criteria);
            foreach($model as $index => $item){
                $message->receiver[$index] = new MUserMessageReceiver;
                $message->receiver[$index]->user_id = intval($item->id);
                $message->receiver[$index]->username = $item->username;
                $message->receiver[$index]->read = 0;
                $message->receiver[$index]->status = 1;
            }
            */
            $message->receiver = -1;
            $message->content = $content;           
            $message->timestamp = new MongoDate();     
            
            if(!isset($showPopup)){
                $showPopup = 0;
            }
            if ($message->save()) {
                $electroServer = new ElectroServerBridge();
                $electroServer->sendMessage($content, $showPopup, $message['_id']->{'$id'});
                return true;
            } else {
                return false;
            }                        
        }
        
      
    }