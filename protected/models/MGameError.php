<?php
class MGameError extends EMongoDocument
    {      
      public $users;
      public $game_version;
      public $app_id;
      public $scene;
      public $environment;
      public $time;
      public $error;
      public $detail;      
      public $debug_log;
 
      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }
 
      // This method is required!
      public function getCollectionName()
      {
        return 'game_error';
      }
 
      public function rules()
      {
        return array(
                    
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'id'  => 'Id',
          'user_id'   => 'User Id',
          'game_version'   => 'Game Version',
        );
      }
      
      public function behaviors()
        {
          return array(
            array(
              'class'=>'ext.YiiMongoDbSuite.extra.EEmbeddedArraysBehavior',
              'arrayPropertyName'=>'users', // name of property
              'arrayDocClassName'=>'MGameErrorUser' // class name of documents in array
            ),
          );
        }
    }