<?php

/**
 * This is the model class for table "file".
 * The followings are the available columns in table 'file':
 * @property integer $id
 * @property string $path
 * @property string $name
 * @property integer $status
 * @property string $create_time
 */
class File extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return File the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'file';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
// NOTE: you should only define rules for those attributes that
// will receive user inputs.
        return array(
            array('path', 'required'),
            array('id, status', 'numerical', 'integerOnly' => true),
            array('path', 'length', 'max' => 256),
            // The following rule is used by search().
// Please remove those attributes that should not be searched.
            array('id, path, name', 'safe', 'on' => 'search'),
            array('path', 'unique', 'on' => 'save'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
// NOTE: you may need to adjust the relation name and the related
// class name for the relations automatically generated below.
        return array(
            'downloadLinks' => array(self::HAS_MANY, 'GameDownloadLink', 'file_id'),
            'imageLinks' => array(self::HAS_MANY, 'GameImageLink', 'file_id'),
            'userAvatars' => array(self::HAS_MANY, 'User', 'avatar'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'path' => 'Path',
            'number_of_use' => 'Number Of Use',
            'status' => 'Status',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
// Warning: Please modify the following code to remove attributes that
// should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('path', $this->path, true);
        $criteria->compare('number_of_use', $this->number_of_use);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function checkDownload() {
        if (!file_exists($this->path))
            return FALSE;
        else
            return TRUE;
    }

    public function download() {
        if (!$this->checkDownload())
            return;
        // We'll be outputting a PDF
        header('Content-Description: File Transfer');
        header('Content-Type: text/plain');
        header('Content-Disposition: attachment; filename=' . basename($this->name));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->path));
        ob_clean();
        flush();
        readfile($this->path);
    }

    public function newFile($filePath, $name, $status = NULL) {
        date_default_timezone_set('Asia/Bangkok');
        if ($status == NULL)
            $status = 1;
        $file = new File();
        $file->path = $filePath;
        $file->create_time = date('Y-m-d H:i:s', time());
        $file->name = $name;
        $file->status = $status;        
        if ($file->save())
            return $file;
        else
            return NULL;
    }

    public function getFile($type) {
//Get file type from input
//Only alow some filetype
        $mimeType = '';
        switch ($type) {
            case 'image': $mimeType = 'image';
                break;
            case 'txt': $mimeType = "text/plain"; break;
            default : return FALSE;
        }
//Get file
        if (!$this->checkDownload())
            return;
        header('Content-Type: ' . $mimeType);
        header('Content-Length: ' . filesize($this->path));
        ob_clean();
        flush();
        readfile($this->path);
//file_put_contents($file->path);
    }

    public function getDirectImageLink() {                
        return Yii::app()->createAbsoluteUrl($this->path);
    }

    /**
     * 
     * @param type $uploadedFile
     * @param string $folder
     * @param type $name
     * @param int $status
     * @return File if success
     */
    public function newUploadedFile($uploadedFile, $folder = NULL, $name = NULL, $status = NULL) {
        $return = array();
        $fileExtAllow = array('jpg', 'jpeg', 'png', 'apk', 'ipa');
        $tmpFile = $uploadedFile['tmp_name'];
        $fileName = $uploadedFile['name'];
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
        if (!in_array($fileExt, $fileExtAllow)) {
            $return['code'] = -1;
            $return['message'] = 'File extension is not allowed';
        } else {
//Set variables
            if ($folder == NULL)
                $folder = Configuration::model()->getConfig('defaut_upload_folder');
            if ($name == NULL)
                $name = $fileName;
            if ($status == NULL)
                $status = 1;

            $newName = md5_file($tmpFile);
            $newFolder = substr($newName, 0, 2);
            $folder = $folder . '/' . $newFolder;
            if (!is_dir($folder))
                mkdir($folder);
            $path = $folder . '/' . $newName;
            if (move_uploaded_file($tmpFile, $path)) {
                $file = File::model()->newFile($path, $name, $status);
                if ($file) {
                    $return['code'] = 1;
                    $return['file'] = $file;
                } else {
                    $return['code'] = -3;
                    $return['message'] = 'Can not create new file';
                }
            } else {
                $return['code'] = -2;
                $return['message'] = 'Can not move file upload to folder';
            }
        }
        return $return;
    }

    public function resizeImage($path = NULL, $ratio = NULL, $height = NULL, $width = NULL) {
//Get old size
        list($oldWidth, $oldHeight) = getimagesize($this->path);
//Get path
        if ($path == NULL)
            $newPath = $this->path;
// Get new sizes
        if ($ratio) {
            $newWidth = $ratio * $oldWidth;
            $newHeight = $ratio * $oldHeight;
        } else {
            $newWidth = $width;
            $newHeight = $height;
        }
// Get type of image
        $info = getimagesize($this->path);
        $type = $info[2];
        $thumb = imagecreatetruecolor($newWidth, $newHeight);
        if ($type === IMAGETYPE_JPEG)
            $source = imagecreatefromjpeg($this->path);
        else if ($type === IMAGETYPE_PNG)
            $source = imagecreatefrompng($this->path);
        else
            return FALSE;
// Resize
        imagecopyresized($thumb, $source, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight); // Output
        if ($type === IMAGETYPE_JPEG)
            if (imagejpeg($thumb, $newPath))
                return TRUE;
            else
                return FALSE;
        else if ($type === IMAGETYPE_PNG)
            if (imagepng($thumb, $newPath))
                return TRUE;
            else
                return FALSE;
    }

    protected function _rand_name($length) {
        $chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        $str = '';
        $size = strlen($chars);
        for ($i = 0; $i < $length; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }

        return $str;
    }

    protected function _getPath($type) {
        switch ($type) {
            case 'user': return 'files/user';
            case 'system': return 'files/system';
            case 'feedback': return 'files/feedback';
        }
    }

    public function saveBinaryFile($fileContent, $folderType, $base64 = FALSE) {
        if ($base64 == TRUE)
            $binaryContent = base64_decode($fileContent);
        $folderPath = $this->_getPath($folderType);        
        $filePath = '';
        do {
            $fileName = $this->_rand_name(32);
            $filePath = $folderPath . '/' . substr($fileName, 0, 2) . '/' . $fileName;
        } while (file_exists($filePath));
        if (!is_dir($folderPath . '/' . substr($fileName, 0, 2)))
            mkdir($folderPath . '/' . substr($fileName, 0, 2));
        if (file_put_contents($filePath, $binaryContent)) { // Ghi file thanh cong
            $file = new File;
            $file->path = $filePath;
            $file->save();
            return $file;
        }
        else
            return NULL;
    }

    protected function _mkdirIfNotExits($baseFolder, $fileName) {
        $folder = $baseFolder . '/' . substr($fileName, 0, 2);
        if (!is_dir($folder))
            return mkdir($folder);
    }

}