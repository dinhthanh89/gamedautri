<?php

/**
 * This is the model class for table "game_platform".
 *
 * The followings are the available columns in table 'game_platform':
 * @property integer $id
 * @property integer $game_id
 * @property integer $platform_id
 * @property string $market_url
 * @property string $bundle_id
 *
 * The followings are the available model relations:
 * @property Game $game
 * @property Platform $platform
 * @property GameVersion[] $gameVersions
 */
class GamePlatform extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return GamePlatform the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'game_platform';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('game_id, platform_id', 'required'),
			array('game_id, platform_id', 'numerical', 'integerOnly'=>true),
			array('market_url, bundle_id', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, game_id, platform_id, market_url, bundle_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'game' => array(self::BELONGS_TO, 'Game', 'game_id'),
			'platform' => array(self::BELONGS_TO, 'Platform', 'platform_id'),
			'gameVersions' => array(self::HAS_MANY, 'GameVersion', 'game_platform_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'game_id' => 'Game',
			'platform_id' => 'Platform',
			'market_url' => 'Market Url',
			'bundle_id' => 'Bundle',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('game_id',$this->game_id);
		$criteria->compare('platform_id',$this->platform_id);
		$criteria->compare('market_url',$this->market_url,true);
		$criteria->compare('bundle_id',$this->bundle_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}