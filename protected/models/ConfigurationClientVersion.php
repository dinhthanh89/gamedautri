<?php

/**
 * This is the model class for table "configuration_client_version".
 *
 * The followings are the available columns in table 'configuration_client_version':
 * @property integer $configuration_client_id
 * @property integer $game_version_id
 * @property string $value
 *
 * The followings are the available model relations:
 * @property GameVersion $gameVersion
 * @property ConfigurationClient $configurationClient
 */
class ConfigurationClientVersion extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ConfigurationClientVersion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'configuration_client_version';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('configuration_client_id, game_version_id', 'required'),
			array('configuration_client_id, game_version_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('configuration_client_id, game_version_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'gameVersion' => array(self::BELONGS_TO, 'GameVersion', 'game_version_id'),
			'configurationClient' => array(self::BELONGS_TO, 'ConfigurationClient', 'configuration_client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'configuration_client_id' => 'Configuration Client',
			'game_version_id' => 'Game Version',
                        'value'=> 'Value'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('configuration_client_id',$this->configuration_client_id);
		$criteria->compare('game_version_id',$this->game_version_id);
                $criteria->compare('value',$this->value);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}