<?php

/**
 * This is the model class for table "promotion_log".
 *
 * The followings are the available columns in table 'promotion_log':
 * @property integer $user_game_action_id
 * @property integer $promotion_active_id
 * @property string $value
 * @property string $timestamp
 */
class PromotionLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PromotionLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'promotion_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_game_action_id, promotion_active_id', 'required'),
			array('user_game_action_id, promotion_active_id', 'numerical', 'integerOnly'=>true),
			array('value', 'length', 'max'=>255),
			array('timestamp', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_game_action_id, promotion_active_id, value, timestamp', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'userGameAction' => array(self::BELONGS_TO, 'UserGameAction', 'user_game_action_id'),
                    'promotionActive' => array(self::BELONGS_TO, 'PromotionActive', 'promotion_active_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_game_action_id' => 'User Game Action',
			'promotion_active_id' => 'Promotion Active',
			'value' => 'Value',
			'timestamp' => 'Timestamp',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_game_action_id',$this->user_game_action_id);
		$criteria->compare('promotion_active_id',$this->promotion_active_id);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('timestamp',$this->timestamp,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}