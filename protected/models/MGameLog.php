<?php
class MGameLog extends EMongoDocument
    {      
      public $game_id;
      public $result;      
      public $timestamp;
      public $users;
 
      // This has to be defined in every model, this is same as with standard Yii ActiveRecord
      public static function model($className=__CLASS__)
      {
        return parent::model($className);
      }
 
      // This method is required!
      public function getCollectionName()
      {
        return 'game_log';
      }
 
      public function rules()
      {
        return array(
                    
        );
      }
 
      public function attributeLabels()
      {
        return array(
          'id'  => 'Id',
          'game_id'   => 'Game Id',
          'result'   => 'Result',
        );
      }
      
//      public function embeddedDocuments()
//      {
//        return array(
//          // property name => embedded document class name
//          'users'=>'MGameLogUser'
//        );
//      }            
       
        public function behaviors()
        {
          return array(
            array(
              'class'=>'ext.YiiMongoDbSuite.extra.EEmbeddedArraysBehavior',
              'arrayPropertyName'=>'users', // name of property
              'arrayDocClassName'=>'MGameLogUser' // class name of documents in array
            ),
          );
        }
      
    }