<?php
class cURL {
	var $headers;
	var $user_agent;
	var $compression;
	var $cookie_file;
	var $proxy;
	var $cookies;
	function cURL($compression = 'gzip', $proxy = '') {
		$this -> headers[] = 'Accept: image/gif, image/x-bitmap, image/jpeg, image/pjpeg';
		$this -> headers[] = 'Connection: Keep-Alive';
		$this -> headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
		$this -> user_agent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)';
		$this -> compression = $compression;
		$this -> proxy = $proxy;
		$this->cookies = FALSE;
	}
	function get($url) {
		$process = curl_init($url);
		curl_setopt($process, CURLOPT_HTTPHEADER, $this -> headers);
		curl_setopt($process, CURLOPT_HEADER, 0);
		curl_setopt($process, CURLOPT_USERAGENT, $this -> user_agent);
		if ($this -> cookies == TRUE)
			curl_setopt($process, CURLOPT_COOKIEFILE, $this -> cookie_file);
		if ($this -> cookies == TRUE)
			curl_setopt($process, CURLOPT_COOKIEJAR, $this -> cookie_file);
		curl_setopt($process, CURLOPT_ENCODING, $this -> compression);
		//curl_setopt($process, CURLOPT_TIMEOUT, 3600);
		if ($this -> proxy)
			curl_setopt($process, CURLOPT_PROXY, $this -> proxy);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
		
		//var_dump($this->headers);
		
		$return = curl_exec($process);
		curl_close($process);
		return $return;
	}

	function post($url, $data) {
            // Get cURL resource
            $curl = curl_init();  
            // Set some options - we are passing in a useragent too here
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => $this -> user_agent,
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => http_build_query($data),
                CURLOPT_FOLLOWLOCATION => 1,
                CURLOPT_CUSTOMREQUEST => "POST"
            ));
            // Send the request & save response to $resp
            $resp = curl_exec($curl);
            // Close request to clear up some resources
            curl_close($curl);
            return $resp;
         }

	function error($error) {
		echo "<center><div style='width:500px;border: 3px solid #FFEEFF; padding: 3px; background-color: #FFDDFF;font-family: verdana; font-size: 10px'><b>cURL Error</b><br>$error</div></center>";
		die ;
	}
}
?>