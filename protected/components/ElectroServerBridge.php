<?php

require_once "curlhelper.php";

class ElectroServerBridge {

	private $host;
	private $port;
	private $protocol;
	private $curlHelper;

	public function __construct($_host = NULL, $_port = NULL, $_protocol = NULL) {
		if ($_host == NULL)
                    //$_host = "10.0.2.58";
                    $_host = "localhost";
		if ($_port == NULL)
			$_port = 8889;
		if ($_protocol == NULL)
			$_protocol = "http";
		$this -> port = $_port;
		$this -> host = $_host;
		$this -> protocol = $_protocol;

		$this -> curlHelper = new cURL();
	}

	public function buildURLForAction($action, $data = NULL) {
		$result = $this -> protocol . "://" . $this -> host . ":" . $this -> port . "/es?command=" . $action;
		if ($data != NULL) {
			$arr = array();
			foreach ($data as $key => $value) {
				$arr[] = urlencode($key) . "=" . urlencode($value);
			}
			$result .= "&" . implode("&", $arr);
		}                 
		return $result;
	}
        
        public function sendPost($data){            
            return $this->curlHelper->post($this -> protocol . "://" . $this -> host .  "/es/",$data);
        }
        
        public function sendMessage($meg,$showPopup,$id=NULL){            
            $data = array("users" => -1, 'id'=>$id, 'msg'=>$meg,'showPopup'=>$showPopup);
            $url = $this -> buildURLForAction('system_message', $data);
            //echo "message electro server at: " . $url;
            return $this -> curlHelper -> get($url);
        }
        public function sendUserMessage($username,$meg,$showPopup,$id=NULL){            
            $data = array("users" => $username,'id'=>$id, 'msg'=>$meg,'showPopup'=>$showPopup);
            $url = $this -> buildURLForAction('system_message', $data);
            //echo "message electro server at: " . $url;
            return $this -> curlHelper -> get($url);
        }
        
        public function sendUserInfo($uid, $field, $value){
            $data = array("uid" => ($uid == NULL ? -1 : $uid), 'field'=>$field, 'value'=>$value);
            $url = $this -> buildURLForAction('update_user_info', $data);
            //echo "message electro server at: " . $url;
            return $this -> curlHelper -> get($url);
        }
        
        public function sendAccessToken($uid, $field, $value){
            $data = array("uid" => ($uid == NULL ? -1 : $uid), 'username'=>$field, 'token'=>$value);
            $url = $this -> buildURLForAction('log_user_access_token', $data);
            //echo "message electro server at: " . $url;
            return $this -> curlHelper -> get($url);
        }

	public function notifyElectroServerToUpdateUserInfo($uid = NULL) {
		$data = array("uid" => ($uid == NULL ? -1 : $uid));
		$url = $this -> buildURLForAction(ESAction::$UPDATE_USER_INFO, $data);
		echo "message electro server at: " . $url;
		return $this -> curlHelper -> get($url);
	}

	public function getIndexHtmlFile() {
		$url = $this -> buildURLForAction(ESAction::$INDEX_HTML);
		echo "message electro server at: " . $url;
		return $this -> curlHelper -> get($url);
	}
        
        public function send($message) {
            return $this->curlHelper->get($message);
        }                
}
?>