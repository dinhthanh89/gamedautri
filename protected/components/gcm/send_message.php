<?php
if (isset($_REQUEST["regId"])) {
	$regId = $_REQUEST["regId"];
	$title = "Esimo";

	include_once './GCM.php';

	$gcm = new GCM();

	$registatoin_ids = array($regId);
	$message = array();
	foreach ($_REQUEST as $key => $value) {
		if ($key == "regId") continue;
		$message[$key] = $value;
	}
	if (!isset($message["title"])) {
		$message["title"] = $title;
	}

	$result = $gcm -> send_notification($registatoin_ids, $message);

	echo $result;
} else {
	echo json_encode(array("code" => 1, "message" => "missing required fields"));
}
?>
