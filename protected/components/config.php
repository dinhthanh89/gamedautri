<?php

class DbConfig {

    static $host = "localhost";
    static $port = "3306";
    static $username = "root";
    static $password = "iamnumberone";
    static $dbName = "eposi_game";
    static $adapter = "Ice_Db_PdoMySqlAdapter";

}

class ESAction {

    static $INDEX_HTML = "index.html";
    static $UPDATE_USER_INFO = "update_user_info";

}

class Constants {
    
    static $template_message = array('register' => 'message_register.php');
}

?>