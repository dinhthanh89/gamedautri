<?php

if (!isset($_REQUEST["device_token"])) {
	die ("Thiếu device uuid");
}

// Adjust to your timezone
date_default_timezone_set('Asia/Ho_Chi_Minh');

// Report all PHP errors
error_reporting(-1);

// Using Autoload all classes are loaded on-demand
require_once 'ApnsPHP/Autoload.php';

// Instanciate a new ApnsPHP_Push object
$push = new ApnsPHP_Push(
	ApnsPHP_Abstract::ENVIRONMENT_SANDBOX,
	'chan-apns-dev-cert.pem'
);

// Set the Root Certificate Autority to verify the Apple remote peer
$push->setRootCertificationAuthority('entrust_root_certification_authority.pem');

// Connect to the Apple Push Notification Service
$push->connect();

// Instantiate a new Message with a single recipient
$message = new ApnsPHP_Message($_REQUEST["device_token"]);
// Set a custom identifier. To get back this identifier use the getCustomIdentifier() method
// over a ApnsPHP_Message object retrieved with the getErrors() message.
$message->setCustomIdentifier("message");

$message -> setSound();
$message -> setText("Bạn nhận được một tin nhắn từ hệ thống Esimo");
$message -> setBadge(0);

foreach ($_REQUEST as $key => $value) {
	if ($key == "device_token") continue;
	switch ($key) {
		case 'alert':
			$message->setText($value);
			break;
		case 'badge':
			$message->setBadge((int) $value);
			break;
		case 'sound':
			$message->setSound($value);
			break;
		default:
			$message->setCustomProperty($key, $value);
			break;
	}
}

// Set the expiry value to 30 seconds
$message->setExpiry(30);

echo("push message: </hr>".$message -> getPayload()."</hr>");

// Add the message to the message queue
$push->add($message);

// Send all messages in the message queue
$push->send();

// Disconnect from the Apple Push Notification Service
$push->disconnect();

// Examine the error message container
$aErrorQueue = $push->getErrors();
if (!empty($aErrorQueue)) {
	var_dump($aErrorQueue);
}
