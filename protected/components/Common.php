<?php

class Common {  
    
    function getChipByMoney($money) {
        $ratio = Configuration::model()->getConfig('money_by_chip');
        return intval($money * $ratio);
    }
    
    function getChipByGold($gold){
        $ratio = Configuration::model()->getConfig('gold_by_chip');
        return intval($gold * $ratio);
    }          
    
    public function echoIfExist($str) {
        if ($str != NULL)
            echo $str;
    }

    public function dump($var, $die = FALSE, $depth = 1000, $color = TRUE) {
        CVarDumper::dump($var, $depth, $color);
        if ($die)
            die();
    }

    public function redirect($route) {
        $redirectUrl = Yii::app()->createUrl($route);
        Yii::app()->controller->redirect($redirectUrl);
        echo "<script type='text/javascript'>top.location.href = '$redirectUrl';</script>";
    }

    public function secondToHour($second, $type) {
        $h = intval($second / 3600);
        if ($type == 'h')
            return $h;
        $m = intval(($second - $h * 3600) / 60);
        if ($type == 'm')
            return $m;
        $s = $second - $h * 3600 - $m * 60;
        if ($type == 's')
            return intval($s);
    }

    public function getTheme() {
        $defaultTheme = 'pc';
        $defaultMobileTheme = 'mobile';
        
        $session = new CHttpSession();
        $session->open();        
        if (isset($session['theme']))
            return $session['theme'];
        else {
            if (self::isMobile())
                return $defaultTheme;
            else {
                return $defaultTheme;
            } 
        }
    }

    public function setTheme($theme) {
        $session = new CHttpSession();
        $session->open();
        $session->timeout = 60;
        $session['theme'] = $theme;
    }

    public function isMobile() {
        $detect = new MobileDetect();
        if($detect->isMobile()){
            return TRUE;
        }else{
            return FALSE;
        }        
    }

    public function requireTheme($theme) {
        if (Yii::app()->theme->name != $theme)
            Yii::app()->controller->redirect(Yii::app()->homeUrl);
    }

    public function alert($message, $redirectUrl = NULL) {
        Yii::app()->controller->render('/site/alert', array('message' => $message, 'redirectUrl' => $redirectUrl)); 
        Yii::app()->end();
    }

    public function getCurrentUrl() {
        return strtolower(Yii::app()->controller->id . '/' . Yii::app()->controller->action->id);
    }

    function isMultiArrayEmpty($multiarray) {
        if (is_array($multiarray) and !empty($multiarray)) {
            $tmp = array_shift($multiarray);
            if (!is_multiArrayEmpty($multiarray) or !is_multiArrayEmpty($tmp)) {
                return false;
            }
            return true;
        }
        if (empty($multiarray)) {
            return true;
        }
        return false;
    }  

    function includeBaokim() {
        require_once('baokim/BaoKimPayment.php');
    }
    
    function includeTheCaoMobile(){
        require_once('thecao_mobile/MobiCard.php');
    }        
    
    function includeShipChung(){
        require_once('shipchung/config.php');
        require_once('shipchung/shipchung.microcheckout.class.php');
    }
    
    function includeFiboSMS() {        
        require_once('FiboSMS/fibosmsconfig.php');       
    }
    
    function includeVhtSMS() {        
        require_once('VhtSMS/config.php');       
    }
    
    function includeAppotaSMS() {        
        require_once('AppotaSMS/config.php');       
    }
    
    function includeApns(){
        require_once 'apns/ApnsPHP/Autoload.php';        
        require_once 'apns/ApnsPHP/Push.php';
        require_once 'apns/ApnsPHP/Message.php';
    }
    
    function includeGcm(){
        require_once 'gcm/GCM.php';
        require_once 'gcm/config.php';
    }
    
    function includeVmgSMS() {        
        require_once('VmgSMS/lib/nusoap.php');       
    }
    
    function includeBridgeVBB() {        
        require_once($_SERVER['DOCUMENT_ROOT'].'/forums/includes/vBulletin_Bridge.php');       
    }            

    function getCurrentThemePath() {
        return Yii::app()->createUrl('themes/' . Yii::app()->theme->name);
    }
   
    function echoWithXssFilter($str) {
        $this->beginWidget('CHtmlPurifier');
        echo $str;
        $this->endWidget();
    }

    function check_email_address($email) {
        // First, we check that there's one @ symbol, and that the lengths are right 
        if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
            // Email invalid because wrong number of characters in one section, or wrong number of @ symbols. 
            return false;
        } // Split it into sections to make life easier 
        $email_array = explode("@", $email);
        $local_array = explode(".", $email_array[0]);
        for ($i = 0; $i < sizeof($local_array); $i++) {
            if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
                return false;
            }
        }
        if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
            // Check if domain is IP. If not, it should be valid domain name 
            $domain_array = explode(".", $email_array[1]);
            if (sizeof($domain_array) < 2) {
                return false; // Not enough parts to domain                
            }
            for ($i = 0; $i < sizeof($domain_array); $i++) {
                if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
                    return false;
                }
            }
        }
        return true;
    }

    function echoWithDot($number) {
        echo number_format($number, 0, ',', '.');
    }
    
    function setCookieLogin($url){
        Yii::app()->request->cookies['url_before_login'] = new CHttpCookie('url_before_login', $url);
    }
    
    function unsetCookieLogin(){
        unset(Yii::app()->request->cookies['url_before_login']);
    }
    
    function simpleSlug($str)
    {        
        $slug = "";        
        $slug = Myvietstring::removeInvalidCharacter($str);        
        $slug = preg_replace('!\s+!', ' ', $slug);
        $slug = trim($slug);
        $slug = Myvietstring::convertToUnsignedString($slug);          
        $slug = strtolower($slug);        
        return $slug;
    }        
		
    //Kiem tra la dang chay tren mobile hoac table
    //Se tra ve 'true' neu la iOS, 'false' neu la Android
    function isSupport()
    {
        $detect = new MobileDetect();
        if($this->isRunningMobile())
        {
            return $detect->is('iOS') || $detect->is('AndroidOS');
        }
        return false;
    }

    function isRunningiOS() 
    {
//        $detect = new MobileDetect();
//        return $detect->is('iOS');
        //if (preg_match("/iP(od|hone|ad)/i", $_SERVER["HTTP_USER_AGENT"]))
        if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'iPad'))
            return TRUE;
              
    }
    
    function isRunningAndroidOS()
    {
//        $detect = new MobileDetect();
//        return $detect->is('AndroidOS');
        if(strstr($_SERVER['HTTP_USER_AGENT'],'Android'))
            return TRUE;
    }
    
    function isOperaMini(){
         if(preg_match('/opera mini/i',$_SERVER['HTTP_USER_AGENT']))
             return TRUE;
    }

    function isRunningMobile()
    {
        $detect = new MobileDetect();
        return $detect->isMobile() || $detect->isTable();
    }
    
    function macPadding($str) {
        $result = array();
        for ($i = 0; $i < strlen($str); $i = $i + 2) {
                $result[] = 16 * hexdec($str[$i]) + hexdec($str[$i+1]);
        }
        $numberToPad = 8 - sizeof($result) % 8;
        for ($i = 0; $i < $numberToPad; $i++)
                $result[] = "0";
        $str = "";
        foreach ($result AS $r) {
                $str .= chr($r);
        }
        return $str;
    }

    function encrypt($str, $key='awbojm11561742ARBLHJ8644') {            
        $cipher = mcrypt_module_open(MCRYPT_3DES, '', 'ecb', '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($cipher), MCRYPT_DEV_RANDOM);        
        mcrypt_generic_init($cipher, $key, $iv);
        $encrypted_text = mcrypt_generic($cipher, $str);
        $encrypted = bin2hex($encrypted_text);
        mcrypt_generic_deinit($cipher);
        mcrypt_module_close($cipher);        
        return strtoupper($encrypted);
    }
    
    function initMemcache(){     
        // connnect 10.0.2.59
        $host = '127.0.0.1';
        $port = 5701;
        $memcache = new Memcache;
        $memcache->addServer($host, $port);
        $stats = @$memcache->getExtendedStats();
        $available = (bool) $stats["$host:$port"];
        if ($available && @$memcache->connect($host, $port)){            
            return $memcache;
        }
        return false;
        
        // connnect 10.0.2.59
        $host = '10.0.2.59';
        $port = 5701;
        $memcache = new Memcache;
        $memcache->addServer($host, $port);
        $stats = @$memcache->getExtendedStats();
        $available = (bool) $stats["$host:$port"];
        if ($available && @$memcache->connect($host, $port)){            
            return $memcache;
        }
        
        // connnect 10.0.2.58
        $memcache2 = new Memcache;
        $host = '10.0.2.58';
        $memcache2->addServer($host, $port);
        $stats = @$memcache2->getExtendedStats();
        $available = (bool) $stats["$host:$port"];
        if ($available && @$memcache2->connect($host, $port)){            
            return $memcache2;
        }
                               
        return False;
                
    }
    
    function build_key_memcache($arr= array()){
        return implode('|', $arr);
    }
    
    function add_key_memcache($key){  
        $memcache = Common::initMemcache();
        if($memcache){
            $arr = $memcache->get('key_memcache_httpd');
            if(!$arr){
                $arr = array($key);
            }else{
                if(!in_array($key,$arr))
                    array_push($arr, $key);            
            }        
            $memcache->set('key_memcache_httpd',$arr);
        }
    }
    
    function updateSitemap(){
        $curlHelper = new cURL();
        $url = Yii::app()->createAbsoluteUrl('ksitemap/index');
        $data = array();                                      
        $result = $curlHelper->post($url, $data);        
    }
    
    function makeBaoKimAPISignature($method, $uri, $getArgs=array(), $postArgs=array(), $priKeyFile)
    {
            if(strpos($uri,'?') !== false)
            {
                list($uri,$get) = explode('?', $uri);
                parse_str($get, $get);
                $getArgs=array_merge($get, $getArgs);
            }

            ksort($getArgs);
            ksort($postArgs);

            $method=strtoupper($method);            
            $data = Common::_combineRawUrlencodeMessage($method, $uri, $getArgs, $postArgs);  
            $priKey = file_get_contents($priKeyFile);
            $priKey = openssl_get_privatekey($priKey);
            assert('$priKey !== false');
            $x=openssl_sign($data, $signature, $priKey, OPENSSL_ALGO_SHA1);
            assert('$x !== false');

            return urlencode(base64_encode($signature));
    }

    function _combineRawUrlencodeMessage($request_method, $request_uri, $get_args, $post_args)
    {
            $message='';
            $message .= $request_method;                        
            $message .= "&" . rawurlencode($request_uri);            
            $message .= "&" . rawurlencode(Common::_normalizeArrayToString($get_args));
            $message .= "&" . rawurlencode(Common::_normalizeArrayToString($post_args));            
            return $message;
    }

    function _normalizeArrayToString($data)
    {
            $items=array();
            Common::_makeQueryStrings($data, $items);
            ksort($items);

            $tmp = array();
            foreach ($items as $k=>$v) $tmp[] = $k.'='.$v;

            return implode('&', $tmp);
    }

    function _makeQueryStrings($data, & $queryStrings, $prefix=null)
    {
            foreach ($data as $k=>$v) {
                    if (!empty($prefix)) $k = $prefix.'['.$k.']';
                    if (is_array($v)) Common::_makeQueryStrings($v, $queryStrings, $k);
                    else $queryStrings[rawurlencode($k)] = rawurlencode($v);
            }
    }

    
}

?>
