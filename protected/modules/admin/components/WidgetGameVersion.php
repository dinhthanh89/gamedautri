<?php

Yii::import('zii.widgets.CPortlet');

class WidgetGameVersion extends CPortlet {

    public  $game_download_id;
    protected $dataGameVersion;
    public function init() {    
         $this->dataGameVersion = new CActiveDataProvider('GameVersion', array(
                            'criteria' => array(
                                'condition' => 'game_download_id= '.$this->game_download_id, //.Yii::app()->user->getUser()->id,                        				                                                            
                            ),
                        )); 
        parent::init();
    }

    protected function renderContent() {     
        $this->render('gameVersion', array(
            'dataGameVersion' => $this->dataGameVersion
        ));       
    }

}