<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $dataGameVersion,    
    'columns' => array(
        array(
            'name' => 'name',
            'type' => 'raw',
            'value'=>'$data->name'
        ),        
        array(
            'class' => 'CButtonColumn',
            'template' => '{view}{update}{delete}',
            'buttons'=>array
            (
                'view' => array
                (
                    'label'=>'View detail version',                    
                    'url'=>'Yii::app()->createUrl("admin/game/viewVersion", array("id"=>$data->id))',
                ),
                'update' => array
                (
                    'label'=>'Update',
                    'url'=>'Yii::app()->createUrl("admin/game/updateVersion", array("id"=>$data->id))',
                ),
            ),
                
            
        ),
    ),
));
?>