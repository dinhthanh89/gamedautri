<?php

Yii::import('zii.widgets.CPortlet');

class GameMenu extends CPortlet {

    public function init() {
        $this->title = CHtml::encode('Game');
        parent::init();
    }

    protected function renderContent() {
        $this->render('gameMenu');
    }

}