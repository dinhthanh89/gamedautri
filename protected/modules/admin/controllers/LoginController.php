<?php

class LoginController extends Controller {

    public $layout = 'login';

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        if (User::model()->isLogin()) {
            if (Yii::app()->user->getUser()->role != 4) {
                $message = 'Not permission';
                $login = 'yes';
                Yii::app()->controller->render('login', array('message' => $message,'login'=>$login));
            }
            else
                Yii::app()->request->redirect('game');
        }else {
            $model = new LoginForm;
// if it is ajax validation request
// collect user input data
            if (isset($_POST['LoginForm'])) {
//            echo'aaa';
//            die();
                $model->attributes = $_POST['LoginForm'];
                if (($model->validate()) && ($model->login())) {
                    if (isset(Yii::app()->request->cookies['url_before_login']->value)) {
                        $url = Yii::app()->request->cookies['url_before_login']->value;
                        K::unsetCookieLogin();
                        $this->redirect(Yii::app()->createUrl($url));
                    }
                    $this->redirect(Yii::app()->request->urlReferrer);
                } else {
                    $user = User::model()->find('username="' . $model->username . '"');
                    if (!$user) {
                        $message = 'User not esxited!';
                        Yii::app()->controller->render('login', array('message' => $message));
                    }
                    $password = $user->createHash(($model->password) . $user->salt);
                    if ($password == $user->password) {
                        if ($user->role != 4) {
                            $message = 'Not permission';
                            Yii::app()->controller->render('login', array('message' => $message));
                        } else {
                            $session = new CHttpSession();
                            $session->open();
                            $session['user'] = $user;
                            Yii::app()->user->setState('roles', $user->userRole->name);
                            Yii::app()->request->redirect('game');
                        }
                    } else {
                        $message = 'Wrong Password';
                        Yii::app()->controller->render('login', array('message' => $message));
                    }
                }
            }
// display the login form
//        echo'aaas';
//        die();
//        $this->render('login', array('model' => $model));
//        Yii::app()->controller->render('login', array('model' => $model));
            $message = '';
            Yii::app()->controller->render('login', array('message' => $message));
        }
    }

}
