<?php

class GameController extends CAdminController {

    public $layout = 'column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {                
        return array(            
            array('allow', // allow authenticated users to access all actions
                'roles' => array('administrator'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'          
        $model = new Game('search');
        if (isset($_GET['Game']))
            $model->attributes = $_GET['Game'];
        $this->render('index', array(
            'model' => $model,
        ));
    }        

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Game();
        if (isset($_POST['Game'])) {
            $model->attributes = $_POST['Game'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {        
        $model = $this->loadModel();                
        if (isset($_POST['Game'])) {
            $model->attributes = $_POST['Game'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionDetail(){  
        $game_id = Yii::app()->request->getParam('id',NULL);  
        $dataGameDevice = new CActiveDataProvider('GameDownload', array(
                            'criteria' => array(
                                'condition' => 'game_id= '.$game_id, //.Yii::app()->user->getUser()->id,                        				                                                            
                            ),
                        ));        
        $this->render('index_gd', array(
            'dataGameDevice' => $dataGameDevice
        ));
    }        

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {            
            if (isset($_GET['id'])) {
                if (Yii::app()->user->isAdmin())
                    $condition = 'status=' . Game::STATUS_SHOW . ' OR status=' . Game::STATUS_HIDE;
                else
                    $condition = '';
                $this->_model = Game::model()->findByPk($_GET['id'], $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }        
        return $this->_model;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}