<?php

class UserController extends CAdminController {

    public $layout = 'column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters() {
//        return array(
//            'accessControl', // perform access control for CRUD operations
//        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {                
//        return array(
//            array('allow', // allow all users to access 'index' and 'view' actions.
//                'actions' => array('index', 'view'),
//                'users' => array('*'),                
//            ),
//            array('allow', // allow authenticated users to access all actions
//                'roles' => array('administrator'),
//            ),
//            array('deny', // deny all users
//                'users' => array('*'),
//            ),
//        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'             
        $model = new User('search');
        if (isset($_GET['User']))
            $model->attributes = $_GET['User'];
        $this->render('index', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new User;
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('index'));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate() {        
        $model = $this->loadModel();                
        if (isset($_POST['User'])) {
            $model->attributes = $_POST['User'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete() {
        if (Yii::app()->request->isPostRequest) {
            // we only allow deletion via POST request
            $this->loadModel()->delete();

            // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
            if (!isset($_GET['ajax']))
                $this->redirect(array('index'));
        }
        else
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
    }
    
    public function actionMessage(){ 
        require_once('protected/components/ElectroServerBridge.php');
        $criteria = new CDbCriteria();
        $criteria->condition = 'id != 0';                
        $model = User::model()->findAll($criteria);
        $error = 0;
        if (isset($_POST['message_user'])) {       
            $content = Yii::app()->db->quoteValue($_POST['message_content']);  
            $template = $_POST['template'];
            $users = explode(',',$_POST['message_user']);
            $values = array();            
            foreach($users as $id){
                if(is_numeric($id)){                    
                    $user = User::model()->findByPk($id);
                    $template_content = $this->renderPartial('/message/'.$template, array('content'=>$content,'username'=>$user->username),true);        
                    $str = '(0, "system", '.$id.', "'.$user->username.'","'.$content.'","'.date("Y-m-d H:i:s").'", 1 )';                     
                    array_push($values, $str);
                }                
            }            
            $values = implode(',', $values);            
            if($values){                
                $sql = 'INSERT INTO user_message (`sender`, `sender_name`, `receiver`, `receiver_name`, `content`, `time_sent`, `status`) VALUES ' . $values;
                $command = Yii::app()->db->createCommand($sql);                
                if($command->execute())
                {
                    $electroServer = new ElectroServerBridge();
                    $electroServer->sendMessage($_POST['message_user'], $template_content);              
                }
                else
                    $error = -1;
            }            
        }
        $this->render('message',array('model'=>$model, 'error'=>$error));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel() {
        if ($this->_model === null) {            
            if (isset($_GET['id'])) {                
                $this->_model = User::model()->findByPk($_GET['id']);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'The requested page does not exist.');
        }        
        return $this->_model;
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}