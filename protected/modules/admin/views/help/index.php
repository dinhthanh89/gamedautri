<?php
$this->breadcrumbs = array(
    'Manage Help',
);
?>
<h1>Manage Help</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'header'=>'No.',
            'value'=>'$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',
        ),  
         array(
            'name' => 'game_id',            
            'value' => '$data->game->name',
        ), 
        array(
            'name' => 'title',            
            'value'=>'$data->title'
        ),
        array(
            'name' => 'content',
            'value' => '$data->content',
        ), 
        array(
            'name' => 'status',
            'type' => 'raw', 
            'value' => function($data,$row){
                if($data->status == 1)              
                    return "Active";
                else
                    return "Inactive";
            },
        ), 
        array(
            'class' => 'CButtonColumn',
             'template' => '{update}{delete}',            
        ),
    ),
));
?>
