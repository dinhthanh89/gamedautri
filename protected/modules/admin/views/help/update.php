<?php
$this->breadcrumbs=array(      
	'Help'=>Yii::app()->createUrl("admin/help"),
	' Update ',
);
?>

<h1>Update <i><?php echo CHtml::encode($model->title); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>