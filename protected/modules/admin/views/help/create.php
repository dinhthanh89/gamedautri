<?php
$this->breadcrumbs=array(
	'Help'=>Yii::app()->createUrl("admin/help"),
	'Create'
);
?>
<h1>Create Help</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>