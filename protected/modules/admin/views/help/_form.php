<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>80,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'title'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
                <?php echo CHtml::activeTextArea($model,'content',array('rows'=>10, 'cols'=>70)); ?>		
		<?php echo $form->error($model,'content'); ?>
	</div>	                     
        
        <div class="row">
		<?php echo $form->labelEx($model,'game_id'); ?>
		<?php echo $form->dropDownList($model,'game_id',CHtml::listData(Game::model()->findAll(array('order' => 'name ASC')), 'id', 'name'), array('empty'=>'Lựa chọn game')); ?>
		<?php echo $form->error($model,'game_id'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array(1=>'Active', 0=>'Inactive')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->