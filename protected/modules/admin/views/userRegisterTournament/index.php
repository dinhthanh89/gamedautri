<?php
/* @var $this UserRegisterTournamentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Register Tournaments',
);

$this->menu=array(
	array('label'=>'Create UserRegisterTournament', 'url'=>array('create')),
	array('label'=>'Manage UserRegisterTournament', 'url'=>array('admin')),
);
?>

<h1>User Register Tournaments</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
