<?php
/* @var $this UserRegisterTournamentController */
/* @var $model UserRegisterTournament */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-register-tournament-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
		<?php echo $form->error($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tournament_id'); ?>
		<?php echo $form->textField($model,'tournament_id'); ?>
		<?php echo $form->error($model,'tournament_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'start_tournament_match_group_id'); ?>
		<?php echo $form->textField($model,'start_tournament_match_group_id'); ?>
		<?php echo $form->error($model,'start_tournament_match_group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'register_time'); ?>
		<?php echo $form->textField($model,'register_time'); ?>
		<?php echo $form->error($model,'register_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'slogan'); ?>
		<?php echo $form->textArea($model,'slogan',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'slogan'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->