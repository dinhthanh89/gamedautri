<?php
/* @var $this UserRegisterTournamentController */
/* @var $model UserRegisterTournament */

$this->breadcrumbs=array(
	'User Register Tournaments'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserRegisterTournament', 'url'=>array('index')),
	array('label'=>'Manage UserRegisterTournament', 'url'=>array('admin')),
);
?>

<h1>Create UserRegisterTournament</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>