<?php
/* @var $this UserRegisterTournamentController */
/* @var $model UserRegisterTournament */

$this->breadcrumbs=array(
	'User Register Tournaments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserRegisterTournament', 'url'=>array('index')),
	array('label'=>'Create UserRegisterTournament', 'url'=>array('create')),
	array('label'=>'Update UserRegisterTournament', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserRegisterTournament', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserRegisterTournament', 'url'=>array('admin')),
);
?>

<h1>View UserRegisterTournament #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'tournament_id',
		'start_tournament_match_group_id',
		'register_time',
		'slogan',
	),
)); ?>
