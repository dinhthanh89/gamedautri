<?php
/* @var $this UserRegisterTournamentController */
/* @var $model UserRegisterTournament */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_id'); ?>
		<?php echo $form->textField($model,'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tournament_id'); ?>
		<?php echo $form->textField($model,'tournament_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'start_tournament_match_group_id'); ?>
		<?php echo $form->textField($model,'start_tournament_match_group_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'register_time'); ?>
		<?php echo $form->textField($model,'register_time'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'slogan'); ?>
		<?php echo $form->textArea($model,'slogan',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->