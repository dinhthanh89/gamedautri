<?php
/* @var $this UserRegisterTournamentController */
/* @var $model UserRegisterTournament */

$this->breadcrumbs=array(
	'User Register Tournaments'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserRegisterTournament', 'url'=>array('index')),
	array('label'=>'Create UserRegisterTournament', 'url'=>array('create')),
	array('label'=>'View UserRegisterTournament', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserRegisterTournament', 'url'=>array('admin')),
);
?>

<h1>Update UserRegisterTournament <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>