<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />

        <!-- blueprint CSS framework -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
        <!--[if lt IE 8]>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
        <![endif]-->

        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.admin.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" />
        <script src="<?php echo Yii::app()->baseUrl . '/js/jquery-1.7.min.js' ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/js/select2.min.js' ?>"></script>
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    </head>

    <body>

        <div class="container" id="page">

            <div id="header">
                <div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
            </div><!-- header -->

            <div id="mainmenu">
                
                <nav>
                    <ul>
                        <li><a href="<?php echo Yii::app()->createUrl('admin'); ?>">Home</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/user'); ?>">User</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/game'); ?>">Game</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/ads'); ?>">Ads</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/config'); ?>">Config</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/help'); ?>">Help</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('admin/Tournament') ?>">Tournament</a>
                            <ul>
                                <li><a href="<?php echo Yii::app()->createUrl('admin/gameAction') ?>">GameAction</a></li>                                           
                            </ul>
                        </li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/logout') ?>">Thoát( <?php echo User::model()->getCurrentUserName() ?> )</a></li>                        
                    </ul>
                </nav>  
            </div><!-- mainmenu -->

            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
            ));
            ?><!-- breadcrumbs -->

                <?php echo $content; ?>

            <div id="footer">
                Copyright &copy; <?php echo date('Y'); ?> by Eposi.<br/>
                All Rights Reserved.<br/>                
            </div><!-- footer -->

        </div><!-- page -->

    </body>
</html>