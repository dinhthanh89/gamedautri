<?php
$this->breadcrumbs=array(
	'Config'=>Yii::app()->createUrl("admin/game"),
	'Create'
);
?>  
<h1>Create Game</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>