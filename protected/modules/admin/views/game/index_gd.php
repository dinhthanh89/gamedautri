<?php
$this->breadcrumbs = array(
    'Manage Game',
);
$arr_tab = array();
foreach($dataGameDevice->getData() as $index => $device){
    $arr_tab['tab'.($index+1)] = array(
                                        'title'=>$device->device,
                                        'content'=>$this->widget('WidgetGameVersion',array('game_download_id'=>$device->id),true),                                                    
                                        );
}
?>
<h1>Manage Game</h1>

<?php
$this->widget('CTabView', array(
    'tabs'=>$arr_tab
))?>
