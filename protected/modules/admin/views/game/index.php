<?php
$this->breadcrumbs = array(
    'Manage Game',
);
?>
<h1>Manage Game</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'name',
            'type' => 'raw',
            'value'=>'CHtml::link(CHtml::encode($data->name),array("game/detail/id/".$data->id))'
        ),
        array(
            'name' => 'category',
            'value' => '$data->category->name',
        ),
        array(
            'name' => 'description',
            'value' => '$data->description',
        ),
        array(
            'name' => 'rule',
            'value' => '$data->rule',
        ),
        array(
            'name' => 'honor',
            'value' => '$data->honor',
        ),
        array(
            'name' => 'social_links',
            'value' => '$data->social_links',
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
