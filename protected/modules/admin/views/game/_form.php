<div class="form">

<?php $form=$this->beginWidget('CActiveForm'); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo CHtml::errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>80,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
                <?php echo CHtml::activeTextArea($model,'description',array('rows'=>10, 'cols'=>70)); ?>		
		<?php echo $form->error($model,'description'); ?>
	</div>

        <div class="row">
		<?php echo $form->labelEx($model,'rule'); ?>
                <?php echo CHtml::activeTextArea($model,'rule',array('rows'=>10, 'cols'=>70)); ?>		
		<?php echo $form->error($model,'rule'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'honor'); ?>
                <?php echo CHtml::activeTextArea($model,'honor',array('rows'=>10, 'cols'=>70)); ?>		
		<?php echo $form->error($model,'honor'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'social_links'); ?>
		<?php echo $form->textField($model,'social_links',array('size'=>80)); ?>
		<?php echo $form->error($model,'social_links'); ?>
	</div>
        
        <div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status',array('1'=>'1','0'=>'0')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>
        
<?php $this->endWidget(); ?>

</div><!-- form -->