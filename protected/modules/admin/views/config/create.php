<?php
$this->breadcrumbs=array(
	'Config'=>Yii::app()->createUrl("admin/config"),
	'Create'
);
?>
<h1>Create Post</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>