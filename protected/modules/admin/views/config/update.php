<?php
$this->breadcrumbs=array(      
	'Config'=>Yii::app()->createUrl("admin/config"),
	' Update '.$model->name,
);
?>

<h1>Update <i><?php echo CHtml::encode($model->name); ?></i></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>