<?php
$this->breadcrumbs = array(
    'Manage Config',
);
?>
<h1>Manage User</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',            
            'value'=>'$data->id'
        ),
        array(
            'name' => 'name',            
            'value'=>'$data->name'
        ),
        array(
            'name' => 'value',
            'value' => '$data->value',
        ),
        array(
            'name' => 'description',
            'value' => '$data->description',
        ),
        array(
            'class' => 'CButtonColumn',
             'template' => '{update}{delete}',            
        ),
    ),
));
?>
