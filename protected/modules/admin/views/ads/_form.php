<?php
/* @var $this AdsController */
/* @var $model Ads */
/* @var $form CActiveForm */
$game = Game::model()->findAll();

?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ads-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    

    <div class="row">
        <?php echo $form->labelEx($model,'game_id'); ?>
        <?php echo $form->dropDownList($model,'game_id',CHtml::listData(Game::model()->findAll(array('order' => 'name ASC')), 'id', 'name'), array('empty'=>'Lựa chọn game')); ?>
        <?php echo $form->error($model,'game_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type'); ?>
        <?php echo $form->dropDownList($model,'type', array('empty'=>'Lựa chọn type','Event'=>'Event','Ads'=>'Ads',)); ?>
        <?php // echo $form->dropDownList($model,'type',CHtml::listData(Ads::model()->findAll(), 'type', 'type'), array('empty'=>'Lựa chọn type')); ?>
        <?php // echo $form->textField($model, 'type'); ?>
        <?php echo $form->error($model, 'type'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'index'); ?>
        <?php echo $form->textField($model, 'index'); ?>
        <?php echo $form->error($model, 'index'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'description'); ?>
        <?php echo $form->textArea($model, 'description', array('rows' => 6, 'cols' => 50)); ?>
        <?php echo $form->error($model, 'description'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'scenes'); ?>
        <?php echo $form->dropDownList($model,'scenes', array('empty'=>'Lựa chọn scenes','login'=>'login','announce'=>'announce','lobby'=>'lobby')); ?>
        <?php echo $form->error($model, 'scenes'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'url'); ?>
        <?php echo $form->textField($model, 'url', array('size' => 60, 'maxlength' => 256)); ?>
        <?php echo $form->error($model, 'url'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'image'); ?>
        <?php echo $form->textField($model, 'image', array('size' => 60, 'maxlength' => 256)); ?>
        <?php echo $form->error($model, 'image'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->