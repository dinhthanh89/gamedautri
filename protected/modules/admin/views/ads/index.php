<?php
/* @var $this AdsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    'Ads',
);

$this->menu = array(
    array('label' => 'Create Ads', 'url' => array('create')),
    array('label' => 'Manage Ads', 'url' => array('admin')),
);
?>

<h1>Ads</h1>

<?php
//$this->widget('zii.widgets.CListView', array(
//    'dataProvider' => $dataProvider,
//    'itemView' => '_view',
//));
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'name' => 'id',
//            'type' => 'raw',
            'value' => '$data->id'
        ),
        array(
            'name' => 'game_id',
            'value' => '$data->game_id',
        ),
        array(
            'name' => 'description',
            'value' => '$data->description',
        ),
        array(
            'name' => 'type',
            'value' => '$data->type',
        ),
        array(
            'name' => 'index',
            'value' => '$data->index',
        ),
        array(
            'name' => 'scenes',
            'value' => '$data->scenes',
        ),
        array(
            'name' => 'url',
            'value' => '$data->url',
        ),
        array(
            'name' => 'image',
            'value' => '$data->image',
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
