<?php
/* @var $this TournamentMatchGroupController */
/* @var $model TournamentMatchGroup */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'match_group'); ?>
		<?php echo $form->textField($model,'match_group'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tournament_id'); ?>
		<?php echo $form->textField($model,'tournament_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'next'); ?>
		<?php echo $form->textField($model,'next'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_start_group'); ?>
		<?php echo $form->textField($model,'is_start_group'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'max_player'); ?>
		<?php echo $form->textField($model,'max_player'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->