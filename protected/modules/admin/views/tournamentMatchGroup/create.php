<?php
/* @var $this TournamentMatchGroupController */
/* @var $model TournamentMatchGroup */

$this->breadcrumbs=array(
	'Tournament Match Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TournamentMatchGroup', 'url'=>array('index')),
	array('label'=>'Manage TournamentMatchGroup', 'url'=>array('admin')),
);
?>

<h1>Create TournamentMatchGroup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>