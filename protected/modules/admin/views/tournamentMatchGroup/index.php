<?php
/* @var $this TournamentMatchGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tournament Match Groups',
);

$this->menu=array(
	array('label'=>'Create TournamentMatchGroup', 'url'=>array('create')),
	array('label'=>'Manage TournamentMatchGroup', 'url'=>array('admin')),
);
?>

<h1>Tournament Match Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
