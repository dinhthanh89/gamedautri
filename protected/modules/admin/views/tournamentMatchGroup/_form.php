<?php
/* @var $this TournamentMatchGroupController */
/* @var $model TournamentMatchGroup */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tournament-match-group-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'match_group'); ?>
        <?php // echo $form->textField($model, 'match_group'); ?>
        <?php echo $form->dropDownList($model, 'match_group', CHtml::listData(MatchGroup::model()->findAll(), 'id', 'name'), array('empty' => 'Lựa chọn match group')); ?>
        <?php echo $form->error($model, 'match_group'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'tournament_id'); ?>
        <?php // echo $form->textField($model, 'tournament_id'); ?>
        <?php echo $form->dropDownList($model, 'tournament_id', CHtml::listData(Tournament::model()->findAll(), 'id', 'name'), array('empty' => 'Lựa chọn giải đấu')); ?>
        <?php echo $form->error($model, 'tournament_id'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'next'); ?>
        <?php echo $form->dropDownList($model, 'next', CHtml::listData(TournamentMatchGroup::model()->findAll(), 'id', 'id'), array('empty' => 'Lựa chọn Next match group')); ?>
        <?php echo $form->error($model, 'next'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'is_start_group'); ?>
        <?php echo $form->dropDownList($model, 'is_start_group', array('0' => 'No', '1' => 'Yes')); ?>
        <?php echo $form->error($model, 'is_start_group'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'max_player'); ?>
        <?php echo $form->textField($model, 'max_player'); ?>
        <?php echo $form->error($model, 'max_player'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->