<?php
/* @var $this TournamentMatchGroupController */
/* @var $model TournamentMatchGroup */

$this->breadcrumbs=array(
	'Tournament Match Groups'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TournamentMatchGroup', 'url'=>array('index')),
	array('label'=>'Create TournamentMatchGroup', 'url'=>array('create')),
	array('label'=>'Update TournamentMatchGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TournamentMatchGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TournamentMatchGroup', 'url'=>array('admin')),
);
?>

<h1>View TournamentMatchGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'match_group',
		'tournament_id',
		'next',
		'is_start_group',
		'max_player',
	),
)); ?>
