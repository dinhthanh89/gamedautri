<?php
/* @var $this TournamentMatchGroupController */
/* @var $model TournamentMatchGroup */

$this->breadcrumbs=array(
	'Tournament Match Groups'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TournamentMatchGroup', 'url'=>array('index')),
	array('label'=>'Create TournamentMatchGroup', 'url'=>array('create')),
	array('label'=>'View TournamentMatchGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TournamentMatchGroup', 'url'=>array('admin')),
);
?>

<h1>Update TournamentMatchGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>