<?php
/* @var $this TournamentController */
/* @var $model Tournament */

$this->breadcrumbs = array(
    'Tournaments' => array('index'),
    $model->name,
);

$this->menu = array(
    array('label' => 'List Tournament', 'url' => array('index')),
    array('label' => 'Create Tournament', 'url' => array('create')),
    array('label' => 'Update Tournament(' . $model->name . ')', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete Tournament', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Tournament', 'url' => array('admin')),
);
?>

<h1>View Tournament <b><?php echo $model->name; ?></b></h1>

<?php
//$this->widget('zii.widgets.CDetailView', array(
//    'data' => $model,
//    'attributes' => array(
//        'id',
//        'start_time_plan',
//        'name',
//        'start_time',
//        'tournament_type_id',
//    ),
//));
//?>
<?php $data = Tournament::model()->findByPk($model->id); ?>
<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
<br />

<b><?php echo CHtml::encode($data->getAttributeLabel('start_time_plan')); ?>:</b>
<?php echo CHtml::encode($data->start_time_plan); ?>
<br />

<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
<?php echo CHtml::encode($data->name); ?>
<br />

<b><?php echo CHtml::encode($data->getAttributeLabel('start_time')); ?>:</b>
<?php echo CHtml::encode($data->start_time); ?>
<br />

<b><?php echo CHtml::encode($data->getAttributeLabel('tournament_type_id')); ?>:</b>
<?php
$tournamentName = TournamentType::model()->find('id=' . $data->tournament_type_id)->name;
echo CHtml::encode($tournamentName);
?>
<br />
<!--<div><a>Create <?php // echo $model->name ?> - Match-group</a></div>
<div><b>All <?php // echo $model->name ?> - Match-group</b></div>-->
<?php // $dataProvider = new CActiveDataProvider('TournamentMatchGroup'); ?>
<?php 
// $this->widget('zii.widgets.CListView', array( 
//	'dataProvider'=>$dataProvider,
//	'itemView'=>"_view_matchGroup",
//        'summaryText'=>'',
//)); 
?>