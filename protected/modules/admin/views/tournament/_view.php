<?php
/* @var $this TournamentController */
/* @var $data Tournament */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_time_plan')); ?>:</b>
	<?php echo CHtml::encode($data->start_time_plan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_time')); ?>:</b>
	<?php echo CHtml::encode($data->start_time); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tournament_type_id')); ?>:</b>
	<?php 
        $tournamentName = TournamentType::model()->find('id='.$data->tournament_type_id)->name;
        echo CHtml::encode($tournamentName); ?>
	<br />


</div>