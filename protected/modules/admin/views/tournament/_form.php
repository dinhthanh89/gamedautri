<?php
/* @var $this TournamentController */
/* @var $model Tournament */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'tournament-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'start_time_plan'); ?>
        <?php // echo $form->textField($model, 'start_time_plan', array('class' => 'timePicker')); ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'model' => $model,
            'attribute' => 'start_time_plan',
            'options' => array(
                'changeMonth' => 'true',
                'changeYear' => 'true',
                'yearRange' => '-99:+2',
                'dateFormat' => 'yy-mm-dd',
            ),
            'htmlOptions' => array(
                'size' => '10', // textField size
                'maxlength' => '10', // textField maxlength
            ),
        ));
        ?>
        <?php echo $form->error($model, 'start_time_plan'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name', array('size' => 60, 'maxlength' => 255)); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <!--	<div class="row">
    <?php // echo $form->labelEx($model,'start_time');   ?>
    <?php // echo $form->textField($model,'start_time');  ?>
    <?php // echo $form->error($model,'start_time');  ?>
            </div>-->

    <div class="row">
        <?php echo $form->labelEx($model, 'tournament_type_id'); ?>
        <?php // echo $form->textField($model, 'tournament_type_id'); ?>
        <?php echo $form->dropDownList($model, 'tournament_type_id', CHtml::listData(TournamentType::model()->findAll(), 'id', 'name'), array('empty' => 'Lựa chọn tournament type')); ?>
        <?php echo $form->error($model, 'tournament_type_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->
<!--<script> 
    $('.timePicker').timepicker({
        showSecond: true,
        timeFormat: 'HH:mm:ss',
        dateFormat: 'yy:mm:dd',
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false }
    });
    $('.datePicker').datepicker({
        showSecond: true,
        timeFormat: 'HH:mm:ss',
        dateFormat: 'yy-mm-dd',
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false }
    });
</script>-->