<?php
/* @var $this TournamentMatchGroupController */
/* @var $data TournamentMatchGroup */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('match_group')); ?>:</b>
	<?php echo CHtml::encode($data->match_group); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tournament_id')); ?>:</b>
	<?php echo CHtml::encode($data->tournament_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('next')); ?>:</b>
	<?php echo CHtml::encode($data->next); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_start_group')); ?>:</b>
	<?php echo CHtml::encode($data->is_start_group); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('max_player')); ?>:</b>
	<?php echo CHtml::encode($data->max_player); ?>
	<br />


</div>