<?php
/* @var $this GameActionController */
/* @var $model GameAction */

$this->breadcrumbs=array(
	'Game Actions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List GameAction', 'url'=>array('index')),
	array('label'=>'Manage GameAction', 'url'=>array('admin')),
);
?>

<h1>Create GameAction</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>