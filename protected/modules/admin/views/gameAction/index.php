<?php
/* @var $this GameActionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Game Actions',
);
$this->menu=array(
	array('label'=>'Create GameAction', 'url'=>array('create')),
	array('label'=>'Manage GameAction', 'url'=>array('admin')),
);
?>

<h1>Game Actions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
