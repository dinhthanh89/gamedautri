<?php
/* @var $this GameActionController */
/* @var $model GameAction */

$this->breadcrumbs=array(
	'Game Actions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List GameAction', 'url'=>array('index')),
	array('label'=>'Create GameAction', 'url'=>array('create')),
	array('label'=>'Update GameAction', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete GameAction', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage GameAction', 'url'=>array('admin')),
);
?>

<h1>View GameAction #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'parent_id',
		'point',
		'description',
		'game_id',
	),
)); ?>
