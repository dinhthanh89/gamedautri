<?php
/* @var $this GameActionController */
/* @var $model GameAction */

$this->breadcrumbs=array(
	'Game Actions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List GameAction', 'url'=>array('index')),
	array('label'=>'Create GameAction', 'url'=>array('create')),
	array('label'=>'View GameAction', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage GameAction', 'url'=>array('admin')),
);
?>

<h1>Update GameAction <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>