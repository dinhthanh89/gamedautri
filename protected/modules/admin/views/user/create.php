<?php
$this->breadcrumbs=array(
	'User'=>Yii::app()->createUrl("admin/user"),
	'Create'
);
?>
<h1>Create User</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>