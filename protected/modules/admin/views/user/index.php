<?php
$this->breadcrumbs = array(
    'Manage User',
);
?>
<h1>Manage User</h1>

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(       
        array(
            'name' => 'username',
            'type' => 'raw',
            'value'=>'CHtml::link(CHtml::encode($data->username))'
        ),
        array(
            'name' => 'email',
            'value' =>'$data->email',
        ),
        array(
            'name' => 'mobile',
            'value' =>'$data->mobile',
        ),
        array(
            'name' => 'gold',
            'value' =>'$data->gold',
        ),
        array(
            'name' => 'chip',
            'value' =>'$data->chip',
        ),
        array(
            'name' => 'experience',
            'value' =>'$data->experience',
        ),
        array(
            'name' => 'role',
            'value' =>'$data->userRole->name',
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
