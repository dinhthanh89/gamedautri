<?php
$this->breadcrumbs = array(
    'Manage User',
);
$data_user = array();
if($model){
    foreach($model as $user){
            $data_user[] = "{id:".$user->id.", text:'".$user->username."'}";			
    }
    $data_user = implode(',',$data_user);
    $data_user = "[".$data_user."]";  		
}
$templates = Constants::getTemplate();
?>
<h1>Send messages to User</h1>

<div class="form">
    <form method="post" action="">
    <?php if($error == 1):?>    
        <p class="required">Gửi mail thành công!</p>        
    <?php endif;?>
    <?php if($error == -1):?>    
        <p class="required">Gửi mail không thành công!</p>        
    <?php endif;?>
    <div class="row">
        <label for="User_username">User</label>
        <input type="hidden" id="user_select" name="message_user" placeholder="Lựa chọn nhân vật..." value=""/>
    </div>
    <div class="row">
        <label for="User_content">Nội dung tin nhắn</label>
        <textarea rows="10" cols="70" name="message_content"></textarea>
    </div>
    <div class="row">
        <label for="User_template">Chọn template</label>
        <select name='template'>
            <?php if($templates):?>
                <?php foreach($templates as $template):?>
                    <option value='<?php echo $template ?>'><?php echo $template ?></option>
                <?php endforeach;?>
            <?php endif;?>
        </select>
    </div>    
    <div class="row buttons">
         <input type="submit" value="Gửi tin nhắn">	
    </div>
    </form>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#user_select').select2({		    
            createSearchChoice:function(term, data) { if ($(data).filter(function() { return this.text.localeCompare(term)===0; }).length===0) {return {id:term, text:term};} },
            data: <?php echo $data_user?>,
            allowClear: true,
            placeholder: "Lựa chọn user",
            //data: [{id:0, text:'Item 1'}, {id:1, text:'Item 2'}, {id:2, text:'Item 3'}],
            multiple: true,		    
            width: "400px"
    });
});
</script>

