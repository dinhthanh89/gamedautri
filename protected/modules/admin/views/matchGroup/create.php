<?php
/* @var $this MatchGroupController */
/* @var $model MatchGroup */

$this->breadcrumbs=array(
	'Match Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MatchGroup', 'url'=>array('index')),
	array('label'=>'Manage MatchGroup', 'url'=>array('admin')),
);
?>

<h1>Create MatchGroup</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>