<?php
/* @var $this MatchGroupController */
/* @var $model MatchGroup */

$this->breadcrumbs=array(
	'Match Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MatchGroup', 'url'=>array('index')),
	array('label'=>'Create MatchGroup', 'url'=>array('create')),
	array('label'=>'Update MatchGroup', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MatchGroup', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MatchGroup', 'url'=>array('admin')),
);
?>

<h1>View MatchGroup #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
