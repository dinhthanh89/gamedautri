<?php

class AdminModule extends CWebModule {

    public $defaultController = 'User';

    public function init() {
         Yii::app()->request->redirect(Yii::app()->createUrl('admin2'));
//        if (Yii::app()->user->getUser()->role != 4) {
//            if (Yii::app()->request->url != '/admin/login') {
//                Yii::app()->request->redirect('/admin/login');
//            }
//        }
//        
        // this method is called when the module is being created
        // you may place code here to customize the module or the application
        // import the module-level models and components
        $this->setImport(array(
            'admin.models.*',
            'admin.components.*',
        ));
    }

    public function beforeControllerAction($controller, $action) {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            //$controller->layout = 'main';
            return true;
        }
        else
            return false;
    }

}