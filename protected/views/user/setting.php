<?php
$model = Yii::app()->user->getUser();
?>
<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/custom-theme/jquery.ui.all.css' ?>">
<!--<script src="<?php echo Yii::app()->baseUrl . '/js/jquery-1.8.3.js' ?>"></script>-->
<script src="<?php echo Yii::app()->baseUrl . '/js/ui/jquery.ui.core.js' ?>"></script>
<script src="<?php echo Yii::app()->baseUrl . '/js/ui/jquery.ui.widget.js' ?>"></script>
<script src="<?php echo Yii::app()->baseUrl . '/js/ui/jquery.ui.datepicker.js' ?>"></script>
<div class="setting-page">
    <div class="change-security-panel">
        <h4>Thông tin bảo mật</h4>
        <form class="change-security-info">

            <h1>Email:</h1>
            <h2>quyetliet@gmail.com</h2>
            <h1>Số CMT:</h1>
            <h2>012345678</h2>
            <h1>Điện thoại:</h1>
            <h2>0987654321</h2>
            <h1>Địa chỉ cụ thể:</h1>
            <input placeholder="- chưa cập nhật -" class="input-form change-password-input">
        </form>
    </div>
    <br>
    <button class="brown-small-button update-end-button">Cập nhật</button>
</div>


<div class="setting-page">
    <div class="user-setting">
        <!-- Begin Cập nhật avatar -->
        <h2><h4>Thay đổi ảnh đại diện</h4></h2>
        <form class="change-avatar">
            <h1>Tải ảnh lên từ máy <br>( dung lượng không quá 1mb)</h1>
            <input type="file"  id="avatarFile" class=""/>
            <input id="selectAvatar" class="normal-button input-small-button" type="button" value="Chọn file">
            <input id="updateAvatar" class="normal-button brown-small-button change-avatar-update" type="button" value="Cập nhật">
        </form>
        <!-- End Cập nhật avatar -->
        <!-- Begin Cập nhật mật khẩu -->
        <h2> <h4>Thay đổi mật khẩu</h4></h2>
        <br>
        <br>
        <div id="updatePassword">
            <input name="oldPassword" id="old-password" type="password" class="input validate input-form change-password-input" placeholder="Mật khẩu cũ" />
            <br>
            <input name="newPassword" id="new-password" type="password" class="input validate input-form change-password-input" placeholder="Mật khẩu mới" />
            <br>
            <input name="repeatPassword" id="repeat-password" type="password" class="input validate input-form change-password-input" placeholder="Gõ lại mật khẩu" />
            <br>    
            <br>
            <?php
            echo CHtml::ajaxButton('Cập nhật', Yii::app()->createAbsoluteUrl('/user/changePassword'), array(// these are ajax options
                'type' => 'POST',
                'data' => array('oldPassword' => 'js: $("#old-password").val()',
                    'newPassword' => 'js: $("#new-password").val()',
                    'repeatPassword' => 'js: $("#repeat-password").val()'
                ),
                'beforeSend' => 'js: function(){validate("#updatePassword",8,200)}',
                'success' => 'js:function(string){
                                        var getData = $.parseJSON(string);
                                        if(getData["code"]== -1) createDiaglog("Lỗi định dạng file", getData["message"]);
                                        if(getData["code"]== 1) {                                        
                                            createDiaglog("Thay đổi thành công", getData["message"]);                                        
                                        }                                    
                                        }'
                    // and don't use semi-colon after val(), you can also pass id here itself            
                    ), array(
                'class' => 'normal-button brown-small-button change-passord-button'
                    )
            );
            ?>
        </div>
        <!-- End Cập nhật mật khẩu -->
        <!-- Begin Cập nhật thông tin -->

        <div class="change-profile-panel">
            <form class="change-profile-info">
                <h1>Họ và tên:</h1>
                <h2>Lê Thị Quyết Liệt</h2>
                <div class="edit-button edit-button-setting"></div>
                <h1>Ngày sinh:</h1>
                <input placeholder="ngày" class="input-form change-profile-input">
                <h3>/</h3>
                <input placeholder="tháng" class="input-form change-profile-input">
                <h3>/</h3>
                <input placeholder="năm" class="input-form change-profile-input">
                <br>
                <h1>Giới tính:</h1>
                <h2>Nữ</h2>
                <div class="edit-button edit-button-setting"></div>
                <br>
                <h1>Nơi ở:</h1>
                <h2>Hà Nội</h2>
                <div class="edit-button edit-button-setting"></div>
            </form>
        </div>

        <div id="updateInformation" class="normal-information left change-profile-panel">
            <h2><h4>Thông tin cá nhân</h4></h2>
            <br>
            <br>
            <table>
                <tr>
                    <td>
                        <label for="name"> <h1>Họ và tên:</h1></label>
                    </td>
                    <td>
                        <input id="last-name" class="input name validate" name="last-name" type="text" value="<?php echo $model->last_name ?>" placeholder="Họ"/>
                        <input id="middle-name" class="input name validate" name="middle-name" type="text" value="<?php echo $model->middle_name ?>" placeholder="Đệm"/>
                        <input id="first-name" class="input name validate" name="first-name" type="text" value="<?php echo $model->first_name ?>" placeholder="Tên"/>
                    </td>
                    <td>
                        <div class="button-edit"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="birthday">Ngày sinh:</label>
                    </td>
                    <td>
                        <input id="birthday" class="input validate" name="day" type="text" value="<?php echo $model->birthday; ?>" placeholder="<?php echo $model->birthday; ?>"/>
                    </td>
                    <td>
                        <div class="button-edit"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="gender">Giới tính:</label>

                    </td>
                    <td>                        
                        <input type="radio" name="gender" value="male" /> Nam<br />
                        <input type="radio" name="gender" value="female" /> Nữ<br />
                    </td>
                    <td>
                        <div class="button-edit"></div>
                    </td>
                </tr>
                <tr>
                </tr>
            </table>
            <?php
            echo CHtml::ajaxButton('Cập nhật', Yii::app()->createAbsoluteUrl('/user/changeInformation'), array(// these are ajax options
                'type' => 'POST',
                'data' => array('lastname' => 'js: $("#last-name").val()',
                    'middlename' => 'js: $("#middle-name").val()',
                    'firstname' => 'js: $("#first-name").val()',
                    'birthday' => 'js: $("#birthday").val()',
                    'gender' => 'js: $("input[name=gender]:checked").val()'
                ),
                'beforeSend' => 'js: function(){validate("#updateInformation",0,200)}',
                'success' => 'js:function(string){
                                            var getData = $.parseJSON(string);
                                            if(getData["code"]== -1) createDiaglog("Lỗi cập nhật ", getData["message"]);
                                            if(getData["code"]== 1) {                                        
                                                createDiaglog("Thay đổi thành công ", getData["message"]);                                        
                                            }                                    
                                            }'
                    // and don't use semi-colon after val(), you can also pass id here itself            
                    ), array(
                'class' => 'normal-button'
                    )
            );
            ?>            
        </div>
        <!-- End Cập nhật thông tin -->
        <!-- Begin Cập nhật bảo mật -->
        <div id="updatePrivateInformation" class="secure-information right">
            <h2>Thông tin bảo mật</h2>
            <br>
            <br>
            <table>
                <tr>
                    <td>
                        <label for="name">Email:</label>
                    </td>
                    <td>
                        <input class="input validate" id="email" name="email" type="text" value=""/>
                    </td>
                <tr>
                <tr>
                    <td>
                        <label for="name">Số CMT:</label>
                    </td>
                    <td><input class="input validate" id="cmt" name="cmt" type="text" value=""/></td>
                </tr>
                <tr>
                    <td><label for="name">Số điện thoại:</label></td>
                    <td><input class="input validate" id="phone" name="phone" type="text" value=""/></td>

                </tr>
                <tr>
                    <td><label for="name">Địa chỉ cụ thể:</label></td>
                    <td><input class="input validate" id="address" name="address" type="text" value=""/></td>
                </tr>
            </table>            
            <?php
            echo CHtml::ajaxButton('Cập nhật', Yii::app()->createAbsoluteUrl('/user/changeInformation'), array(// these are ajax options
                'type' => 'POST',
                'data' => array('email' => 'js: $("#email").val()',
                    'cmt' => 'js: $("#cmt").val()',
                    'phone' => 'js: $("#phone").val()',
                    'address' => 'js: $("#address").val()',
                ),
                'beforeSend' => 'js: function(){validate("#updatePrivateInformation",0,200)}',
                'success' => 'js:function(string){
                                            var getData = $.parseJSON(string);
                                            if(getData["code"]== -1) createDiaglog("Lỗi cập nhật", getData["message"]);
                                            if(getData["code"]== 1) {                                        
                                                createDiaglog("Thay đổi thành công", getData["message"]);                                        
                                            }                                    
                                            }'
                    // and don't use semi-colon after val(), you can also pass id here itself            
                    ), array(
                'class' => 'normal-button'
                    )
            );
            ?> 
        </div>
        <!-- End Cập nhật bảo mật -->
    </div>
</div>
<script>
    //Set input file
    $('#avatarFile').css({height: 0, width: 0, 'overflow': 'hidden'});
    $('#selectAvatar').click(function() {
        $('#avatarFile').click();
    })
    $('#avatarFile').change(function() {
    })
    //Set button update avatar
    $('#updateAvatar').click(function() {
        if (!$('#avatarFile').val())
            createDiaglog('Chưa chọn file', 'Xin vui lòng chọn file để upload');
        else {
            var fd = new FormData();
            fd.append("avatar", $("#avatarFile")[0].files[0]);
            $.ajax({
                url: ' <?php echo Yii::app()->createAbsoluteUrl('/user/changeAvatar') ?>',
                cache: false,
                type: 'POST',
                contentType: false,
                processData: false,
                data: fd,
                success: function(string) {
                    var getData = $.parseJSON(string);
                    if (getData['code'] == -1)
                        createDiaglog('Lỗi định dạng file', getData['message']);
                    if (getData['code'] == 1) {
                        avatarUrl = getData['imageLink'];
                        createDiaglog('Thay đổi thành công', getData['message']);
                        $('#avatar').attr('src', avatarUrl);
                    }
                }
            })
        }
    })

    //Set birthday input
    $('#birthday').datepicker({
        dateFormat: "yy-mm-dd",
        changeYear: true,
        changeMonth: true,
        width: '30px',
        yearRange: "1900:2012",
        showAnim: 'slideDown'
    });
    $("#birthday").datepicker("setDate", "<?php echo $model->birthday; ?>");
    $().datepicker($.datepicker.regional[ "fr" ]);
    //validate
    function validate(selector, minlength, maxlength) {
        data = $(selector).children('.validate');
        for (var i = 0; i < data.length; i++) {
            if (!data[i].value) {
                createDiaglog('Điền thiếu thông tin', 'Xin vui lòng điền đầy đủ thông tin');
                return false;
            }
            if (data[i].value < minlength) {
                createDiaglog('Độ dài thông tin gõ vào quá ngắn', 'Xin vui lòng điền thông tin có độ dài nhỏ nhất ' + minlength + ' ký tự', 150);
                return false;
            }
            if (data[i].value > maxlength) {
                createDiaglog('Độ dài thông tin gõ vào quá dài', 'Xin vui lòng điền thông tin có độ dài lớn nhất ' + maxlength + ' ký tự', 150);
                return false;
            }
        }
        return true;
    }

</script>