<?php
$user = Yii::app()->user->getUser(); 
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->searchUser($user->id,3),           
    'itemsCssClass' => 'history-table',
    'htmlOptions' => array('class' => 'wrapper-table'),    
    'summaryText' => '',
    'emptyText' => 'Bạn chưa thực hiện lần nạp chip nào',
    'columns' => array(  
        array(
            'name' => 'timestamp',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>".$data->timestamp."</span></div>";                
            },
        ),
        array(  
            'header' => '<a>Trò chơi</a>',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>".$data->gameLog->game->name."</span></div>";                
            },
        ),
        array(  
            'header' => '<a>Kênh</a>',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>Vip</span></div>";                
            },
        ),
        array(  
            'header' => '<a>Phòng</a>',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>123</span></div>";                
            },
        ),
        array(
            'name' => 'value',
            'type' => 'raw',
            'value' =>function($data,$row){
                if($data->value > 0)
                    return "<div class='tabble-text'><span class='in-chips'>".Yii::app()->format->formatNumber($data->value)."</span></div>"; 
                else 
                    return "<div class='tabble-text'><span class='out-chips'>".Yii::app()->format->formatNumber($data->value)."</span></div>";                 
            },
        ),           
    ),
   'pager'=>array(
        'header'         => '',        
        'cssFile' => true,
        'class'=> 'CLinkPager',
        'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => '&lt;',
        'nextPageLabel'  => '&gt;',
        'lastPageLabel'  => '&gt;&gt;',
    ),     
    'pagerCssClass'=>'pagination', 
));
?>