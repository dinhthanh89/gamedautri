<div class="message-panel message-panel-active">
    <div class="message-panel-left">
        <div class="property-game-tag-out">
            <div class="property-game-tag property-game-tag-current">CHƠI GAME</div>
            <div class="property-game-tag">GIAO DỊCH CHIP</div>
            <div class="property-game-tag">NẠP CHIP</div>
        </div>
        <div id="download-right-panel-in" >
        </div>
    </div>
    <div class="message-panel-border"></div>

    <div class="message-panel-right property-item-panel">
        <div class="message-panel-right-item message-panel-right-item-active">            
             <?php echo $this->renderPartial('_history_game_transaction',array('model'=>$model),true);?>   
        </div>
        <div class="message-panel-right-item">
             <?php echo $this->renderPartial('_history_user_transaction',array('model'=>$model),true);?>      
        </div>
        <div class="message-panel-right-item">
             <?php echo $this->renderPartial('_history_recharge',array('model'=>$model),true);?>      
        </div>
    </div>
    <div style="clear: both"></div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
            setUserPanel();
            setTabHistory();        
    })
</script>