<!--<script >
    var $checked = 0;
    function click_checkbox(){
        var $checkbox = $('.checkbox');
        $checkbox.attr('checked', !$checkbox.attr('checked'));
        if($checked == 0) {
            $checked =1;
            $('.checkbox-wrapper').css("background-position","-550px -320px");
        }
        else {
            $checked = 0;
            $('.checkbox-wrapper').css("background-position","-530px -320px");
        }
    };   

</script>-->

<div class="main-page">
    <div class="page-in">
        <div class="register-content">
            <div class="breadcrumbs" id ="tree-link">
                <a href="#">Trang chủ  >></a>
                <a href="#">Quên mật khẩu</a>
            </div>
            <div class="page-in-border-1"></div>
            <div class="message-panel-left">
                <div class="property-game-tag-out">

                </div>
                <div id="download-right-panel-in">
                </div>
            </div>
            <div class="message-panel-border"></div>
            <div class="message-panel-right property-item-panel">
                <div class="form form-register-in">
                    <h2>Lấy lại mật khẩu</h2>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'users-index-form',
                        'enableAjaxValidation' => TRUE,
                        'clientOptions' => array(
                            'validateOnSubmit' => TRUE,
                            'hideErrorMessage' => FALSE,
                        )
                    ));
                    ?>
                    <?php //echo $form->errorSummary($model); ?>


                    <ul>
<!--                        <li>
                            <?php // echo $form->labelEx($model, 'Tên đăng nhập:', array('class' => 'label register-field')); ?>
                            <?php // echo $form->textField($model, 'username', array('class' => 'input register-input')); ?>
                            <?php // echo $form->error($model, 'username', array('class' => 'error')); ?>
                        </li>-->
                        <li>
                            <?php echo $form->labelEx($model, 'Địa chỉ email:', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'email', array('class' => 'error')); ?>
                        </li> 
                    </ul>
                    <!--<p>Mật khẩu mới sẽ được gửi vào email của bạn</p>-->
                    <div class="row buttons">
                        <?php echo CHtml::submitButton('Gửi yêu cầu', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
            <div style="clear: both"></div><!-- form -->
        </div>
    </div>
</div>