
<?php
$user = Yii::app()->user->getUser(); 
$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider' => $model->searchUser($user->id,2),           
    'itemsCssClass' => 'history-tabble',
    'htmlOptions' => array('class' => 'wrapper-history-table'),
    'summaryText' => '',
    'emptyText' => 'Bạn chưa thực hiện lần nạp chip nào',
    'columns' => array(  
        array(
            'name' => 'timestamp',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>".$data->timestamp."</span></div>";                
            },
        ),
        array(  
            'name' => 'recharge_method_payment_name',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>".$data->recharge_method_payment_name."</span></div>";                
            },
        ),
        array(  
            'name' => 'recharge_amount',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>".Yii::app()->format->formatNumber($data->recharge_amount)."</span></div>";                
            },
        ),       
        array(
            'name' => 'value',
            'type' => 'raw',
            'value' =>function($data,$row){
                if($data->value > 0)
                    return "<div class='tabble-text'><span class='in-chips'>".Yii::app()->format->formatNumber($data->value)."</span></div>"; 
                else 
                    return "<div class='tabble-text'><span class='out-chips'>".Yii::app()->format->formatNumber($data->value)."</span></div>";                 
            },
        ),
        array(  
            'header' => '<a>Điểm vip</a>',
            'type' => 'raw', 
            'value' =>function($data,$row){
                return "<div class='tabble-text'><span>1</span></div>";                
            },
        ),
    ),
   'pager'=>array(
        'header'         => '',
        'cssFile' => true,
        'class'=>'CLinkPager',
        'firstPageLabel' => '&lt;&lt;',
        'prevPageLabel'  => '&lt;',
        'nextPageLabel'  => '&gt;',
        'lastPageLabel'  => '&gt;&gt;',
    ),                
));
?>