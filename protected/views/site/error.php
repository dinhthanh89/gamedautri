<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle = Yii::app()->name . ' - Error';
$this->breadcrumbs = array(
    'Error',
);
?>
<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Lỗi</a>
        </div>
        <div class="page-in-border-1"></div>
        <div style="padding: 10px 10px 10px 20px; color: white">
            <h2>Lỗi <?php echo $code; ?></h2>

            <div class="error">
                Đường dẫn không tồn tại
                <?php //echo CHtml::encode($message); ?>
            </div>
        </div>
    </div>
</div>