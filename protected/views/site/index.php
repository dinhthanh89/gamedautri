<?php

/* @var $this SiteController */

$this->pageTitle = Yii::app()->name;
if (Yii::app()->request->getParam('newuser') == 1 && Yii::app()->user->is_login()):?>
    <script>
        createDiaglog('Đăng k? thành công', 'B?n đ? đăng k? thành công và có th? s? d?ng ch?c năng c?a ngư?i dùng', 200);
    </script>
<?php
endif;
?>   
<div class="slide">    
    <div class="main"> 
        <div class="slide-left">
            <div class="slide-left-content">
                <div class="slide-left-item">
                    <img src="images/xcb_banner.jpg" width="" height="" alt="slide">
                </div> 
                <div class="slide-left-item">
                    <img src="images/slide_2.jpg" width="" height="" alt="slide">
                </div>
                <div class="slide-left-item">
                    <img src="images/slide_1.jpg" width="" height="" alt="slide">
                </div>
                 <div class="slide-left-item">
                    <a href="<?php echo Yii::app()->createUrl('news/detail?id=37') ?>"><img src="images/slide_4.jpg" width="" height="" alt="slide"></a>
                </div>
            </div>
            <div class="radio-slide">
                <div class="radio-check" contextmenu="0" id="radio-check1"></div>
                <div class="radio-check" contextmenu="1" id="radio-check2"></div>
                <div class="radio-check" contextmenu="2" id="radio-check3"></div>
                <div class="radio-check" contextmenu="3" id="radio-check4"></div>
            </div>                
        </div>
        <a class="slide-play-game" href="<?php echo Yii::app()->createUrl('play') ?>"></a>
        <a class="slide-download-game" href="<?php echo Yii::app()->createUrl('download')?>"></a>
        <div class="slide-right">
            
        </div>
    </div>
    <script type="text/javascript">                
        autoSlide();
        //Set slide click
        $('.radio-check').click(function(){
            slideNow = parseFloat($(this).attr('contextmenu'));
            setSlideContent(slideNow);
        })
        $('.slide-left-content').click(function() {
            nextSlide();
        })
    </script>   
</div>
<div class="feature">
    <div class="feature-top-shadow"></div>
    <div class="feature-main">
        <div class="main">
            <div class="feature-title">Hệ thống game trên ESIMO</div>
            <ul>
                <?php if(isset($games)): ?>
                <?php foreach($games as $index => $game):?>
                <li class="<?php ($index == 0) ? "first-child" : "" ?>">     
                    <?php if($game->status == 1): ?>
                    <a href="<?php echo Yii::app()->createUrl($game->url) ?>">
                        <img width="150px" height="150px" alt="<?php echo $game->name ?>" src="<?php echo Yii::app()->createAbsoluteUrl($game->image) ?>"/>
                    </a>
                    <?php else: ?>
                        <span></span>
                        <img width="150px" height="150px" alt="<?php echo $game->name ?>" src="<?php echo Yii::app()->createAbsoluteUrl($game->image) ?>"/>
                    <?php endif;?>
                    <div>
                        <?php if($game->status == 1): ?>
                        <a class="play-now" href="<?php echo Yii::app()->createUrl($game->url) ?>"></a>
                        <a class="download-link" href="<?php echo Yii::app()->createUrl('download/detail?id='.$game->id) ?>"></a>
                        <?php else:?>
                        <div class="coming-soon-out"><div class="coming-soon">Coming soon!</div></div>
                        <?php endif;?>
                    </div>
                </li>                
                <?php endforeach;?>
                <?php endif;?>               
            </ul>
        </div>
    </div>
    <div class="feature-bottom-shadow"></div>
</div>   
    
<div class="recharge-info">
    <div class="main">
        <div class="recharge-title">
            <span>NẠP CHIP BẰNG TIN NHẮN</span>
            <a href="<?php echo Yii::app()->createUrl('recharge') ?>">NẠP CHÍP BẰNG THẺ CÀO >></a>
        </div>        
        <div class="recharge-content">
            <span class="recharge-contruct"></span>
            <span class="recharge-text">Gửi</span>
            <span class="recharge-phone"></span> 
            <ul>
                <?php if(isset($infoRecharge)): ?>
                <?php $ratio = Configuration::model()->getConfig('gold_by_chip');?>
                <?php foreach($infoRecharge as $index => $item):?>
                <li><span><?php echo number_format($item->value, 0, ',', '.'); ?> VNĐ/tin</span><span><?php echo number_format((ceil($item->value * $item->ratio) * $ratio), 0, ',', '.') ?> chips</span></li>
                <?php if($index != (count($infoRecharge) - 1)):?>
                <li class="border-shadow"></li> 
                <?php endif;?>
                <?php endforeach;?>
                <?php endif;?>                
            </ul>            
        </div>
    </div>            
</div>    