<?php
//$model = new Game(); 
$androidLinks = $model->downloadLinks('android');
$iosLinks = $model->downloadLinks('ios');
$cs = Yii::app()->getClientScript();
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/all.js');
?>
<script type="text/javascript">
    // Set variables
    var content =[];
    content[1] = '<?php echo $model->description; ?>';
    content[2] = '<?php echo $model->rule; ?>';
    content[3] = '<?php echo $model->honor; ?>';
    content[4]= '<?php echo $model->old_version; ?>';
    point = <?php echo $model->pointVote(); ?>;
    gameId = <?php echo $model->id ?>;
    
    $(document).ready(function(){
        //Point of game
        //Set star for game
        setDefaultStar(point);
        //Set content for main content
        $('#content').html(content[1]);
        //Set menu for content
        $('#content-menu').children().click(function () {
            if($(this).hasClass('button-active')) return;
            $('#content-menu .button-active').removeClass('button-active').addClass('button');
            $(this).addClass('button-active');
            $('#content').html(content[$(this).attr("contextmenu")]);
        });
        //Set hover for star
        $('.star').hover(
        function () {
            $(this).prevAll('.star').each(function(){
                setFinalStar($(this));
            })
            $(this).nextAll('.star').each(function(){
                setZeroStar($(this));
            })
            setFinalStar($(this));
        },
        function (){
            setDefaultStar(point);
        });
        //Set click vote
        $('.star').click(function(){
            var point = $(this).attr('point');
            var url = '<?php echo Yii::app()->createAbsoluteUrl('/site/vote/') ?>' + '/id/'+ gameId +'/point/'+point;
            $.ajax({
                url: url
            }).done(function(vote){
                //alert(msg);
                if(vote == 0) alert('Có lỗi');
                else{
                    window.point = vote;
                    setDefaultStar(vote);
                }
            })
        })
    })
</script>
<div class="download-detail" style="min-height: 900px;  height: auto">
    <div class="breadcrumbs">
        <a href="#">Trang chủ >></a>
        <a href="#">Download</a>
    </div>
    <div class="top-crossbar"></div>
    <div class="game-detail">
        <div class="left-sidebar">
            <img class="game-avatar" src="<?php echo $model->imageLink('avatar'); ?>"/>
            <a class="download" href="#download"></a>
            <div class="vote">
                <div class="star" point="1"></div>
                <div class="star" point="2"></div>
                <div class="star" point="3"></div>
                <div class="star" point="4"></div>
                <div class="star" point="5"></div>
            </div>
            <table>
                <tr>
                    <td><p class="label">Thể loại:</p></td>
                    <td><p class="value"><?php echo $model->categoryName(); ?></p></td>
                </tr>
                <tr>
                    <td><p class="label">Phiên bản:</p></td>
                    <td><p class="value"><?php echo $model->current_version; ?></p></td>
                </tr>
                <tr>
                    <td><p class="label">Lượt tải:</p></td>
                    <td><p class="value"><?php echo $model->count_download; ?></p></td>
                </tr>
            </table>
        </div>
        <div class="right-sidebar">
            <div class="menu" id="content-menu">
                <div class="menu-1 button-active" contextmenu="1" >
                    <p>Giới thiệu</p>
                </div>
                <div class="menu-1 button" contextmenu="2">
                    <p>Luật chơi</p>
                </div>
                <div class="menu-1 button" contextmenu="3">
                    <p>Vinh danh</p>
                </div>
                <div class="menu-1 button" contextmenu="4">
                    <p>Phiên bản cũ</p>
                </div>
            </div>
            <div class="social">
                <a class="facebook" href="<?php echo $model->socialLink('facebook'); ?>"></a>
                <a class="google" href="<?php echo $model->socialLink('google'); ?>" ></a>
                <a class="twitter" href="<?php echo $model->socialLink('twitter'); ?>" ></a>
                <a class="yahoo" href="<?php echo $model->socialLink('yahoo'); ?>"></a>    
            </div>
            <div class="content">
                <p id="content">
                    <?php echo $model->description; ?>
                </p>
            </div>
            <div class="slide">
                slide
            </div>
        </div>
    </div>
    <div id="download" class="download">
        <div class="left-sidebar"> 
            <div class="title">
                <div class="left">
                </div>
                PHỎM CHO ANDROID
                <div class="right">
                </div>
            </div>
            <?php
            if (isset($androidLinks)):
                foreach ($androidLinks as $value):
                    ?>
                    <div class="download-item">
                        <div class="direct-download" onclick="window.location.assign('<?php echo $value->direct() ?>')" >
                            <p>
                                <?php echo $value->description; ?>
                            </p>
                        </div>
                        <div class="right">
                            <div class="size">
                                Dung lượng: <span><?php echo $value->size; ?>M</span>
                            </div>
                            <div class="version">
                                Phiên bản: <span><?php if (isset($value->version)) echo $value->version; else echo $model->version; ?></span>
                            </div>
                        </div>
                        <a class="store google" href="<?php echo $value->store_link; ?>">
                        </a>
                        <div class="device">
                            Thiết bị hỗ trợ:
                            <p><?php echo $value->devices; ?></p>
                        </div>
                        <div class="bottom-cross"></div>
                    </div>
                    <?php
                endforeach;
            endif;
            ?>
        </div>
        <div class="right-sidebar">
            <?php ?>
            <div class="title">
                <div class="left">
                </div>
                PHỎM CHO IOS
                <div class="right">
                </div>
            </div>
            <?php
            if (isset($iosLinks)):
                foreach ($iosLinks as $value):
                    ?>
                    <div class="download-item">
                        <div class="direct-download" onclick="window.location.assign('<?php echo $value->direct(); ?>')" >
                            <p><?php echo $value->description; ?></p>
                        </div>
                        <div class="right">
                            <div class="size">
                                Dung lượng: <span><?php echo $value->size ?>M</span>
                            </div>
                            <div class="version">
                                Phiên bản: <span><?php if (isset($value->version)) echo $value->version; else echo $model->version; ?></span>
                            </div>
                        </div>
                        <a class="store google" href="<?php echo $value->store; ?>">
                        </a>
                        <div class="device">
                            Thiết bị hỗ trợ:
                            <p><?php echo $value->device; ?></p>
                        </div>
                        <div class="bottom-cross"></div>
                    </div>
                    <?php
                endforeach;
            endif;
            ?>
        </div>
    </div>
</div>
