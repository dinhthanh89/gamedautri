<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1,user-scalable=no">
	<title>Cửa hàng</title>
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/style.min.css' ?>">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/shop.css' ?>">	
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>             
        <script src="<?php echo Yii::app()->baseUrl . '/js/jquery-ui-1.9.2.custom.min.js' ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/js/jquery.bpopup.min.js' ?>"></script>
</head>
<body>
<div id="content">    
<?php if(isset($categoryProduct)): ?>
<?php $j = 0; ?>    
<?php foreach($categoryProduct as $category):?>
<?php if($category['info']): ?>    
    <div class="tabs <?php echo ($j == 0) ? 'active' : '' ?> <?php echo "category-root-".$category['info']->id; ?> <?php if(isset($category['childrens'])) echo "has-child" ?>">    
    <?php if(isset($category['childrens'])): ?>
    <ul class="sub-tab">
        <?php foreach($category['childrens'] as $k => $children): ?>
        <li id="category-children-<?php echo $children->id ?>" class="tab-li <?php if($k == 0) echo 'tab-active' ?>">
            <a><?php echo $children->name ?></a>
        </li>        
        <?php endforeach;?>
    </ul>
    <?php endif; ?>        
    <?php if(isset($category['childrens'])):?>
    <div class="wrapper">    
    <?php $k = 0;?>    
    <?php foreach($category['childrens'] as $children):?>        
        <div class="category-children-<?php echo $children->id ?> sub-content <?php echo ($k == 0) ? 'sub-active' : '' ?>">            
            <?php if(!empty($product[$children->id]) && count($product[$children->id]) > 0): ?>                    
                <?php foreach($product[$children->id] as $item): ?><div class="product-item">
                    <a href="">
                        <div class="product-id" style="display:none"><?php echo $item->id; ?></div>
                        <div class="product-image">
                            <span>
                                <img src="<?php echo Yii::app()->createAbsoluteUrl($item->image); ?>">
                            </span>
                        </div>
                        <div class="product-name"><?php echo $item->name; ?></div>
                        <div class="product-description" style="display:none"><?php echo $item->description; ?></div>
                        <div class="category-code" style="display:none"><?php echo $item->categoryProduct->code_identifier; ?></div>
                        <?php if($item->chip > 0): ?>
                            <div><div class="product-chip"><?php echo $item->chip; ?></div><img class="image-icon" width="9px" height="8px" style="margin-left:3px;display:inline-block;vertical-align: 2px;" src="<?php echo Yii::app()->createAbsoluteUrl('css/images/icon_chip.png') ?>"/></div>
                        <?php endif;?>
                        <?php if($item->gold > 0):?>    
                            <div><div class="product-gold"><?php echo $item->gold; ?></div><img class="image-icon" width="9px" height="8px" style="margin-left:3px;display:inline-block;vertical-align: 2px;" src="<?php echo Yii::app()->createAbsoluteUrl('css/images/icon_gold.png') ?>"/></div>
                        <?php endif;?>    
                    </a>
                </div><?php endforeach;?>
            <?php else:?>
            <div>Chưa có sản phẩm nào!</div>
            <?php endif;?>                
        </div>    
    <?php $k++;?>    
    <?php endforeach;?>
    </div>    
    <?php else:?>
        <?php if(!empty($product[$category['info']->id]) && count($product[$category['info']->id]) > 0): ?>
        <?php foreach($product[$category['info']->id] as $item): ?><div class="product-item">
                <a href="">
                    <div class="product-image">
                        <span>
                            <img src="<?php echo Yii::app()->createAbsoluteUrl($item->image); ?>">
                        </span>
                    </div>                                                              
                    <div class="product-id" style="display:none"><?php echo $item->id; ?></div>
                    <div class="product-name" style="<?php echo ($category['info']->code_identifier == 'doi_chip') ? 'display:none' : ''?>"><?php echo $item->name; ?></div>
                    <div class="product-description" style="display:none"><?php echo $item->description; ?></div>
                    <div class="category-code" style="display:none"><?php echo $item->categoryProduct->code_identifier; ?></div>
                    <?php if($item->chip > 0): ?>
                    <div><div class="product-chip"><?php echo $item->chip; ?></div><img class="image-icon" width="9px" height="8px" style="margin-left:3px;display:inline-block;vertical-align: 2px;" src="<?php echo Yii::app()->createAbsoluteUrl('css/images/icon_chip.png') ?>"/></div>
                    <?php endif;?>
                    <?php if($item->gold > 0):?>    
                        <div><div class="product-gold"><?php echo $item->gold; ?></div><img class="image-icon" width="9px" height="8px" style="margin-left:3px;display:inline-block;vertical-align: 2px;" src="<?php echo Yii::app()->createAbsoluteUrl('css/images/icon_gold.png') ?>"/></div>
                    <?php endif;?>    
                </a></div><?php endforeach;?>
        <?php else:?>
        <div>Chưa có sản phẩm nào</div>
        <?php endif;?>
    <?php endif;?>       
    </div>
<?php endif;?> 
<?php $j++;?>    
<?php endforeach;?>
<?php endif;?>
</div> 
<div id="menu">
<div class="grandien-top"></div>    
<ul>
    <?php if(isset($categoryProduct)): ?>
    <?php $i = 0; $totalWidth = 0;?>
    <?php foreach($categoryProduct as $index => $category):?>
    <?php if($category['info']): ?>
        <?php         
        $num = count($categoryProduct);
        $width = round((100/$num), 0);
        if($i != ($num - 1))
            $totalWidth += $width;
        ?>
        <li style="<?php if($i != ($num-1)) echo 'width:'.$width.'%'; else echo 'width:'.(100- $totalWidth).'%'; ?> <?php if($category['info']->status == 0) echo ";color:#666" ?>" id="<?php echo "category-root-".$category['info']->id; ?>" class="menu-li <?php if($i == 0) echo 'menu-active'?> <?php if($category['info']->status == 1) echo 'menu-display' ?>">
            <a><div class="menu-name"><?php echo $category['info']->name ?></div></a>
            <?php if($i != ($num-1)):?>
            <div></div>
            <?php endif;?>
        </li>
        <?php $i ++; ?>
    <?php endif;?>    
    <?php endforeach;?>
    <?php endif;?>
</ul>
</div>
<div id="popup" style="display:none">
    <span class="button b-close"><span>X</span></span> 
    <div class="popup-content">
        
    </div>   
    <div class="error" style="display:none;color:red"></div>
    <div class="popup-button"><span><a>Đồng ý mua</a></span></div>
</div>     
<div id="checkout" style="display:none">
    <div class="checkout-top"><span class="back-home"><span></span></span><span id="checkout_product_name"></span><span id="checkout_product_price"></span></div>
    <div id="error_info" style="color:red;display:none"></div>
    <form>
        <div class="form-left">
            <div>Thông tin nhận hàng</div>
            <input id="form_username" type="text" value="" disabled="disabled">            
            <input id="form_phone" type="text" value="" placeholder="Số điện thoại">            
            <input id="form_email" type="text" value="" placeholder="Email">            
            <input id="form_address" type="text" value="" placeholder="Địa chỉ">            
        </div>
        <div class="form-right">
            <div>Số lượng</div>
            <input id="form_qty" type="number" name="" value="">
            <textarea id="form_comment" name="" placeholder="Nội dung ..."></textarea>
            <div class="total"><label>Tổng :</label><span></span></div>
            <div class="form-button"><span><a>Xác nhận mua</a></span></div>
        </div>
    </form>
</div>    
</body>
<script type="text/javascript">      
    $(document).ready(function(){
        var config = 73;
        var width = $(window).width();        
        if(width > 700){
            config = 108;            
            $('.product-image').css('width','100px');
            $('.product-image').css('height','100px');
            $('.product-image').css('line-height','94px');
            $('.product-item').css('padding-left','10px');
            $('.product-item').css('padding-right','10px');
            $('.product-image img').css('max-height','90px');
            $('.product-image img').css('max-width','90px');           
            $('.product-item').css('max-width','108px');       
            $('.image-icon').width('18px');
            $('.image-icon').height('16px');
            $('.image-icon').css('vertical-align','-3px');
            $('.product-name,.product-chip,.product-gold,.ui-tabs.ui-tabs-vertical .ui-tabs-nav li').css('font-size','80%');                          
        }
                
        cMainItem = Math.floor(width/config);                
        tmp = width - config * cMainItem;              

        margin = Math.ceil(tmp / (cMainItem + 1));                 
        $('.tabs').not('.has-child').children('.product-item').css('margin-left',margin);

        cItem = Math.floor((0.75 * width)/config);         
        tmp2 = (0.75 * width) - (config* cItem);         
        margin2 = Math.ceil(tmp2 /(cItem + 1));               
        $('.has-child .product-item').css('margin-left',margin2);            
        
    })
    $('#menu li.menu-display').click(function(){
        $('.tabs').removeClass('active');
        $('.menu-li').removeClass('menu-active');
        var cl = $(this).attr('id');    
        $(this).addClass('menu-active');
        $('.'+cl).addClass('active');
        $('#checkout').hide();
        $('#content').show();
    });//
    
    $('.sub-tab li').mousedown(function(){
        $('.sub-content').removeClass('sub-active');
        $('.tab-li').removeClass('tab-active');
        var clsub = $(this).attr('id');   
        $(this).addClass('tab-active');
        $('.'+clsub).addClass('sub-active');
    })
    $('.tabs').tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
    
    var img ='';
    var product_id = '';
    var product_name = '';
    var product_description = '';
    var category_code = '';
    var product_gold = '';
    var product_chip = '';
    var bPopup;
    
    $('.product-item').bind('click', function(e) {
        // Prevents the default action to be triggered. 
        e.preventDefault();
        $('.error').hide();
        $('.popup-button').show();
        img = $(this).find('img').attr('src');
        product_name = $(this).find('.product-name').html();
        product_description = $(this).find('.product-description').html();
        category_code = $(this).find('.category-code').html();
        product_gold = $(this).find('.product-gold').html();
        product_chip = $(this).find('.product-chip').html();
        product_id = $(this).find('.product-id').html();
        
        var html = '<div class="popup-img"><span></span><img src="'+img+'"></div><div class="popup-column-right"><div class="popup-product-name">'+product_name+'</div><div class="popup-product-description">'+product_description+'</div></div>';    
        html += '<div class="popup-text">Mua vật phẩm này bạn cần  ';
        if(product_gold != null)
            html += '<span>'+product_gold+' Gold</span>';
        
        if(product_chip != null)
            html += '<span> '+product_chip+' Chip</span>';
        
        html += '</div>';
        $('.popup-content').html(html);
        var width = $(window).width();
        if(width > 700){
            $('.popup-img').css('width','100px');
            $('.popup-img').css('height','100px');
            $('.popup-button span,.popup-product-name,.popup-text,.popup-product-description').css('font-size','90%');
        }
        // Triggering bPopup when click event is fired
        bPopup = $('#popup').bPopup({
            modalClose: false,
            opacity: 0.6,
            positionStyle: 'fixed' //'fixed' or 'absolute'
        }); 
    });
    
    var username = '<?php echo $user->username ?>';
    var email = '<?php echo $user->email ?>';
    var phone = '<?php echo $user->mobile ?>';
    var address = '<?php echo $user->address ?>';
    var qty = 1;
    
    $('.popup-button span').click(function(){        
        if(category_code == 'doi_chip'){            
            var checkout2 = {
                username: username,                
                product_id:product_id,                
                accessToken: '<?php echo $accessToken; ?>'
            }
            $.ajax({
              url: '<?php echo Yii::app()->createUrl('api/checkoutGoldToChip'); ?>',
              method: "POST",
              dataType: 'json',
              data:checkout2,
              success: function(obj){                   
                  if(obj.code != 0){
                      $('.error').html(obj.message);
                      $('.error').show();
                  }else{     
                      $('.popup-content').html(obj.message);
                      $('.popup-button').hide();
                     // bPopup.close(); 
                  }
              }
          }); 
        }else{                    
            bPopup.close();        
            $('#content').hide();
            $('#checkout').show();    
            $('#checkout_product_name').html(product_name);
            var price = '';
            if(product_gold != null)
                 price = product_gold+' Gold';

            if(product_chip != null)
                price += product_chip+' Chip';

            $('#checkout_product_price').html(price);
            if(username != '')
                $('#form_username').val(username);
            if(email != '')
                $('#form_email').val(email);
            if(phone != '')
                $('#form_phone').val(phone);
            if(address != '')
                $('#form_address').val(address);
            $('#form_qty').val(qty);
            $('.total span').html(price);
        }
    });
    
    $('#form_qty').change(function(){
        var total = '';
        var quatity = $('#form_qty').val();
        if(product_gold != null)
             total = (parseInt(product_gold) * parseInt(quatity)) + ' Gold';
        
        if(product_chip != null)
            total += ' '+(parseInt(product_chip) * parseInt(quatity))+' Chip';
        
        $('.total span').html(total);
    })
    
    $('.back-home').click(function(){
        $('#checkout').hide();
        $('#content').show();
        //$('#menu').css('position','absolute');
    })
    
    $('#checkout input').focus(function(){
        //$('#menu').css('position','fixed');
        $('#checkout').css('padding-bottom','50%');
        var pthis = this;
        $('html, body').animate({
            scrollTop: $(pthis).offset().top
        }, 1000);        
    })
    
    $('.form-button span').click(function(){        
        var checkout = {
            username: $("#form_username").val(),
            email:$("#form_email").val(),
            phone:$("#form_phone").val(),
            address:$('#form_address').val(),
            product_id:product_id,
            qty: $('#form_qty').val(),
            comment: $('#forum_comment').val(),
            accessToken: '<?php echo $accessToken; ?>'
        }
        $.ajax({
          url: '<?php echo Yii::app()->createUrl('api/checkout'); ?>',
          method: "POST",
          dataType: 'json',
          data:checkout,
          success: function(obj){                                      
              if(obj.code != 0){
                    $('#error_info').html(obj.message);                
                    $('#error_info').show();
                    $('html, body').animate({
                        scrollTop: $('#error_info').offset().top
                    }, 1000);  
//                  html = '<div>'+obj.message+'</div>';
//                  $('.popup-content').html(html);
//                  $('.popup-button').remove();
//                    // Triggering bPopup when click event is fired
//                    bPopup2 = $('#popup').bPopup({
//                        modalClose: false,
//                        opacity: 0.6,
//                        positionStyle: 'fixed' //'fixed' or 'absolute'
//                    }); 
                 //$('#content').show();
                 //$('#checkout').hide();                   
              }else{                            
                  $('#content').show();
                  $('#checkout').hide();       
                  bPopup3 = $('#popup').bPopup({
                        modalClose: false,
                        opacity: 0.6,
                        positionStyle: 'fixed' //'fixed' or 'absolute'
                    }); 
                  $('.popup-content').html(obj.message);
                  $('.popup-button').hide();
              }
          }
      });  
    })
                    
</script>
</html>

