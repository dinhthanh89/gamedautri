<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="initial-scale=1,user-scalable=no">
	<title>Sự kiện</title>

	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/idangerous.swiper.css' ?>">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/normalize.css' ?>">
	<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/css/movies-app.css' ?>">
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>     
        <script src="<?php echo Yii::app()->baseUrl . '/js/idangerous.swiper-2.1.min.js' ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/js/movies-app.js' ?>"></script>
</head>
<body>
    <div id="fb-root"></div>
    <script type="text/javascript">
    var base_url = "<?php echo Yii::app()->createAbsoluteUrl(''); ?>";    
    <!--
    window.fbAsyncInit = function() {
     FB.init({appId: '207710049415562', status: true, cookie: true, xfbml: true});
     FB.Event.subscribe('edge.create', function(href, widget) {
     // Do something, e.g. track the click on the "Like" button here        
     
     window.location.href = base_url+"/api/likeFacebook/index.php";
     });

    };
    (function() {
     var e = document.createElement('script');
     e.type = 'text/javascript';
     e.src = document.location.protocol + '//connect.facebook.net/en_US/all.js';
     e.async = true;
     document.getElementById('fb-root').appendChild(e);
     }());
    //-->
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#invite_facebook').click(function(){
                event.preventDefault();
                newInvite();
            });            
        });
        function newInvite(){
            var receiverUserIds = FB.ui({ 
                   method : 'apprequests',
                   message: 'Invite your Facebook friends to use this app.'
            },
            function(response) {                                           
                var send_invitation_url=base_url+'/api/inviteFacebook'+'?to='+response.to;                
                window.location = send_invitation_url;
                $.ajax({
                    url:send_invitation_url,
                    data:{
                        to:response.to
                        },
                    dataType:"json",
                    type: 'POST',
                    success: function(data){
                        
                    },
                    errors: function(data){
                        
                    }

                })                
              }
            );
            //http://developers.facebook.com/docs/reference/dialogs/requests/
        }
        
    </script>

	<div class="swiper-container swiper-content">
            <div class="swiper-wrapper">
                <?php if(isset($model)):?>
                <?php foreach($model as $item):?>
                <div class="swiper-slide" style="overflow: auto; height: auto;">
                    <div class="inner">
<!--                        <h1>Movie 1</h1>-->
                        <img class="movie-pic" src="<?php echo Yii::app()->createUrl($item->main_image); ?>" height="150" width="150" alt="">
                        <div class="movie-text"><?php echo $item->title; ?></div>                        
                        <div class="movie-text"><?php echo $item->content; ?></div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <?php endforeach;?>
                <?php endif;?>
            </div>
	</div>
	<div class="swiper-container swiper-nav">
            <div class="swiper-wrapper">
                <?php if(isset($model)):?>
                <?php foreach($model as $item):?>
                    <div class="swiper-slide active-nav">
                        <span class="angle"></span>
                        <img src="<?php echo Yii::app()->createUrl($item->main_image) ?>" alt="">
<!--                        <div class="title">Movie 1</div>-->
                    </div>			
                <?php endforeach;?>
                <?php endif;?>
            </div>
	</div>			
</body>
</html>