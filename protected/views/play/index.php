<div class="content-play">    
    <div class="main body content-play-main">
        <div class="background-eclip-play">
            <img src="<?php echo Yii::app()->createAbsoluteUrl('css/images/background-eclip.png') ?>"/>
        </div>
        <div class="group-game-play">
            <div class="text-play">Mời bạn chọn game</div>
            <ul>
                <?php if(isset($games)): ?>
                <?php foreach($games as $index => $game):?>
                <li>                    
                    <a href="<?php echo Yii::app()->createUrl($game->url) ?>">
                        <?php if($game->status != 1):?>
                        <span class="comming-soon-img"></span>
                        <?php endif;?>
                        <img width="150px" height="150px" src="<?php echo Yii::app()->createAbsoluteUrl($game->image) ?>"/>                
                    </a>
                </li>                
                <?php endforeach;?>
                <?php endif;?>               
            </ul>
        </div>
    </div>
</div>
