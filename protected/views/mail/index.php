<html>
    <head>
    </head>
    <body>
        Dear <b><?php
            echo $userinfo->username;
            ?></b>
        <br>
        <br>
        Có người đã sử dụng chức năng quên mật khẩu với tài khoản có email của bạn trên website <?php echo Yii::app()->createAbsoluteUrl(''); ?> , nếu đó là bạn xin vui lòng bấm vào địa chỉ để xác nhận :
        <br>
        <a href="<?php echo Yii::app()->createAbsoluteUrl("site/resetPassword?access_token=".$userinfo->access_token); ?>"><?php echo Yii::app()->createAbsoluteUrl("site/resetPassword?access_token=".$userinfo->access_token); ?></a>        
        <br>
        Lưu ý link chỉ hiệu lực trong vòng 5 ngày !
        <br>
        <br>
        
        <p><strong>Trong trường hợp bạn cần sự hỗ trợ của chúng tôi. Bạn có thể chọn một trong 3 cách sau:</strong></p>
        1. Gọi điện cho bộ phận hỗ trợ khách hàng tại: 0126 25 00000<br>
        2. Email cho chúng tôi tới địa chỉ: <a target="_blank" href="mailto:hotro@esimo.vn">hotro@esimo.vn</a><br>
        3. Chat trực tuyến với nhân viên tư vấn <a target="_blank" title="Tư vấn trực tuyến" href="<?php echo Yii::app()->homeUrl ?>" style="text-decoration:underline">tại đây</a><br>
        <br>
        <font style="font-style: italic">Nếu không phải là bạn xin vui lòng bỏ qua email này</font>
        <br>
        <p>Cảm ơn bạn,</p><br>
                
    </body>
</html>