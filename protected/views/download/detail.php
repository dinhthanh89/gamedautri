<div class="main-page">
    <div class="page-in">
        <div class="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a href="<?php echo Yii::app()->createUrl('download') ?>">Download</a>>><a><?php echo $model->name ?></a>
        </div>
        <div class="page-in-border-1"></div>
        <div class="phom-out phom-out-ios">           
            <div class="game-detail-content">
                <img width="200px" height="200px" src="<?php echo Yii::app()->createAbsoluteUrl($model->image) ?>">                
                    <span class="title"><?php //echo $model->name ?></span>                
                    <?php if(isset($gameVersionModel)): ?>
                    <?php foreach($gameVersionModel as $item):?>
                    <div class="phom-item">
                        
                        <?php if($item->gamePlatform->platform->name == 'android'):?>
                        <div class="phom-android">PHỎM TRÊN ANDROID</div>
                        <a id="android-link" onclick="downloadFile($('#android-link').attr('link'));" link='<?php echo $item->id?>' class="download-game-button">                                        
                            <?php $classStore = 'download-on-google-play'?>
                            <span>DOWN FILE .APK CHO ANDROID</span>                                                           
                        </a>
                        <div class="phom-item-info">
                            <span>Dung lượng:</span>
                            <span><?php echo $item->getSize()?></span>
                            <br>
                            <span>Phiên bản:</span>
                            <span><?php echo $item->getCurrentVersion() ?></span>                      
                        </div>
                        <?php elseif($item->gamePlatform->platform->name == 'ios'): ?>
                        <div class="phom-ios">PHỎM TRÊN IOS</div>
                        <a id="ios-link" onclick="downloadFile($('#ios-link').attr('link'));" link='<?php echo $item->id?>' class="download-game-button">                                                            
                            <?php $classStore = 'download-on-app-store'?>
                            <span>DOWN FILE .IPA CHO IOS</span>
                        </a>
                        <div class="phom-item-info">
                            <span>Dung lượng:</span>
                            <span><?php echo $item->getSize()?></span>
                            <br>
                            <span>Phiên bản:</span>
                            <span><?php echo $item->getCurrentVersion();?></span>                      
                        </div>
                        <?php else:?>
                            <?php $classStore = '';?>
                        <?php endif;?>                        
                        <a target="_blank" class="<?php echo $classStore ?>" href="<?php echo $item->gamePlatform->market_url; ?>"></a>
                    </div>                        
                    <?php endforeach;?>
                    <?php endif;?>                
                <div style="clear: both"></div>
            </div>
            <div class="game-detail-description">
                <?php echo $model->description ?>
            </div>
            <div class="game-detail-guide"></div>
            
        </div>            
    </div>
</div>