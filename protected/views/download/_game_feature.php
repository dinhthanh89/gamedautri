<div class="card-game-in">
    <a class="card-game-logo" href="<?php echo Yii::app()->createAbsoluteUrl('/download/detail/id/'.$data->id); ?>"><img src="<?php echo Yii::app()->createAbsoluteUrl($data->image); ?>"/></a>
    <div class="game-stars">
<!--        <div class="game-details"> 
            <div class="rate-stars" data-average="12" data-id="1"></div>            
        </div>-->
        <div class="game-details">
            <span class="game-details-name">Phiên bản:</span>
            <span class="game-details-info"><?php if($data->getCurrentVersion('web')) echo $data->getCurrentVersion('web')->getCurrentVersion();?></span>
            <div></div>
            <span class="game-details-name">Lượt tải:</span>
            <span class="game-details-info"><?php echo $data->countDownload;?></span>
        </div>
        <div class="group-links-download">
            <?php if($data->status == 1): ?>
            <a class="play-now" href="<?php echo Yii::app()->createUrl($data->url) ?>"></a>
            <a class="download-link" href="<?php echo Yii::app()->createUrl('download/detail?id='.$data->id) ?>"></a>
            <?php else:?>
            <div class="coming-soon-out"><div class="coming-soon">Coming soon!</div></div>
            <?php endif;?>
        </div>
<!--        <div class="like-game-form">  
            <div class="like-item margin-left-5">
                <script>(function(d, s, id) {
                      var js, fjs = d.getElementsByTagName(s)[0];
                      if (d.getElementById(id)) return;
                      js = d.createElement(s); js.id = id;
                      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=328563347242091";
                      fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                </script>
                <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
            </div>
            <div class="like-item margin-left-5">
                <div class="g-plusone" data-size="tall" data-href="https://developers.facebook.com/docs/plugins/"></div>
                 Place this render call where appropriate 
                <script type="text/javascript">
                    (function() {
                       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                       po.src = 'https://apis.google.com/js/plusone.js';
                       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                    })();
                </script>    
            </div>
            <div class="like-item margin-left-10">
                <a href="https://developers.facebook.com/docs/plugins/" class="twitter-share-button" data-count="vertical" data-lang="en">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>            
            </div>    
        </div>-->
    </div>
</div>
<div class="card-game-border-2"></div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.rate-stars').jRating();
    });
</script>
