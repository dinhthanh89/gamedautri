<?php
$detect = new MobileDetect();
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
?>
<div class="main-page">
    <div class="page-in">
        <div class="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Download</a>
        </div>
        <div class="page-in-border-1"></div>
        <a class="game-name">DOWNLOAD GAME</a>
        <div class="download-out">
            <div id="download-left-panel">                
                <div id="card-game">                      
                    <?php $this->widget('zii.widgets.CListView', array(
                                    'dataProvider'=>$dataGameFeature,
                                    'itemView'=>'_game_feature',
                                    'summaryText'=>'',
                                    )); 
                    ?>
                </div>    
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
</div>