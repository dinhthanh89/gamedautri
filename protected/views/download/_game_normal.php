<div class="casino-game-item">
    <div class="casino-game-logo"></div>
    <div class="casino-game-info">
        <h1>Lucky 777</h1>
        <p>Luck 777 là trò chơi  tương tự như game điện tử sèng nhưng đơn giản hơn không cần chọn cửa đặt, chỉ cần đặt chip và nhấn nút bắt đầu để 3 vòng tròn quay. Nếu may mắn bạn sẽ quay được COMBO và chắc chắn đổi đời</p>
    </div>
    <div class="game-stars">
        <div class="stars">
            <span class="star"></span>
            <span class="star"></span>
            <span class="star"></span>
            <span class="half-star"></span>
            <span class="non-star"></span>
        </div>
        <div class="game-details">
            <span class="game-details-name">Thể loại:</span>
            <span class="game-details-info">Game bài</span>
            <div></div>
            <span class="game-details-name">Phiên bản:</span>
            <span class="game-details-info">Vesion 2.0.2</span>
            <div></div>
            <span class="game-details-name">Lượt tải:</span>
            <span class="game-details-info">1.150.000</span>
        </div>
        <div class="like-game-form">
            <div class="facebook-like">
                <div class="like-number">
                    <span>23456</span></div>
                <div class="facebook-like-button"></div>
            </div>
            <div class="google-like">
                <div class="like-number">
                    <span>23456</span></div>
                <div class="google-like-button"></div>
            </div>
            <div class="twitter-like">
                <div class="like-number">
                    <span>23456</span>
                </div>
                <div class="twitter-like-button"></div>
            </div>
        </div>
    </div>
</div>