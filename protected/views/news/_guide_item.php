<a href="<?php echo $data->getDetailLink(); ?>">
    <div class="guide-item">
        <div class="guide-item-title"><?php echo $data->title ?></div>
        <div class="guide-item-sapo"><?php echo $data->sapo?></div>
        <div class="guide-border"><div class="border-shadow"></div></div>
    </div>
</a>    