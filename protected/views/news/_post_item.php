 <div class="post-item">
    <a href="<?php echo $data->getDetailLink(); ?>"> 
        <img class="image-news" width="150px" height="150px" src="<?php echo Yii::app()->createAbsoluteUrl($data->main_image) ?>"/>
    </a>
    <div class="post-main">
        <div class="title"><a href="<?php echo $data->getDetailLink(); ?>"><?php echo $data->title; ?></a></div>
        <div class="sapo"><?php echo $data->sapo;?></div>
    </div>
</div>