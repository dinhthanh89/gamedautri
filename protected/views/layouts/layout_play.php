<?php
$user = Yii::app()->user->getUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta name="ROBOTS" content="index,follow">
        <meta name="AUTHOR" content="Esimo.vn">
        <meta http-equiv="EXPIRES" content="0">
        <meta name="RESOURCE-TYPE" content="DOCUMENT">
        <meta name="DISTRIBUTION" content="GLOBAL">
        <meta name="COPYRIGHT" content="Copyright (c) by Esimo">
        <meta name="Googlebot" content="index,follow,archive">
        <meta name="RATING" content="GENERAL">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                                
        
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('images/favicon.png');?>" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/main.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/style.css' ?>" type="text/css" rel="stylesheet" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>        
        <script src="<?php echo Yii::app()->baseUrl . '/js/all.js' ?>"></script>
        <script type="text/javascript">
            baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <?php include_once'googleanalytic.php'; ?>
    </head>
    <body class="body-play">                     
        <div class="nav" style="margin-top:0px;">            
            <div class="nav-main">
                <div class="nav-main-border"></div>
                <div class="main">
                    <h1 class="logo-play"><a href="<?php echo Yii::app()->homeUrl ?>"></a></h1>
                    <div class="nav-group-link">
                        <?php $curr_action = Yii::app()->controller->action->id; ?> 
                        <span class="<?php if($curr_action == 'phom') echo 'current' ?>">
                            <a class="nav-link" href="<?php echo Yii::app()->createUrl('play/phom')?>">Phỏm</a> 
                        </span>
                        <span class="space">|</span>
                        <span class="<?php if($curr_action == 'tlmn') echo 'current' ?>">
                            <a class="nav-link" href="<?php echo Yii::app()->createUrl('play/tlmn')?>">Tiến lên miền nam</a>
                        </span>
                        <span class="space">|</span> 
                        <span class="<?php if($curr_action == 'chan') echo 'current' ?>">
                            <a class="nav-link" href="<?php echo Yii::app()->createUrl('play/chan')?>">Chắn</a>
                        </span>
                    </div>
                </div> 
            </div>    
            <div class="nav-bottom-shadow"></div>
        </div>
        
        <?php echo $content;?>
        
        <div class="footer">
            <div class="footer-top-shadow"></div>
            <div class="footer-main">
                <div class="main">
                    <ul class="footer-items">
                        <li class="footer-item">
                            <?php $this->widget('NewsWidget') ?>                       
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                             <?php $this->widget('PostWidget', array('category_id'=>14)) ?>                           
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                             <?php $this->widget('ContactWidget') ?> 
                        </li>
                    </ul> 
                </div>    
            </div>
            <div class="footer-bottom-shadow"></div>                 
        </div>
        <div class="copyright">@ 2013 ESIMO</div>                       
    </body>
</html>