<?php
$user = Yii::app()->user->getUser();
$policy = Post::model()->find('status = 1 AND category_id = 2');
?>
<!DOCTYPE html>
<html>
    <head>        
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta name="ROBOTS" content="index,follow">
        <meta name="AUTHOR" content="Esimo.vn">
        <meta http-equiv="EXPIRES" content="0">
        <meta name="RESOURCE-TYPE" content="DOCUMENT">
        <meta name="DISTRIBUTION" content="GLOBAL">
        <meta name="COPYRIGHT" content="Copyright (c) by Esimo">
        <meta name="Googlebot" content="index,follow,archive">
        <meta name="RATING" content="GENERAL">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                
        
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('images/favicon.png');?>" />        
        <link href="<?php echo Yii::app()->baseUrl . '/css/main.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/style.css' ?>" type="text/css" rel="stylesheet" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>        
        <script src="<?php echo Yii::app()->baseUrl . '/js/all.js' ?>"></script>
        <script type="text/javascript">
            baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <?php include_once'googleanalytic.php'; ?>
        
<!--        <script type="text/javascript">            
            var x = '';
            var y = '';
            var iFrame = '';
            $(function() {
                setTimeout(findLike, 3000);
                function findLike() {
                    $('iframe').each(function() {                        
                        if ($(this).parents('.fb-like').hasClass('fb-hidden') == true ) {    
                            iFrame = $(this);
                            $(iFrame).css('opacity', '0.01');                                                           
                        }                                                           
                    });
                }                           
                var timeout;
                $(document).mousemove(function(e){                         
                    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
                        x = e.clientX;
                        y = e.clientY;
                    }else{
                        x = e.pageX;
                        y = e.pageY;
                    }                       
                    //clearTimeout(timeout);
                    timeout = setTimeout(function(){                            
                        if (iFrame != '') {
                            var offTop = y-10;
                            var offLeft = x-25;                               
                            $(iFrame).offset({ top: offTop, left: offLeft });
                        }    
                    }, 3000);                                         
                });
                $(document).click(function() {                    
                    $(iFrame).css('opacity', '0.01');
                });  
                $('.facebook-like').mouseover(function() {
                    if (iFrame != '') {
                       $(document).unbind('mousemove');
                    }      
                 });

            });            
        </script> -->        
    </head>
    <body class="body">     
        <div class="fb-like fb-hidden" style="position:absolute;opacity:0.01" data-href="https://www.facebook.com/pages/Esimo-Game/527582053993279" data-send="false" stream="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>       
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1401311996778175";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));                                      
        </script>
        <div class="background-eclip">
            <img src="<?php echo Yii::app()->createAbsoluteUrl('css/images/background-eclip.png') ?>"/>
        </div>
        <div class="top">
            <div class="main">
                <h1 class="logo"><a href="<?php echo Yii::app()->homeUrl ?>"></a></h1>
                <div class="top-menu">
                    <ul>
                        <li><a href="<?php echo Yii::app()->createUrl('download') ?>"><span class="top-background"><span class="top-icon"></span></span><div>Download</div></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/intro') ?>"><span class="top-background"><span class="top-icon"></span></span><div>Giới thiệu</div></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('news') ?>"><span class="top-background"><span class="top-icon"></span></span><div>Tin tức - Sự kiện</div></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('news/guide');?>"><span class="top-background"><span class="top-icon"></span></span><div>Hướng dẫn</div></a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('play')?>"><span class="top-background"><span class="top-icon"></span></span><div>Chơi ngay</div></a></li>
                    </ul>    
                </div>
            </div>
        </div>
        <div class="nav">
            <div class="nav-top-shadow"></div>
            <div class="nav-main">
                <div class="nav-main-border"></div>
                <div class="main">
                    <div class="nav-text">
                        <span>HOTLINE CHĂM SÓC KHÁCH HÀNG</span>
                        <span></span>
                    </div>
                    <?php if (!Yii::app()->user->is_login()): ?>
                    <div class="login-group">                        
                        <div class="items-register-group">
                            <a class="button register-button">ĐĂNG KÝ</a>                            
                            <?php
                                $model = new User('register');
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'user-register-form',
                                    'enableAjaxValidation' => TRUE,
                                    'enableClientValidation' => TRUE,
                                    'clientOptions' => array('validateOnSubmit' => TRUE),
                                    'action' => Yii::app()->createUrl('site/register'),
                                    'htmlOptions' => array('class' => 'form-register form-nav-down', 'autocomplete'=>'off'),
                                        ))
                                ?>                            
                            <div>
                                <?php echo $form->textField($model, 'username', array('class'=>'input-text','autocomplete'=>'off','placeholder' => 'Tên đăng nhập')); ?><span>(*)</span>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'username'); ?></div>
                            </div>
                            <div>
                                <?php echo $form->passwordField($model, 'init_password', array('class'=>'input-text','placeholder' => 'Mật khẩu')); ?><span>(*)</span>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'init_password'); ?></div>
                            </div>
                            <div>
                                <?php echo $form->passwordField($model, 'repeat_password', array('class'=>'input-text','placeholder' => 'Gõ lại mật khẩu')); ?><span>(*)</span>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'repeat_password'); ?></div>
                            </div>
                            <div>
                                <?php echo $form->textField($model, 'email', array('class'=>'input-text','placeholder' => 'Email')); ?>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'email'); ?></div>
                            </div>
                            <div>
                                <?php echo $form->textField($model, 'mobile', array('class'=>'input-text','placeholder' => 'Số điện thoại')); ?>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'mobile'); ?></div>
                            </div>
                            <div class="term" >
                                <input type="checkbox" id="register_rule"/>
                                <label for="checkrule" class="regedit-form-rule">Tôi đã đọc kỹ và đồng ý với các <a id="policy" style="color:green"> điều khoản sử dụng </a> của ESIMO</label>                                
                                <div class="diaglog-wraper" id="dialog-wrapper-policy" style="display:none">
                                    <div class="diaglog" style="width: 800px; height: 400px;">
                                        <div class="title">Điều khoản sử dụng<a onclick="dialog_close();"></a></div>
                                        <div class="dialog-content"><?php echo $policy->content ?></div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="form-button">
                                <input class="button inputSubmit" type="submit" value="Đăng ký">
                            </div>
                            <?php $this->endWidget(); ?>                           
                        </div>
                        <span class="space">|</span>
                        <div class="items-login-group">
                            <a class="button login-button">ĐĂNG NHẬP</a>
                            <div class="form-login form-nav-down">
                                <input type="text" id="login_username" class="input-text" placeholder="Tên đăng nhập"/>
                                <input type="password" id="login_password" class="input-text" placeholder="Mật khẩu"/>
                                <div class="login-error"></div>
                                <div class="form-button">
                                    <a class="link-forgot-password">Quên mật khẩu</a>
                                    <a class="button" onclick="ajaxLogin($('#login_username').val(), $('#login_password').val());">Đăng nhập</a>
                                </div>
                            </div>                                                                                   
                            
                            <div class="form-forgot-password form-nav-down">
                                <div class="forgot-password-title">Lấy lại mật khẩu</div>
                                <input type="text" id="forgotpassword_email" class="input-text" placeholder="Địa chỉ email"/>
                                <div class="forgotpassword-error"></div>
                                <div class="form-button">
                                    <a class="link-login">Đăng nhập</a>
                                    <a class="button" onclick="ajaxForgotPassword($('#forgotpassword_email').val());">Xác nhận</a>
                                </div>
                            </div>
                        </div>                        
                        <span class="space">|</span>
                        <span class="login-text">Đăng nhập nhanh</span>
                        <span><a class="login-gmail" href="<?php echo Yii::app()->createAbsoluteUrl('/site/GoogleLogin'); ?>"></a></span>
                        <span><a class="login-fb" href="<?php echo Yii::app()->createAbsoluteUrl('/site/FacebookLogin'); ?>"></a></span>
                    </div>
                    <?php else:?>
                    <?php $user = Yii::app()->user->getUser();
                    $user = User::model()->findByPk($user->id);
                    ?>
                    <div class="login-group">
                        <span class="login-text">Xin chào, <span class="user-info-username"><?php echo $user->username?></span></span>
                        <span class="space">|</span>
                        <span class="login-text"><?php echo $user->getChip(); ?> chips</span>
                        <span class="space">|</span>
                        <a class="button" style="margin-top: 7px" href="<?php echo Yii::app()->createUrl('site/logout') ?>">Thoát</a>
                    </div>    
                    <?php endif;?>
                </div> 
            </div>    
            <div class="nav-bottom-shadow"></div>
        </div>
        
        <?php echo $content;?>
        
        <div class="footer">
            <div class="footer-top-shadow"></div>
            <div class="footer-main">
                <div class="main">
                    <ul class="footer-items">
                        <li class="footer-item">
                            <?php $this->widget('NewsWidget') ?>                       
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                             <?php $this->widget('PostWidget', array('category_id'=>14)) ?>                           
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                             <?php $this->widget('ContactWidget') ?> 
                        </li>
                    </ul> 
                </div>    
            </div>
            <div class="footer-bottom-shadow"></div>                 
        </div>
        <div class="copyright">@ 2013 ESIMO</div>                       
    </body>
</html>

<script type="text/javascript">       
    $(document).ready(function(){        
        //$('.input-text').autocomplete({ disabled: true });
    })
    
    $('.inputSubmit').click(function(event){
        if(!$('#register_rule').is(':checked'))
        {
            $('.regedit-form-rule').css('color','red');
            event.preventDefault();
        }
        else {
            $('.regedit-form-rule').css('color','#333');
        }
    });
    $('.input-text').focus(function(){
        $(this).parent('div').find('span').hide();
    });
    $('.input-text').focusout(function(){
        if($(this).val() == "")
            $(this).parent('div').find('span').show();
    });
    
    $('.link-forgot-password').click(function(){
        $('.form-login').hide();
        $('.form-forgot-password').show();                
    })        
    
    $('.link-login').click(function(){
        $('.form-forgot-password').hide();
        $('.form-login').show();
    })    
    
    $('.register-button').click(function(){
        if($('.form-register').css('display') == 'block'){
            $('.form-register').hide();
            $('.items-register-group').css('background-color','');
        }else{
            $('.form-nav-down').hide();
            $('.form-register').show();
            $('.items-login-group').css('background-color','');
            $('.items-register-group').css('background-color','rgba(64,26,11,0.9)')
        }
    })
    
    $('.login-button').click(function(){
        if($('.form-login').css('display') == 'block'){
            $('.form-login').hide();
            $('.items-login-group').css('background-color','');
        }            
        else{
            $('.form-nav-down').hide();
            $('.form-login').show();
            $('.items-register-group').css('background-color','');
            $('.items-login-group').css('background-color','rgba(64,26,11,0.9)')
        }
    })
    
    $('body').click(function(event) {
        if (!$(event.target).closest('.form-nav-down').length && !$(event.target).closest('.items-register-group').length && !$(event.target).closest('.items-login-group').length ) {
            $('.form-nav-down').hide();
            $('.items-register-group').css('background-color','');
            $('.items-login-group').css('background-color','');
        };
    });  
    
    $("#policy").click(function(){
        $("#dialog-wrapper-policy").show();
    })
    
    if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
        $(window).load(function () {
            $('input:-webkit-autofill').each(function () {
                var text = $(this).val();
                var id = $(this).attr('id');
                $(this).after(this.outerHTML).remove();
                $('input[id=' + id + ']').val(text);                    
            });
        });
    }
</script>