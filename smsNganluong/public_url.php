<?php
	//define('PASSCODE', '142329cf024d8bd2a3576086b61bdc05');
        define('PASSCODE', '8459b921a72cb3dc1beb7a4be2e4922a');
	
	$data = serialize($_REQUEST);
	writeFileLog('./log/log.txt', $data);
	
	if (@$_REQUEST['function'] == 'BillUpdate') {            
		$checksum = getChecksum($_REQUEST, PASSCODE);
		if ($checksum == @$_REQUEST['checksum']) {	                    
                    //call service esimo
                    $domain = $_SERVER['HTTP_HOST'];
                    $wsdl = 'http://'.$domain.'/api/rechargesmsnganluong?WSDL';                    
                    $client = new soapclient($wsdl);
                    
                    $response = $client->getMoNganluong($_REQUEST['message'],$_REQUEST['client_mobile'],$_REQUEST['transaction_id'],$_REQUEST['service_id'],$_REQUEST['keyword']);                     
                    $response = json_decode($response,JSON_FORCE_OBJECT);                    
                    if($response['IsSuccess'] == true){
                        echo '1';
                        die();
                    }else{
                        echo '0';
                        die();
                    }                                            
		} else {                    
			echo '0';
			die();
		}
	}
	echo '0';
	
	function getChecksum($params, $password)
	{
		$md5 = array();
		$map = getMap();
		foreach ($map as $key) {
			$md5[$key] = $params[$key];
		}
		$md5 = implode('|', $md5).'|'.$password;
		return md5($md5);
	}
	
	function getMap()
	{
		return array(
			'reciver_email',
			'transaction_id',
			'price',
			'amount',
			'fee',
			'ref_code',
			'keyword',
			'service_id',
			'message',
			'client_mobile',
			'telco',
		);
	}
	
	function writeFileLog($file_name, $data)
	{
		$fp = fopen($file_name,'a');
		if ($fp) {
			$line = date("H:i:s, d/m/Y:  ",time()).$data. " \n";
			fwrite($fp,$line);
			fclose($fp);
		}
	}
?>