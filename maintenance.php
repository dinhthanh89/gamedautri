<script type="text/javascript" src="/js/jquery-1.8.3.js"></script>
<script type="text/javascript">
function secondToHour($second,$type) {
    $d = parseInt($second / 86400);
    if($d==0)
    {
        $h = parseInt($second / 3600);
        if($h >= 10) $strH =  $h;
        else $strH = '0' + $h;
    }
    else {
        $strH = $d + ' ngày';
    }
    if ($type == 'h')
        return $strH;
    if($d==0){
        $m = parseInt(($second - $h * 3600) / 60);

    }
    else {
        $m = parseInt(($second - $d * 86400) / 3600);
    }
    if($m >= 10)   $strM =  $m;
    else $strM = '0' + $m;
    if ($type == 'm')
        return $strM;
    if($d == 0){
        $s = parseInt ($second - $h * 3600 - $m * 60);
    }
    else {
        $s = parseInt (($second - $d * 86400 - $m * 3600)/60);
    }
    if($s >= 10) $strS =  $s;
    else $strS = '0' + $s;
    if ($type == 's')
        return $strS;
}

CountdownAble = function(jqueryObject) {
    this.jqueryObject = jqueryObject;
}

CountdownAble.prototype.start = function() {
    var jqueryObject = this.jqueryObject;
    function changeHtml(time) {
        jqueryObject.attr('time',time);
        jqueryObject.children('.hour').html(secondToHour(time, 'h'));
        jqueryObject.children('.minute').html(secondToHour(time, 'm'));
        jqueryObject.children('.second').html(secondToHour(time, 's'));
    }
    
    function countdown(time) {
        if(time === undefined) {
            time =0;
        }
        if(time == 0){
            if ( typeof kExpireTime == 'function' ) { 
                kExpireTime();
            }
        }
        else {
            changeHtml(time);
            setTimeout(countdown, 1000, time - 1);
        }
    }
    
    countdown(this.jqueryObject.attr('time'));
}

jQuery.fn.startCountdownAll = function() {
    //var count = 0;
    $(this).each(function() {
        new CountdownAble($(this)).start();
    })
}
</script>

<body style="text-align: center">
    <img src="files/maintenance.png"/>
    <div style="font-size: 36px;">Mời bạn quay lại sau: <b style="color:red">15h00</b></div>
<!--    <div class="K-remain-time" time="18000" style="font-size: 36px;">
        <span class="hour"></span>:<span class="minute"></span>:<span class="second"></span>
    </div>-->
    
    <div>
        <div style="margin:0 auto;font-size: 20px; width:700px;text-align: left">
            <hr style="color:#99CC99" />
            <p style="font-size: 24px;font-weight: bold">[Thông báo] Bảo trì và nâng hệ thống máy chủ ESIMO ngày 11/03/2014</p>
            <br>
            Chào tất cả các bạn!
            <br>
            ESIMO xin thông báo chi tiết về thời gian bảo trì  và nâng cấp hệ thống máy chủ:<br>
            Thời gian dự kiến: từ 9h00  đến 15h00 ngày 11/03/2014<br>
            Nội dụng: Bảo trì  và nâng cấp hệ thống.<br>
            Phạm vi: Dịch vụ các game Chắn – Phỏm – Tiến Lên Miền Nam và toàn bộ hệ thống website<br>
            Lưu ý: Trong thời gian bảo trì các bạn sẽ không thể đăng nhập game. Để bảo đảm an toàn cho tài khoản của mình, hãy thoát khỏi game trước khi việc bảo trì được tiến hành và vui lòng không cố gắng đăng nhập hoặc nạp tiền vào thời gian này để tránh những trường hợp lỗi nhân vật đáng tiếc.<br>
            Chúng tôi sẽ có thêm thông báo chính thức khi hoàn thành việc nâng cấp này.<br>
            Xin chân thành cảm ơn!
        </div>
    </div>
</body>

<script>
    $('.K-remain-time').startCountdownAll();    
</script>

