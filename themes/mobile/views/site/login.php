<div class="main-page">
    <div class="content-page">                                        
        <div class="form-register-in" style="text-align: left;">
            <h2 class="form-register-tittle" style="margin-bottom: 30px;">Đăng nhập</h2>
            <form class="forgot-password-form" method="POST" action="<?php echo Yii::app()->createUrl(Common::getCurrentUrl()); ?>">            
            <ul>                        
                <li>
                    <label class="label register-field">Tài khoản:</label><br>
                    <input type="text" class="input register-input" name="username" value=""/>                           
                </li>
                <li>
                    <label class="label register-field">Mật khẩu:</label><br>
                    <input type="password" class="input register-input" name="password" value=""/>
                </li>
            </ul>
            <div class="row buttons" style="padding-top: 10px;" >
                <?php echo CHtml::submitButton('Đăng nhập', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
            </div>
            </form>    
        </div>                         
    </div>
</div>
