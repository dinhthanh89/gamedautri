<?php if(isset($model)): ?>
<?php foreach($model as $item):?>
<div class="main-page">
    <a href="<?php echo Yii::app()->createUrl('site/detail?id='.$item->id) ?>">
        <div class="game-info">
            <img src="<?php echo Yii::app()->createUrl($item->image); ?>"/>
            <div class="game-info-details">
                <div class="game-info-item game-info-name">Chiếu vương</div> 
                <div class="game-info-item">Version: <?php if(($item->getCurrentVersion($device))) echo $item->getCurrentVersion($device)->getCurrentVersion(); else echo "N/A"; ?></div>                
                <div class="game-info-item">Dung lượng: <?php if(($item->getCurrentVersion($device))) echo $item->getCurrentVersion($device)->getSize(); else echo "N/A"?></div>
                <div class="game-info-item">Số lượt tải: <?php  echo $item->countDownload + 6688;?></div>
            </div>
        </div>         
    </a> 
    <?php if($item->code_identifier == 'chan'):?>
    <span class="game-hot"></span>
    <?php endif;?>
    <?php if($device == "ios"):?>    
        <a href="<?php if($item->getCurrentVersion('ios')) echo $item->getCurrentVersion('ios')->gamePlatform->market_url ?>"><span class="game-download"></span></a>
    <?php elseif($device == "android"):?>
        <a href="<?php if($item->getCurrentVersion('android')) echo "market://details?id=".$item->getCurrentVersion('android')->gamePlatform->bundle_id; ?>"><span class="game-download"></span></a>
    <?php endif;?>   
</div>
<?php endforeach;?>
<?php endif;?>

<!--<div class="slide">
    <a class="slide-prev"></a> 
    <a class="slide-next"></a>
    <div class="swiper-container">
      <div class="swiper-wrapper">
          <div class="swiper-slide"><img src="images/slide_5.jpg"> </div>
        <div class="swiper-slide"><img src="images/slide_3.jpg"> </div>
        <div class="swiper-slide"><img src="images/slide_2.jpg"> </div>
        <div class="swiper-slide"><img src="images/slide_1.jpg"> </div>
      </div>
     </div>            
</div>

<script type="text/javascript">          
    var mySwiper = new Swiper('.swiper-container',{    
    loop:true,
    grabCursor: true,
    autoplay: 3000
  })
  $('.slide-prev').on('click', function(e){
    e.preventDefault()
    mySwiper.swipePrev()
  })
  $('.slide-next').on('click', function(e){
    e.preventDefault()
    mySwiper.swipeNext()
  })   
</script>  -->