<div class="main-page">
    <div class="content-page">        
            <div style="text-align: left">
                <div class="form-register-in">
                    <h2 class="form-register-tittle">Đăng kí tài khoản</h2>                                                            
                    <?php
                    $model = new User('register');
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'user-register-form',
                        'enableAjaxValidation' => TRUE,
                        'enableClientValidation' => TRUE,
                        'clientOptions' => array(
//                            'validateOnType'=>TRUE,
//                            'validateOnChange'=>TRUE,
                            'validateOnSubmit' => TRUE,
                            'hideErrorMessage' => FALSE,
                        ),
                        'action' => Yii::app()->createUrl('site/register'),
                        'htmlOptions' => array('class' => 'login-form-openid'),
                            ))
                    ?>                       
                    <ul>
                        <li>
                            <?php echo $form->labelEx($model, 'username', array('class' => 'label register-field')); ?></br>
                            <?php echo $form->textField($model, 'username', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'username', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'init_password', array('class' => 'label register-field')); ?></br>
                            <?php echo $form->passwordField($model, 'init_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'init_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'repeat_password', array('class' => 'label register-field')); ?></br>
                            <?php echo $form->passwordField($model, 'repeat_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'repeat_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'email', array('class' => 'label register-field')); ?></br>
                            <?php echo $form->textField($model, 'email', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'email', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'mobile', array('class' => 'label register-field')); ?></br>
                            <?php echo $form->textField($model, 'mobile', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'mobile', array('class' => 'error')); ?>
                        </li>
                    </ul>
                    <div class="row buttons" >                                               
                        <br>
                        <?php echo CHtml::submitButton('Đăng ký', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
        </div>
</div>