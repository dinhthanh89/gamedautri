<div class="main-page">
    <div class="game-title">Tải game Chiếu vương - <span>MIỄN PHÍ</span></div>
    <div class="game-info">
        <img src="<?php echo Yii::app()->createUrl($model->image); ?>"/>
        <div class="game-info-details">
            <div class="game-info-item game-info-name">Chiếu vương</div> 
            <div class="game-info-item"><span>Version: </span><?php if(($model->getCurrentVersion($device))) echo $model->getCurrentVersion($device)->getCurrentVersion(); else echo "N/A"; ?></div>
            <div class="game-info-item"><span>Hỗ trợ: </span><?php if(($model->getCurrentVersion($device))) echo $model->getCurrentVersion($device)->description; else echo "N/A"?></div>
            <div class="game-info-item"><span>Dung lượng: </span><?php if(($model->getCurrentVersion($device))) echo $model->getCurrentVersion($device)->getSize(); else echo "N/A"?></div>
            <div class="game-info-item"><span>Số lượt tải: </span><?php  echo $model->countDownload + 6688;?></div>
        </div>
    </div>
    <?php if($device == "ios"):?>
        <div class="ios-not-jailbreak">
            <a href="<?php if($model->getCurrentVersion('ios')) echo $model->getCurrentVersion('ios')->gamePlatform->market_url ?>">
                <div class="border"><span></span><div class="text-link" style="<?php if(Common::isOperaMini()) echo "color:#000"?>"> Tải từ App Store</div></div>
            </a>
        </div>
        <p class="p-italic">(dành cho thiết bị chưa jailbreak)</p>
        <div class="ios-jailbreak">
            <a href="itms-services://?action=download-manifest&url=<?php echo Yii::app()->createAbsoluteUrl('files/games/download_'.$model->code_identifier.'.xml') ?>">
                <div class="border"><span></span><div class="text-link"> Cài đặt trực tiếp</div></div>   
            </a>            
            
        </div>
        <p class="p-italic">(dành cho thiết bị đã jailbreak)</p>
    <?php elseif($device == "android"):?>
        <div class="android-chplay">
            <a href="<?php if($model->getCurrentVersion('android')) echo "market://details?id=".$model->getCurrentVersion('android')->gamePlatform->bundle_id; //echo $model->getCurrentVersion('android')->gamePlatform->market_url ?>">
                <div class="border"><span></span><div class="text-link">Tải từ Play Store</div></div>   
            </a>
        </div>    
        <div class="android-not-chplay">
            <a onclick="downloadFile($(this).attr('link'));" link='<?php echo $model->getCurrentVersion('android')->id?>'>
                <div class="border"><span></span><div class="text-link">Tải file trực tiếp</div></div>
            </a>
        </div>
        <p class="p-italic">(cài trực tiếp)</p>
    <?php else:?>
        <div class="not-support">
<!--            <a>                
                <div class="border">
                    <span></span><div class="text-link">Phiên bản chưa được hỗ trợ</div>                    
                </div>   
            </a>-->
        </div> 
    <?php endif;?>
    
    <div class="game-description">
        Bạn có muốn thử tài phán đoán – tính tư duy logic - thêm chút may mắn ? Bạn bè bạn ở xa ? Hãy đến với cổng game chieuvuong.com. Tại đây bạn có thể thỏa sức tham gia các ván bài với bạn bè của mình và giao lưu với mọi người. Luật chơi và cách thức như ngoài đời thật.
    </div>
</div>

<div class="slide">
    <a class="slide-prev"></a> 
    <a class="slide-next"></a>
    <div class="swiper-container">
      <div class="swiper-wrapper">
<!--          <div class="swiper-slide"><img src="images/slide_5.jpg"> </div>-->
        <div class="swiper-slide"><img src="<?php echo Yii::app()->createUrl('images/slide_3.jpg') ?>"> </div>
        <div class="swiper-slide"><img src="<?php echo Yii::app()->createUrl('images/slide_2.jpg') ?>"> </div>
        <div class="swiper-slide"><img src="<?php echo Yii::app()->createUrl('images/slide_1.jpg') ?>"> </div>
      </div>
     </div>            
</div>

<script type="text/javascript">          
    var mySwiper = new Swiper('.swiper-container',{    
    loop:true,
    grabCursor: true,
    autoplay: 3000
  })
  $('.slide-prev').on('click', function(e){
    e.preventDefault()
    mySwiper.swipePrev()
  })
  $('.slide-next').on('click', function(e){
    e.preventDefault()
    mySwiper.swipeNext()
  })   
</script>  

<div class="separator"><span>TẢI GAME CHẮN TRÊN CÁC THIẾT BỊ</span></div>

<div class="main-page">
    <div class="game-device-title">Tải Chiếu vương cho nền tảng iOS</div>
    <div class="ios-not-jailbreak">
        <a href="<?php if($model->getCurrentVersion('ios')) echo $model->getCurrentVersion('ios')->gamePlatform->market_url ?>">
            <div class="border"><span></span><div class="text-link" style="<?php if(Common::isOperaMini()) echo "color:#000"?>">Tải từ App Store</div></div>
        </a>
    </div>
    <p class="p-italic">(dành cho thiết bị chưa jailbreak)</p>
    
    <div class="ios-jailbreak">
        <?php if($device == "ios"):?>
        <a href="itms-services://?action=download-manifest&url=<?php echo Yii::app()->createAbsoluteUrl('files/games/download_'.$model->code_identifier.'.xml') ?>">
            <div class="border"><span></span><div class="text-link"> Cài đặt trực tiếp</div></div>   
        </a>
        <?php else:?>
        <a onclick="downloadFile($(this).attr('link'));" link='<?php if($model->getCurrentVersion('ios')) echo $model->getCurrentVersion('ios')->id?>'>
            <div class="border"><span></span><div class="text-link">Tải file trực tiếp</div></div>
        </a>
        <?php endif;?>
    </div>
    <p class="p-italic">(dành cho thiết bị đã jailbreak)</p>
    
    <div class="game-device-title game-device-android">Tải Chiếu vương cho nền tảng Android</div>
    <div class="android-chplay">
        <a href="<?php if($model->getCurrentVersion('android')){ if($device == "android") echo "market://details?id=".$model->getCurrentVersion('android')->gamePlatform->bundle_id;else echo $model->getCurrentVersion('android')->gamePlatform->market_url; } ?>">
            <div class="border"><span></span><div class="text-link">Tải từ Play Store</div></div>
        </a>
    </div>    
    <div class="android-not-chplay">
        <a onclick="downloadFile($(this).attr('link'));" link='<?php if($model->getCurrentVersion('android')) echo $model->getCurrentVersion('android')->id?>'>
            <div class="border"><span></span><div class="text-link">Tải file trực tiếp</div></div>
        </a>
    </div>
    <p class="p-italic">(cài trực tiếp)</p>
</div>

<script type="text/javascript">
function install() {
            if (confirm ("Neu may ban chua jaibreak, an dong y de toi trang cai dat thong qua TestFlight. Neu khong an bo qua!!!")) {
                    alert("Thuc hien chuyen tiep toi trang cai dat. An vao connect device va cai dat ung dung TestFight de co the cai dat truc tiep Game. Xin Cam On!!!");
                    window.location = "https://testflightapp.com/install/532414d4d18a0c69933f02bb9f6eaa29-ODU0OTg0OA/";
            } else {
                    alert("an install (hoac 'cai dat') de cai dat game, ban phai dam bao may ban da jailbreak");
                    url = '<?php echo Yii::app()->createAbsoluteUrl('files/games/download_chan.xml') ?>';
                    window.location = "itms-services://?action=download-manifest&url="+url;
            }
    }    
</script>