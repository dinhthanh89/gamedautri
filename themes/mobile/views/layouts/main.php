<?php 
$configs = Configuration::model()->getConfig('support_contact');  
$curr_controller = Yii::app()->controller->id;
$curr_action = Yii::app()->controller->action->id;
?>
<!DOCTYPE html>
<html>
    <head>        
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="ROBOTS" content="index,follow">
        <meta name="AUTHOR" content="chieuvuong.com">
        <meta http-equiv="EXPIRES" content="0">
        <meta name="RESOURCE-TYPE" content="DOCUMENT">
        <meta name="DISTRIBUTION" content="GLOBAL">
        <meta name="COPYRIGHT" content="Copyright (c) by Chieuvuong">
        <meta name="Googlebot" content="index,follow,archive">
        <meta name="RATING" content="GENERAL">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                
        
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('images/favicon.png');?>" />                
        <link href="<?php echo Yii::app()->baseUrl . '/css/style_mobile.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/idangerous.swiper.css' ?>" type="text/css" rel="stylesheet" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>        
        <script src="<?php echo Yii::app()->baseUrl . '/js/all.js' ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/js/idangerous.swiper-2.1.min.js' ?>"></script>
        <script type="text/javascript">
            baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <?php include_once'googleanalytic.php'; ?>           
    </head>
    <body>
        <div id="header">
            <div class="top-header">
                <h1><a href="<?php echo Yii::app()->homeUrl; ?>"><img width="173px" height="33px" src="<?php echo Yii::app()->createUrl('images/m_logochieuvuong.png') ?>"/></a></h1>                
                <a class="phone-number" href="tel:<?php echo (!isset($config->mobile)) ? $configs->mobile : '0169 8486 249' ?>"></a>
            </div>  
            <div class="top-menu">
                <ul>
                    <li class="<?php if($curr_controller == "site" && ($curr_action == "index" || $curr_action == "detail")) echo "active" ?>"><a href="<?php echo Yii::app()->homeUrl; ?>">TẢI GAME</a></li>
                    <li class="<?php if($curr_controller == "news") echo "active" ?>"><a href="<?php echo Yii::app()->createUrl('news') ?>">SỰ KIỆN</a></li>
                    <li class="last-child <?php if($curr_controller == "site" && $curr_action == "intro") echo "active" ?>"><a href="<?php echo Yii::app()->createUrl('site/intro') ?>">GIỚI THIỆU</a></li>
                </ul>
            </div>    
            <div class="clear-both"></div>
            <div class="bottom-header"></div>
        </div>
        <div class="quick-register" style="text-align:center;margin-bottom: 7px; font-size: 12px;">  
            <?php if (!isset($_SESSION['user']) || $_SESSION['user'] == NULL): ?>
                <a href="<?php echo Yii::app()->createUrl('site/loginMobile') ?>">Đăng nhập</a>
                <span class="space">|</span>
                <a href="<?php echo Yii::app()->createUrl('site/register') ?>">Đăng ký ngay</a>
            <?php else:?>
            <?php 
                $user = User::model()->findByPk($_SESSION['user']['id']);
            ?>
                <div class="login-group">
                    <span class="login-text">Xin chào, <span class="user-info-username"><?php echo $user->username?></span></span>
                    <span class="space">|</span>
                    <span class="login-text"><?php echo $user->getChip(); ?> chips</span>
                    <span class="space">|</span>
                    <span class="login-text"><?php echo $user->getGold(); ?> golds</span>
                    
                    <div>
                    <a class="button" style="margin-top: 7px" href="<?php echo Yii::app()->createUrl('user/exchange') ?>">Đổi gold</a>
                    <span class="space">|</span>
                    <a class="button" style="margin-top: 7px" href="<?php echo Yii::app()->createUrl('site/logout') ?>">Thoát</a>
                    </div>
                </div>    
            <?php endif;?>
        </div>
        <?php echo $content;?>        
        <div id="footer">
            <div class="copyright">@CHIEUVUONG 2015</div>
            <div class="footer-contact">
                <span><a href="tel:<?php echo (!isset($config->mobile)) ? $configs->mobile : '0169 8486 249' ?>"><b>Hotline: </b><?php echo (!isset($config->mobile)) ? $configs->mobile : '0125 26 00000'?></a></span>
<!--                <span><b>Email: </b><?php echo (!isset($config->email)) ? $configs->email : 'hotro@esimo.vn'?></span>-->
                - <span><a href="<?php if(!Common::isOperaMini()) echo Yii::app()->createUrl('site/setDevice?device=pc') ?>">Phiên bản Web</a></span>
            </div>
        </div>
    </body>     
</html>        