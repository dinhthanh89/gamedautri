<?php $this->widget('zii.widgets.CListView', array(
                'dataProvider'=>$dataPost,
                'itemView'=>'_post_item',
                'htmlOptions' => array('class' => 'wrapper-table'),  
                'summaryText'=>'',
                'emptyText' => 'Chưa có bài viết nào!'                
                )); 
?>
<?php if($countPost > count($dataPost->getData())):?>
<div class="more"><a href="<?php echo Yii::app()->createUrl('news?p='.($page+1)) ?>">Xem thêm</a></div>
<?php endif; ?>
<script type="text/javascript">    
    $('.pager').remove();
</script>

  