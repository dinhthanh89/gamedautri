<div class="main-page">
    <a href="<?php echo $data->getDetailLink(); ?>">
        <div class="item-post">
            <div style="width:50px; height:50px;display:inline-block;vertical-align:top;background-image: url('<?php echo Yii::app()->createUrl($data->main_image) ?>'); background-size:100% auto; background-position:center"></div>
<!--            <img src="<?php echo Yii::app()->createUrl($data->main_image)?>"/>-->
            <div class="post-content" style="vertical-align:top;">
                <span><?php echo $data->title; ?></span>
            </div>
            <span class="button-detail"></span>
        </div>
    </a>    
</div>

<?php 
$page = Yii::app()->request->getParam('p');
if(!isset($page))
    $page = 1;
if($index % 3 == 2): ?>

<div class="slide">
    <a class="slide-prev"></a> 
    <a class="slide-next"></a>
    <div class="swiper-container swiper-container-<?php echo $index ?>">
      <div class="swiper-wrapper">
<!--          <div class="swiper-slide"><img src="images/slide_5.jpg"></div>-->
        <div class="swiper-slide"><img src="images/slide_3.jpg"> </div>
        <div class="swiper-slide"><img src="images/slide_2.jpg"> </div>
        <div class="swiper-slide"><img src="images/slide_1.jpg"> </div>
      </div>
     </div>            
</div>

<script type="text/javascript">    
    var index =  <?php echo $index; ?>;    
    var mySwiper = new Swiper('.swiper-container-'+index,{    
    loop:true,
    grabCursor: true,
    autoplay: 3000
  })
  $('.slide-prev').on('click', function(e){
    e.preventDefault();
    mySwiper.swipePrev();
  })
  $('.slide-next').on('click', function(e){
    e.preventDefault();
    mySwiper.swipeNext();
  })   
</script> 

<?php endif; ?>
