<?php $ratio = Configuration::model()->getConfig('gold_by_chip'); ?>
<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Đổi gold thành chip</a>
        </div>
        <div class="page-in-border-1"></div>
        <div class="main-page-item cart-form">
            <form name="exchange" action="<?php echo Yii::app()->createUrl('user/exchange') ?>" method="post">
            <div id="body12" style=" margin: 0 auto;  padding: 10px;  width: auto; color: #fff">
            <div style="color:#000;margin:10px 0;font-size:14px" align="center">
                    Bạn đang có <span style="color:red"><?php echo (isset($user)) ? $user->getGold() : 0 ?> golds</span> và <span style="color:red"><?php echo (isset($user)) ? $user->getChip() : 0 ?> chips</span>
            </div>
            <div style="color:#000;margin:10px 0;font-size:14px" align="center">
                    Tỉ lệ chuyển đổi <span style="color:red">1 golds</span> = <span style="color:red"><?php echo (isset($ratio)) ? $ratio : 0 ?> chips</span>
            </div>    
            <div style="color:#000;margin:10px 0;font-size:14px" align="center">
                    Chọn gói chip bạn muốn nhận
            </div>    
            <div style="text-align:center;list-style:none; padding:none;text-align:center">
                <?php if(isset($product)):?>
                <?php foreach($product as $p):?>
                <div style="display:inline-block ;padding:0px;width: 100px" align="center" style="">
                <label for="<?php echo $p->id ?>"><img src="<?php echo Yii::app()->createAbsoluteUrl($p->image)?>" /></label><br>
                    <input type="radio" name="gold" checked="true" value="<?php echo $p->gold ?>" id="<?php echo $p->id ?>"  /><label for="92"><?php echo $p->name ?></label>
                </div>
                <?php endforeach;?>
                <?php endif;?>
            </div>
            <div style="text-align: center">   
                <div style="display: inline-block;text-align: center;">                                        
                    <div class="input-row">
                        <span colspan="3">                        
                            <button type="submit" name="exchangeSubmit" style="width:80px;height:30px" >Chuyển đổi</button>
                        </span>
                    </div>	
                </div>
            </div>

            </div>
            </form>
        </div>        
    </div>
    <div style="clear: both"></div>
</div>
