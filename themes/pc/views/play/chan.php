<script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>
<script type="text/javascript">
    <!--
    var unityObjectUrl = "http://webplayer.unity3d.com/download_webplayer-3.x/3.0/uo/UnityObject2.js";
    if (document.location.protocol == 'https:')
        unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
    document.write('<script type="text\/javascript" src="' + unityObjectUrl + '"><\/script>');
    -->
</script>
<script type="text/javascript">
    <!--
    var params = {
        backgroundcolor: "000000",
        bordercolor: "000000",
        textcolor: "FFFFFF",
        logoimage: "<?php echo Yii::app()->createAbsoluteUrl('/images/Logo.png')?>",
        progressbarimage: "<?php echo Yii::app()->createAbsoluteUrl('images/processbarProcess.png')?>",
        progressframeimage: "<?php echo Yii::app()->createAbsoluteUrl('images/processbarBackground.png')?>",
        width: 960, 
        height: 640,
        disableContextMenu: true,
        disableExternalCall: false,
        disableFullscreen: false,
        params: { enableDebugging:"1" }
    };
			
    var u = new UnityObject2({ params: params });
			
    jQuery(function() {

        var $missingScreen = jQuery("#unityPlayer").find(".missing");
        var $brokenScreen = jQuery("#unityPlayer").find(".broken");
        $missingScreen.hide();
        $brokenScreen.hide();

        u.observeProgress(function (progress) {
            switch(progress.pluginStatus) {
                case "broken":
                    $brokenScreen.find("a").click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        u.installPlugin();
                        return false;
                    });
                    $brokenScreen.show();
                    break;
                case "missing":
                    $missingScreen.find("a").click(function (e) {
                        e.stopPropagation();
                        e.preventDefault();
                        u.installPlugin();
                        return false;
                    });
                    $missingScreen.show();
                    break;
                case "installed":
                    $missingScreen.remove();
                    break;
                case "first":
                    break;
            }
        });
        u.initPlugin(jQuery("#unityPlayer")[0], "<?php echo Yii::app()->createUrl($gameVersion->file_path) ?>");
    });
    -->
</script>
<div class="main">
    <div id="unityPlayer">
        <div class="missing">
            <a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now!">
                <img alt="Unity Web Player. Install now!" src="http://webplayer.unity3d.com/installation/getunity.png" width="193" height="63" />
            </a>
        </div>
    </div>
</div>