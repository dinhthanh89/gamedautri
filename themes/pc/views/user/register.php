<div class="main-page">
    <div class="page-in">
        <div class="register-content">
            <div class="breadcrumbs" id ="tree-link">
                <a href="#">Trang chủ >></a>
                <a href="#">Đăng ký</a>
            </div>
            <div class="page-in-border-1"></div>
            <div class="message-panel-left">
                <div class="property-game-tag-out">

                </div>
                <div id="download-right-panel-in">
                </div>
            </div>
            <div class="message-panel-border"></div>
            <div class="message-panel-right property-item-panel">
                <div class="form-register-in">
                    <h2 class="form-register-tittle">Đăng ký tài khoản</h2>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'users-index-form',
                        'enableAjaxValidation' => TRUE,
                        'enableClientValidation' => TRUE,
                        'action' => Yii::app()->createUrl('/user/register'),
                        'clientOptions' => array(
                            'validateOnSubmit' => TRUE,
                            'hideErrorMessage' => FALSE,
                        )
                    ));
                    ?>
                    <?php //echo $form->errorSummary($model); ?>


                    <ul>
                        <li>
                            <?php echo $form->labelEx($model, 'username', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'username', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'username', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'init_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'init_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'init_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'repeat_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'repeat_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'repeat_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'email', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'email', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'mobile', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'mobile', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'mobile', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'identity_card_number', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'identity_card_number', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'identity_card_number', array('class' => 'error')); ?>
                        </li>
                    </ul>
                    <div class="row buttons" >
                        <input type="checkbox" style="display: inline-block;margin-left: 15px;cursor: pointer" id="checkrule"/>
                        <div class="term" id="term" style="display: inline-block;margin-left: 5px;cursor: pointer" >
                            <label for="checkrule">Tôi đã đọc kỹ và đồng ý với các <a style="color:green" href="#"> điều khoản sử dụng </a> của ESIMO</label>
                        </div>
                        <script>
                            $('#checkrule').click(function() {
                                if (!$('#checkrule').is(':checked')) {
                                    $('#term').css('color', 'red');
                                }
                                else {
                                    $('#term').css('color', 'inherit');
                                }
                            });
                            $('#users-index-form').submit(function(event) {
                                if (!$('#checkrule').is(':checked')) {
                                    event.preventDefault();
                                    $('#term').css('color', 'red');
                                }
                            })
                        </script>
                        <br>
                        <?php echo CHtml::submitButton('Đăng ký', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
            <div style="clear: both"></div><!-- form -->
        </div>
    </div>
</div>
