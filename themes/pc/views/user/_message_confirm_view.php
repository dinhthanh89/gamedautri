<div class="info-friend">   
    
    <img class="avatar-image" width="30px" height="30px" src="<?php echo $data->user->getAvatar()?>">
    <div><?php echo $data->user->getFullName(); ?></div>
    <div><?php echo $data->user->getChip(); ?></div>    
    <?php
        echo CHtml::ajaxButton ('Từ chối', 
                                Yii::app()->createAbsoluteUrl('/user/confirmFriend'), 
                                array(// these are ajax options
                                    'type'=>'POST',
                                    'data' => array('id'=> $data->id,
                                                    'status' => -1,
                                                ),                                    
                                    'success'=>'js:function(string){
                                        var getData = $.parseJSON(string);
                                        if(getData["code"]== -1) createDiaglog("Lỗi định dạng file", getData["message"]);
                                        if(getData["code"]== 1) {                                        
                                            createDiaglog("Thay đổi thành công", getData["message"]);                                        
                                        }                                    
                                        }'
                                          // and don't use semi-colon after val(), you can also pass id here itself            
                                ),
                                array(
                                    'class'=>'normal-button'    
                                    )    
        );
         echo CHtml::ajaxButton ('Chấp nhận', 
                                Yii::app()->createAbsoluteUrl('/user/confirmFriend'), 
                                array(// these are ajax options
                                    'type'=>'POST',
                                    'data' => array('id'=> $data->id,
                                                    'status' => 1,
                                                ),                                    
                                    'success'=>'js:function(string){
                                        var getData = $.parseJSON(string);
                                        if(getData["code"]== -1) createDiaglog("Lỗi định dạng file", getData["message"]);
                                        if(getData["code"]== 1) {                                        
                                            createDiaglog("Thay đổi thành công", getData["message"]);                                        
                                        }                                    
                                        }'
                                          // and don't use semi-colon after val(), you can also pass id here itself            
                                ),
                                array(
                                    'class'=>'normal-button'    
                                    )    
        );
        ?>
    
</div>	
