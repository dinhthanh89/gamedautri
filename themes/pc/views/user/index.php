<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/all.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery-ui-1.9.2.custom.js');
?>
<script >
    var baseUrl = '<?php echo Yii::app()->baseUrl; ?>'
    var content = [];    
    function onClick(event) {
        event.preventDefault();event.stopPropagation();        
    }        
    
    $(document).ready(function(){              
        $('.footer-top').html('');
        content[1] = $('#main-content').html();
        $('#content-menu').children().click(function () {
            var menu = $(this).attr("contextmenu");
            if(! content[menu]) {
                //Ajax de lay noi dung tu server
                $.ajax({
                    url: baseUrl + '/user/index',
                    type: 'GET',
                    data: 'type=ajax'+'&content='+menu,
                    success: function(string) {
                        window.content[menu] = string;
                        $('#main-content').html(content[menu]);
                    }
                });
            }
            else $('#main-content').html(content[$(this).attr("contextmenu")]);
            if($(this).hasClass('tag-current')) return;
            $('#content-menu .tag-current').removeClass('tag-current').addClass('tag');
            $(this).addClass('tag-current');            
        });

    });
</script>
<div class="main-page">
<div class="page-in">
    <div class="tree-link">
        <a>Trang cá nhân</a>
    </div>
    <div class="profile-border-top"></div>        
    <div class="tag-group-profile" id="content-menu">

        <div class="tag-current" contextmenu="1" >
            <h1>Hồ sơ cá nhân</h1>
        </div>
        <div class="tag" contextmenu="2" >
            <h1>Hộp thư</h1>
        </div>
        <div class="tag" contextmenu="3" >
            <h1>Bạn bè</h1>
        </div>
        <div class="tag" contextmenu="4" >
            <h1>Tài sản</h1>
        </div>
        <div class="tag" contextmenu="5" >
            <h1>Bang hội của tôi</h1>
        </div>
        <div class="tag" contextmenu="6" >
            <h1>Lịch sử</h1>
        </div>
        <div class="tag" contextmenu="7" >
            <h1>Cài đặt</h1>
        </div>
        <div class="change-chips-button">

        </div>
    </div>
    <div id="main-content">
    <div class="profile-info-panel">
        <div class="profile-panel-left">
            <div class="avatar">
                <img src="<?php echo Yii::app()->user->getUser()->getAvatar(); ?>" height="205px" width="205px" alt="avatar"/>
            </div>
            <div class="exp-bar-border">
                <div class="exp-bar">
                    <span class="level">120</span>
                    <div class="exp-full-border">
                        <div class="exp-full">
                            <div class="exp-current"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="profile-panel-right">
            <div class="profile-status">
                <h3>Cần tuyển gấp vú em online 24/7 canh bàn 03 kênh Vip game Phỏm</h3>
                <div class="edit-button"></div>
            </div>
            <div class="profile-info-details">
                <div class="profile-info-left">
                    <div class="nick-name">
                        <h2>girl thần bài</h2>
                        <div class="edit-button"></div>
                    </div>
                    <br>
                    <br>
                    <h1>Họ và tên:</h1>
                    <span><?php echo yii::app()->user->getUser()->getFullName(); ?></span><br>
                    <h1>Giới tính:</h1>
                    <span><?php echo yii::app()->user->getUser()->getGender(); ?></span><br>
                    <h1>Nơi ở:</h1>
                    <span><?php echo yii::app()->user->getUser()->getAddress(); ?></span><br>
                    <h1>Chip:</h1>
                    <span><?php echo yii::app()->user->getUser()->getChip(); ?></span><br>
                    <h1>Gold:</h1>
                    <span><?php echo yii::app()->user->getUser()->getGold(); ?></span><br>
                    <h1>Điểm vip:</h1>
                    <span>1.500 point</span><br>
                    <h1>Kinh nghiệm:</h1>
                    <span>Lever 120</span>

                </div>
                <div class="profile-info-right">
                    <br>
                    <br>
                    <br><br><br>
                    <p>Bạn đã tham gia bang hội nào cả. Bạn đã đủ điều kiện lập một bang hội:</p>

                    <h1>Tiền chip: </h1>
                    <span>1.000.000 chips</span><br>
                    <h1>Kinh nghiệm:</h1>
                    <span>Lever 10</span><br>
                    <br>
                    <div class="creat-group"></div>
                    <div class="search-group"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="bot-profile-page">
        <div class="ribbons">
            <div class="ribbons-tittle">

            </div>

            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>
            <div class="ribbon">
                <div class="ribbon-icon"></div>
                <h1>THÁNH BÀI</h1>
                <h1>(Tiến lên miền nam)</h1>
                <h1>Tuần 3 tháng 12</h1>
            </div>

        </div>
        <span class ="profile-border-bot"></span>
        <div class="profile-bot-right">
            <div>

            </div>
        </div>
    </div>
    </div>    
    <div STYLE="clear: both"></div>
</div>
</div>