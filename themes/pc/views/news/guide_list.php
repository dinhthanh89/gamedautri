<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a href="<?php echo Yii::app()->createUrl('news/guide') ?>">Hướng dẫn</a>>><a><?php echo $dataCategory->name ?></a>
        </div>
        <div class="page-in-border-1"></div>
        <div class="column-left">                        
            <ul class="ul-style-gradient">
                <?php if(isset($dataPostCategory)):?>
                <?php foreach($dataPostCategory as $item):?>
                    <li class="<?php if($item->id == $dataCategory->id) echo "active" ?>"><div><a href="<?php echo Yii::app()->createUrl('news/guide?id='.$item->id); ?>"><?php echo $item->name ?></a></div> </li>                                            
                <?php endforeach;?>
                <?php endif;?>
            </ul>
        </div>
        <div class="column-right">           
            <div class="content">
                <?php $this->widget('zii.widgets.CListView', array(
                                'dataProvider'=>$dataPost,
                                'itemView'=>'_guide_item',
                                'htmlOptions' => array('class' => 'wrapper-table'),  
                                'summaryText'=>'',
                                'emptyText' => 'Chưa có hướng dẫn nào!',
                                'pager'=>array(
                                    'header'  => '',        
                                    'cssFile' => true,
                                    'class'=> 'CLinkPager',
                                    'firstPageLabel' => '&lt;&lt;',
                                    'prevPageLabel'  => '&lt;',
                                    'nextPageLabel'  => '&gt;',
                                    'lastPageLabel'  => '&gt;&gt;',
                                ),     
                                'pagerCssClass'=>'pagination', 
                                )); 
                ?>                           
            </div>
            <?php $this->widget('QuestionWidget',array('title'=>FALSE)); ?>      
        </div>
    </div>
    <div style="clear: both"></div>
</div>