<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <?php if($dataPost->category_id == 8):?>
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Tin tức</a>
            <?php else:?>
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a href="<?php echo Yii::app()->createUrl('news/guide') ?>">Hướng dẫn</a><a href="<?php echo Yii::app()->createUrl('news/guide?id='.$dataPost->category_id) ?>"><?php //echo $dataPost->category->name; ?></a>
            <?php endif;?>
        </div>
        <div class="page-in-border-1"></div>
        <div class="column-left">   
            <?php if($dataPost->category_id == 8): ?>
            <div class="title">Tin khác</div>
            <div class="page-in-border-2"></div>
            <ul class="ul-style-dis">               
                <?php if(isset($otherPost)): ?>
                <?php foreach($otherPost as $item):?>   
                    <li><span></span><a href="<?php echo $item->getDetailLink() ?>"><?php echo $item->title ?></a></li>                         
                <?php endforeach;?>
                <?php endif;?>                                                     
            </ul>
            <?php else:?>            
            <ul class="ul-style-gradient">
                <?php if(isset($dataGuide)):?>
                <?php foreach($dataGuide as $item):?>
                    <li class="<?php if($item->id == $dataPost->id) echo "active" ?>"><div><a href="<?php echo $item->getDetailLink() ?>"><?php echo $item->title ?></a></div> </li>                                            
                <?php endforeach;?>
                <?php endif;?>
            </ul>
            <?php endif;?>
            
        </div>
        <div class="column-right">
            <div class="info-news">
                <div class="title-news"><?php echo $dataPost->title; ?></div>
                <div style="display: inline-block;">
                    <img class="image-news" style="max-width:150px;max-height: 150px;" src="<?php echo Yii::app()->createAbsoluteUrl($dataPost->main_image) ?>" />
                </div>
                <div class="post-main">
                    <div class="list-like">
                         <?php $this->widget('LikeWidget', array('link'=>$dataPost->getDetailLink())) ?>
                    </div>
                    <div class="time-news"><?php echo date_format(new DateTime($dataPost->create_time), 'd/m/Y H:i') ?></div>
                    <div class="sapo-news"><?php echo $dataPost->sapo ?></div>
                </div>
                <div class="content-news"><?php echo $dataPost->content; ?></div>
            </div>
<!--            <div class="page-in-border-2"></div>
            <div class="other-news">
                <div class="title-97663E">Tin khác</div>
                <ul class="ul-style-dis">
                    <?php if(isset($otherPost)): ?>
                    <?php foreach($otherPost as $item):?>   
                        <li><span></span><a class="color-FF9805 margin-left-5" href="<?php echo $item->getDetailLink();?>"><?php echo $item->title; ?></a></li>
                    <?php endforeach;?>
                    <?php endif;?>                   
                </ul>
            </div>-->
            <div class="page-in-border-2"></div>
            <div class="list-comment">
                <div class="title-97663E">Bình luận</div>
                <div class="fb-comments" style="background:#BEBCBC;border: 1px solid #666666;font-weight:normal;padding:8px 0px;" data-href="<?php echo Yii::app()->createAbsoluteUrl($dataPost->getDetailLink()); ?>" data-width="680" data-num-posts="20"></div>
                <script>(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = "//connect.facebook.net/vi_VN/all.js#xfbml=1&appId=548488101849362";
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
                </script>
                <?php //$this->widget('CommentWidget', array('id'=>$dataPost->id)) ?>                                
            </div>
            <?php if($dataPost->category_id != 8):?>
            <?php $this->widget('QuestionWidget', array('title'=>FALSE)); ?>      
            <?php endif;?>
        </div>
    </div>
    <div style="clear: both"></div>
</div>