<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Hướng dẫn</a>
        </div>
        <div class="page-in-border-1"></div>        
        <div class="content-wrapper">            
            <div class="content">
                <?php $this->widget('zii.widgets.CListView', array(
                                'dataProvider'=>$dataPostCategory,
                                'itemView'=>'_category_item',
                                'htmlOptions' => array('class' => 'wrapper-table'),  
                                'summaryText'=>'',
                                'emptyText' => 'Không có danh mục nào!',
                                'pager'=>array(
                                    'header'         => '',        
                                    'cssFile' => true,
                                    'class'=> 'CLinkPager',
                                    'firstPageLabel' => '&lt;&lt;',
                                    'prevPageLabel'  => '&lt;',
                                    'nextPageLabel'  => '&gt;',
                                    'lastPageLabel'  => '&gt;&gt;',
                                ),     
                                'pagerCssClass'=>'pagination', 
                                )); 
                ?>                           
            </div>
            <?php $this->widget('QuestionWidget',  array('title'=>TRUE)) ?>            
        </div>        
    </div>
    <div style="clear: both"></div>
</div>