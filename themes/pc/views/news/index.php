<div class="main-page">
    <div class="page-in">
        <div class ="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a>Tin tức</a>
        </div>
        <div class="page-in-border-1"></div>
        <div class="column-left">            
            <div class="title">Tin khác</div>
            <div class="page-in-border-2"></div>
            <ul class="ul-style-dis">
                <?php if(isset($otherPost)):?>
                <?php foreach($otherPost as $item):?>
                    <li><span></span><a href="<?php echo $item->getDetailLink() ?>"><?php echo $item->title ?></a></li>                                            
                <?php endforeach;?>
                <?php endif;?>
            </ul>
        </div>
        <div class="column-right">
<!--            <div class="top-banner">
                <a href="<?php echo Yii::app()->createUrl('play') ?>"><img src="<?php echo Yii::app()->createAbsoluteUrl('files/post/thamgiangay.jpg') ?>"/></a>
            </div>-->
            <div class="content">
                <?php $this->widget('zii.widgets.CListView', array(
                                'dataProvider'=>$dataPost,
                                'itemView'=>'_post_item',
                                'htmlOptions' => array('class' => 'wrapper-table'),  
                                'summaryText'=>'',
                                'emptyText' => 'Chưa có bài viết nào!',
                                'pager'=>array(
                                    'header'         => '',        
                                    'cssFile' => true,
                                    'class'=> 'CLinkPager',
                                    'firstPageLabel' => '&lt;&lt;',
                                    'prevPageLabel'  => '&lt;',
                                    'nextPageLabel'  => '&gt;',
                                    'lastPageLabel'  => '&gt;&gt;',
                                    'maxButtonCount'=>5, 
                                ),     
                                'pagerCssClass'=>'pagination', 
                                )); 
                ?>                           
            </div>
        </div>
    </div>
    <div style="clear: both"></div>
</div>