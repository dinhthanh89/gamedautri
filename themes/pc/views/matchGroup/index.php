<?php
/* @var $this MatchGroupController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Match Groups',
);

$this->menu=array(
	array('label'=>'Create MatchGroup', 'url'=>array('create')),
	array('label'=>'Manage MatchGroup', 'url'=>array('admin')),
);
?>

<h1>Match Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
