<?php
/* @var $this MatchGroupController */
/* @var $model MatchGroup */

$this->breadcrumbs=array(
	'Match Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MatchGroup', 'url'=>array('index')),
	array('label'=>'Create MatchGroup', 'url'=>array('create')),
	array('label'=>'View MatchGroup', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MatchGroup', 'url'=>array('admin')),
);
?>

<h1>Update MatchGroup <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>