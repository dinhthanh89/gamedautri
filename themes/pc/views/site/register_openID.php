<?php
$session = new CHttpSession;
$session->open();
$provider = $session['openIdType'];
if (isset($session['profile']['username']))
    $suggestUsername = $session['profile']['username'];
else
    $suggestUsername = '';
?>
<div class="main-page">
    <div class="page-in">
        <div class="register-content">
            <div class ="tree-link">
                <a href="<?php echo Yii::app()->homeUrl ?>">Trang chủ</a>>><a>Đăng nhập với Open ID</a>
            </div>
            <div class="page-in-border-1"></div>
            <div class="message-panel-left">
                <div class="property-game-tag-out">

                </div>
                <div id="download-right-panel-in">
                </div>
            </div>
            <div class="message-panel-border"></div>
            <div class="message-panel-right property-item-panel">
                <div class="form-register-in">
                    <h2 class="form-register-tittle">Đăng nhập với OpenID</h2>
                    <p>Tài khoản <?php echo $provider; ?> (<?php echo $session['profile']['email'] ?>) này chưa kết nối với hệ thống của chúng tôi!</p>
                    <div>
                        <p>Nếu bạn đã có tài khoản trong hệ thống, xin hãy đăng nhập: </p>
<!--                        <form method="POST" name="mergeOpenID" id="mergeOpenID" action="<?php echo Yii::app()->createUrl('site/mergeOpenId'); ?>" style="line-height: 30px">
                            <ul>
                                <li>
                                    <div class="register-field">Tên đăng nhập</div>
                                    <input id="username" type="text" placehoder="Tên đăng nhập" class="register-input" name="username"/><br>
                                </li>
                                <li>
                                    <div id="password" class="register-field">Mật khẩu</div>
                                    <input type="password" placehoder="Mật khẩu" class="register-input" name ="password"/><br>
                                </li>
                            </ul>
                            <input type="submit" value="Đăng nhập" class="brown-small-button" name="submit"/> 
                        </form>-->
                    </div>
                    Nếu chưa, xin  vui lòng tạo tài khoản mới:
                    <?php
                    $model = new User('registerOpenId');
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'user-register-form-openid',
                        'enableAjaxValidation' => TRUE,
                        'enableClientValidation' => TRUE,
                        'clientOptions' => array(
//                            'validateOnType'=>TRUE,
//                            'validateOnChange'=>TRUE,
                            'validateOnSubmit' => TRUE,
                            'hideErrorMessage' => FALSE,
                        ),
                        'action' => Yii::app()->createUrl('site/RegisterOpenId'),
                        'htmlOptions' => array('class' => 'login-form-openid'),
                            ))
                    ?>   
                    <?php echo $form->errorSummary($model); ?>
                    <ul>
                        <li>
                            <?php echo $form->labelEx($model, 'username', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'username', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'username', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'init_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'init_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'init_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'repeat_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'repeat_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'repeat_password', array('class' => 'error')); ?>
                        </li>
                    </ul>
                    <div class="row buttons" >
                        <!--            <div class="checkbox-wrapper" onclick="click_checkbox();">
                                    </div>-->
                        <input type="checkbox" style="display: inline-block;margin-left: 15px;cursor: pointer" id="checkrule"/>
                        <div class="term" id="term" style="display: inline-block;margin-left: 5px;cursor: pointer" >
                            <label for="checkrule">Tôi đã đọc kỹ và đồng ý với các <a style="color:green" href="#"> điều khoản sử dụng </a> của Gamedautri</label>
                        </div>
<!--                        <script>
                            $('#checkrule').click(function() {
                                if (!$('#checkrule').is(':checked')) {
                                    $('#term').css('color', 'red');
                                }
                                else {
                                    $('#term').css('color', 'inherit');
                                }
                            });
                            $('#users-index-form').submit(function(event) {
                                if (!$('#checkrule').is(':checked')) {
                                    event.preventDefault();
                                    $('#term').css('color', 'red');
                                }
                            })
                        </script>-->
                        <br>
                        <?php echo CHtml::submitButton('Đăng ký', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
            <div style="clear: both"></div><!-- form -->
        </div>
    </div>
</div>
<!--<script>

$('#submit').click(function(event) {
if ($checked == 1)
    return;
else {
    event.preventDefault();
    createDiaglog('Điều khoản sử dụng', 'Bạn chưa đồng ý với các điều khoản sử dụng, xin vui lòng đồng ý để có thể đăng ký !', 150);
}
}
)
</script>-->