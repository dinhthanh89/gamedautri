<?php
$cs = Yii::app()->clientScript;
$cs->registerScriptFile('/js/jquery-1.7.min.js');
?>

<script>
    $(document).ready(function(){
        alert('<?php echo $message; ?>');
<?php if (isset($redirectUrl)): ?>
            window.location = '<?php echo $redirectUrl ?>';
<?php endif; ?>
    });
</script>