<div class="main-page">
    <div class="page-in">
        <div class="register-content">
            <div class="tree-link">
                <a href="<?php echo Yii::app()->homeUrl;?>">Trang chủ</a>>><a>Lấy lại mật khẩu</a>
            </div>
            <div class="page-in-border-1"></div>
            <div class="message-panel-left">
                <div class="property-game-tag-out">

                </div>
                <div id="download-right-panel-in">
                </div>
            </div>
            <div class="message-panel-border"></div>
            <div class="message-panel-right property-item-panel">
                <div class="form-register-in">
                    <h2 class="form-register-tittle" style="margin-bottom: 30px;">Lấy lại mật khẩu trên Esimo</h2>
                    <form class="forgot-password-form" method="POST" action="<?php echo Yii::app()->createUrl(Common::getCurrentUrl()); ?>">
                    <?php echo CHtml::hiddenField('access_token', $access_token) ?>
                    <ul>                        
                        <li>
                            <label class="label register-field">Mật khẩu:</label>
                            <input type="password" class="input register-input" name="password" value=""/>                           
                        </li>
                        <li>
                            <label class="label register-field">Nhập lại mật khẩu:</label>
                            <input type="password" class="input register-input" name="repassword" value=""/>
                        </li>
                    </ul>
                    <div class="row buttons" style="padding-left: 153px" >
                        <?php echo CHtml::submitButton('Xác nhận', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    </form>    
                </div>
            </div>
            <div style="clear: both"></div><!-- form -->
        </div>
    </div>
</div>
