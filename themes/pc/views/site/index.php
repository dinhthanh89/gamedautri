<?php
/* @var $this SiteController */
$config = Configuration::model()->getConfig('support_contact');
$this->pageTitle = Yii::app()->name;
if (Yii::app()->request->getParam('newuser') == 1 && Yii::app()->user->is_login()):?>
    <script>
        createDiaglog('Đăng kí thành công', 200);
    </script>
<?php
endif;
?>
    <div class="border-img"><img src="<?php echo Yii::app()->createUrl('css/images/boder.png') ?>" /></div>
    <div class="feature">
    <div class="feature-top-shadow"></div>
    <div class="border-repeat"></div>
    <div class="feature-main">
        <div class="main">
            <!--<div class="feature-title">Hệ thống game trên XÓM CỜ BẠC</div>-->
            <ul>
                <?php if(isset($games)): ?>
                <?php foreach($games as $index => $game):?>
                <li class="<?php ($index == 0) ? "first-child" : "" ?>">     
                    <?php if($game->status == 1): ?>
                    <a href="<?php echo Yii::app()->createUrl($game->url) ?>">
                        <img width="150px" height="100px" alt="<?php echo $game->name ?>" src="<?php echo Yii::app()->createUrl($game->image) ?>"/>
                    </a>
                    <?php else: ?>
                        <span></span>
                        <img width="150px" height="100px" alt="<?php echo $game->name ?>" src="<?php echo Yii::app()->createUrl($game->image) ?>"/>
                    <?php endif;?>
<!--                    <div style="white-space: nowrap">
                        <?php if($game->status == 1): ?>
                        <a class="play-now" href="<?php echo Yii::app()->createUrl($game->url) ?>"></a>
                        <a class="download-link" href="<?php echo Yii::app()->createUrl('download/detail?id='.$game->id) ?>"></a>
                        <?php else:?>
                        <div class="coming-soon-out"><div class="coming-soon">Coming soon!</div></div>
                        <?php endif;?>
                    </div>-->
                </li>                
                <?php endforeach;?>
                <?php endif;?>               
            </ul>
        </div>
    </div>
    <div class="feature-bottom-shadow"></div>
</div>
<div class="slide">    
    <div class="main"> 
        <div class="slide-left">
            <div class="slide-left-content">
<!--                <div class="slide-left-item">
                    <a href="<?php echo Yii::app()->createUrl('bai-viet/khuyen-mai-nhan-dip-tet-nguyen-dan-giap-ngo.htm') ?>"><img src="images/slide_5.jpg" width="" height="" alt="slide"></a>
                </div>-->
                <div class="slide-left-item">
                    <img class="img-slide" src="images/banner-dautri.png" width="" height="" alt="slide">
                    <!--<img class="img-slide-a"src="images/gd-slide.png" width="" height="" alt="slide">-->
                </div> 
                <div class="slide-left-item">
                    <img class="img-slide" src="images/banner-dautri.png" width="" height="" alt="slide">
                    <!--<img class="img-slide-a"src="images/gd-slide.png" width="" height="" alt="slide">-->
                </div>
                <div class="slide-left-item">
                    <img class="img-slide" src="images/banner-dautri.png" width="" height="" alt="slide">
                    <!--<img class="img-slide-a"src="images/gd-slide.png" width="" height="" alt="slide">-->
                </div>
                
<!--                <div class="slide-left-item">
                    <a href="<?php echo Yii::app()->createUrl('news/detail?id=37') ?>"><img src="images/slide_4.jpg" width="" height="" alt="slide"></a>
                </div>-->
            </div>
            <div class="radio-slide">
                <div class="radio-check" contextmenu="0" id="radio-check1"></div>
                <div class="radio-check" contextmenu="1" id="radio-check2"></div>
                <div class="radio-check" contextmenu="2" id="radio-check3"></div>                
            </div>                
        </div>
        <a class="slide-play-game" href="<?php echo Yii::app()->createUrl('play') ?>"></a>
        <a class="slide-download-game" href="<?php echo Yii::app()->createUrl('download')?>"></a>
        <div class="slide-right">
            
        </div>
    </div>
    <script type="text/javascript">                
        autoSlide();
        //Set slide click
        $('.radio-check').click(function(){
            slideNow = parseFloat($(this).attr('contextmenu'));
            setSlideContent(slideNow);
        })
        $('.slide-left-content').click(function() {
            nextSlide();
        })
    </script>   
</div>

    <div class="top-caothu">
        <img class="img-slide-b" src="images/top-caothu.png" width="" height="" alt="slide"><br>
        TOP ĐẠI GIA
    </div>
    <div class="top-chip-gold">
    <div class="main">
        <?php $this->widget('TopGoldWidget') ?> 
    </div>
    </div>
    <div class="border-img"><img src="<?php echo Yii::app()->createUrl('css/images/boder.png') ?>" /></div>
    <div class="newsevent">
        <div class="main">
        <?php $this->widget('NewsEventWidget') ?>  
        </div>
    </div>
    <div class="border-img"><img src="<?php echo Yii::app()->createUrl('css/images/boder.png') ?>" /></div>
    
    <div class="download-adm-top">
    <div class="main">
        <div class="download-adm">
            <img class="img-slide-b" src="images/appios.png" width="" height="" alt="slide"><br><br>
            </a>
            GAME ĐẤU TRÍ tương thích với tất cả các thiết bị di động của iOS: iPhone 3s, iPhone 4, iPhone 4s, iphone 5, iPhone 5s, iPhone 5c, iPhone 6, iPhone 6+, iPad 1, iPad 2, iPad 3, iPod
        </div>
        <div class="download-adm">
            <img class="img-slide-b" src="images/appadroid.png" width="" height="" alt="slide"><br><br>
            GAME ĐẤU TRÍ tương thích với tất cả các dòng điện thoại Adroid từ phiên bản: Adroid 1.0, Adroid 2.0, Adroid 3.0, Adroid 4.0, Adroid 5.0
        </div>
        <div class="download-adm">
            <iframe src="http://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/gamedautri?fref=ts&amp;width=245&amp;colorscheme=light&amp;show_faces=true&amp;connections=9&amp;stream=false&amp;header=false&amp;height=270" scrolling="no" frameborder="0" scrolling="no" style="border: medium none; overflow: hidden; height: 100%; width: 100%;background:none;"></iframe>
        </div>
    </div>
    </div>