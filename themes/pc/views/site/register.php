<?php

?>
<div class="main-page">
    <div class="page-in">
        <div class="register-content">
            <div class ="tree-link">
                <a href="<?php echo Yii::app()->homeUrl ?>">Trang chủ</a>>><a>Đăng kí</a>
            </div>
            <div class="page-in-border-1"></div>            
            <div class="message-panel-border"></div>
            <div class="message-panel-right property-item-panel">
                <div class="form-register-in">
                    <h2 class="form-register-tittle">Đăng kí tài khoản</h2>                                                            
                    <?php
                    $model = new User('register');
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'user-register-form',
                        'enableAjaxValidation' => TRUE,
                        'enableClientValidation' => TRUE,
                        'clientOptions' => array(
//                            'validateOnType'=>TRUE,
//                            'validateOnChange'=>TRUE,
                            'validateOnSubmit' => TRUE,
                            'hideErrorMessage' => FALSE,
                        ),
                        'action' => Yii::app()->createUrl('site/register?partner='.$partner),
                        'htmlOptions' => array('class' => 'login-form-openid'),
                            ))
                    ?>                       
                    <ul>
                        <li>
                            <?php echo $form->labelEx($model, 'username', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'username', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'username', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'init_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'init_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'init_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'repeat_password', array('class' => 'label register-field')); ?>
                            <?php echo $form->passwordField($model, 'repeat_password', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'repeat_password', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'email', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'email', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'email', array('class' => 'error')); ?>
                        </li>
                        <li>
                            <?php echo $form->labelEx($model, 'mobile', array('class' => 'label register-field')); ?>
                            <?php echo $form->textField($model, 'mobile', array('class' => 'input register-input')); ?>
                            <?php echo $form->error($model, 'mobile', array('class' => 'error')); ?>
                        </li>
                    </ul>
                    <div class="row buttons" >
                        <!--            <div class="checkbox-wrapper" onclick="click_checkbox();">
                                    </div>-->
                        <input type="checkbox" style="display: inline-block;margin-left: 15px;cursor: pointer" id="checkrule"/>
                        <div class="term" id="term" style="display: inline-block;margin-left: 5px;cursor: pointer" >
                            <label for="checkrule">Tôi đã đọc kỹ và đồng ý với các <a class="policy" style="color:green"> điều khoản sử dụng </a> của Game Đấu Trí</label>
                        </div>
<!--                        <script>
                            $('#checkrule').click(function() {
                                if (!$('#checkrule').is(':checked')) {
                                    $('#term').css('color', 'red');
                                }
                                else {
                                    $('#term').css('color', 'inherit');
                                }
                            });
                            $('#users-index-form').submit(function(event) {
                                if (!$('#checkrule').is(':checked')) {
                                    event.preventDefault();
                                    $('#term').css('color', 'red');
                                }
                            })
                        </script>-->
                        <br>
                        <?php echo CHtml::submitButton('Đăng ký', array('class' => 'button brown-small-button', 'id' => 'submit')); ?>
                    </div>
                    <?php $this->endWidget(); ?> 
                </div>
            </div>
            <div style="clear: both"></div><!-- form -->
        </div>
    </div>
</div>
<script>
     $('.register-button').bind('click');              
</script>