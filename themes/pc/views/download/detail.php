<div class="main-page">
    <div class="page-in">
<!--        <div class="tree-link">
            <a href="<?php echo Yii::app()->homeUrl; ?>">Trang chủ</a>>><a href="<?php echo Yii::app()->createUrl('download') ?>">Download</a>>><a>Xóm cờ bạc</a>
        </div>-->
        <!--<div class="page-in-border-1"></div>-->
        <div class="phom-out phom-out-ios">           
            <div class="game-detail-content">
                <img width="200px" height="200px" src="<?php echo Yii::app()->createAbsoluteUrl($model->image) ?>">    
                <?php $this->widget('PostHdWidget', array('category_id'=>14)) ?>       
                    <span class="title"><?php // echo $model->name ?></span> 
                    <div class="top-caothu">
                        <img class="img-slide-b" src="<?php echo Yii::app()->createUrl('images/top-caothu.png') ?>" width="" height="" alt="slide"><br>
                    </div>
                    <?php if(isset($gameVersionModel)): ?>
                    <?php foreach($gameVersionModel as $item):?>
                    <div class="phom-item">
                        
                        <?php if($item->gamePlatform->platform->code_identifier == 'android'):?>
                        <a id="android-link" href="<?php echo $item->gamePlatform->market_url; ?>" link='<?php echo $item->id?>' class="download-game-button-adroid">                                        
                            <?php $classStore = 'download-on-google-play'?>                                               
                        </a>
                        <div class="phom-item-info">
                            <div class="dtios">GAME ĐẤU TRÍ trên ANDROID</div>
                            <a onclick="downloadFile($('#android-link').attr('link'));" link='<?php echo $item->id?>'>
                            <div class="ipa">Cài trực tiếp FILE .APK</div>
                            </a>
                            <span>Dung lượng:</span>
                            <span><?php echo $item->getSize()?></span>
                            <br>
                            <span>Phiên bản:</span>
                            <span><?php echo $item->getCurrentVersion() ?></span>  
                            <div class="">Tương thíc với tất cả các dòng điện thoại Adroid từ phiên bản: Adroid 1.0, Adroid 2.0, Adroid 3.0, Adroid 4.0, Adroid 5.0</div>
                        </div>
                        <?php elseif($item->gamePlatform->platform->code_identifier == 'ios'): ?>
                        <a id="ios-link" href="<?php echo $item->gamePlatform->market_url; ?>"  link='<?php echo $item->id?>' class="download-game-button">                                                            
                            <?php $classStore = 'download-on-app-store'?>
                        </a>
                        <div class="phom-item-info">
                            <div class="dtios">GAME ĐẤU TRÍ trên iOS</div>
                            <a onclick="downloadFile($('#ios-link').attr('link'));" link='<?php echo $item->id?>'>                            
                            <div class="ipa">Cài trực tiếp FILE .IPA</div>
                            </a>
                            <span>Dung lượng:</span>
                            <span><?php echo $item->getSize()?></span>
                            <br>
                            <span>Phiên bản:</span>
                            <span><?php echo $item->getCurrentVersion();?></span>   
                            <div class="">Tương thíc với tất cả các thiết bị di động của iOS: iPhone 3s, iPhone 4, iPhone 4s, iphone 5, iPhone 5s, iPhone 5c, iPhone 6, iPhone 6+, iPad 1, iPad 2, iPad 3, iPod</div>                            
                        </div>
                        <?php else:?>
                            <?php $classStore = ''; ?>
                        <?php endif;?>                        
                        
                    </div>                        
                    <?php endforeach;?>
                    <?php endif;?>                
                <div style="clear: both"></div>
            </div>
            <div class="game-detail-description">
                <?php // echo $model->description ?>
            </div>
            <div class="game-detail-guide"></div>
            
        </div>            
    </div>
</div>