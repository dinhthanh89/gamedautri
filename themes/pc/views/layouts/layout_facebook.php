<?php
$user = Yii::app()->user->getUser();
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta name="ROBOTS" content="index,follow">
        <meta name="AUTHOR" content="Esimo.vn">
        <meta http-equiv="EXPIRES" content="0">
        <meta name="RESOURCE-TYPE" content="DOCUMENT">
        <meta name="DISTRIBUTION" content="GLOBAL">
        <meta name="COPYRIGHT" content="Copyright (c) by Esimo">
        <meta name="Googlebot" content="index,follow,archive">
        <meta name="RATING" content="GENERAL">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                                
        
        <link rel="shortcut icon" href="<?php echo Yii::app()->createUrl('images/favicon.png');?>" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/main.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/style.css' ?>" type="text/css" rel="stylesheet" />       
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>        
        <script src="<?php echo Yii::app()->baseUrl . '/js/all.js' ?>"></script>
        <script type="text/javascript">
            baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <?php include_once'googleanalytic.php'; ?>
    </head>
    <body class="body-play">                     
        <div class="nav" style="margin-top:0px;">            
            <div class="nav-main">
                <div class="nav-main-border"></div>
                <div class="main">
                    <h1 class="logo-play"><a href="<?php echo Yii::app()->homeUrl ?>" target="blank"></a></h1>                    
                </div> 
            </div>    
            <div class="nav-bottom-shadow"></div>
        </div>
        
        <?php echo $content;?>
        <div class="share" style="text-align:center">
            <div class="shareItem">
                <div class="fb-like" data-href="https://facebook.com/pages/Esimo-Game/527582053993279" data-send="false" data-layout="button_count" data-width="90" data-height="21" data-show-faces="true"></div>                    
            </div>            
            <div class="shareItem">
                <script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
                <g:plusone style="width:90px;height:20px;" href='https://plus.google.com/u/0/b/113172227836581654151/113172227836581654151'></g:plusone>
            </div>
            <div class="shareItem">
                <a href="https://twitter.com/esimogame" class="twitter-share-button" data-lang="en" style="width:90px;height:20px;">Tweet</a>
                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>            
            </div>
            
        </div>
        <div class="footerLinks">
            <a href="<?php echo Yii::app()->createAbsoluteUrl(''); ?>" target="blank">HOTLINE: 0126 25 00000</a> | <a href="<?php echo Yii::app()->createAbsoluteUrl('site/intro'); ?>" target="blank">Giới thiệu</a> | <a href="<?php echo Yii::app()->createAbsoluteUrl('site/policy'); ?>" target="blank">Điều khoản</a>
        </div>
        <div class="copyright">@ 2013 ESIMO</div>                       
    </body>
</html>