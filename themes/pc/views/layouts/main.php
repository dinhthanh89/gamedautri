<?php
$user = Yii::app()->user->getUser();
$policy = Post::model()->find('status = 1 AND category_id = 2');
$config = Configuration::model()->getConfig('support_contact');
$curr_controller_a = Yii::app()->controller->id;
$curr_action_a = Yii::app()->controller->action->id;
?>
<!DOCTYPE html>
<html>
    <head>        
        <title><?php echo isset($this->pageTitle) ? $this->pageTitle : Yii::app()->name; ?></title>
        <meta name="ROBOTS" content="index,follow">
        <meta name="AUTHOR" content="gamedautri.com">
        <meta http-equiv="EXPIRES" content="0">
        <meta name="RESOURCE-TYPE" content="DOCUMENT">
        <meta name="DISTRIBUTION" content="GLOBAL">
        <meta name="COPYRIGHT" content="Copyright (c) by Gamedautri">
        <meta name="Googlebot" content="index,follow,archive">
        <meta name="RATING" content="GENERAL">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                
                
        <script language="javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
        <link rel="shortcut icon" href="" />        
        <link href="<?php echo Yii::app()->baseUrl . '/css/main.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/style.css' ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo Yii::app()->baseUrl . '/css/font-awesome.css' ?>" type="text/css" rel="stylesheet" />
        <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>        
        <script src="<?php echo Yii::app()->baseUrl . '/js/all.js' ?>"></script>
        <script type="text/javascript">
            baseUrl = '<?php echo Yii::app()->baseUrl; ?>';
        </script>
        <?php include_once'googleanalytic.php'; ?>
        <script language="javascript">
        $(document).ready(function(){
	(function($){
		//Căn giữa phần tử thuộc tính là absolute so với phần hiển thị của trình duyệt, chỉ dùng cho phần tử absolute đối với body
		$.fn.absoluteCenter = function(){
			this.each(function(){
				var top = -($(this).outerHeight() / 2)+'px';
				var left = -($(this).outerWidth() / 2)+'px';
				$(this).css({'position':'absolute', 'position':'fixed', 'margin-top':top, 'margin-left':left, 'top':'50%', 'left':'50%'});
				return this;
			});
		}
	})(jQuery);
        
        $(document).keydown(function(e){            
            if(e.keyCode == 13){
                 ajaxLogin($('#user1').val(), $('.pass').val());
            }
        });
	
	$('a#show-popup').click(function(){
		//Đặt biến cho các đối tượng để gọi dễ dàng
		var bg=$('div#popup-bg');
		var obj=$('div#popup');
		var btnClose=obj.find('#popup-close');
		//Hiện các đối tượng
		bg.animate({opacity:0.2},0).fadeIn(200); //cho nền trong suốt
		obj.fadeIn(200).draggable({cursor:'move',handle:'#popup-header'}).absoluteCenter(); //căn giữa popup và thêm draggable của jquery UI cho phần header của popup
		//Đóng popup khi nhấn nút
		btnClose.click(function(){
			bg.fadeOut(200);
			obj.fadeOut(200);
		});
		//Đóng popup khi nhấn background
		bg.click(function(){
			btnClose.click(); //Kế thừa nút đóng ở trên
		});
		//Đóng popup khi nhấn nút Esc trên bàn phím
		$(document).keydown(function(e){
			if(e.keyCode==27){
				btnClose.click(); //Kế thừa nút đóng ở trên
			}
		});
		return false;
	});
});
</script>
<!--        <script type="text/javascript">            
            var x = '';
            var y = '';
            var iFrame = '';
            $(function() {
                setTimeout(findLike, 3000);
                function findLike() {
                    $('iframe').each(function() {                        
                        if ($(this).parents('.fb-like').hasClass('fb-hidden') == true ) {    
                            iFrame = $(this);
                            $(iFrame).css('opacity', '0.01');                                                           
                        }                                                           
                    });
                }                           
                var timeout;
                $(document).mousemove(function(e){                         
                    if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
                        x = e.clientX;
                        y = e.clientY;
                    }else{
                        x = e.pageX;
                        y = e.pageY;
                    }                       
                    //clearTimeout(timeout);
                    timeout = setTimeout(function(){                            
                        if (iFrame != '') {
                            var offTop = y-10;
                            var offLeft = x-25;                               
                            $(iFrame).offset({ top: offTop, left: offLeft });
                        }    
                    }, 3000);                                         
                });
                $(document).click(function() {                    
                    $(iFrame).css('opacity', '0.01');
                });  
                $('.facebook-like').mouseover(function() {
                    if (iFrame != '') {
                       $(document).unbind('mousemove');
                    }      
                 });

            });            
        </script> -->        
    </head>
    <body class="body">
        <div id="popup-bg"></div>
                    <div id="popup">
                        <div id="popup-header">ĐĂNG KÝ<span id="popup-close" title="Close">x</span></div>
                        <?php
                                $model = new User('register');
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'user-register-form',
                                    'enableAjaxValidation' => TRUE,
                                    'enableClientValidation' => TRUE,
                                    'clientOptions' => array('validateOnSubmit' => TRUE),
                                    'action' => Yii::app()->createUrl('site/register'),
                                    'htmlOptions' => array('autocomplete'=>'off'),
                                        ))
                                ?> 
                        <div id="popup-content">
                            <div style="color: #FFCC00; font-size: 16px; text-align: center;">Vui lòng điền chính xác thông tin tài khoản trong dấu (*)</div>
                            <div class="form-popup">                       
                                <div>
                                    <?php echo $form->textField($model, 'username', array('class'=>'user','autocomplete'=>'off','placeholder' => 'Tên đăng nhập')); ?><span>(*)</span>
                                    <div class="regedit-error-message"><?php echo $form->error($model, 'username'); ?></div>
                                </div>
                                <div>
                                    <?php echo $form->passwordField($model, 'init_password', array('class'=>'user','placeholder' => 'Mật khẩu')); ?><span>(*)</span>
                                    <div class="regedit-error-message"><?php echo $form->error($model, 'init_password'); ?></div>
                                </div>
                                <div>
                                    <?php echo $form->passwordField($model, 'repeat_password', array('class'=>'user','placeholder' => 'Gõ lại mật khẩu')); ?><span>(*)</span>
                                    <div class="regedit-error-message"><?php echo $form->error($model, 'repeat_password'); ?></div>
                                </div>
                            </div>
                            <div class="form-popup">                       
                            <div>
                                <?php echo $form->textField($model, 'email', array('class'=>'user','placeholder' => 'Email')); ?><span>(*)</span>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'email'); ?></div>
                            </div>
                            <div>
                                <?php echo $form->textField($model, 'mobile', array('class'=>'user','placeholder' => 'Số điện thoại')); ?><span>(*)</span>
                                <div class="regedit-error-message"><?php echo $form->error($model, 'mobile'); ?></div>
                            </div>
                            <?php //$this->widget('CCaptcha'); ?><?php //echo CHtml::textField('captcha'); ?><span>(*)</span>
                            </div>
                            <div class="term" >
                                <input type="checkbox" id="register_rule"/>
                                <label for="checkrule" class="regedit-form-rule">Tôi đã đọc kỹ và đồng ý với các <a class="policy" style="color:#FFFF00" target="_blank" href="<?php echo Yii::app()->createUrl('site/policy') ?>"> điều khoản sử dụng </a> của GameDauTri</label>                                                                
                            </div>
                            <div class="form-button">
                                <input class="button inputSubmit" type="submit" value="Đăng ký">
                            </div>
                            <?php $this->endWidget(); ?> 
                        </div>
                    </div>
        <div class="inner-body">
<!--        <div class="fb-like fb-hidden" style="position:absolute;opacity:0.01" data-href="https://www.facebook.com/pages/Esimo-Game/527582053993279" data-send="false" stream="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>       
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1401311996778175";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));                                      
        </script>-->        
        <div class="top">
            <div class="main">
                <h1 class="logo"><a href="<?php echo Yii::app()->homeUrl ?>"></a></h1>
                    <?php if (!isset($_SESSION['user']) || $_SESSION['user'] == NULL): ?>
                    <div class="login-group">                        
                        <div class="items-register-group">
<!--                            <a class="button register-button">ĐĂNG KÝ</a>                            -->
                    <div class="allform">
                        <div class="form-dn"  >                       
                            <input id="user1" class="user" type="text" name="user" placeholder="Tên đăng nhập">
                            <input class="pass" type="password" name="password" placeholder="Mật khẩu">
                            <div class="login-error"></div>
                        </div>
                        <a onclick="ajaxLogin($('#user1').val(), $('.pass').val());">
                        <img class="img-dn" src="<?php echo Yii::app()->createUrl('css/images/buttom.png') ?>" />
                        </a>
                    </div>
                    <div class="dangky">
                        <div class="dangky-mail">
                            <a href="#" id="show-popup">
                                <img src="<?php echo Yii::app()->createUrl('css/images/dangky.png') ?>" /> ĐĂNG KÝ</a>
                        </div>
                        <div class="loginfa">
                            <a href="<?php echo Yii::app()->createAbsoluteUrl('/site/FacebookLogin'); ?>">
                            <img src="<?php echo Yii::app()->createUrl('css/images/loginfa.png') ?>" />
                            </a>
                        </div>
                    </div>
                    
                    </div>
                    <?php else:?>
                    <?php 
                    $user = User::model()->findByPk($_SESSION['user']['id']);
                    ?>
                    <div class="login-group">
                        <span class="login-text">Xin chào, <span class="user-info-username"><?php echo $user->username?></span></span>
                        <span class="space">|</span>
                        <span class="login-text"><?php echo $user->getGold(); ?> golds</span>
                        <span class="space">|</span>
                        <span class="login-text"><?php echo $user->getChip(); ?> chips</span>
                        <span class="space">|</span>
                        <a class="button" style="margin-top: 7px" href="<?php echo Yii::app()->createUrl('user/exchange') ?>">Đổi gold</a>
                        <span class="space">|</span>
                        <a class="button" style="margin-top: 7px" href="<?php echo Yii::app()->createUrl('site/logout') ?>">Thoát</a>
                    </div>    
                    <?php endif;?>
            </div>
        </div>
        <div class="nav">           
            <div class="nav-top-shadow"></div>
            <div class="border-repeat"></div>
            <div class="nav-main">
                <!--<div class="nav-main-border"></div>-->
                <div class="main">
                    <div class="top-menu">
                        <ul>
                            <a href="<?php echo Yii::app()->homeUrl ?>"><i class="fa fa-home fa-2x" style="line-height:50px; color: #ffffff; margin: 0px 30px 0px 30px;"></i></a>
                            <li class="<?php if($curr_controller_a == "download") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('download') ?>"><span>Download</span></a></li>
                            <li class="<?php if($curr_controller_a == "site" && $curr_action_a == "intro") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('site/intro') ?>"><span>Giới thiệu</span></a></li>
                            <li class="<?php if($curr_controller_a == "news" && $curr_action_a == "index") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('news') ?>"><span>Tin tức - Sự kiện</span></a></li>
                            <li class="<?php if($curr_controller_a == "news" && $curr_action_a == "guide") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('news/guide');?>"><span>Hướng dẫn</span></a></li>
                            <li class="<?php if($curr_controller_a == "forums") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('forums')?>"><span>Diễn đàn</span></a></li>
                            <li class="<?php if($curr_controller_a == "play") echo "actived" ?>"><a href="<?php echo Yii::app()->createUrl('play')?>"><span>Chơi ngay</span></a></li>
                            <!--<span class="flower"></span>-->
                        </ul>    
                    </div> 
                
                </div> 
            </div>   
            <div class="buttom-menu"></div>
            <div class="nav-bottom-shadow"></div>
            <div class="nav-text">
    <span>HOTLINE CHĂM SÓC KHÁCH HÀNG</span>
    <span class="sdt"><?php if(isset($config->mobile)) echo $config->mobile ?></span>
<!--                        <span><img src="<?php echo Yii::app()->createUrl('css/images/sdt.png') ?>" /></span>-->
</div>
    </div>
    
        </div>
        
        <?php echo $content;?>
        
        <div class="footer">
            <div class="footer-top-shadow"></div>
            <div class="border-repeat"></div>
            <div class="footer-main">
                <div class="main">
                    <ul class="footer-items">
                        
                        <li class="footer-item">
                             <?php $this->widget('PostWidget', array('category_id'=>14)) ?>                           
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                            <?php $this->widget('TagWidget') ?>                       
                        </li>
                        <li class="item-border-right"></li>
                        <li class="footer-item">
                             <?php $this->widget('ContactWidget') ?> 
                        </li>
                    </ul> 
                </div>    
            </div>
            <div class="footer-bottom-shadow"></div>                 
        </div>
        <div class="copyright">@ <?php echo date('Y') ?> GAME ĐẤU TRÍ</div>      
        </div>
    </body>
</html>

<script type="text/javascript">       
    $(document).ready(function(){        
        //$('.input-text').autocomplete({ disabled: true });
        $('#forgot_password').click(function(){
            $(this).hide();
            $('#loading').show();
            ajaxForgotPassword($('#forgotpassword_email').val());
        })
        
    })
    
    $('.inputSubmit').click(function(event){
        if(!$('#register_rule').is(':checked'))
        {
            $('.regedit-form-rule').css('color','red');
            event.preventDefault();
        }
        else {
            $('.regedit-form-rule').css('color','#333');
        }
    });
    $('.input-text').focus(function(){
        $(this).parent('div').find('span').hide();
    });
    $('.input-text').focusout(function(){
        if($(this).val() == "")
            $(this).parent('div').find('span').show();
    });
    
    $('.link-forgot-password').click(function(){
        $('.form-login').hide();
        $('.form-forgot-password').show();                
    })        
    
    $('.link-login').click(function(){
        $('.form-forgot-password').hide();
        $('.form-login').show();
    })    
    
    $('.register-button').click(function(){
        if($('.form-register').css('display') == 'block'){
            $('.form-register').hide();
            $('.items-register-group').css('background-color','');
        }else{
            $('.form-nav-down').hide();
            $('.form-register').show();
            $('.items-login-group').css('background-color','');
            $('.items-register-group').css('background-color','rgba(16,35,33,0.9)')
        }
    })
    
    $('.login-button').click(function(){
        if($('.form-login').css('display') == 'block'){
            $('.form-login').hide();
            $('.items-login-group').css('background-color','');
        }            
        else{
            $('.form-nav-down').hide();
            $('.form-login').show();
            $('.items-register-group').css('background-color','');
            $('.items-login-group').css('background-color','rgba(16,35,33,0.9)')
        }
    })
    
    $('body').click(function(event) {
        if (!$(event.target).closest('.form-nav-down').length && !$(event.target).closest('.items-register-group').length && !$(event.target).closest('.items-login-group').length ) {
            $('.form-nav-down').hide();
            $('.items-register-group').css('background-color','');
            $('.items-login-group').css('background-color','');
        };
    });  
    
    $(".policy").click(function(){
        $("#dialog-wrapper-policy").show();
    })
    
    if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
        $(window).load(function () {
            $('input:-webkit-autofill').each(function () {
                var text = $(this).val();
                var id = $(this).attr('id');
                $(this).after(this.outerHTML).remove();
                $('input[id=' + id + ']').val(text);                    
            });
        });
    }
</script>