/********************************************** Javascript for dialog box ***********************************************/
var redirectUrl;
function dialog_close() {
    $('.diaglog-wraper').hide();
    if (redirectUrl)
        window.location.href = redirectUrl;
}
var $checked = 0;
function click_checkbox() {
    var $checkbox = $('.checkbox');
    $checkbox.attr('checked', !$checkbox.attr('checked'));
    if ($checked == 0) {
        $checked = 1;
        $('.checkbox-wrapper').css("background-position", "-550px -320px");
    }
    else {
        $checked = 0;
        $('.checkbox-wrapper').css("background-position", "-530px -320px");
    }
}
;
function createDiaglog(title, content, width, height, redirect) {
    //Add the div dialog-warpper
    $('body').append('<div class="diaglog-wraper"><div class="diaglog"></div></div>');
    // Add title
    $('.diaglog').append('<div class="title">' + title + '</div>');
    //Add cotent
    $('.diaglog').append('<div class="dialog-content">' + content + '</div>');
    //Add ok button
    $('.diaglog').append('<div class="ok-wrapper"><input type="button" class="ok" id="diaglog-ok" onclick="dialog_close();"/></div>');
    if (!height)
        var height = 150;
    if (!width)
        var width = 150;
    $('.diaglog').css('width', width + 'px');
    $('.diaglog').css('height', height + 'px');
    if (redirect)
        window.location = redirect;
    //Set focus to OK buton
    $('#diaglog-ok').focus();

}
//Close diaglog
$('#diaglog-ok').click(function() {
    dialog_close();
});
/********************************************* End javascript for dialog box ********************************************/
/******************************************** Javascript for download page **********************************************/

function register_submit_form() {
//alert('hehe');
//$('this').submit();
}
function isInteger(num) {
    return num == parseInt(num);
}
;
function setZeroStar(ui) {
    ui.each(function() {
        $(this).removeClass('final-star half-star');
    });
    ui.each(function() {
        $(this).addClass('zero-star');
    });
}
;
function setHalfStar(ui) {
    ui.each(function() {
        $(this).removeClass('final-star zero-star');
    });
    ui.each(function() {
        $(this).addClass('half-star');
    });
}
;
function setFinalStar(ui) {
    ui.each(function() {
        $(this).removeClass('half-star zero-star');
    });
    ui.each(function() {
        $(this).addClass('final-star');
    });
}
;
function setDefaultStar(point) {
    var isInte = isInteger(point);
    var count = parseInt(point);
    $('.star').each(function() {
        if (count > 0)
            setFinalStar($(this));
        if (count == 0)
            if (isInte == true)
                setZeroStar($(this));
            else
                setHalfStar($(this));
        if (count < 0)
            setZeroStar($(this));
        count--;
    })
}
;
/******************************************** End javascript for download page *********************************************/
/******************************************** Javascript for ajax login  ***************************************************/
function ajaxLogin(username, password) {    
    $.ajax({
        url: baseUrl + '/site/login',
        type: 'POST',
        cache: false,
        data: 'username=' + username + '&password=' + password + '&type=' + 'ajax',
        success: function(string) {
            var data = $.parseJSON(string);
            if (data.code == 0 || data.code == -1){
                $('.login-error').html(data.message);
            }else{
                window.location = data.redirect;
            }                                   
        }
    })    
}

function ajaxLoginPopup(username, password) {    
    $.ajax({
        url: baseUrl + '/site/login',
        type: 'POST',
        cache: false,
        data: 'username=' + username + '&password=' + password + '&type=' + 'ajax',
        success: function(string) {
            var data = $.parseJSON(string);
            if (data.code == 0 || data.code == -1){
                $('.login-error').html(data.message);
            }else{
                window.location = data.redirect;
            }                                   
        }
    })    
}

function ajaxLoginUnity(username, password) {    
    $.ajax({
        url: baseUrl + '/site/login',
        type: 'POST',
        cache: false,
        data: 'username=' + username + '&password=' + password + '&type=' + 'ajax',
        success: function(string) {
            var data = $.parseJSON(string);           
        }
    })    
}

function ajaxForgotPassword(email){
    $.ajax({
        url: baseUrl + '/site/forgotPassword',
        type: "POST",
        cache: false,
        data: 'email=' + email,
        success: function(string){
            var data = $.parseJSON(string);
            $('#loading').hide();
            $('#forgot_password').show();
            if(data.code == 0 || data.code == -1 || data.code == -2)
                $('.forgotpassword-error').html(data.message);
            else{
                alert('Gửi email thành công, vui lòng vào email của bạn để lấy lại mật khẩu');
                window.location = data.redirect;
            }
        }
    })
}
function checkEnter() {
//    alert('aaa');
    $('.login').keypress(function(e) {
        if (e.which == 13) {
            ajaxLogin($('#username').val(), $('#password').val());
        }
    });
}

/******************************************** End Javascript for ajax login  ***********************************************/
/********************************************* Javascript for slideshow ****************************************************/
function setSlideContent(slideNumber) {
    var currentContent = 0;
    $('#slide .slide-content').children().each(function() {
        currentContent++;
        if (currentContent == slideNumber) {
            $(this).fadeIn(1000);
        }
        else
            $(this).hide();
    })
    setNavigator(slideNumber);
}
function setSlideContext() {
    numberSlide = $('#slide .slide-content').children().length;
    $('#slide .slide-content').children().hide().css('position', 'absolute');
    for (var i = 1; i <= numberSlide; i++) {
        $('.navigator').append('<div class="navigator-button" contextmenu="' + i + '"> </div>');
    }
}
function setNavigator(navigatorNumber) {
    var currentNavigator = 0;
    $('.navigator').children().each(function() {
        currentNavigator++;
        if (currentNavigator == navigatorNumber) {
            $(this).addClass('navigator-button-active');
        }
        else
            $(this).removeClass('navigator-button-active');
    })
}


/*var numberSlide;
 /*setSlideContext();*/
//slideNow = 0;
//autoSlide();
var slideNow = 0;

function autoSlide() {
    if (slideNow >= 3)
        slideNow = 0;    
    setSlideContent(slideNow);
    setTimeout(autoSlide, 4000);
    slideNow++;
}

function setSlideContent(slideNumber) {
    if(slideNumber == 0){
        $('.slide-play-game').hide();
        $('.slide-download-game').hide();
    }else{
        $('.slide-play-game').show();
        $('.slide-download-game').show();
    }
    var currentContent = 0;
    $(".slide-left-content").children().each(function() {
        if (currentContent === slideNumber) {
            $(this).fadeIn(1000);
        }
        else {
            $(this).hide();
        }
        currentContent++;
    });
    setRadioChecked(slideNumber);
}
function setRadioChecked(radioNumber) {
    var currentRadio = 0;
    $('.radio-slide').children().each(function() {

        if (currentRadio === radioNumber) {
            $(this).addClass('radio-checked');
        }
        else {
            $(this).removeClass('radio-checked');
        }
        currentRadio++;
    });
}


function nextSlide() {
    if (slideNow >= 2)
        slideNow = 0;
    else
        slideNow++;
    setSlideContent(slideNow);
}

function prevSlide(){
    if (slideNow < 0)
        slideNow = 2;
    else
        slideNow--;
    setSlideContent(slideNow);
}

/********************************************* End javascript for slideshow ************************************************/
function setUserPanel() {
    $('.tag').each(function() {
        $(this).click(function() {
            var index = $(this).parents('.tag-group-profile').find('.tag').index(this);
            var panelIndex = 0;
            $('.tag').each(function() {
                if (panelIndex === index) {
                    $(this).parent('.tag-border').addClass('tag-border-current');
                    $(this).addClass('tag-current');
                }
                else {
                    $(this).parent('.tag-border').removeClass('tag-border-current');
                    $(this).removeClass('tag-current');
                }
                panelIndex++;
            });
            panelIndex = 0;
            $('.message-panel').each(function() {
                if (panelIndex === index) {
                    $(this).addClass('message-panel-active');
                } else {
                    $(this).removeClass('message-panel-active');
                }
                panelIndex++;
            });
        });
    });
}
;
function setTabHistory() {
    $('.property-game-tag').each(function() {
        $(this).click(function() {
            var index = $(this).parents('.property-game-tag-out').find('.property-game-tag').index(this);
            var panelIndex = 0;
            $('.property-game-tag').each(function() {
                if (panelIndex === index) {
                    $(this).addClass('property-game-tag-current');
                }
                else {
                    $(this).removeClass('property-game-tag-current');
                }
                panelIndex++;
            });
            panelIndex = 0;
            $('.message-panel-right-item').each(function() {
                if (panelIndex === index) {
                    $(this).addClass('message-panel-right-item-active');
                } else {
                    $(this).removeClass('message-panel-right-item-active');
                }
                panelIndex++;
            });
        });
    });
}

function setInfoDetailsTag() {
    $('.tag').each(function() {
        $(this).click(function() {
            var index = $(this).parents('.tag-group').find('.tag').index(this);
            var panelIndex = 0;
            $(this).parents('.tag-group').find('.tag').each(function() {
                if (panelIndex === index) {
                    $(this).addClass('tag-current');
                }
                else {
                    $(this).removeClass('tag-current');
                }
                panelIndex++;
            });
            panelIndex = 0;
            $('.info-details-bot').each(function() {
                if (panelIndex === index) {
                    $(this).addClass('info-details-bot-active');
                } else {
                    $(this).removeClass('info-details-bot-active');
                }
                panelIndex++;
            });
        });
    });
}

function downloadFile(downloadFile) {
    if (!downloadFile)
        createDiaglog('Download link wrong', 'Xin hãy nhập đầy đủ thông tin', 120);
    // Ajax query
    else {        
        document.location = baseUrl + '/download/downloadInServer?type=ajax&downloadFile='+downloadFile;
        /*$.ajax({
            url: baseUrl + '/download/downloadInServer',
            type: 'GET',
            cache: false,            
            data: 'downloadFile=' + downloadFile + '&type=' + 'ajax',
            success: function(string) {
                var data = $.parseJSON(string);
                console.log(data);
                if (data.code == 1){
                    window.location = data.redirect;
                    //createDiaglog('Download starting!', '', 150,data.redirect);
                }
                if (data.code == -1 || data.code == -2){  
                    alert('Tải file trên server không thành công');
                    //createDiaglog('Thông báo', "Tải file trên server không thành công", 150);
                }
            }
        })*/
    }
}