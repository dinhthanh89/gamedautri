<?php
//if(($_SERVER['HTTP_X_REAL_IP'] != '118.70.67.125')){    
//    $uri = 'http://';
//    $uri .= $_SERVER['HTTP_HOST'];    
//    header('Location:'.$uri.'/maintenance.php');
//}
    
//$this->redirect('maintenance.php');
date_default_timezone_set('Asia/Bangkok');
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET,HEAD,POST,OPTIONS,TRACE');
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Allow-Headers: origin, content-type, accept');
// change the following paths if necessary
//$yii=dirname(__FILE__).'/protected/yii-1.1.12/framework/yii.php';
$yii=dirname(__FILE__).'/protected/yii-1.1.12/framework/yiilite.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();

