<?php
/*
 *	$Id: wsdlclient7.php,v 1.2 2007/11/06 14:49:10 snichol Exp $
 *
 *	WSDL client sample.
 *
 *	Service: WSDL
 *	Payload: document/literal
 *	Transport: http
 *	Authentication: digest
 */
require_once('/lib/nusoap.php');

$client = new nusoap_client("http://localhost:8080/Call_VMG_SentMT_php/sendMT.xml", 'wsdl',						'', '', '', '');
$err = $client->getError();
if ($err) {
	echo '<h2>Test-Constructor error</h2><pre>' . $err . '</pre>';
}
$result = $client->call('sendMT', 
array(
'userID'=>'84982031201',
'message'=>'test',
'serviceID'=>'8079',
'commandCode'=>'tntest',
'messageType'=>'1',
'requestID'=>'111',
'totalMessage' => '1',
'messageIndex' => '1',
'isMore' => '0',
'contentType' => '0'
),'','','');

// Check for a fault
if ($client->fault) {
	echo '<h2>Fault</h2><pre>';
	print_r($result);
	echo '</pre>';
} else {
	// Check for errors
	$err = $client->getError();
	if ($err) {
		// Display the error
		echo '<h2>Error</h2><pre>' . $err . '</pre>';
	} else {
		// Display the result
		echo '<h2>Result</h2><pre>';
		print_r($result);
		echo '</pre>';
	}
}
echo '<h2>Request</h2><pre>' . htmlspecialchars($client->request, ENT_QUOTES) . '</pre>';
echo '<h2>Response</h2><pre>' . htmlspecialchars($client->response, ENT_QUOTES) . '</pre>';
echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->debug_str, ENT_QUOTES) . '</pre>';
?>
