<?php

error_reporting(0);
require_once("lib/nusoap.php");
$domain = $_SERVER['HTTP_HOST'];

$ns="http://".$domain."/smsVmg/service.php";

$server = new soap_server();
$server->configureWSDL('MOReceiver',$ns);
$server->wsdl->schemaTargetNamespace=$ns;
$server->soap_defencoding = 'UTF-8';
$server->register('messageReceiver',
array('User_ID' => 'xsd:string',
    		'Service_ID' => 'xsd:string',  
    		'Command_Code'=> 'xsd:string',  
    		'Message'=> 'xsd:string',  
    		'Request_ID'=> 'xsd:string'),        
array('return' => 'xsd:string'),
    'urn:MOReceiver',                      
    'urn:MOReceiver#messageReceiver',                 
    'rpc',                                 
    'encoded',                             
    'Get MO'           
);

function messageReceiver($userid,$serviceid,$commandcode,$message,$requestid){
    
    $file = 'log.txt';
    // Open the file to get existing content
    $current = @file_get_contents($file);
    // Append a new person to the file
    $current .= date('Y-m-d h:i:s')."-".$userid."-".$serviceid."-".$commandcode."-".$message."-".$requestid."\r\n";
    // Write the contents back to the file
    @file_put_contents($file, $current);
        
    return new soapval('return','xsd:string',getMO($userid,$serviceid,$commandcode,$message,$requestid));
}

if ( !isset( $HTTP_RAW_POST_DATA ) ) $HTTP_RAW_POST_DATA =file_get_contents( 'php://input' );
$server->service($HTTP_RAW_POST_DATA);

function getMO($userid,$serviceid,$commandcode,$message,$requestid) { 
    $domain = $_SERVER['HTTP_HOST'];
    $clientEsimo = new nusoap_client('http://'.$domain.'/api/rechargesmsvmg', array('wsdl'));    

    $paramEsimo=array(
                'userid'=>$userid,                        
                'serviceid'=>$serviceid,
                'commandcode'=>$commandcode,
                'message'=>$message,                        
                'requestid'=>$requestid);                        
    $message = $clientEsimo->call('getMO', $paramEsimo); 
    $message = json_decode($message);
  
    $wsdl = "http://".$domain."/smsVmg/sendMT.xml";//duong dan file xml save duoc ve sau khi chay http://123.29.69.168/api/services/sendMT?wsdl. Chu y nho sua cac link local trong file do thanh 123.29.69.168
    $client=new nusoap_client($wsdl, array('wsdl'));  
    $param=array(
                       'userID'=>$userid,
                       'message'=>$message->message,
                       'serviceID'=>$serviceid,
                       'commandCode'=>$commandcode,
                       'messageType'=>$message->messageType,
                       'requestID'=>$requestid,
                       'totalMessage' => '1',
                       'messageIndex' => '1',
                       'isMore' => '0',
                       'contentType' => '0');
   $result = $client->call('sendMT', $param);
    return "1"; 

}

?>
